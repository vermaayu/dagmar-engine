macro(dagmar_add_library library_name library_alias library_type)
    if(${library_type} STREQUAL "INTERFACE")
        add_library(${library_name} INTERFACE)
    else()
        add_library(${library_name} ${library_type}
            "${ARGN}")
    endif()
    
    file(GLOB_RECURSE public_headers RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} "${CMAKE_CURRENT_SOURCE_DIR}/include/*.h")
    set_target_properties(${library_name} PROPERTIES PUBLIC_HEADER "${public_headers}")

    message(STATUS "${public_headers}")

    install(TARGETS ${library_name}
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    )

    install(DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/include/"
        DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/"
        FILES_MATCHING
        PATTERN "*.h"
    )

    add_library(${library_alias} ALIAS ${library_name})

    if(NOT (${library_type} STREQUAL "INTERFACE"))
        file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "add_library(${library_name} ${library_type} IMPORTED)\n")
        file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "set_property(TARGET ${library_name} PROPERTY IMPORTED_LOCATION \${CMAKE_CURRENT_SOURCE_DIR}/../lib/${dagmar_library_prefix}${library_name}${dagmar_library_suffix})\n")
        file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "set_property(TARGET ${library_name} PROPERTY INCLUDE_DIRECTORIES \${CMAKE_CURRENT_SOURCE_DIR}/../include/)\n")
        file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "set_property(TARGET ${library_name} PROPERTY INTERFACE_INCLUDE_DIRECTORIES \${CMAKE_CURRENT_SOURCE_DIR}/../include/)\n")
        file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "set_property(TARGET ${library_name} PROPERTY IMPORTED_GLOBAL 1)\n")
        file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "target_link_libraries(${library_name} INTERFACE OpenGL::GL)\n")
        file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "target_link_libraries(${library_name} INTERFACE tinyobjloader::tinyobjloader)\n")
        file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "target_link_libraries(${library_name} INTERFACE glfw::glfw)\n")
        file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "target_link_libraries(${library_name} INTERFACE glew::glew)\n")
        file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "add_library(${library_alias} ALIAS ${library_name})\n")
        file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "\n")
        file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_lib.cmake "   ${library_alias}\n")
    endif()
endmacro()

macro(start_generate_library_list)
    if(EXISTS ${CMAKE_INSTALL_PREFIX}/build_site/engine_lib.cmake)
        file(REMOVE ${CMAKE_INSTALL_PREFIX}/build_site/engine_lib.cmake)
    endif()

    file(WRITE ${CMAKE_INSTALL_PREFIX}/build_site/engine_lib.cmake)

    file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_lib.cmake "add_library(engine_dummy INTERFACE)\n")
    file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_lib.cmake "target_link_libraries(engine_dummy INTERFACE\n")
endmacro()

macro(end_generate_library_list)
    file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_lib.cmake ")\n")
    file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_lib.cmake "add_library(Dagmar::Engine ALIAS engine_dummy)\n")

    file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "target_link_libraries(systems INTERFACE Dagmar::Renderer)\n")
    file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "target_link_libraries(systems INTERFACE Dagmar::Graphics)\n")
    file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "target_link_libraries(systems INTERFACE Dagmar::Application)\n")
    file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "target_link_libraries(application INTERFACE Dagmar::ModuleLifecycle)\n")
    file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "target_link_libraries(renderer_lib INTERFACE imgui::imgui)\n")
    file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "target_link_libraries(ui_lib INTERFACE imgui::imgui)\n")
    file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "target_link_libraries(scene INTERFACE imgui::imgui)\n")
    file(APPEND ${CMAKE_INSTALL_PREFIX}/build_site/engine_libs.cmake "target_link_libraries(module_lifecycle INTERFACE imgui::imgui)\n")
endmacro()