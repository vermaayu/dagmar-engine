#include <includes/DODBlocks.glsl>

#ifdef VERTEX_SHADER
/////////////////// VS INPUT ///////////////////
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inUV;
layout(location = 3) in vec4 inColor;
////////////////////////////////////////////////

///////////////// VS OUTPUTS /////////////////// 
layout(location = 0) out vec3 outFragPos;
layout(location = 1) out vec3 outFragNormal;
layout(location = 2) out vec2 outUV;
layout(location = 3) out vec4 outFragColor;
////////////////////////////////////////////////

void main()
{
    gl_Position = camera_data.projectionMatrices[scene_data.currentCamera] * camera_data.modelMatrices[scene_data.currentCamera] * entities_data.modelMatrices[draw_data.entityID] * vec4(inPosition, 1.0f);

    outFragPos = vec3(entities_data.modelMatrices[draw_data.entityID] * vec4(inPosition, 1.0f));
    outFragNormal = normalize((transpose(inverse(entities_data.modelMatrices[draw_data.entityID])) * vec4(inNormal, 0.0)).xyz);
    outFragColor = inColor;
    outUV = inUV;
}
#endif


#ifdef FRAGMENT_SHADER
///////////////// INPUTS //////////////////////
layout(location = 0) in vec3 inFragPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inUV;
layout(location = 3) in vec4 inFragColor;
///////////////////////////////////////////////

layout(location = 29, binding = 0) uniform sampler2D modelTexture;

////////////////////// OUTPUTS ////////////////
layout(location = 0) out vec4 outFBColor;
///////////////////////////////////////////////

vec4 coloring()
{
    vec4 color = material_data.materials[draw_data.materialID].ambient * inFragColor + material_data.materials[draw_data.materialID].emissive;
    vec3 fragPos = inFragPos;
    vec3 normal = inNormal;
    vec4 fragColor = inFragColor;
    vec4 diffuseTexColor = texture(modelTexture, inUV);
    vec4 diffuse = material_data.materials[draw_data.materialID].diffuse * diffuseTexColor;
    
    color += diffuse;
    color.a = 1.0;

    return color;
}

void main()
{
    outFBColor = coloring();
}
#endif