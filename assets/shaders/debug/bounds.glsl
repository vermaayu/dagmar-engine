#include <includes/DODBlocks.glsl>

#ifdef VERTEX_SHADER
layout(location = 0) in vec3 aPos;

void main()
{
    gl_Position = camera_data.projectionMatrices[scene_data.currentCamera] * camera_data.modelMatrices[scene_data.currentCamera] * entities_data.modelMatrices[draw_data.entityID] * vec4(aPos, 1.0);
}
#endif

#ifdef FRAGMENT_SHADER
layout (location = 0) out vec4 FragColor;

void main()
{
   FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);
}
#endif