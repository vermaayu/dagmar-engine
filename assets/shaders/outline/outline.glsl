#include <includes/DODBlocks.glsl>

#ifdef VERTEX_SHADER
// Input
layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;

void main()
{
    float outlineWidth = 0.05;
    // Make sure the models have smoothed normals

    vec4 outPos = camera_data.projectionMatrices[scene_data.currentCamera] * camera_data.modelMatrices[scene_data.currentCamera] * entities_data.modelMatrices[scene_data.selectedEntity] * vec4(inNormal * outlineWidth + inPos.xyz, 1.0);
    
    gl_Position = outPos;
}
#endif


#ifdef FRAGMENT_SHADER
layout (location = 0) out vec4 FragColor;
layout (location = 13) uniform vec4 outlineColor;

void main()
{
   FragColor = outlineColor;
}
#endif