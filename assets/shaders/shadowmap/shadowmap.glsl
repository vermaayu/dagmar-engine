#include <includes/DODBlocks.glsl>

#ifdef VERTEX_SHADER
layout(location = 0) in vec3 aPos;

void main()
{
    gl_Position = light_data.projectionMatrices[light_draw_data.lightEntityID] * light_data.modelMatrices[light_draw_data.lightEntityID] * entities_data.modelMatrices[draw_data.entityID] * vec4(aPos, 1.0);
}
#endif


#ifdef FRAGMENT_SHADER
layout (location = 0) out vec4 FragColor;

void main()
{
   FragColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
}
#endif