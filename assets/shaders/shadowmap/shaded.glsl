#include <includes/DODBlocks.glsl>

#ifdef VERTEX_SHADER
/////////////////// VS INPUT ///////////////////
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inUV;
layout(location = 3) in vec4 inColor;
////////////////////////////////////////////////

///////////////// VS OUTPUTS /////////////////// 
layout(location = 0) out vec3 outFragPos;
layout(location = 1) out vec3 outFragNormal;
layout(location = 2) out vec2 outUV;
layout(location = 3) out vec4 outFragColor;
////////////////////////////////////////////////

void main()
{
    gl_Position = camera_data.projectionMatrices[scene_data.currentCamera] * camera_data.modelMatrices[scene_data.currentCamera] * entities_data.modelMatrices[draw_data.entityID] * vec4(inPosition, 1.0f);

    outFragPos = vec3(entities_data.modelMatrices[draw_data.entityID] * vec4(inPosition, 1.0f));
    outFragNormal = normalize((transpose(inverse(entities_data.modelMatrices[draw_data.entityID])) * vec4(inNormal, 0.0)).xyz);
    outFragColor = inColor;
    outUV = inUV;
}
#endif


#ifdef FRAGMENT_SHADER
///////////////// INPUTS //////////////////////
layout(location = 0) in vec3 inFragPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inUV;
layout(location = 3) in vec4 inFragColor;
///////////////////////////////////////////////

#include <includes/ModelTextures.glsl>

////////////////////// OUTPUTS ////////////////
layout(location = 0) out vec4 outFBColor;
///////////////////////////////////////////////

#include <includes/UtilsLight.glsl>

vec4 coloring()
{
    vec3 fragPos = inFragPos;
    vec3 normal = inNormal;
    vec4 fragColor = inFragColor;

    vec4 ambientTextureColor = texture(modelAmbientTexture, inUV);
    vec4 diffuseTexColor = texture(modelTexture, inUV);
    vec4 specularTextureColor = texture(modelSpecularTexture, inUV);
    vec4 bumpTextureColor = texture(modelBumpTexture, inUV);
    float alphaTextureColor = texture(modelAlphaTexture, inUV).r;

    vec4 color = material_data.materials[draw_data.materialID].ambient * inFragColor * ambientTextureColor + material_data.materials[draw_data.materialID].emissive;

    vec4 diffuse = material_data.materials[draw_data.materialID].diffuse * diffuseTexColor;
    vec4 specular = vec4(material_data.materials[draw_data.materialID].specular.rgb, 1.0) * specularTextureColor;
    float specularExp = material_data.materials[draw_data.materialID].specularExp;

    vec3 fragToCam = normalize(camera_data.positions[scene_data.currentCamera] - fragPos);
    vec3 reflected = reflect(-fragToCam, normal);

    specular *= texture(sampler_cube_map, reflected);

    for(int i = 0; i < MAX_DIR_LIGHTS; i++)
    {
        if(i == scene_data.currentDirectionalLights)
        {
            break;
        }

        int lightID = DIR_LIGHT_SHADOW_MAPS * i;

        vec3 fragToLight = normalize(light_data.positions[lightID] - fragPos);
        float shadow = 0.0;
        
        for(int j = 0; j < DIR_LIGHT_SHADOW_MAPS; j++)
        {
            shadow += get_shadow(fragPos, normal, lightID + j, directional_shadow_maps, i * DIR_LIGHT_SHADOW_MAPS + j);
        }

        shadow = ceil(shadow / float(DIR_LIGHT_SHADOW_MAPS));
        color += shadow * blinn_phong(fragToCam, fragToLight, normal, diffuse, fragColor, specular, specularExp, light_data.colors[lightID]);
    }

    for(int i = 0; i < MAX_SPOT_LIGHTS; i++)
    {
        if(i == scene_data.currentSpotLights)
        {
            break;
        }

        int lightID = DIR_LIGHT_SHADOW_MAPS * scene_data.currentDirectionalLights + SPOT_LIGHT_SHADOW_MAPS * i;

        vec3 fragToLight = normalize(light_data.positions[lightID] - fragPos);

        float shadow = 0.0;

        for(int j = 0; j < SPOT_LIGHT_SHADOW_MAPS; j++)
        {
            shadow += get_shadow(fragPos, normal, lightID + j, spot_shadow_maps, i * SPOT_LIGHT_SHADOW_MAPS + j);
        }

        shadow = ceil(shadow / float(SPOT_LIGHT_SHADOW_MAPS));
        color += shadow * blinn_phong(fragToCam, fragToLight, normal, diffuse, fragColor, specular, specularExp, light_data.colors[lightID]);
    }

    for(int i = 0; i < MAX_POINT_LIGHTS; i++)
    {
        if(i == scene_data.currentPointLights)
        {
            break;
        }

        int lightID = DIR_LIGHT_SHADOW_MAPS * scene_data.currentDirectionalLights + SPOT_LIGHT_SHADOW_MAPS * scene_data.currentSpotLights + POINT_LIGHT_SHADOW_MAPS * i;
        
        vec3 fragToLight = normalize(light_data.positions[lightID] - fragPos);

        float shadow = 0.0;

        for(int j = 0; j < POINT_LIGHT_SHADOW_MAPS; j++)
        {
            shadow += get_shadow(fragPos, normal, lightID + j, point_shadow_maps, i * POINT_LIGHT_SHADOW_MAPS + j);
        }

        shadow = ceil(shadow / float(POINT_LIGHT_SHADOW_MAPS));
        // return vec4(shadow, 0.0, 0.0, 1.0);
        color += shadow * blinn_phong(fragToCam, fragToLight, normal, diffuse, fragColor, specular, specularExp, light_data.colors[lightID]);
    }

    color.a = fragColor.a * alphaTextureColor;

    return color;
}

void main()
{
    outFBColor = coloring();
}
#endif