#ifdef VERTEX_SHADER
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec2 aTexCoords;

layout(location = 0) out vec2 TexCoords;

void main()
{
    gl_Position = vec4(aPos.x, aPos.y, 0.0, 1.0); 
    TexCoords = aTexCoords;
}  
#endif


#ifdef FRAGMENT_SHADER
layout(location = 0) in vec2 TexCoords;

layout(binding = 0) uniform sampler2D screenTexture;

layout(location = 0) out vec4 FragColor;

void main()
{ 
    FragColor = texture(screenTexture, TexCoords);
}
#endif