#include <includes/DODBlocks.glsl>

#ifdef VERTEX_SHADER

layout(location = 0) in vec3 inVertexPos;
layout(location = 1) in vec2 inVertexTexCoord;

layout(location = 0) out vec2 outFragTexCoord;

void main()
{
    outFragTexCoord = inVertexTexCoord;
    gl_Position = vec4(inVertexPos, 1.0f);
}
#endif

#ifdef FRAGMENT_SHADER

layout (location = 0) in vec2 inTexCoord;

layout (binding = 0) uniform sampler2D gbufferPosition;
layout (binding = 1) uniform sampler2D gbufferNormal;
layout (binding = 2) uniform sampler2D gbufferUV;
layout (binding = 3) uniform sampler2D gbufferBaseColor;
layout (binding = 4) uniform sampler2D gbufferAmbient;
layout (binding = 5) uniform sampler2D gbufferDiffuse;
layout (binding = 6) uniform sampler2D gbufferSpecularSpecExponent;
layout (binding = 7) uniform sampler2D gbufferEmissive;

layout(location = 0) out vec4 FragColor;

#include <includes/UtilsLight.glsl>

vec4 coloring()
{
    vec4 fp = texture(gbufferPosition, inTexCoord);

    if(fp.a == 0)
    {
        // TODO Set it the default background color
        return vec4(0.0, 0.0, 0.0, 1.0);
    }

    // vec3 normal = ((texture(gbufferNormal, inTexCoord).rgb) - vec3(0.5)) * 2.0;
    vec3 normal = texture(gbufferNormal, inTexCoord).rgb;

    vec4 diffuse = texture(gbufferDiffuse, inTexCoord);

    vec4 baseColorWithAlpha = texture(gbufferBaseColor, inTexCoord);

    vec4 specularWithExp = texture(gbufferSpecularSpecExponent, inTexCoord);
    float specularExp = specularWithExp.a * MAX_SPECULAR_POWER;

    vec4 ambient = texture(gbufferAmbient, inTexCoord);
    vec4 emissive = texture(gbufferEmissive, inTexCoord);

    vec3 fragPos = fp.xyz;
    vec4 fragColor = baseColorWithAlpha;
    vec4 color = ambient * fragColor + emissive;
    vec4 diffuseTexColor = diffuse;

    vec3 fragToCam = normalize(camera_data.positions[scene_data.currentCamera] - fragPos);
    vec3 reflected = reflect(-fragToCam, normal);
    specularWithExp.rgb *= texture(sampler_cube_map, reflected).rgb;

    for(int i = 0; i < MAX_DIR_LIGHTS; i++)
    {
        if(i == scene_data.currentDirectionalLights)
        {
            break;
        }

        int lightID = DIR_LIGHT_SHADOW_MAPS * i;

        vec3 fragToLight = normalize(light_data.positions[lightID] - fragPos);
        float shadow = 0.0;
        
        for(int j = 0; j < DIR_LIGHT_SHADOW_MAPS; j++)
        {
            shadow += get_shadow(fragPos, normal, lightID + j, directional_shadow_maps, i * DIR_LIGHT_SHADOW_MAPS + j);
        }

        shadow = ceil(shadow / float(DIR_LIGHT_SHADOW_MAPS));
        color += shadow * blinn_phong(fragToCam, fragToLight, normal, diffuseTexColor, fragColor, vec4(specularWithExp.rgb, 1.0), specularExp, light_data.colors[lightID]);
    }

    for(int i = 0; i < MAX_SPOT_LIGHTS; i++)
    {
        if(i == scene_data.currentSpotLights)
        {
            break;
        }

        int lightID = DIR_LIGHT_SHADOW_MAPS * scene_data.currentDirectionalLights + SPOT_LIGHT_SHADOW_MAPS * i;

        vec3 fragToLight = normalize(light_data.positions[lightID] - fragPos);

        float shadow = 0.0;

        for(int j = 0; j < SPOT_LIGHT_SHADOW_MAPS; j++)
        {
            shadow += get_shadow(fragPos, normal, lightID + j, spot_shadow_maps, i * SPOT_LIGHT_SHADOW_MAPS + j);
        }

        shadow = ceil(shadow / float(SPOT_LIGHT_SHADOW_MAPS));
        color += shadow * blinn_phong(fragToCam, fragToLight, normal, diffuseTexColor, fragColor, vec4(specularWithExp.rgb, 1.0), specularExp, light_data.colors[lightID]);
    }

    for(int i = 0; i < MAX_POINT_LIGHTS; i++)
    {
        if(i == scene_data.currentPointLights)
        {
            break;
        }

        int lightID = DIR_LIGHT_SHADOW_MAPS * scene_data.currentDirectionalLights + SPOT_LIGHT_SHADOW_MAPS * scene_data.currentSpotLights + POINT_LIGHT_SHADOW_MAPS * i;
        
        vec3 fragToLight = normalize(light_data.positions[lightID] - fragPos);

        float shadow = 0.0;

        for(int j = 0; j < POINT_LIGHT_SHADOW_MAPS; j++)
        {
            shadow += get_shadow(fragPos, normal, lightID + j, point_shadow_maps, i * POINT_LIGHT_SHADOW_MAPS + j);
        }

        shadow = ceil(shadow / float(POINT_LIGHT_SHADOW_MAPS));
        // return vec4(shadow, 0.0, 0.0, 1.0);
        color += shadow * blinn_phong(fragToCam, fragToLight, normal, diffuseTexColor, fragColor, vec4(specularWithExp.rgb, 1.0), specularExp, light_data.colors[lightID]);
    }

    color.a = fragColor.a;

    return color;
}


void main()
{
    FragColor = coloring();
}
#endif