#include <includes/DODBlocks.glsl>

#ifdef VERTEX_SHADER
// Input
layout(location = 0) in vec3 inVertexPos;
layout(location = 1) in vec3 inVertexNormal;
layout(location = 2) in vec2 inVertexTexCoord;
layout(location = 3) in vec4 inVertexColor;

// Output
layout(location = 0) out vec3 outFragPos;
layout(location = 1) out vec3 outFragNormal;
layout(location = 2) out vec2 outTexCoords;
layout(location = 3) out vec4 outFragColor;

void main()
{
    gl_Position = camera_data.projectionMatrices[scene_data.currentCamera] * camera_data.modelMatrices[scene_data.currentCamera] * entities_data.modelMatrices[draw_data.entityID] * vec4(inVertexPos, 1.0f);

    outFragPos = vec3(entities_data.modelMatrices[draw_data.entityID] * vec4(inVertexPos, 1.0f));
    outFragNormal = normalize((transpose(inverse(entities_data.modelMatrices[draw_data.entityID])) * vec4(inVertexNormal, 0.0)).xyz);
    outFragColor = inVertexColor;
    outTexCoords = inVertexTexCoord;
}
#endif



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



#ifdef FRAGMENT_SHADER
layout(location = 0) in vec3 inFragPos;
layout(location = 1) in vec3 inFragNormal;
layout(location = 2) in vec2 inTexCoord;
layout(location = 3) in vec4 inFragColor;

// Done in model space
layout(location = 0) out vec4 gbufferPosition;
layout(location = 1) out vec4 gbufferNormal;
layout(location = 2) out vec4 gbufferUV;
layout(location = 3) out vec4 outBaseColor;
layout(location = 4) out vec4 outAmbient;
layout(location = 5) out vec4 outDiffuse;
layout(location = 6) out vec4 outSpecularSpecExponent;
layout(location = 7) out vec4 outEmissive;

#include <includes/ModelTextures.glsl>

void main()
{
    float alpha = texture(modelAlphaTexture, inTexCoord).r;
    
    if(alpha <= 0.5)
    {
        discard;
    }
    
    gbufferPosition = vec4(inFragPos, 1.0);
    gbufferNormal = vec4(inFragNormal, 0.0);
    // gbufferNormal = vec4(inFragNormal * 0.5 + vec3(0.5), 0.0);
    gbufferUV = vec4(inTexCoord, 0.0, 1.0);

    // outBaseColor = modelColor;
    outBaseColor = inFragColor;
    outBaseColor.a *= alpha;
    outAmbient = material_data.materials[draw_data.materialID].ambient * texture(modelAmbientTexture, inTexCoord);
    outDiffuse = material_data.materials[draw_data.materialID].diffuse * texture(modelTexture, inTexCoord);
    outSpecularSpecExponent = vec4(material_data.materials[draw_data.materialID].specular.rgb * texture(modelSpecularTexture, inTexCoord).rgb, material_data.materials[draw_data.materialID].specularExp / MAX_SPECULAR_POWER);
    outEmissive = material_data.materials[draw_data.materialID].emissive;
}
#endif