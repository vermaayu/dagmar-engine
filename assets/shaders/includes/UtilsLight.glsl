/**
 * @brief Calculates the shadow at a pixel - fragPos should be in model space, not camera space
 * @return 0 if it's in shadow, 1 if it's not in shadow
 */
float get_shadow(vec3 fragPos, vec3 normal, int lightID, sampler2DArray texArray, int layer)
{
    vec4 fragFromLightPerspective = light_data.projectionMatrices[lightID] * light_data.modelMatrices[lightID] * vec4(fragPos, 1.0);
    vec3 projCoords = (fragFromLightPerspective.xyz / fragFromLightPerspective.w) * 0.5 + 0.5;

    if(projCoords.x < 0 || projCoords.x > 1 || projCoords.y < 0 || projCoords.y > 1)
    {
        return 0;
    }

    float closestDepth = texture(texArray, vec3(projCoords.xy, layer)).r;
    float currentDepth = projCoords.z;

    float bias = max(0.0005, 0.0005 * (1.0 - dot(normalize(normal), light_data.positions[lightID])));
    return (currentDepth - bias <= closestDepth) ? 1.0 : 0.0; 
}

vec4 blinn_phong(vec3 _fragToCam, vec3 _fragToLight, vec3 _normal, vec4 _diffuse, vec4 _fragColor, vec4 _specular, float _specExponent, vec4 _lightColor)
{
    vec3 halfVec = normalize(_fragToCam + _fragToLight);
    float kd = max(0.0, dot(_fragToLight, _normal));
    float ks = pow(max(0.0, dot(_normal, halfVec)), _specExponent);
    vec4 diffuse = kd * _diffuse * _fragColor * _lightColor;
    vec4 specular = ks * _specular;

    return vec4(diffuse + specular);
}