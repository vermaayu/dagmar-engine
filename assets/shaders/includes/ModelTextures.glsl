#define MODEL_AMBIENT_TEXTURE_BINDING 14
#define MODEL_DIFFUSE_TEXTURE_BINDING 15
#define MODEL_SPECULAR_TEXTURE_BINDING 16
#define MODEL_BUMP_TEXTURE_BINDING 17
#define MODEL_ALPHA_TEXTURE_BINDING 18

layout(binding = MODEL_AMBIENT_TEXTURE_BINDING) uniform sampler2D modelAmbientTexture;
// TODO MODIFY NAME TO MODELDIFFUSETEXTURE
layout(binding = MODEL_DIFFUSE_TEXTURE_BINDING) uniform sampler2D modelTexture;
layout(binding = MODEL_SPECULAR_TEXTURE_BINDING) uniform sampler2D modelSpecularTexture;
layout(binding = MODEL_BUMP_TEXTURE_BINDING) uniform sampler2D modelBumpTexture;
layout(binding = MODEL_ALPHA_TEXTURE_BINDING) uniform sampler2D modelAlphaTexture;