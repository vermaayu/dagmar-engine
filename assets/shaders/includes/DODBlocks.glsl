#define MAX_CAMERAS 8
#define MAX_LIGHTS 64
#define MAX_DIR_LIGHTS 3
#define MAX_POINT_LIGHTS 8
#define MAX_SPOT_LIGHTS 32
#define MAX_RENDERABLE_ENTITIES 512
#define MAX_ENTITIES 512

// SOON 4 for cascaded
#define DIR_LIGHT_SHADOW_MAPS 1
#define SPOT_LIGHT_SHADOW_MAPS 1
#define POINT_LIGHT_SHADOW_MAPS 6
#define MAX_LIGHTS_WITH_ADDITIONAL_FACES ((MAX_DIR_LIGHTS * DIR_LIGHT_SHADOW_MAPS) + (MAX_SPOT_LIGHTS * SPOT_LIGHT_SHADOW_MAPS) +  (MAX_POINT_LIGHTS * POINT_LIGHT_SHADOW_MAPS))

#define MAX_SPECULAR_POWER 64

#define directionalLightShadowMapsIndex 20
#define spotLightShadowMapsIndex 21
#define pointLightShadowMapsIndex 22
#define cubeMapIndex 23

struct Material
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 emissive;
    float specularExp;
};

layout(binding = 0, std140) uniform SCENE_DATA
{
    int selectedEntity;
    int currentLightCount;
    int selectedLight;
    int currentCamera;
    int selectedCamera;

    float zNear;
    float zFar;

    int currentDirectionalLights;
    int currentSpotLights;
    int currentPointLights;

} scene_data;

layout(binding = 1, std140) uniform CAMERA_DATA
{
    // TODO ADD NEAR & FAR PLANES
    vec3 positions[MAX_CAMERAS];
    mat4 projectionMatrices[MAX_CAMERAS];
    mat4 modelMatrices[MAX_CAMERAS];
    mat4 inverseViewProj[MAX_CAMERAS];
} camera_data;

layout(binding = 2, std140) uniform LIGHT_DATA
{
    vec3 positions[MAX_LIGHTS_WITH_ADDITIONAL_FACES];
    mat4 projectionMatrices[MAX_LIGHTS_WITH_ADDITIONAL_FACES];
    // View matrices (so it's actually the inverse of the model)
    mat4 modelMatrices[MAX_LIGHTS_WITH_ADDITIONAL_FACES];
    mat4 inverseViewProj[MAX_LIGHTS_WITH_ADDITIONAL_FACES];
    vec4 colors[MAX_LIGHTS_WITH_ADDITIONAL_FACES];
} light_data;

layout(binding = 3, std140) uniform MATERIAL_DATA
{
    Material materials[MAX_RENDERABLE_ENTITIES];
} material_data;

layout(binding = 4, std140) uniform ENTITIES_DATA
{
    mat4 modelMatrices[MAX_ENTITIES];
} entities_data;

layout(binding = 5, std140) uniform DRAW_DATA
{
    int entityID;
    int materialID;
    vec4 encodedID;
} draw_data;

layout(binding = 6, std140) uniform LIGHT_DRAW_DATA
{
    int lightEntityID;
} light_draw_data;

/*
layout(binding = 8, std140) uniform PHYSICS_COLLIDER2D_DATA
{
    vec4 scales[MAX_ENTITIES];
    vec4 centers[MAX_ENTITIES];
} physics_collider2d_data;

layout(binding = 9, std140) uniform PHYSICS_COLLIDERSPHERE_DATA
{
    vec4 scales[MAX_ENTITIES];
    vec4 centers[MAX_ENTITIES];
} physics_collider_sphere_data;
*/

layout(binding = directionalLightShadowMapsIndex) uniform sampler2DArray directional_shadow_maps;
layout(binding = spotLightShadowMapsIndex) uniform sampler2DArray spot_shadow_maps;
layout(binding = pointLightShadowMapsIndex) uniform sampler2DArray point_shadow_maps;
layout(binding = cubeMapIndex) uniform samplerCube sampler_cube_map;