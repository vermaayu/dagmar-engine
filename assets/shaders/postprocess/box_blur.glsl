#ifdef VERTEX_SHADER
layout(location = 0) in vec3 inVertexPos;
layout(location = 1) in vec2 inVertexTexCoord;
layout(location = 0) out vec2 outFragTexCoord;

void main()
{
    outFragTexCoord = inVertexTexCoord;
    gl_Position = vec4(inVertexPos, 1.0f);
}
#endif



#ifdef FRAGMENT_SHADER
layout(location = 0) in vec2 inTexCoord;
layout(binding = 0) uniform sampler2D screen;
layout(binding = 1) uniform sampler2D screenDepth;

layout(location = 0) out vec4 FragColor;

#include <includes/UtilsPostProcess.glsl>

layout(binding = postProcessIndex, std140) uniform POST_PROCESS_DATA
{
    int size;
} post_process_data;

void main()
{
    vec4 color = texture(screen, inTexCoord);
    ivec2 textureSize = textureSize(screen, 0);

    float tsizeX = 1.0 / float(textureSize.x);
    float tsizeY = 1.0 / float(textureSize.y);

    int size = post_process_data.size;

    for (int i = -size; i <= size; i++)
    {
        for (int j = -size; j <= size; j++)
        {
            vec2 offset = vec2(tsizeX * i, tsizeY * j);
            color += texture(screen, inTexCoord + offset);
        }
    }

    FragColor = color / pow(size * 2 + 1, 2);
}
#endif