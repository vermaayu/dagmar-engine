#ifdef VERTEX_SHADER
// Input
layout(location = 0) in vec3 inVertexPos;
layout(location = 1) in vec2 inVertexTexCoord;


// Output
layout(location = 0) out vec2 outFragTexCoord;

void main()
{
    outFragTexCoord = inVertexTexCoord;
    gl_Position = vec4(inVertexPos, 1.0f);
}
#endif


#ifdef FRAGMENT_SHADER
layout(location = 0) in vec2 inTexCoord;

layout(binding = 0) uniform sampler2D screen;
layout(binding = 1) uniform sampler2D screenDepth;

layout(location = 0) out vec4 FragColor;

const float invGamma = 1.0 / 2.2;

void main()
{
    vec3 color = texture(screen, inTexCoord).rgb;

    // vec3 mapped = color / (color + vec3(1.0));
    
    vec3 mapped = pow(color, vec3(invGamma));
    FragColor = vec4(mapped, 1.0);
}
#endif
