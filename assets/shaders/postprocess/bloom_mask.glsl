#ifdef VERTEX_SHADER
// Input
layout(location = 0) in vec3 inVertexPos;
layout(location = 1) in vec2 inVertexTexCoord;


// Output
layout(location = 0) out vec2 outFragTexCoord;

void main()
{
    outFragTexCoord = inVertexTexCoord;
    gl_Position = vec4(inVertexPos, 1.0f);
}
#endif

#ifdef FRAGMENT_SHADER
layout(location = 0) in vec2 inTexCoord;

layout(binding = 0) uniform sampler2D screen;
layout(binding = 1) uniform sampler2D screenDepth;
layout(location = 0) out vec4 FragColor;

#include <includes/UtilsPostProcess.glsl>

layout(binding = postProcessIndex, std140) uniform POST_PROCESS_DATA
{
    float threshold;
    float offset;
} post_process_data;

void main()
{
    float threshold = post_process_data.threshold;

    vec3 color = texture(screen, inTexCoord).rgb;
    float grayscale = dot(color, vec3(0.21, 0.72, 0.07));

    if(grayscale > threshold)
    {
        FragColor = vec4(color, 1.0);
    }
    else
    {
        FragColor = vec4(0.0, 0.0, 0.0, 1.0);
    }
}
#endif