#ifdef VERTEX_SHADER
// Input
layout(location = 0) in vec3 inVertexPos;
layout(location = 1) in vec2 inVertexTexCoord;

// Output
layout(location = 0) out vec2 outFragTexCoord;

void main()
{
    outFragTexCoord = inVertexTexCoord;
    gl_Position = vec4(inVertexPos, 1.0f);
}
#endif



#ifdef FRAGMENT_SHADER
layout(location = 0) in vec2 inTexCoord;

layout(binding = 0) uniform sampler2D screen;
layout(binding = 1) uniform sampler2D screenDepth;

layout(location = 0) out vec4 FragColor;

#include <includes/UtilsPostProcess.glsl>

layout(binding = postProcessIndex, std140) uniform POST_PROCESS_DATA
{
    float amount;
} post_process_data;


const float amount = 0.4;

void main()
{
    float amount = post_process_data.amount;

    vec2 texSize = textureSize(screen, 0).xy;

    float neighbor = -amount;
    float center = 4.0 * amount + 1.0;

    vec4 fragColor = texture(screen, (inTexCoord + vec2(0, 0) / texSize)) * center +
    texture(screen, (inTexCoord + vec2(-1, 0) / texSize)) * neighbor +
    texture(screen, (inTexCoord + vec2(1, 0) / texSize)) * neighbor +
    texture(screen, (inTexCoord + vec2(0, -1) / texSize)) * neighbor +
    texture(screen, (inTexCoord + vec2(0, 1) / texSize)) * neighbor;

    FragColor = vec4(fragColor.xyz, 1.0);
}
#endif