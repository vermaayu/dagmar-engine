#ifdef VERTEX_SHADER

layout(location = 0) in vec3 inVertexPos;
layout(location = 1) in vec2 inVertexTexCoord;

layout(location = 0) out vec2 outFragTexCoord;

void main()
{
    outFragTexCoord = inVertexTexCoord;
    gl_Position = vec4(inVertexPos, 1.0f);
}
#endif

#ifdef FRAGMENT_SHADER

layout(location = 0) in vec2 inTexCoord;

layout(binding = 0) uniform sampler2D screen;
layout(binding = 1) uniform sampler2D screenDepth;
layout(binding = 2) uniform sampler2D mask;

layout(location = 0) out vec4 FragColor;

void main()
{
    vec3 color = texture(screen, inTexCoord).rgb;
    vec3 colorMask = texture(mask, inTexCoord).rgb;

    vec3 mapped = color + colorMask;
    FragColor = vec4(mapped, 1.0);
}
#endif