#ifdef VERTEX_SHADER
// Input
layout(location = 0) in vec3 inVertexPos;
layout(location = 1) in vec2 inVertexTexCoord;


// Output
layout(location = 0) out vec2 outFragTexCoord;

void main()
{
    outFragTexCoord = inVertexTexCoord;
    gl_Position = vec4(inVertexPos, 1.0f);
}
#endif

#ifdef FRAGMENT_SHADER
layout(location = 0) in vec2 inTexCoord;

layout(binding = 0) uniform sampler2D screen;
layout(binding = 1) uniform sampler2D screenDepth;

layout(location = 0) out vec4 FragColor;

#include <includes/UtilsPostProcess.glsl>

layout(binding = postProcessIndex, std140) uniform POST_PROCESS_DATA
{
    float rOffset;
    float gOffset;
    float bOffset;
} post_process_data;


// float rOffset =  0.009;
// float gOffset =  0.006;
// float bOffset = -0.006;

void main()
{
    vec3 fragColor;
    fragColor.r = texture(screen, inTexCoord + vec2(post_process_data.rOffset)).r;
    fragColor.g = texture(screen, inTexCoord + vec2(post_process_data.gOffset)).g;
    fragColor.b = texture(screen, inTexCoord + vec2(post_process_data.bOffset)).b;
    FragColor = vec4(fragColor, 1.0);
}
#endif