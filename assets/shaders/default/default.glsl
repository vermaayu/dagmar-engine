#ifdef VERTEX_SHADER
    layout(location = 0) in vec3 inVertexPos;
    layout(location = 1) in vec2 inVertexTexCoord;

    layout(location = 0) out vec2 outFragTexCoord;

    void main()
    {
        outFragTexCoord = inVertexTexCoord;
        gl_Position = vec4(inVertexPos, 1.0f);
    }
#endif


#ifdef FRAGMENT_SHADER
    layout(location = 0) in vec2 inTexCoord;

    layout(location = 0) out vec4 FragColor;

    layout(binding = 0) uniform sampler2D screen;

    void main()
    {
        vec4 fragColor = texture(screen, inTexCoord);
        FragColor = fragColor;
    }
#endif