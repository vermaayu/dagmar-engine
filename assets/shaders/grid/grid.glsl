#include <includes/DODBlocks.glsl>

#ifdef VERTEX_SHADER

layout (location = 0) in vec2 aPos;
layout (location = 1) in vec2 aTexCoords;

///////////////// VS OUTPUTS /////////////////// 
layout(location = 0) out vec3 outNear;
layout(location = 1) out vec3 outFar;
layout(location = 2) out mat4 outProjection;
layout(location = 6) out mat4 outView;
////////////////////////////////////////////////

vec3 unproject(vec3 point)
{
    vec4 unprojectedPoint = camera_data.inverseViewProj[scene_data.currentCamera] * vec4(point, 1.0);
    return unprojectedPoint.xyz / unprojectedPoint.w;
}

void main()
{
    outNear = unproject(vec3(aPos.xy, 0.0));
    outFar = unproject(vec3(aPos.xy, 1.0));

    outProjection = camera_data.projectionMatrices[scene_data.currentCamera];
    outView = camera_data.modelMatrices[scene_data.currentCamera];

    gl_Position = vec4(aPos, 0.0, 1.0);
}
#endif


////////////////////////////////////////////////////////////////////////

#ifdef FRAGMENT_SHADER

layout(location = 0) in vec3 inNear;
layout(location = 1) in vec3 inFar;
layout(location = 2) in mat4 inProjection;
layout(location = 6) in mat4 inView;

layout(location = 0) out vec4 FragColor;

// Scale = distance between lines
vec4 grid(vec3 fragPos, float scale)
{
    vec2 coord = fragPos.xz * scale;

    vec2 derivative = fwidth(coord);
    vec2 grid = abs(fract(coord - 0.5) - 0.5) / derivative;

    float lwidth = 0.7;
    float line = clamp(min(grid.x, grid.y) - lwidth, 0.0, 1.0);

    vec4 color = vec4(vec3(1.0 - min(line, 1.0)), 1.0 - min(line, 1.0));

    float scalar = 1.0f;
    float minx = min(derivative.x, 1.0);
    float minz = min(derivative.y, 1.0);

    if ((fragPos.x > -minx * scalar) && (fragPos.x < minx * scalar))
    {
        color.r = 0.0;
        color.g = 0.0;
        color.b = 1.0;
    }

    if ((fragPos.z > -minz * scalar) && (fragPos.z < minz * scalar))
    {
        color.r = 1.0;
        color.g = 0.0;
        color.b = 0.0;
    }

    return color;
}

float computeDepth(vec3 pos)
{
    vec4 clipPos = inProjection * inView * vec4(pos.xyz, 1.0);
    float ndcDepth = clipPos.z / clipPos.w;
    return ((gl_DepthRange.diff * ndcDepth) + gl_DepthRange.near + gl_DepthRange.far) / 2.0;
}

void main()
{
    float t = -inNear.y / (inFar.y - inNear.y);
    vec3 fragPos = inNear + t * (inFar - inNear);

    float linearDepth = computeDepth(fragPos);
    gl_FragDepth = linearDepth;

    float falloffDistance = 0.015;

    float dst = length(inView * vec4(fragPos, 1.0));

    float spotlight = min(0.4, clamp((1.5 - falloffDistance * length(inView * vec4(fragPos.x, 0.0, fragPos.z, 1.0f))), 0.01, 1.0));
    
    vec4 tenth = grid(fragPos, 1.0);

    FragColor = (tenth) * float(t > 0);
    FragColor.a *= spotlight;
}

#endif