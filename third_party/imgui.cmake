add_library(imgui STATIC
    imgui/imgui.cpp
    imgui/imgui_demo.cpp
    imgui/imgui_draw.cpp
    imgui/imgui_tables.cpp
    imgui/imgui_widgets.cpp
    imgui/backends/imgui_impl_glfw.cpp
    imgui/backends/imgui_impl_opengl3.cpp
)

target_include_directories(imgui PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/imgui/
    ${CMAKE_CURRENT_SOURCE_DIR}/imgui/backends
)

target_link_libraries(imgui
    glfw::glfw
    glew::glew
)

file(GLOB_RECURSE public_headers
    "${CMAKE_CURRENT_SOURCE_DIR}/imgui/*.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/imgui/*.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/imgui/*.inl"
)

install( FILES ${public_headers} DESTINATION ${CMAKE_INSTALL_PREFIX}/include/${dir} )

install(TARGETS imgui)

add_library(imgui::imgui ALIAS imgui)