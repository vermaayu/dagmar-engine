add_library(imguizmo STATIC
    ImGuizmo/ImGuizmo.cpp
    ImGuizmo/ImCurveEdit.cpp
    ImGuizmo/ImGradient.cpp
    ImGuizmo/ImSequencer.cpp
)

target_include_directories(imguizmo PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/ImGuizmo/
)

target_link_libraries(imguizmo PUBLIC
    glfw::glfw
    glew::glew
    imgui::imgui
)

install(TARGETS imguizmo)

add_library(imguizmo::imguizmo ALIAS imguizmo)