add_library(glm::glm_static ALIAS glm_static)

file(GLOB_RECURSE public_headers RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/glm/
    "${CMAKE_CURRENT_SOURCE_DIR}/glm/*.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/glm/*.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/glm/*.inl"
)

install(TARGETS glm_static
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

foreach ( file ${public_headers} )
    get_filename_component( dir ${file} DIRECTORY )
    install( FILES glm/${file} DESTINATION include/${dir} )
endforeach()