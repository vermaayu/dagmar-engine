add_library(stb INTERFACE)

target_include_directories(stb INTERFACE
    ${CMAKE_CURRENT_SOURCE_DIR}/stb
)

add_library(stb::stb ALIAS stb)

file(GLOB_RECURSE public_headers RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/stb/
    "${CMAKE_CURRENT_SOURCE_DIR}/stb/*.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/stb/*.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/stb/*.inl"
)

foreach ( file ${public_headers} )
    get_filename_component( dir ${file} DIRECTORY )
    install( FILES stb/${file} DESTINATION include/${dir} )
endforeach()