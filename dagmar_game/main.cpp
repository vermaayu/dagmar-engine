// Include standard headers
#include <cstdio>

// Include GLM
#include <glm/gtc/matrix_transform.hpp>

#define STB_IMAGE_IMPLEMENTATION

#include "dagmar/DagmarGame.h"
#include "dagmar/ResourceLoader.h"

using namespace glm;

#define TINYOBJLOADER_IMPLEMENTATION


int main()
{
    auto application = std::make_shared<dag::DagmarGame>();

    application->run();

    return 0;
}
