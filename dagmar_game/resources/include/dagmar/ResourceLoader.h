#pragma once
#include <memory>

namespace dag
{
    class ResourceLoader
    {
      public:
        ResourceLoader() = default;
        ~ResourceLoader() = default;

    	// Loads the internal meshes, textures, etc
        void loadScreenQuad();
    	void loadInternalAssets();
    };

	inline std::shared_ptr<ResourceLoader> resourceLoader(new ResourceLoader());
} // namespace dag