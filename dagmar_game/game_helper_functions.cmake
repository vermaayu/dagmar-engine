macro(dagmar_add_game_library library_name library_type)
    if(${library_type} STREQUAL "INTERFACE")
        add_library(${library_name} INTERFACE)
    else()
        add_library(${library_name} ${library_type}
            "${ARGN}")
    endif()
    
    target_compile_definitions(${library_name} PUBLIC
	    $<$<BOOL:${ENGINE_GAME_BUILD}>:ENGINE_GAME_BUILD>
    )
endmacro()