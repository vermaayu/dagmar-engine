#include "dagmar/DagmarGame.h"

#include "dagmar/EventDispatcher.h"
#include "dagmar/PhysicsSystem.h"
#include "dagmar/ResourceLoader.h"
#include "dagmar/Scene.h"
#include "dagmar/RendererLayer.h"
#include "dagmar/GameLayer.h"
#include "dagmar/FileSystem.h"
#include "dagmar/Component.h"

//#ifdef ENGINE_GAME_BUILD
//#include "output_scene_data.h"
//#endif // ENGINE_GAME_BUILD

dag::DagmarGame::DagmarGame() :
    Application()
{
    // system("pause");
    // Load the other VAOs
    resourceLoader->loadScreenQuad();
    resourceLoader->loadInternalAssets();
    
#ifdef ENGINE_GAME_BUILD
    Log::debug("deserializing scene");
    dag::scene->deserialize(dag::filesystem::getAssetsPath() / "output_scene_data.json");

#else
    scene->initialize();
	
    dag::scene->createEntity("First entity");
    dag::scene->createEntity("Second entity");
    dag::scene->createEntity("Third entity");

    int i = 0;
    for (auto& entity : dag::scene->entities)
    {
        if (i == 0)
        {
            coordinator->addComponent(entity, dag::RigidBody{ .impulse = glm::vec3(20.0f, 0.0f, 0.0f), .mass = 0.05f });
            coordinator->addComponent(entity, dag::CollisionSphere{ .radius = 0.1f, .center = glm::vec3(0.0f) });
        }
        else
        {
            coordinator->addComponent(entity, dag::RigidBody{ .impulse = glm::vec3(-20.0f, 0.0f, 0.0f), .mass = 0.049f });
            coordinator->addComponent(entity, dag::CollisionSphere{ .radius = 0.1f, .center = glm::vec3(0.0f) });
        }
        ++i;
    }

    auto const& cameraSystem = coordinator->getSystem<CameraSystem>();
    auto const& lightSystem = coordinator->getSystem<LightSystem>();
    cameraSystem->setEditorCamera(coordinator->createEntity());
    coordinator->addComponent(cameraSystem->activeCamera, dag::Transform{});


    coordinator->getComponent<dag::Transform>(cameraSystem->activeCamera).updateModelMatrix();

    auto& transform = coordinator->getComponent<Transform>(cameraSystem->activeCamera);

    scene->createEntity("Default light");
    auto& lightTransform = coordinator->getComponent<Transform>(scene->entities.back());
    lightTransform.setPosition(glm::vec3(0, 0, 100));

    //coordinator->addComponent(cameraSystem->editorCamera, dag::Transform{});
    coordinator->addComponent(cameraSystem->editorCamera, dag::CameraComponent{});

    auto& cameraComponent = coordinator->getComponent<CameraComponent>(cameraSystem->editorCamera);
    cameraComponent.isPossessed = true;
    cameraComponent.isScreenDependent = true;

    transform.setPosition(glm::vec3(0.0f, 30.0f, 30.0f));
    transform.setRotation(glm::vec3(45.0f, transform.rotation.y, transform.rotation.z));
    cameraComponent.zNear = 1.0f;
    cameraComponent.zFar = 10000.0f;

    cameraSystem->descriptors.emplace_back(Descriptor(RenderingSystem::UniformFuncs::UniformMatrix4fv, "view", cameraComponent.getView(transform.position, transform.rotation)));
    cameraSystem->descriptors.emplace_back(Descriptor(RenderingSystem::UniformFuncs::UniformMatrix4fv, "projection", cameraComponent.getProjection()));
    cameraSystem->descriptors.emplace_back(Descriptor(RenderingSystem::UniformFuncs::Uniform3fv, "cameraPosition", transform.position));

    lightSystem->descriptors.emplace_back(Descriptor(RenderingSystem::UniformFuncs::Uniform3fv, "lightPosition", lightTransform.position));
#endif // ENGINE_GAME_BUILD

	windowManager->swapContext();
	
    gameLayer = std::make_shared<GameLayer>();
    layerManager.addLayer(gameLayer);
    rendererLayer = std::make_shared<RendererLayer>();
    layerManager.addLayer(rendererLayer);

    coordinator->getSystem<PhysicsSystem>()->gameState = dag::GameState::Play;
    gameLayer->gameState = dag::GameState::Play;

    windowManager->window.maximize();
    windowManager->disableCursor();
}

void dag::DagmarGame::run()
{
    while (running)
    {
        timeManager->updateFrameStart();    	
        layerManager.updateLayers();
    	windowManager->window.onUpdate();
    }
}

void dag::DagmarGame::onEvent(dag::Event& e)
{
    Application::onEvent(e);
}

bool dag::DagmarGame::onWindowClose(dag::WindowCloseEvent& e)
{
    return Application::onWindowClose(e);
}