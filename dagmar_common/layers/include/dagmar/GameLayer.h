#pragma once
#include "dagmar/KeyPressedEvent.h"
#include "dagmar/KeyReleasedEvent.h"
#include "dagmar/KeyTypedEvent.h"
#include "dagmar/Layer.h"
#include "dagmar/MouseButtonPressedEvent.h"
#include "dagmar/MouseButtonReleasedEvent.h"
#include "dagmar/MouseMovedEvent.h"
#include "dagmar/MouseScrolledEvent.h"
#include "dagmar/WindowResizeEvent.h"
#include <dagmar/RenderingSystem.h>
#include <dagmar/PlayerControllerSystem.h>
#include "dagmar/WindowManager.h"
#include "dagmar/Scene.h"
#include <memory>

namespace dag
{
    /**
   * @brief Layer class for the editor UI
   *
   * Mainly initializes the library used to show the UI
   */
    class GameLayer : public Layer
    {
      private:
        std::shared_ptr<RenderingSystem> renderingSystem;

      public:
      
        GameState gameState = GameState::Stop;
        float lastPosX = 0.0f;
        float lastPosY = 0.0f;
        float mouseDragSensitivity = 0.10f;
        float airResistance = 0.1f;
        glm::vec3 gravity = glm::vec3(0.0f, -9.8f, 0.0f);

        glm::vec3 forwardVector = glm::vec3(0.0f, 0.0f, -1.0f);
        glm::vec3 rightVector = glm::vec3(1.0f, 0.0f, 0.0f);
        glm::vec3 upVector = glm::vec3(0.0f, 1.0f, 0.0f);
        
        GameLayer();
        virtual ~GameLayer() = default;

        void onAttach() override;
        void onDetach() override;
        void onUpdate(float const& dt = 0.01f) override;
        void onEvent(Event& event) override;

    	bool hasBeenReset = false;
    	void reset();

      private:
        bool onKeyTyped(KeyTypedEvent& event);
        bool onKeyPressed(KeyPressedEvent& event);
        bool onKeyReleased(KeyReleasedEvent& event);
        bool onMouseScrolled(MouseScrolledEvent& event);
        bool onMouseMoved(MouseMovedEvent& event);
        bool onMouseButtonPressed(MouseButtonPressedEvent& event);
        bool onMouseButtonReleased(MouseButtonReleasedEvent& event);
        bool onWindowResize(WindowResizeEvent& event);
        void updateMovement(float const& dt, PlayerController& playerController, glm::vec3& velocity, Transform& transform);
    };
} // namespace dag