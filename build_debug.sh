#!/bin/bash

mkdir build
pushd build
cmake .. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=../install
cmake --build . -j 4 --target install

popd
