#!/bin/bash

mkdir build
pushd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../install
cmake --build .
cmake --install .
popd
