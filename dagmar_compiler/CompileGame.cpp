#include <cstdlib>
#include <string>
#include <iostream>
#include <filesystem>

#include "dagmar/Log.h"

int main(int argc, char* argv[])
{	
#ifdef NDEBUG
#ifdef DEBUG_SYMBOLS
	std::string build_config = "RelWithDebInfo";
#else
    std::string build_config = "Release";
#endif
#else
    std::string build_config = "Debug";
#endif

    std::string command = std::string("chdir ") + std::filesystem::path(argv[0]).parent_path().make_preferred().string();
    command += std::string(" && cmake .. -DCMAKE_BUILD_TYPE=" + build_config + " -DENGINE_GAME_BUILD=ON");
    command += std::string(" && cmake --build . --config " + build_config);
    command += std::string(" && cmake --install . --config " + build_config);

    return std::system(command.c_str());
}