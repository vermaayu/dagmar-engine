#pragma once
#include <string>

namespace dag
{
    /**
     * @brief Interface class for any psuedo-singleton 
     * managers within the system
     */
    class Module
    {
      public:
        // Should be implemented with contents that would
        // normally be in constructor
        virtual void startup() = 0;

        // Should be implemented with contents that would
        // normally be in destructor
        virtual void shutdown() = 0;

        // Implement a printable name for the manager in question
        virtual std::string getName() const = 0;

      protected:
        // default and deleted constructors and destructors.
        Module() = default;
        virtual ~Module() = default;
        Module(Module const&) = delete;
        Module& operator=(Module const&) = delete;
    };
} // namespace dag
