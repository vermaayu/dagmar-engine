dagmar_add_library(layers Dagmar::Layers STATIC
    Layer.cpp
    LayerManager.cpp)

target_include_directories(layers PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(layers
    Dagmar::Events
)
