#include "dagmar/LayerManager.h"

#include <iostream>

dag::LayerManager::~LayerManager()
{
    for (auto layer : layers)
    {
        layer->onDetach();
        //layer.reset();
    }
}

void dag::LayerManager::addLayer(std::shared_ptr<Layer> const& layer)
{
    layers.emplace_back(layer);
    layer->onAttach();
}

void dag::LayerManager::removeLayer(std::shared_ptr<Layer>& layer)
{
    auto const it = std::find(layers.begin(), layers.end(), layer);

	if (it != layers.end())
	{
        layer->onDetach();
		layers.erase(it);
	}
}

void dag::LayerManager::removeLayer(size_t const& layerID)
{
    if (layerID < layers.size())
    {
        layers[layerID]->onDetach();
        layers.erase(layers.begin() + layerID);
    }
}

void dag::LayerManager::updateLayers()
{
    for(auto it = layers.rbegin(); it != layers.rend(); ++it)
    {
        (*it)->onUpdate();
    }
}
