#include "dagmar/Layer.h"

#include <iostream>

/**
 * @brief Constructor for layer
 */
dag::Layer::Layer(std::string const& name) :
    name(name)
{
}

/**
 * @brief On Attach Function
 *
 * Called when a layer is attached
 */
void dag::Layer::onAttach()
{
}

/**
 * @brief On Detach Function
 *
 * Called when a layer is detached
 */
void dag::Layer::onDetach()
{
}

/**
 * @brief On Update function
 *
 * Called when the layer has to update stuff
 */
void dag::Layer::onUpdate(float const& dt)
{
}

/**
 * @brief On Event function
 *
 * Called when an event happened
 */
void dag::Layer::onEvent(Event& event)
{
}