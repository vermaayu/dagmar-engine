#include "dagmar/Profiler.h"
#include "dagmar/FileSystem.h"
#include "dagmar/ModuleLifecycle.h"

#include <iostream>
#include <stdexcept>
#include <thread>
#include <filesystem>
#include <algorithm>

#ifdef _WIN32
#include "windows.h"
#include "psapi.h"
#include "winnt.h"
#include <stdio.h>
#else
#include "sys/types.h"
#include "sys/sysinfo.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#endif

void dag::Profiler::startup()
{
    previous100.assign(100, 0);

    #ifdef _WIN32
	memInfo.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&memInfo);

    SYSTEM_INFO sysInfo;
    FILETIME ftime, fsys, fuser;

    GetSystemInfo(&sysInfo);
    numProcessors = sysInfo.dwNumberOfProcessors;

    GetSystemTimeAsFileTime(&ftime);
    memcpy(&lastCPU, &ftime, sizeof(FILETIME));

    self = GetCurrentProcess();
    GetProcessTimes(self, &ftime, &ftime, &fsys, &fuser);
    memcpy(&lastSysCPU, &fsys, sizeof(FILETIME));
    memcpy(&lastUserCPU, &fuser, sizeof(FILETIME));
    #endif

    std::filesystem::create_directories("log");

    outputCpuLog.open("log/cpu.log", std::ios::out);
    outputMemoryLog.open("log/mem.log", std::ios::out);

    if (!outputCpuLog)
    {
        throw std::runtime_error("failed to open");
    }
    if (!outputMemoryLog)
    {
        throw std::runtime_error("failed to open");
    }

    profilerThread = std::thread([&](void) {
        auto lastRun = std::chrono::high_resolution_clock::now();

        double timeSinceLastRun;
        while (!shuttingDown)
        {
            timeSinceLastRun = std::chrono::duration<double, std::ratio<1>>(std::chrono::high_resolution_clock::now() - lastRun).count();
            if (systemsManager->isShuttingDown())
            {
                break;
            }
            std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
            std::time_t now_c = std::chrono::system_clock::to_time_t(now);
            std::tm now_tm = *std::localtime(&now_c);

#ifdef _WIN32
            auto memUsage = dag::profiler->getCurrentPhysicalMemoryUsed() * 0.000001;
#else
            auto memUsage = dag::profiler->getCurrentPhysicalMemoryUsed() * 0.001;
#endif

            outputCpuLog << "[" << now_tm.tm_hour << ":" << now_tm.tm_min << ":" << now_tm.tm_sec << " " << now_tm.tm_mday << ":" << now_tm.tm_mon +1 << ":" << 1900 + now_tm.tm_year << "] " << dag::profiler->getCurrentCPUUsed() << "\n";
            outputMemoryLog << "[" << now_tm.tm_hour << ":" << now_tm.tm_min << ":" << now_tm.tm_sec << " " << now_tm.tm_mday << ":" << now_tm.tm_mon + 1 << ":" << 1900 + now_tm.tm_year << "] " << memUsage << "\n";

            prevVecMutex.lock();
            if (memUsage > maxMemThusFar)
            {
                maxMemThusFar = memUsage;
            }
            std::rotate(previous100.begin(), previous100.begin() + 1, previous100.end());
            previous100[99] = memUsage;
            prevVecMutex.unlock();

            lastRun = std::chrono::high_resolution_clock::now();
            // this should be sleep_until
            std::this_thread::sleep_until(lastRun + std::chrono::milliseconds(1000 / 20));
        }
    });
}


void dag::Profiler::shutdown()
{
    shuttingDown = true;
    profilerThread.join();
    outputCpuLog.close();
    outputMemoryLog.close();
}

#ifdef _WIN32
dag::TotalMemoryType dag::Profiler::getTotalVirtualMemory()
{
	return memInfo.ullTotalPageFile;
}

dag::MemoryType dag::Profiler::getCurrentVirtualMemoryUsed()
{
    std::lock_guard<std::mutex> guard(requestMutex);
	PROCESS_MEMORY_COUNTERS_EX pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
	return pmc.PrivateUsage;
}

dag::TotalMemoryType dag::Profiler::getTotalPhysicalMemory()
{
	return memInfo.ullTotalPhys;
}

dag::MemoryType dag::Profiler::getCurrentPhysicalMemoryUsed()
{
    std::lock_guard<std::mutex> guard(requestMutex);
	PROCESS_MEMORY_COUNTERS_EX pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
	return pmc.WorkingSetSize;
}

double dag::Profiler::getCurrentCPUUsed()
{
    std::lock_guard<std::mutex> guard(requestMutex);
    FILETIME ftime, fsys, fuser;
    ULARGE_INTEGER now, sys, user;
    double percent;

    GetSystemTimeAsFileTime(&ftime);
    memcpy(&now, &ftime, sizeof(FILETIME));

    GetProcessTimes(self, &ftime, &ftime, &fsys, &fuser);
    memcpy(&sys, &fsys, sizeof(FILETIME));
    memcpy(&user, &fuser, sizeof(FILETIME));
    percent = (sys.QuadPart - lastSysCPU.QuadPart) +
        (user.QuadPart - lastUserCPU.QuadPart);
    percent /= (now.QuadPart - lastCPU.QuadPart);
    percent /= numProcessors;
    lastCPU = now;
    lastUserCPU = user;
    lastSysCPU = sys;

    return percent * 100.0;
}
#else

dag::TotalMemoryType dag::Profiler::getTotalVirtualMemory()
{
    struct sysinfo memInfo;

    sysinfo (&memInfo);
    long long totalVirtualMem = memInfo.totalram;
    //Add other values in next statement to avoid int overflow on right hand side...
    totalVirtualMem += memInfo.totalswap;
    totalVirtualMem *= memInfo.mem_unit;
    return totalVirtualMem;
}

int parseLine(char* line){
    // This assumes that a digit will be found and the line ends in " Kb".
    int i = strlen(line);
    const char* p = line;
    while (*p <'0' || *p > '9') p++;
    line[i-3] = '\0';
    i = atoi(p);
    return i;
}

dag::MemoryType dag::Profiler::getCurrentVirtualMemoryUsed()
{
    std::lock_guard<std::mutex> guard(requestMutex);
    FILE* file = fopen("/proc/self/status", "r");
    int result = -1;
    char line[128];

    while (fgets(line, 128, file) != NULL){
        if (strncmp(line, "VmSize:", 7) == 0){
            result = parseLine(line);
            break;
        }
    }
    fclose(file);
    return result;
}

dag::TotalMemoryType dag::Profiler::getTotalPhysicalMemory()
{
    struct sysinfo memInfo;

    sysinfo (&memInfo);
    long long totalPhysMem = memInfo.totalram;
    //Multiply in next statement to avoid int overflow on right hand side...
    totalPhysMem *= memInfo.mem_unit;
    return totalPhysMem;
}

dag::MemoryType dag::Profiler::getCurrentPhysicalMemoryUsed()
{
    std::lock_guard<std::mutex> guard(requestMutex);
    FILE* file = fopen("/proc/self/status", "r");
    int result = -1;
    char line[128];

    while (fgets(line, 128, file) != NULL){
        if (strncmp(line, "VmRSS:", 6) == 0){
            result = parseLine(line);
            break;
        }
    }
    fclose(file);
    return result;
}

double dag::Profiler::getCurrentCPUUsed()
{
    std::lock_guard<std::mutex> guard(requestMutex);
    return 0;
}
#endif

void dag::Profiler::imguiPlotMem()
{
    prevVecMutex.lock();
    auto currMemstring = std::to_string(previous100[99]).substr(0, std::to_string(previous100[99]).find(".") + 3);
    auto maxMemstring = std::to_string(maxMemThusFar).substr(0, std::to_string(maxMemThusFar).find(".") + 3);
    ImGui::PlotLines("Lines", previous100.data(), previous100.size(), 0, ("Curr: " + currMemstring + " MB, Prev High: " + maxMemstring + " MB").c_str(), 0, maxMemThusFar, ImVec2(0, 80.0f));
    prevVecMutex.unlock();
}
