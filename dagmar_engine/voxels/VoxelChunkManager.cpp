#include "dagmar/VoxelChunkManager.h"
#include "dagmar/PlayerControllerSystem.h"

float randomInRange2(float min, float max)
{

    // get range
    float range = max - min;
    // get random number
    float randomNumber = rand();
    // scale to given range
    randomNumber /= RAND_MAX;

    randomNumber *= range;
    // translate to min
    randomNumber += min;

    return randomNumber;
}

dag::VoxelChunkManager::VoxelChunkManager()
{
    startingPosition = glm::vec3(0.0f, 0.0f, 0.0f);
    this->maxSize = 10;
    //loadedChunks.resize(maxSize * maxSize);
    this->loadedSize = 3;
    this->chunkSize = 5;
    outside = Voxel();
    outside.active = false;
    outside.blockType = BlockTypes::Unused;

    scale2D = 0.01f;
    scale3D = 0.015f;
    iterations3D = 3;
    iterations2D = 4;
    persistence2D = 0.5f;
    persistence3D = 0.5f;
    hasWater = true;
    waterHeight = 15;
    hasTrees = true;
    hasFlowers = true;
    treeProbability = 1.5f;
    flowerProbability = 1.0f;
    minTreeHeight = 2;
    maxTreeHeight = 4;
    minTreeWidth = 1;
    maxTreeWidth = 2;
    terrainType = 2;

    model.shapes.resize(2);
    model.shapes[0].material.dissolve = 1.0f;
    model.shapes[1].material.dissolve = 0.0f;
	
    flowerVertices = {
        {.pos = {0.103, 0, -0.091}, .normal = {0, -1, -0}}, {.pos = {0.103, 0, 0.109}, .normal = {0, -1, -0}}, {.pos = {-0.097, 0, 0.109}, .normal = {0, -1, -0}}, {.pos = {-0.097, 0, -0.091}, .normal = {0, -1, -0}}, {.pos = {0.103, 1, -0.091}, .normal = {0, 1, -0}}, {.pos = {-0.097, 1, -0.091}, .normal = {0, 1, -0}}, {.pos = {-0.097, 1, 0.109}, .normal = {0, 1, -0}}, {.pos = {0.103, 1, 0.109}, .normal = {0, 1, -0}}, {.pos = {0.103, 0.5335, 0.309}, .normal = {0, 0, 1}}, {.pos = {-0.097, 0.5335, 0.309}, .normal = {0, 0, 1}}, {.pos = {-0.097, 0.2665, 0.309}, .normal = {0, 0, 1}}, {.pos = {0.103, 0.2665, 0.309}, .normal = {0, 0, 1}}, {.pos = {0.103, 0.5335, -0.091}, .normal = {1, 0, -0}}, {.pos = {0.103, 0.5335, 0.109}, .normal = {1, 0, -0}}, {.pos = {0.103, 0.2665, 0.109}, .normal = {1, 0, -0}}, {.pos = {0.103, 0.2665, -0.091}, .normal = {1, 0, -0}}, {.pos = {-0.097, 0.5335, -0.291}, .normal = {0, 0, -1}}, {.pos = {0.103, 0.5335, -0.291}, .normal = {0, 0, -1}}, {.pos = {0.103, 0.2665, -0.291}, .normal = {0, 0, -1}}, {.pos = {-0.097, 0.2665, -0.291}, .normal = {0, 0, -1}}, {.pos = {-0.097, 0.5335, 0.109}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 0.5335, -0.091}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 0.2665, -0.091}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 0.2665, 0.109}, .normal = {-1, 0, -0}}, {.pos = {0.103, 0.8, 0.109}, .normal = {0, 0, 1}}, {.pos = {-0.097, 0.8, 0.109}, .normal = {0, 0, 1}}, {.pos = {-0.097, 0.5335, 0.109}, .normal = {0, 0, 1}}, {.pos = {0.103, 0.5335, 0.109}, .normal = {0, 0, 1}}, {.pos = {-0.097, 0, 0.109}, .normal = {0, 0, 1}}, {.pos = {0.103, 0, 0.109}, .normal = {0, 0, 1}}, {.pos = {0.103, 0.2665, 0.109}, .normal = {0, 0, 1}}, {.pos = {-0.097, 0.2665, 0.109}, .normal = {0, 0, 1}}, {.pos = {0.103, 0.8, -0.091}, .normal = {1, 0, -0}}, {.pos = {0.103, 0.8, 0.109}, .normal = {1, 0, -0}}, {.pos = {0.103, 0, 0.109}, .normal = {1, 0, -0}}, {.pos = {0.103, 0, -0.091}, .normal = {1, 0, -0}}, {.pos = {-0.097, 0.8, -0.091}, .normal = {0, 0, -1}}, {.pos = {0.103, 0.8, -0.091}, .normal = {0, 0, -1}}, {.pos = {0.103, 0.5335, -0.091}, .normal = {0, 0, -1}}, {.pos = {-0.097, 0.5335, -0.091}, .normal = {0, 0, -1}}, {.pos = {0.103, 0, -0.091}, .normal = {0, 0, -1}}, {.pos = {-0.097, 0, -0.091}, .normal = {0, 0, -1}}, {.pos = {-0.097, 0.2665, -0.091}, .normal = {0, 0, -1}}, {.pos = {0.103, 0.2665, -0.091}, .normal = {0, 0, -1}}, {.pos = {-0.097, 0.8, 0.109}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 0.8, -0.091}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 0, -0.091}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 0, 0.109}, .normal = {-1, 0, -0}}, {.pos = {0.103, 0.5335, 0.309}, .normal = {1, 0, -0}}, {.pos = {0.103, 0.2665, 0.309}, .normal = {1, 0, -0}}, {.pos = {0.103, 0.5335, 0.109}, .normal = {0, 1, -0}}, {.pos = {-0.097, 0.5335, 0.109}, .normal = {0, 1, -0}}, {.pos = {-0.097, 0.5335, 0.309}, .normal = {0, 1, -0}}, {.pos = {0.103, 0.5335, 0.309}, .normal = {0, 1, -0}}, {.pos = {-0.097, 0.2665, 0.309}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 0.5335, 0.309}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 0.2665, 0.109}, .normal = {0, -1, -0}}, {.pos = {0.103, 0.2665, 0.109}, .normal = {0, -1, -0}}, {.pos = {0.103, 0.2665, 0.309}, .normal = {0, -1, -0}}, {.pos = {-0.097, 0.2665, 0.309}, .normal = {0, -1, -0}}, {.pos = {0.103, 0.2665, -0.291}, .normal = {1, 0, -0}}, {.pos = {0.103, 0.5335, -0.291}, .normal = {1, 0, -0}}, {.pos = {0.103, 0.2665, -0.091}, .normal = {0, -1, -0}}, {.pos = {-0.097, 0.2665, -0.091}, .normal = {0, -1, -0}}, {.pos = {-0.097, 0.2665, -0.291}, .normal = {0, -1, -0}}, {.pos = {0.103, 0.2665, -0.291}, .normal = {0, -1, -0}}, {.pos = {-0.097, 0.5335, -0.291}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 0.2665, -0.291}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 0.5335, -0.091}, .normal = {0, 1, -0}}, {.pos = {0.103, 0.5335, -0.091}, .normal = {0, 1, -0}}, {.pos = {0.103, 0.5335, -0.291}, .normal = {0, 1, -0}}, {.pos = {-0.097, 0.5335, -0.291}, .normal = {0, 1, -0}}, {.pos = {-0.097, 0.8, 0.309}, .normal = {0, 0, 1}}, {.pos = {0.103, 0.8, 0.309}, .normal = {0, 0, 1}}, {.pos = {0.103, 1, 0.309}, .normal = {0, 0, 1}}, {.pos = {-0.097, 1, 0.309}, .normal = {0, 0, 1}}, {.pos = {0.303, 0.8, 0.109}, .normal = {1, 0, -0}}, {.pos = {0.303, 0.8, -0.091}, .normal = {1, 0, -0}}, {.pos = {0.303, 1, -0.091}, .normal = {1, 0, -0}}, {.pos = {0.303, 1, 0.109}, .normal = {1, 0, -0}}, {.pos = {0.103, 0.8, -0.291}, .normal = {0, 0, -1}}, {.pos = {-0.097, 0.8, -0.291}, .normal = {0, 0, -1}}, {.pos = {-0.097, 1, -0.291}, .normal = {0, 0, -1}}, {.pos = {0.103, 1, -0.291}, .normal = {0, 0, -1}}, {.pos = {-0.297, 0.8, -0.091}, .normal = {-1, 0, -0}}, {.pos = {-0.297, 0.8, 0.109}, .normal = {-1, 0, -0}}, {.pos = {-0.297, 1, 0.109}, .normal = {-1, 0, -0}}, {.pos = {-0.297, 1, -0.091}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 0.8, 0.109}, .normal = {0, -1, -0}}, {.pos = {0.103, 0.8, 0.109}, .normal = {0, -1, -0}}, {.pos = {0.103, 0.8, 0.309}, .normal = {0, -1, -0}}, {.pos = {-0.097, 0.8, 0.309}, .normal = {0, -1, -0}}, {.pos = {0.103, 1, 0.109}, .normal = {1, 0, -0}}, {.pos = {0.103, 1, 0.309}, .normal = {1, 0, -0}}, {.pos = {0.103, 0.8, 0.309}, .normal = {1, 0, -0}}, {.pos = {-0.097, 1, 0.309}, .normal = {0, 1, -0}}, {.pos = {0.103, 1, 0.309}, .normal = {0, 1, -0}}, {.pos = {-0.097, 1, 0.109}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 0.8, 0.309}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 1, 0.309}, .normal = {-1, 0, -0}}, {.pos = {0.103, 0.8, -0.091}, .normal = {0, -1, -0}}, {.pos = {0.303, 0.8, -0.091}, .normal = {0, -1, -0}}, {.pos = {0.303, 0.8, 0.109}, .normal = {0, -1, -0}}, {.pos = {0.103, 1, -0.091}, .normal = {0, 0, -1}}, {.pos = {0.303, 1, -0.091}, .normal = {0, 0, -1}}, {.pos = {0.303, 0.8, -0.091}, .normal = {0, 0, -1}}, {.pos = {0.303, 1, 0.109}, .normal = {0, 1, -0}}, {.pos = {0.303, 1, -0.091}, .normal = {0, 1, -0}}, {.pos = {0.103, 1, 0.109}, .normal = {0, 0, 1}}, {.pos = {0.303, 0.8, 0.109}, .normal = {0, 0, 1}}, {.pos = {0.303, 1, 0.109}, .normal = {0, 0, 1}}, {.pos = {-0.097, 0.8, -0.091}, .normal = {0, -1, -0}}, {.pos = {-0.097, 0.8, -0.291}, .normal = {0, -1, -0}}, {.pos = {0.103, 0.8, -0.291}, .normal = {0, -1, -0}}, {.pos = {-0.097, 1, -0.091}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 1, -0.291}, .normal = {-1, 0, -0}}, {.pos = {-0.097, 0.8, -0.291}, .normal = {-1, 0, -0}}, {.pos = {0.103, 1, -0.291}, .normal = {0, 1, -0}}, {.pos = {-0.097, 1, -0.291}, .normal = {0, 1, -0}}, {.pos = {0.103, 1, -0.091}, .normal = {1, 0, -0}}, {.pos = {0.103, 0.8, -0.291}, .normal = {1, 0, -0}}, {.pos = {0.103, 1, -0.291}, .normal = {1, 0, -0}}, {.pos = {-0.297, 0.8, 0.109}, .normal = {0, -1, -0}}, {.pos = {-0.297, 0.8, -0.091}, .normal = {0, -1, -0}}, {.pos = {-0.097, 1, 0.109}, .normal = {0, 0, 1}}, {.pos = {-0.297, 1, 0.109}, .normal = {0, 0, 1}}, {.pos = {-0.297, 0.8, 0.109}, .normal = {0, 0, 1}}, {.pos = {-0.297, 1, -0.091}, .normal = {0, 1, -0}}, {.pos = {-0.297, 1, 0.109}, .normal = {0, 1, -0}}, {.pos = {-0.097, 1, -0.091}, .normal = {0, 0, -1}}, {.pos = {-0.297, 0.8, -0.091}, .normal = {0, 0, -1}}, {.pos = {-0.297, 1, -0.091}, .normal = {0, 0, -1}}, {.pos = {0.103, 1, -0.091}, .normal = {0, 1, -0}}, {.pos = {-0.097, 1, -0.091}, .normal = {0, 1, -0}}, {.pos = {-0.097, 1, 0.109}, .normal = {0, 1, -0}}, {.pos = {0.103, 1, 0.109}, .normal = {0, 1, -0}}};

    flowerIndices = {
        4, 5, 6, 6, 7, 4, 8, 9, 10, 10, 11, 8, 12, 13, 14, 14, 15, 12, 16, 17, 18, 18, 19, 16, 20, 21, 22, 22, 23, 20, 24, 25, 26, 26, 27, 24, 28, 29, 30, 30, 31, 28, 32, 33, 13, 13, 12, 32, 34, 35, 15, 15, 14, 34, 36, 37, 38, 38, 39, 36, 40, 41, 42, 42, 43, 40, 44, 45, 21, 21, 20, 44, 46, 47, 23, 23, 22, 46, 14, 13, 48, 48, 49, 14, 50, 51, 52, 52, 53, 50, 20, 23, 54, 54, 55, 20, 56, 57, 58, 58, 59, 56, 12, 15, 60, 60, 61, 12, 62, 63, 64, 64, 65, 62, 22, 21, 66, 66, 67, 22, 68, 69, 70, 70, 71, 68, 72, 73, 74, 74, 75, 72, 76, 77, 78, 78, 79, 76, 80, 81, 82, 82, 83, 80, 84, 85, 86, 86, 87, 84, 88, 89, 90, 90, 91, 88, 33, 92, 93, 93, 94, 33, 7, 6, 95, 95, 96, 7, 97, 44, 98, 98, 99, 97, 89, 100, 101, 101, 102, 89, 37, 103, 104, 104, 105, 37, 4, 7, 106, 106, 107, 4, 108, 24, 109, 109, 110, 108, 100, 111, 112, 112, 113, 100, 45, 114, 115, 115, 116, 45, 5, 4, 117, 117, 118, 5, 119, 32, 120, 120, 121, 119, 111, 88, 122, 122, 123, 111, 25, 124, 125, 125, 126, 25, 6, 5, 127, 127, 128, 6, 129, 36, 130, 130, 131, 129, 132, 133, 134, 134, 135, 132};
}

// dag::VoxelChunkManager::VoxelChunkManager(glm::vec3 position, int maxSize, int loadedSize, int chunkSize)
// {
//     startingPosition = position;
//     this->maxSize = maxSize;
//     loadedChunks.resize(maxSize * maxSize);
//     this->loadedSize = loadedSize;
//     this->chunkSize = chunkSize;
//     outside = Voxel();
//     outside.active = false;
//     outside.blockType = BlockTypes::Unused;
// }

void dag::VoxelChunkManager::reset()
{
    loadedChunks.clear();
    //loadedChunks.shrink_to_fit();
    loadedChunks = std::map<size_t, std::unique_ptr<VoxelChunk>>{};
    //loadedChunks.resize(maxSize * maxSize);
    //loadedChunks.shrink_to_fit();
    oldPosition = glm::vec3(-100.0f);
}

bool dag::VoxelChunkManager::update(glm::vec3 const& cameraPosition)
{
    /*
    if(view changed){
        updateLoadList();
        load();
        updateViewList();
        regenrateVisible();
    }
    */

    if(glm::length2(cameraPosition - oldPosition) < 0.1f && !shouldUpdate){
        
        return false;
    }

    oldPosition = cameraPosition;
    glm::vec3 localPosition = cameraPosition - startingPosition;

    if(localPosition.x < 0.0f || localPosition.z < 0.0f){
        return false;
    }

    checkToLoad((int)localPosition.x / chunkSize, (int)localPosition.z / chunkSize);
    load();
    checkToUnload((int)localPosition.x / chunkSize, (int)localPosition.z / chunkSize);
    unload();
    // std::cout << "To load: ";
    // for(size_t i = 0; i < toLoadIDs.size(); ++i){
    //     std::cout << toLoadIDs[i] << ' ';
    // }
    // std::cout << "\nLoaded: ";
    // for(size_t i = 0; i < loadedChunks.size(); ++i){
    //     std::cout << bool(loadedChunks[i]) << ' ';
    // }

    // std::cout << "\nToUnload: ";
    // for(size_t i = 0; i < toUnloadIDs.size(); ++i){
    //     std::cout << toUnloadIDs[i] << ' ';
    // }
    // std::cout << "\n" << std::endl;
    if (!toLoadIDs.empty() || !toUnloadIDs.empty() || shouldUpdate)
    {

        render();
        shouldUpdate = false;
        return true;
    }
    return false;
}

void dag::VoxelChunkManager::renderFlower(Shape& shape, glm::vec3 const& startPos, size_t const& i, size_t const& j, size_t const& k, glm::vec4 const& color)
{
    uint32_t index = shape.vertices.size();
    // std::cout << "Flower vertices: " << flowerVertices.size() << std::endl;
    Vertex vert;

    for (size_t vertIndex = 0; vertIndex < flowerVertices.size(); ++vertIndex)
    {
        vert = flowerVertices[vertIndex];
        vert.pos += startPos + glm::vec3(i + 0.5f, j, k + 0.5f);
        vert.color = blockColors[BlockTypes::Grass];

        shape.vertices.push_back(vert);
    }

    for (size_t ind = 0; ind < flowerIndices.size(); ++ind)
    {
        shape.indices.push_back(flowerIndices[ind] + index);
        // if(ind >= flowerIndices.size() - 119){
        //     if((ind >= flowerIndices.size() - 59 && ind <= flowerIndices.size() - 57) || (ind >= flowerIndices.size() - 40 && ind <= flowerIndices.size() - 0)){
        //         shape.vertices[shape.indices.back()].color = {1.0f, 1.0f, 0.0f, 1.0f};
        //     }
        //     else{
        //         shape.vertices[shape.indices.back()].color = {1.0f, 0.0f, 0.0f, 1.0f};
        //     }
        // }

        if (ind >= flowerIndices.size() - 126)
        {
            shape.vertices[shape.indices.back()].color = color;
        }
    }
}

void dag::VoxelChunkManager::computeModel(Shape& shape, bool isTransparent)
{
    shape.vertices = std::vector<dag::Vertex>{};

    std::vector<int> corners(4);

    std::array<glm::vec3, 6> faceNormals = {
        glm::vec3(1, 0, 0),
        glm::vec3(-1, 0, 0),
        glm::vec3(0, 1, 0),
        glm::vec3(0, -1, 0),
        glm::vec3(0, 0, 1),
        glm::vec3(0, 0, -1)};
    Vertex vertex;

    for (size_t voxelChunkID = 0; voxelChunkID < loadedChunks.size(); ++voxelChunkID)
    {   
        if(loadedChunks[voxelChunkID].get() == nullptr){
            continue;
        }
        for (size_t i = 0; i < loadedChunks[voxelChunkID]->blockSize; ++i)
        {
            for (size_t j = 0; j < loadedChunks[voxelChunkID]->blockHeight; ++j)
            {
                for (size_t k = 0; k < loadedChunks[voxelChunkID]->blockSize; ++k)
                {
                    auto& voxel = loadedChunks[voxelChunkID]->getVoxel(i, j, k);

                    // if (shouldFrustumCull && frustumCulling && !isInFrustum(loadedChunks[voxelChunkID]->startingPoint, i, j, k))
                    // {
                    //     continue;
                    // }

                    if (isTransparent)
                    {
                        if (voxel.blockType != BlockTypes::Water)
                        {
                            continue;
                        }
                    }
                    else
                    {

                        if (!voxel.isActive() && voxel.blockType != BlockTypes::Flower)
                        {
                            continue;
                        }
                    }

                    if (voxel.blockType == BlockTypes::Flower)
                    {
                        int colorIndex = randomInRange2(0, flowerColorSize - 1);
                        renderFlower(shape, loadedChunks[voxelChunkID]->startingPoint, i, j, k, flowerColors[colorIndex]);
                        continue;
                    }

                    if (!(loadedChunks[voxelChunkID]->getVoxel(i, j, k - 1).isActive(isTransparent) || getVoxel(loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j, k - 1.0f)).isActive(isTransparent)))
                    {
                        // front face
                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j, k);
                        vertex.normal = faceNormals[5];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[0] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i + 1, j, k);
                        vertex.normal = faceNormals[5];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[1] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i + 1, j + 1, k);
                        vertex.normal = faceNormals[5];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[2] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j + 1, k);
                        vertex.normal = faceNormals[5];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[3] = shape.vertices.size() - 1;

                        shape.indices.push_back(corners[0]);
                        shape.indices.push_back(corners[2]);
                        shape.indices.push_back(corners[1]);

                        shape.indices.push_back(corners[0]);
                        shape.indices.push_back(corners[3]);
                        shape.indices.push_back(corners[2]);
                    }

                    if (!(loadedChunks[voxelChunkID]->getVoxel(i - 1, j, k).isActive(isTransparent) || getVoxel(loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i - 1.0f, j, k)).isActive(isTransparent)))
                    {
                        // left face
                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j, k + 1);
                        vertex.normal = faceNormals[1];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[0] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j, k);
                        vertex.normal = faceNormals[1];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[1] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j + 1, k);
                        vertex.normal = faceNormals[1];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[2] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j + 1, k + 1);
                        vertex.normal = faceNormals[1];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[3] = shape.vertices.size() - 1;

                        shape.indices.push_back(corners[0]);
                        shape.indices.push_back(corners[2]);
                        shape.indices.push_back(corners[1]);

                        shape.indices.push_back(corners[0]);
                        shape.indices.push_back(corners[3]);
                        shape.indices.push_back(corners[2]);
                    }

                    if (!(loadedChunks[voxelChunkID]->getVoxel(i, j, k + 1).isActive(isTransparent) || getVoxel(loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j, k + 1.0f)).isActive(isTransparent)))
                    {
                        // back face
                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i + 1, j, k + 1);
                        vertex.normal = faceNormals[4];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[0] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j, k + 1);
                        vertex.normal = faceNormals[4];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[1] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j + 1, k + 1);
                        vertex.normal = faceNormals[4];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[2] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i + 1, j + 1, k + 1);
                        vertex.normal = faceNormals[4];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[3] = shape.vertices.size() - 1;

                        shape.indices.push_back(corners[0]);
                        shape.indices.push_back(corners[2]);
                        shape.indices.push_back(corners[1]);

                        shape.indices.push_back(corners[0]);
                        shape.indices.push_back(corners[3]);
                        shape.indices.push_back(corners[2]);
                    }

                    if (!(loadedChunks[voxelChunkID]->getVoxel(i + 1, j, k).isActive(isTransparent) || getVoxel(loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i + 1.0f, j, k)).isActive(isTransparent)))
                    {
                        // right face
                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i + 1, j, k);
                        vertex.normal = faceNormals[0];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[0] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i + 1, j, k + 1);
                        vertex.normal = faceNormals[0];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[1] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i + 1, j + 1, k + 1);
                        vertex.normal = faceNormals[0];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[2] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i + 1, j + 1, k);
                        vertex.normal = faceNormals[0];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[3] = shape.vertices.size() - 1;

                        shape.indices.push_back(corners[0]);
                        shape.indices.push_back(corners[2]);
                        shape.indices.push_back(corners[1]);

                        shape.indices.push_back(corners[0]);
                        shape.indices.push_back(corners[3]);
                        shape.indices.push_back(corners[2]);
                    }

                    if (!(loadedChunks[voxelChunkID]->getVoxel(i, j + 1, k).isActive(isTransparent) || getVoxel(loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j + 1.0f, k)).isActive(isTransparent)))
                    {
                        // top face
                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j + 1, k);
                        vertex.normal = faceNormals[2];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[0] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i + 1, j + 1, k);
                        vertex.normal = faceNormals[2];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[1] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i + 1, j + 1, k + 1);
                        vertex.normal = faceNormals[2];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[2] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j + 1, k + 1);
                        vertex.normal = faceNormals[2];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[3] = shape.vertices.size() - 1;

                        shape.indices.push_back(corners[0]);
                        shape.indices.push_back(corners[2]);
                        shape.indices.push_back(corners[1]);

                        shape.indices.push_back(corners[0]);
                        shape.indices.push_back(corners[3]);
                        shape.indices.push_back(corners[2]);
                    }

                    if (!(loadedChunks[voxelChunkID]->getVoxel(i, j - 1, k).isActive(isTransparent) || getVoxel(loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j - 1.0f, k)).isActive(isTransparent)))
                    {
                        // bottom face
                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j, k + 1);
                        vertex.normal = faceNormals[3];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[0] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i + 1, j, k + 1);
                        vertex.normal = faceNormals[3];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[1] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i + 1, j, k);
                        vertex.normal = faceNormals[3];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[2] = shape.vertices.size() - 1;

                        vertex.pos = loadedChunks[voxelChunkID]->startingPoint + glm::vec3(i, j, k);
                        vertex.normal = faceNormals[3];
                        vertex.color = blockColors[voxel.blockType];
                        shape.vertices.push_back(vertex);
                        corners[3] = shape.vertices.size() - 1;

                        shape.indices.push_back(corners[0]);
                        shape.indices.push_back(corners[2]);
                        shape.indices.push_back(corners[1]);

                        shape.indices.push_back(corners[0]);
                        shape.indices.push_back(corners[3]);
                        shape.indices.push_back(corners[2]);
                    }
                }
            }
        }
    }
}

void dag::VoxelChunkManager::computeModelHelper(size_t thread_id, size_t thread_num)
{
    /*computeModel(model.shapes[0], false, thread_id, thread_num);
    computeModel(model.shapes[1], true, thread_id, thread_num);*/
}

void dag::VoxelChunkManager::render()
{
    // std::cout << "Active camera: " << playerControllerSystem->cameraSystem->activeCamera << std::endl;
    // if()

    for (size_t i = 0; i < model.shapes.size(); ++i)
    {
        // std::cout << "shape: " << i << std::endl;
        // std::cout << model.shapes[i].VAOIndex << std::endl;
        //renderingSystem->vaos[model.shapes[i].VAOIndex].destroy();
        //renderingSystem->vaos[model.shapes[i].VAOIndex] = dag::VertexArrayObject<float, uint32_t>{};

        model.shapes[i].vertices.clear();
        model.shapes[i].indices.clear();

        // std::cout << model.shapes[i].vertices.capacity() << std::endl;
        // std::cout << model.shapes[i].indices.capacity() << std::endl;

        model.shapes[i].vertices = std::vector<Vertex>(0);
        model.shapes[i].indices = std::vector<uint32_t>(0);

        // std::cout << model.shapes[i].vertices.capacity() << std::endl;
        // std::cout << model.shapes[i].indices.capacity() << std::endl;
    }

    // model.shapes.resize(loadedChunks.size() * 2);
    // model.shapes.shrink_to_fit();
    // std::cout << "render()" << std::endl;

    //std::vector<std::thread> computeThreads;

    //size_t num_threads = 1;

    /*for (size_t thread_id = 0; thread_id < num_threads; ++thread_id)
    {
        computeThreads.emplace_back(&dag::VoxelChunkManager::computeModelHelper, this, thread_id, num_threads);
    }*/

    /*computeThreads.emplace_back(&dag::VoxelChunkManager::computeModel, this, std::ref(model.shapes[0]), false, static_cast<size_t>(0), num_threads);
    computeThreads.emplace_back(&dag::VoxelChunkManager::computeModel, this, std::ref(model.shapes[1]), true, static_cast<size_t>(0), num_threads);

    for (size_t thread_id = 0; thread_id < computeThreads.size(); ++thread_id)
    {
        computeThreads[thread_id].join();
    }*/

    computeModel(model.shapes[0], false);
    computeModel(model.shapes[1], true);

    std::vector<size_t> sizes = { 53687091 * 4, 5368709 * 4 };

    for (size_t i = 0; i < model.shapes.size(); ++i)
    {
        /*renderingSystem->vaos[model.shapes[i].VAOIndex].indexBuffer.destroy();
        renderingSystem->vaos[model.shapes[i].VAOIndex].vertexBuffer.destroy();
        renderingSystem->vaos[model.shapes[i].VAOIndex].destroy();
        renderingSystem->vaos[model.shapes[i].VAOIndex] = dag::VertexArrayObject<float, uint32_t>{};
        renderingSystem->vaos[model.shapes[i].VAOIndex] = std::move(dag::resourceManager->generateVAO(model.shapes[i], resourceManager->atts));*/

        // shape 0
        // vb: 319611680
        // ib: 40755776

        // shape 1
        // vb: 29029088
        // ib: 3628664

        // shape 0
        // vb: 536870912
        // ib: 53687091

        // shape 1
        // vb: 53687091
        // ib: 5368709


        dag::Mesh shapeMesh;
        resourceManager->getMesh(model.shapes[i], shapeMesh);

        /*auto vb = dag::VertexBuffer<float>(shapeMesh.vertices);
        vb.generate();

        for (auto const& att : resourceManager->atts)
        {
            vb.addAttribute(att);
        }

        auto ib = dag::IndexBuffer<uint32_t>(shapeMesh.indices);
        ib.generate();

        auto vao = dag::VertexArrayObject<float, uint32_t>(vb, ib);
        vao.generate();
        vao.aggregate(GL_STATIC_DRAW);*/

        auto& curr_vao = renderingSystem->vaos[model.shapes[i].VAOIndex];

        //curr_vao.bind();

        curr_vao.vertexBuffer.bind();
        glNamedBufferSubData(curr_vao.vertexBuffer.id, 0, shapeMesh.vertices.size() * sizeof(float), shapeMesh.vertices.data());
        curr_vao.vertexBuffer.activateAttributes();
        /*curr_vao.vertexBuffer.elements = std::vector<float>{};
        curr_vao.vertexBuffer.elements.resize(shapeMesh.vertices.size());
        curr_vao.vertexBuffer.elements.assign(shapeMesh.vertices.size() * sizeof(float), 0);
        curr_vao.vertexBuffer.elements.shrink_to_fit();*/
        curr_vao.vertexBuffer.size = shapeMesh.vertices.size();


        curr_vao.indexBuffer.bind();
        glNamedBufferSubData(curr_vao.indexBuffer.id, 0, shapeMesh.indices.size() * sizeof(uint32_t), shapeMesh.indices.data());
        /*curr_vao.indexBuffer.elements = std::vector<uint32_t>{};
        curr_vao.indexBuffer.elements.resize(shapeMesh.indices.size());
        curr_vao.indexBuffer.elements.assign(shapeMesh.indices.size(), 0);
        curr_vao.indexBuffer.elements.shrink_to_fit();*/
        curr_vao.indexBuffer.size = shapeMesh.indices.size();

       // curr_vao.unbind();

        curr_vao.indexBuffer.unbind();
        curr_vao.vertexBuffer.unbind();

        //dag::Mesh shapeMesh;
        //resourceManager->getMesh(model.shapes[i], shapeMesh);

        //renderingSystem->vaos[model.shapes[i].VAOIndex].vertexBuffer.update(shapeMesh.vertices.data(), 0, shapeMesh.vertices.size() * sizeof(float), true);
        //renderingSystem->vaos[model.shapes[i].VAOIndex].vertexBuffer.elements.assign(shapeMesh.vertices.size(), 0);
        //renderingSystem->vaos[model.shapes[i].VAOIndex].vertexBuffer.elements = shapeMesh.vertices;

        //renderingSystem->vaos[model.shapes[i].VAOIndex].indexBuffer.update(shapeMesh.indices.data(), 0, shapeMesh.indices.size() * sizeof(uint32_t), true);
        //renderingSystem->vaos[model.shapes[i].VAOIndex].indexBuffer.elements.assign(shapeMesh.indices.size(), 0);
        //renderingSystem->vaos[model.shapes[i].VAOIndex].indexBuffer.elements = shapeMesh.indices;

        //renderingSystem->vaos[model.shapes[i].VAOIndex].bind();

        //renderingSystem->vaos[model.shapes[i].VAOIndex].unbind();

        //renderingSystem->vaos[model.shapes[i].VAOIndex].indexBuffer.elements.shrink_to_fit();
        //renderingSystem->vaos[model.shapes[i].VAOIndex].vertexBuffer.elements.shrink_to_fit();
        //renderingSystem->vaos.shrink_to_fit();
    }

    for (size_t i = 0; i < model.shapes.size(); ++i)
    {
        model.shapes[i].vertices.clear();
        model.shapes[i].indices.clear();

        model.shapes[i].vertices = std::vector<Vertex>(0);
        model.shapes[i].indices = std::vector<uint32_t>(0);
    }

    // std::cout << "end render()" << std::endl;

    // std::cout << "loaded shapes size: " << model.shapes.size() << std::endl;
}

dag::Voxel& dag::VoxelChunkManager::getVoxel(glm::vec3 const& position)
{
    glm::vec3 localPosition = position - startingPosition;

    if (position.y - startingPosition.y > chunkSize * 8)
    {
        // std::cout << "Outside height" << std::endl;
        return outside;
    }

    int index = getIndex((int)localPosition.x / chunkSize, (int)localPosition.z / chunkSize);

    if (index < 0)
    {
        return outside;
    }

    // DONE, works correctly

    if (loadedChunks[index].get() != nullptr)
    {
        return loadedChunks[index]->getVoxel(position);
    }
    else
    {
        // std::cout << "Outside loaded" << std::endl;
        return outside;
    }
}

glm::vec3 dag::VoxelChunkManager::getVoxelPosition(glm::vec3& position)
{
    glm::vec3 localPosition = position - startingPosition;

    // if
    int index = getIndex((int)localPosition.x / chunkSize, (int)localPosition.z / chunkSize);

    if (index < 0)
    {
        return glm::vec3(-1, -1, -1);
    }

    if (loadedChunks[index].get() != nullptr)
    {

        return loadedChunks[index]->getVoxelPosition(position);
    }
    else
    {
        return glm::vec3(-1, -1, -1);
    }
}

void dag::VoxelChunkManager::checkToLoad(int x, int y)
{
    int halfSize = (loadedSize - 1) / 2;
    //toLoadIDs.clear();
    //toLoadIDs.shrink_to_fit();
    toLoadIDs = std::map<size_t, uint32_t>{};

    // std::cout << "Camera pos: " << x << ' ' << y  << " half size " << halfSize << std::endl;

    size_t count = 0;

    int minX, minY, maxX, maxY;

    minX = x - halfSize < 0 ? 0 : x - halfSize;
    minY = y - halfSize < 0 ? 0 : y - halfSize;
    maxX = x + halfSize >= maxSize ? maxSize - 1 : x + halfSize;
    maxY = y + halfSize >= maxSize ? maxSize - 1 : y + halfSize;

    for (size_t i = minX; i <= maxX; ++i)
    {
        for (size_t j = minY; j <= maxY; ++j)
        {
            int index = getIndex(i, j);
            bool shouldAdd = true;

            if(!loadedChunks[index])
            {
                toLoadIDs.insert_or_assign(count, index);
                count++;
            }
        }
    }
}

void dag::VoxelChunkManager::checkToUnload(int x, int y)
{
    int halfSize = (loadedSize - 1) / 2;
    toUnloadIDs.clear();
    //toUnloadIDs.shrink_to_fit();
    toUnloadIDs = std::map<size_t, uint32_t>{};

    // std::cout << "Camera pos: " << x << ' ' << y  << " half size " << halfSize << std::endl;

    size_t map_index_count = 0;

    int minX, minY, maxX, maxY;

    minX = x - halfSize < 0 ? 0 : x - halfSize;
    minY = y - halfSize < 0 ? 0 : y - halfSize;
    maxX = x + halfSize >= maxSize ? maxSize - 1 : x + halfSize;
    maxY = y + halfSize >= maxSize ? maxSize - 1 : y + halfSize;

    for (size_t i = 0; i < loadedChunks.size(); ++i)
    {
        if(loadedChunks[i]){
            int x = i / maxSize;
            int y = i % maxSize;

            if (x < minX || x > maxX || y < minY || y > maxY)
            {
                toUnloadIDs.insert_or_assign(map_index_count, i);
                map_index_count++;
            }
        }
    }
}

void dag::VoxelChunkManager::unload()
{
    for (size_t i = 0; i < toUnloadIDs.size(); ++i)
    {
        loadedChunks[toUnloadIDs[i]].reset();
        // loadedChunks[toUnloadIDs[i]] = nullptr;
    }
}

void dag::VoxelChunkManager::loadWorkerFunction(size_t start_index, size_t end_index)
{
    for (size_t i = start_index; i < end_index; ++i)
    {
        loadedChunks[toLoadIDs[i]]->generateTerrain();
    }
}

void dag::VoxelChunkManager::load()
{
    // loadedChunks.clear();

    if(toLoadIDs.empty()){
        return;
    }

    for (size_t i = 0; i < toLoadIDs.size(); ++i)
    {
        glm::vec3 position = startingPosition + glm::vec3((toLoadIDs[i] / maxSize) * chunkSize, 0.0f, (toLoadIDs[i] % maxSize) * chunkSize);
        loadedChunks[toLoadIDs[i]] = std::make_unique<VoxelChunk>(VoxelChunk(chunkSize, position, terrainType, scale2D, scale3D, iterations2D, iterations3D, persistence2D, persistence3D, hasWater, waterHeight, hasTrees, treeProbability, minTreeHeight, maxTreeHeight, minTreeWidth, maxTreeWidth, flowerProbability, hasFlowers));
    }


    std::vector<std::thread> voxelBuilderThreads;

    size_t numThreads = static_cast<size_t>(std::thread::hardware_concurrency() * 0.75);

    Log::info("Building Voxel with ", numThreads, " threads");
    Log::info("Total Voxel index width: ", toLoadIDs.size());

    for (size_t curr_thread = 0; curr_thread < numThreads; ++curr_thread)
    {
        size_t indexWidth = toLoadIDs.size() / numThreads;

        if (curr_thread != numThreads - 1)
        {
            //Log::info("Thread ", curr_thread, " voxel index width: ", curr_thread * indexWidth, " -> ", (curr_thread + 1) * indexWidth);
            voxelBuilderThreads.emplace_back(&dag::VoxelChunkManager::loadWorkerFunction, this, curr_thread * indexWidth, (curr_thread + 1) * indexWidth);
        }
        else
        {
            //Log::info("Thread ", curr_thread, " voxel index width: ", curr_thread * indexWidth, " -> ", toLoadIDs.size());
            voxelBuilderThreads.emplace_back(&dag::VoxelChunkManager::loadWorkerFunction, this, curr_thread * indexWidth, toLoadIDs.size());
        }
    }

    size_t count = 0;
    for (auto& vbt : voxelBuilderThreads)
    {
        vbt.join();
        //Log::info("Thread ", count, " joined");
        count++;
    }

    // std::cout << "Loaded chunks: " << std::endl;

    
}