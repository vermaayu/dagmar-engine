dagmar_add_library(voxel Dagmar::Voxel STATIC
    Voxel.cpp
    VoxelChunk.cpp
    VoxelChunkManager.cpp
)

target_include_directories(voxel PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(voxel PUBLIC 
    glm::glm
    Dagmar::Components
    Dagmar::ECS
    Dagmar::Systems
)
