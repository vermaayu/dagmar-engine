#pragma once

#include <iostream>
#include <glm/glm.hpp>
#include <array>

namespace dag
{
    /*
    * @brief Assumes a voxel is 1 unit in each dimension, editable by setting the scale of the transform for the voxel entity
    */
    enum BlockTypes
        {
            Rock = 0,
            Soil = 1,
            Grass = 2,
            Sand = 3,
            DarkerRock = 4,
            Water = 5,
            Default = 6,
            Red = 7,
            Wood = 8,
            Leaves = 9,
            Unused = 10,
            Flower = 11
        };
    inline glm::vec4 blockColors[] = {
        glm::vec4(0.36f, 0.35f, 0.29f, 1.0f), // Rock
        glm::vec4(0.52f, 0.36f, 0.09f, 1.0f), // Soil
        glm::vec4(0.04f, 0.52f, 0.2f, 1.0f), // Grass
        glm::vec4(0.69f, 0.62f, 0.3f, 1.0f), // Sand
        glm::vec4(0.28f, 0.28f, 0.18f, 1.0f), // Rock
        glm::vec4(0.06f, 0.0f, 0.78f, 0.6f), // Water
        glm::vec4(0.3f, 0.3f, 0.3f, 1.0f), // Default
        glm::vec4(0.9f, 0.0f, 0.0f, 1.0f), // Red
        glm::vec4(0.38f, 0.28f, 0.0f, 1.0f), // Wood
        glm::vec4(0.34f, 0.63f, 0.25f, 1.0f), // Leaves
        glm::vec4(0.0f, 0.0f, 0.0f, 1.0f), // Unused
        glm::vec4(0.34f, 0.63f, 0.25f, 1.0f) // Flower
    };

    inline glm::vec4 flowerColors[] = {
        glm::vec4(0.9f, 0.0f, 0.0f, 1.0f), // Red
        glm::vec4(0.9f, 0.9f, 0.0f, 1.0f), // Yellow
        glm::vec4(0.9f, 0.5f, 0.0f, 1.0f), // Orange
        glm::vec4(0.9f, 0.0f, 0.5f, 1.0f), // Pink
        glm::vec4(0.9f, 0.0f, 0.9f, 1.0f), // Purple
        glm::vec4(0.0f, 0.0f, 0.9f, 1.0f), // Blue
        glm::vec4(0.9f, 0.9f, 0.9f, 1.0f), // White
    };
    inline int blockColorSize = 5, flowerColorSize = 7;
    inline float blockColorLimits[] = {
        0.8f, // Rock
        0.6f, // Soil
        0.4f, // Grass
        0.2f, // Sand
        0.0f  // Rock - darker
    };
    class Voxel
    {
      public:
        Voxel();
        bool active;
        BlockTypes blockType;
        inline bool isActive(bool const& isTransparent = false){
            if(isTransparent){
                return this->active;
            }
            if(!this->active){
                return false;
            }
            else{
                return this->blockType != BlockTypes::Water && this->blockType != BlockTypes::Flower;
            }
        }
    };
} // namespace dag