#pragma once

#include "dagmar/VoxelChunk.h"
#include "dagmar/ResourceData.h"
#include "dagmar/ResourceManager.h"
#include "dagmar/RenderingSystem.h"
#include <memory>
#include <map>

namespace dag
{
    /**
     * @brief Chunk manager that manages all the high scale voxel scene
     * The total available scene is maxSize x 1 x maxSize chunks.
     * The loaded area around the camera is loadedSize x 1 x loadedSize chunks
     * and the actual loaded chunks are stored inside the loadedChunks
     */

    class VoxelChunkManager
    {
      public:
        // glm::vec3 cameraPosition;
        glm::vec3 startingPosition;
        // std::map<uint32_t, std::unique_ptr<VoxelChunk>> loadedChunks;
        std::map<size_t, std::unique_ptr<VoxelChunk>> loadedChunks;
        std::map<size_t, uint32_t> toLoadIDs, toUnloadIDs;
        
        // std::vector<glm::vec3> vertices, normals;
        // std::vector<glm::vec4> colors;
        // std::vector<uint32_t> indices;
        dag::Model model;
        size_t meshHash;
        bool shouldUpdate = false;
        Voxel outside;

        float scale2D;
        float scale3D;
        int iterations3D;
        int iterations2D;
        float persistence2D;
        float persistence3D;
        bool hasWater;
        int waterHeight;
        bool hasTrees;
        bool hasFlowers;
        float treeProbability;
        float flowerProbability;
        int minTreeHeight;
        int maxTreeHeight;
        int minTreeWidth;
        int maxTreeWidth;
        int terrainType; 

        std::vector<Vertex> flowerVertices;
        std::vector<uint32_t> flowerIndices;

        dag::Entity currVoxelEntity = UINT32_MAX;
        dag::Entity currVoxelMainEntity = UINT32_MAX;

        glm::vec3 oldPosition = glm::vec3(-100.0f);

      public:
        VoxelChunkManager();
        // VoxelChunkManager(glm::vec3 position, int maxSize, int loadedSize, int chunkSize);

        bool voxelExists() { return UINT32_MAX != currVoxelEntity; }

        std::shared_ptr<RenderingSystem> renderingSystem;

        int maxSize;    // in chunks
        int loadedSize; // in chunks, please make it odd for easier computation
        int chunkSize;
        glm::mat4 cameraView;
        void checkToLoad(int x, int y);
        void loadWorkerFunction(size_t start_index, size_t end_index);
        void load();
        void checkToUnload(int x, int y);
        void unload();
        void render();
        void reset();
        Voxel& getVoxel(glm::vec3 const& position);
        glm::vec3 getVoxelPosition(glm::vec3& position);
        void renderFlower(Shape& shape, glm::vec3 const& startPos, size_t const& i, size_t const& j, size_t const& k, glm::vec4 const& color);
        void computeModelHelper(size_t thread_id, size_t thread_num);
        void computeModel(Shape& shape, bool isTransparent);
        bool update(glm::vec3 const& cameraPosition);
        inline int getIndex(int x, int y)
        {
            if (x < 0 || x >= maxSize || y < 0 || y >= maxSize)
            {
                return -1;
            }
            return x * maxSize + y;
        }
    };

    inline std::shared_ptr<VoxelChunkManager> voxelManager(new VoxelChunkManager());
} // namespace dag