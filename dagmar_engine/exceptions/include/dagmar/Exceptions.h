#pragma once
#include <exception>

namespace dag
{
    /**
     * @Exception class for shader compilation errors
     *
     * Must be constructed with OpenGL shader type
     *
     */
    class ShaderCompilationException : public std::exception
    {
        private:
            int shader_type;
        public:
            // deleted default constructor
            ShaderCompilationException() = delete;

            // virtual destructor
            virtual ~ShaderCompilationException() throw() {};

            // only constructor requires shader type
            ShaderCompilationException(int const & shader_type) : shader_type(shader_type) {}

            const char* what() const noexcept override;
    };

    /**
     * @Exception class for shader program linking errors
     *
     */
    class ShaderProgramLinkingException : public std::exception
    {
        public:
            // virtual destructor
            virtual ~ShaderProgramLinkingException() throw() {};
            // returns error message
            const char* what() const noexcept override;
    };

    /**
     * @Exception class for the ECS
     */
    class ECSException : public std::exception
    {
        public:
            enum class ErrorType
            {
                InsertComponentAlreadyExistent,
                RemoveComponentNotExistent,
                GetComponentNotExistent,
                RegisterComponentAlreadyExistent,
                GetComponentTypeNotExistent,
                GetComponentArrayNotExistent,
                RegisterSystemAlreadyExistent,
                SetSystemSignatureNotExistent,
                GetSystemSignatureNotExistent,
                CreateEntityTooManyEntities,
                DestroyEntityOutOfRange,
                SetEntitySignatureOutOfRange,
                GetEntitySignatureOutOfRange
            };

        private:
            ErrorType errorType;

        public:
            // only constructor
            ECSException(ErrorType const& errorType) : errorType(errorType) {}

            // virtual destructor
            virtual ~ECSException() throw() {};

            // returns error message
            const char* what() const noexcept override;
    };
}
