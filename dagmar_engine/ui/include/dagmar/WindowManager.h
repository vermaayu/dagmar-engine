#pragma once
#include "dagmar/Log.h"
#include "dagmar/Module.h"
#include <memory>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include "dagmar/Window.h"

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

namespace dag
{
    /**
     * @brief WindowManager takes care of any operations to do with initialising destroying the UI
     *
     */
    class WindowManager : public Module
    {
        using EventCallbackFn = std::function<void(Event&)>;

      public:
        ImGuiContext* ctx;

        Window window;

        Window loadingWindow;

        bool initialized = false;

      public:
        WindowManager() = default;
        ~WindowManager() = default;

        void startup() override;
        void shutdown() override;

    	void createMainWindow();
        void createSplashWindow(bool const& randomized = false);
        void swapContext();

        std::string getName() const override;

        void disableCursor() const;
        void enableCursor() const;
        void hideCursor() const;
    };

    inline std::shared_ptr<WindowManager> windowManager(new WindowManager());
} // namespace dag