#pragma once
// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include <map>
#include <memory>

#include "dagmar/Module.h"
#include "glm/vec2.hpp"

namespace dag
{
    /**
    * @brief KeyManager contains the state of all pressed keys
    *
    */

    class KeyManager : public Module
    {
      private:
        std::map<int, bool> keys;

      public:
        // default for Module
        KeyManager() = default;
        ~KeyManager() = default;

        // If this KeyInput is enabled and the given key is monitored,
        // returns pressed state.  Else returns false.
        bool isKeyDown(int key);
        bool isMouseButtonDown(int mouseButton);
        glm::vec2 getMousePosition();
    	

      private:
        // Used internally to update key states. Called by the GLFW callback.
        void setKeyDown(int key, bool isDown);

      public:
        // Registers GLFW callback
        void startup() override;
        // does nothing
        void shutdown() override;
        std::string getName() const override
        {
            return "KeyManager";
        }

      private:
        // The GLFW callback for key events.
        static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
        static void charCallback(GLFWwindow* window, unsigned c);
        static void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);
    };

    inline std::shared_ptr<KeyManager> keyManager(new KeyManager());
} // namespace dag