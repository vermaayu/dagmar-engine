#pragma once

#include <GL/glew.h>
#include "GLFW/glfw3.h"

#include "dagmar/Event.h"
#include <functional>

namespace dag
{
    class Window
    {
        using EventCallbackFn = std::function<void (Event&)>;

      public:
        struct WindowInfo
        {
            std::string title;
            uint32_t width = 1280;
            uint32_t height = 720;
            bool vsync = true;
            EventCallbackFn eventCallbackFn;
            bool decorated = true;
            bool install = true;
            bool visible = true;
        };

      public:
        GLFWwindow* handle;
        WindowInfo info;

    private:
        inline static bool glewInitialized = false;
    	
      public:
        virtual ~Window();
        virtual void create(WindowInfo const& windowInfo);
        virtual void onUpdate();
        virtual uint32_t getWidth() const;
        virtual uint32_t getHeight() const;
        void focus() const;

        virtual void setEventCallbackFn(EventCallbackFn const& callback);
        virtual void setVSync(bool const& enabled);
        virtual bool getVSync() const;
        virtual void maximize();
        virtual void minimize();
        virtual void hide();
        virtual void show();

    	virtual void center();

        virtual void destroy();
    };
} // namespace dag
