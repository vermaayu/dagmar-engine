dagmar_add_library(ui_lib Dagmar::UI STATIC
    WindowManager.cpp
    KeyManager.cpp
    Window.cpp)

target_include_directories(ui_lib PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}/include"
)

target_link_libraries(ui_lib PUBLIC
    imgui::imgui
    glfw::glfw
    glew::glew
    Dagmar::ModuleLifecycle
    Dagmar::Log
    Dagmar::Scene
    Dagmar::Events
    Dagmar::Filesystem
)

configure_file(default_imgui.ini ${DAGMAR_ENGINE_BINARY_DIR}/default_imgui.ini)
configure_file(default_imgui.ini ${DAGMAR_ENGINE_INSTALL_DIR}/default_imgui.ini)
