#include "dagmar/Texture.h"
#include <GL/glew.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <stdexcept>

/**
 * @brief Destroy funcs
 */
void dag::Texture::destroy()
{
    if (id != 0)
    {
        glDeleteTextures(1, &id);
    }
}

/**
 * @brief Returns whether the format is depth or not
 */
bool dag::Texture::isDepthFormat()
{
    switch (info.format)
    {
        case dag::TextureFormat::DEPTH24STENCIL8:
        {
            return true;
        }
        case dag::TextureFormat::DEPTH32:
        {
            return true;
        }
        default:
        {
            return false;
        }
    }
}

uint32_t dag::Texture::formatToGL(TextureFormat const& format)
{
    switch (format)
    {
        case TextureFormat::R8:
            return GL_R8;
        case TextureFormat::RG8:
            return GL_RG8;
        case TextureFormat::RGBA8:
            return GL_RGBA8;
        case TextureFormat::RGB8:
            return GL_RGB8;
        case TextureFormat::RGBA16F:
            return GL_RGBA16F;
        case TextureFormat::DEPTH24STENCIL8:
            return GL_DEPTH24_STENCIL8;
        case TextureFormat::DEPTH32:
            return GL_DEPTH32F_STENCIL8;
        default:
            return GL_RGB8;
    }
}

uint32_t dag::Texture::attachmentToGL(TextureAttachment const& type)
{
    switch (type)
    {
        case TextureAttachment::COLOR_ATTACHMENT:
            return GL_COLOR_ATTACHMENT0;
        case TextureAttachment::DEPTH_ATTACHMENT:
            return GL_DEPTH_STENCIL_ATTACHMENT;
        case TextureAttachment::STENCIL_ATTACHMENT:
            return GL_STENCIL_ATTACHMENT;
        case TextureAttachment::DEPTH_STENCIL_ATTACHMENT:
            return GL_DEPTH_STENCIL_ATTACHMENT;
        default:
            return GL_COLOR_ATTACHMENT0;
    }
	
}

void dag::Texture::setFormats(int32_t const& channels, TextureFormat& engineFormat, int32_t& internalTypeFormat)
{
    switch (channels)
    {
        case 1:
        {
            internalTypeFormat = GL_RED;
            engineFormat = TextureFormat::R8;
            break;
        }
        case 2:
        {
            internalTypeFormat = GL_RG;
            engineFormat = TextureFormat::RG8;
            break;
        }
        case 3:
        {
            internalTypeFormat = GL_RGB;
            engineFormat = TextureFormat::RGB8;
            break;
        }
        case 4:
        {
            internalTypeFormat = GL_RGBA;
            engineFormat = TextureFormat::RGBA8;
            break;
        }
        default:
        {
            throw std::runtime_error("Error: Bad texture format / Not supported number of channels.");
        }
    }
}
