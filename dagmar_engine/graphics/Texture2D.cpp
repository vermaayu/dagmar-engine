#include "dagmar/Texture2D.h"

#include <GL/glew.h>
#include <stdexcept>

#include <iostream>

#include <cmath>
#include <stb_image.h>

/**
 * @brief Creates a 2D texture using the information gathered
 * param textureInfo 
 */
void dag::Texture2D::create(dag::TextureInfo const& textureInfo)
{
    if (id != 0)
    {
        destroy();
    }

    this->info = textureInfo;

    glCreateTextures(GL_TEXTURE_2D, 1, &id);

    glTextureParameteri(id, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTextureParameteri(id, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTextureParameteri(id, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    if (textureInfo.samples > 1)
    {
        glTextureStorage2DMultisample(id, textureInfo.samples, formatToGL(textureInfo.format), textureInfo.width, textureInfo.height, GL_FALSE);
    }
    else
    {
        if (textureInfo.mipmaps)
        {
            glTextureStorage2D(id, static_cast<size_t>(std::max(std::log2(textureInfo.width), std::log2(textureInfo.height))), formatToGL(textureInfo.format), textureInfo.width, textureInfo.height);
        }
        else
        {
            glTextureStorage2D(id, 1, formatToGL(textureInfo.format), textureInfo.width, textureInfo.height);
        }
    }
}

/**
 * @brief Creates a texture from file
 */
void dag::Texture2D::create(std::filesystem::path const& path)
{
    std::cout << path << std::endl;
    int width;
    int height;
    int nrChannels;
    //HACK
    std::cout << "Loading: " << path << std::endl;
    unsigned char* data = stbi_load(path.string().c_str(), &width, &height, &nrChannels, 0);

    if (data == nullptr)
    {
        throw std::runtime_error("Error: Texture2D::create(std::string path) - empty texture.");
    }

    int32_t internalTypeFormat;
    TextureFormat engineFormat = TextureFormat::NONE;

    setFormats(nrChannels, engineFormat, internalTypeFormat);

    create(TextureInfo{.width = static_cast<uint32_t>(width), .height = static_cast<uint32_t>(height), .depth = 1, .format = engineFormat, .mipmaps = true});

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTextureSubImage2D(id, 0, 0, 0, width, height, internalTypeFormat, GL_UNSIGNED_BYTE, data);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

    glGenerateTextureMipmap(id);

    stbi_image_free(data);
}

/**
 * @brief Binds the texture
 */
void dag::Texture2D::bind(uint32_t const& slot) const
{
    glBindTextureUnit(slot, id);
}

/**
 * @brief Binds the texture
 */
void dag::Texture2D::bind() const
{
    glBindTexture(info.samples > 1 ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D, id);
}

void dag::Texture2D::unbind() const
{
    glBindTexture(info.samples > 1 ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D, 0);
}

void dag::Texture2D::unbind(uint32_t const& slot) const
{
    glBindTextureUnit(slot, 0);
}

void dag::Texture2D::resize(uint32_t const& width, uint32_t const& height)
{
    if (width >= 8192 || height >= 8192)
    {
        throw std::runtime_error("Error: Maximum width/height is 8192 px.");
    }

    info.width = width;
    info.height = height;

    create(info);
}
