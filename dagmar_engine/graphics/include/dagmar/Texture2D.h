#pragma once
#include <filesystem>
#include <string>

#include "dagmar/Texture.h"

namespace dag
{
    /*
     * @brief Texture2D class
     */
    class Texture2D final : public Texture
    {
      public:
    	// Create func with texture info
        void create(TextureInfo const& textureInfo) override;

    	// Create func with path
        void create(std::filesystem::path const& path);
        
        // Binds the texture
        void bind(uint32_t const& slot) const override;
        void bind() const override;
    	
        // Unbinds the texture
        void unbind() const override ;
        void unbind(uint32_t const& slot) const override;

    	void resize(uint32_t const& width, uint32_t const& height);
    };
} // namespace dag