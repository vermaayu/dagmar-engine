#pragma once
#include <cstdint>
#include <vector>

#include "Framebuffer.h"
// #include "dagmar/Framebuffer.h"

namespace dag
{
    /**
	 * @brief Base RenderPass class
	 *
	 * Currently used only by OpenGL, can be extended for other APIs
	 */
    class RenderPass
    {
      public:
        Framebuffer* readColorFramebuffer;
        Framebuffer* readDepthFramebuffer;
        std::vector<Texture*> inputAttachments;
        Framebuffer* writeFramebuffer;
        std::vector<std::vector<float>> clearValues;

    	std::string name = "";

      public:
        RenderPass(Framebuffer* readColorFramebuffer, Framebuffer* readDepthFramebuffer, std::vector<Texture*> const& inputAttachments, Framebuffer* writeFramebuffer, std::vector<std::vector<float>> const& clearValues, std::string const& name = "") :
            readColorFramebuffer(readColorFramebuffer), readDepthFramebuffer(readDepthFramebuffer), inputAttachments(inputAttachments), writeFramebuffer(writeFramebuffer), clearValues(clearValues), name(name) {};

        virtual ~RenderPass() = default;
        virtual void begin();
        virtual void end();
    };
} // namespace dag