#pragma once
#include "dagmar/Texture.h"
#include "dagmar/Texture2D.h"
#include <array>
#include <cstdint>
#include <memory>
#include <vector>

namespace dag
{
    /**
     * @brief Framebuffer class
     */
    class Framebuffer
    {
        //private:
      public:
        uint32_t id = 0;
        uint32_t width = 0;
        uint32_t height = 0;

        //std::vector<std::shared_ptr<Texture>> colorAttachments;
        //std::shared_ptr<Texture> depthAttachment;

    	std::vector<Texture*> colorAttachments = {};
        Texture* depthAttachment = nullptr;

    private:
        std::vector<int32_t> colorAttachmentBoundLayers;
        int32_t depthAttachmentBoundLayer;
        uint32_t depthAttachmentType;

      public:
        // Default constructor
        Framebuffer() = default;

        // Creates the framebuffer
        void create(uint32_t const& width, uint32_t const& height, bool isBackBuffer = false);

        // Destroys the framebuffer
        void destroy();

        void attachTexture(Texture *const texture, TextureAttachment const& type, int32_t const& offset = 0);
        void attachTexture(Texture* const texture, TextureAttachment const& type, int32_t const& offset, int32_t const& layer);

        void setDrawBuffers(uint32_t const& count);

        // Gets the color attachment at id
        Texture* getColorAttachment(uint32_t const& id);

        // Gets the depth attachment id
        Texture* getDepthAttachment();

        // Binds the framebuffer
        void bind() const;

        // Unbind the framebuffer
        void unbind() const;

    	void reattachTextures();

        std::array<unsigned char, 4> getPixelData(int32_t const& x, int32_t const& y) const;

        static void blit(Framebuffer& src, Framebuffer& dst, int32_t const& bitfield);
    };
} // namespace dag