#pragma once

#include "dagmar/VertexArrayObject.h"
#include <cstdint>
#include <vector>

namespace dag
{
    /**
     * @brief Simple data mesh class
     */
    class Mesh
    {
      public:
        std::vector<float> vertices;
        std::vector<uint32_t> indices;

      public:
        Mesh() = default;

        // Constructor
        Mesh(std::vector<float> const& vertices, std::vector<uint32_t> const& indices);

        // Generator for VAO
        VertexArrayObject<float, uint32_t> generateVAO(std::vector<dag::VertexAttribute> const& attributes);
    };
}; // namespace dag
