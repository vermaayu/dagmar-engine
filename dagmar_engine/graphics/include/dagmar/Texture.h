#pragma once
#include <cstdint>

namespace dag
{
    /**
     * @brief Texture format (fixed atm)
     */
    enum class TextureFormat
    {
        // Invalid
        NONE = 0,
        // Colors
        R8,
        RG8,
        RGB8,
        RGBA8,
    	RGBA16F,
        // Depths /-+/ stencil
        DEPTH24STENCIL8,
        DEPTH32,
        // Defaults
        DEPTH = DEPTH24STENCIL8
    };

	/**
	 * @brief Texture Attachment Type
	 */
	enum class TextureAttachment
	{
		NONE = 0,
		COLOR_ATTACHMENT = 1 << 0,
		DEPTH_ATTACHMENT = 1 << 1,
		STENCIL_ATTACHMENT = 1 << 2,
		DEPTH_STENCIL_ATTACHMENT = 1 << 3
	};

    /**
     * @brief TextureInfo
     */
    struct TextureInfo
    {
        uint32_t width = 0;
        uint32_t height = 0;
        uint32_t depth = 0;
        uint32_t samples = 1;
        uint32_t arrayLayers = 0;
        TextureFormat format = TextureFormat::NONE;
        bool mipmaps = false;

    	int32_t usage = 0x0;

        enum TypeBitsFlags : int32_t
        {
            COLOR = 1 << 0,
            DEPTH = 1 << 1,
            STENCIL = 1 << 2
        };
    };

    /**
    * @brief Texture class
    */
    class Texture
    {
      public:
        uint32_t id = 0;

      public:
        TextureInfo info;

      public:
        Texture() = default;
        virtual ~Texture() = default;

        // Create func for texture
        virtual void create(TextureInfo const& textureInfo) {};

        // Destroys the texture
        virtual void destroy();
    	
        // Binds the texture
        virtual void bind(uint32_t const& slot) const = 0 ;
        virtual void bind() const = 0;
        // Unbinds the texture
        virtual void unbind (uint32_t const& slot) const = 0;
        virtual void unbind() const = 0;

        bool isDepthFormat();

        static uint32_t formatToGL(TextureFormat const& format);
        static uint32_t attachmentToGL(TextureAttachment const& format);
    	static void setFormats(int32_t const& channels, TextureFormat& engineFormat, int32_t& internalTypeFormat);
    };
} // namespace dag