
#include "dagmar/TextureCube.h"

#include <GL/glew.h>
#include <stdexcept>

#include <iostream>

#include <stb_image.h>

#include <thread>

/**
 * @brief Creates a cube texture using the information gathered
 * param textureInfo 
 */
void dag::TextureCube::create(dag::TextureInfo const& textureInfo)
{
    this->info = textureInfo;

    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_CUBE_MAP, id);

    /*
	// NEW STYLE
	glCreateTextures(GL_TEXTURE_CUBE_MAP, 1, &id);

	// OLD STYLE
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    */

    glTextureParameteri(id, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTextureParameteri(id, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTextureParameteri(id, GL_TEXTURE_WRAP_R, GL_REPEAT);
    glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTextureParameteri(id, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    //glTextureStorage2D(GL_TEXTURE_CUBE_MAP, 0, formatToGL(textureInfo.format), textureInfo.width, textureInfo.height);
    //glTextureStorage3D(id, 0, formatToGL(textureInfo.format), textureInfo.width, textureInfo.height, 6);
}

/**
 * @brief Creates a texture from file
 */
void dag::TextureCube::create(std::vector<std::string> const& paths)
{
    if (paths.empty())
    {
        throw std::runtime_error("Error: TextureCube::create(std::vector<std::string> const& path) - empty texture paths.");
    }

    int width;
    int height;
    int nrChannels;

    { // TEMPORARY FILLING DATA
        // This is used to fill in width/height for a face of the cube
        // so we can use it later
        uint8_t* data = stbi_load(paths[0].c_str(), &width, &height, &nrChannels, 0);
        stbi_image_free(data);
    }

    int32_t internalTypeFormat;
    TextureFormat engineFormat = TextureFormat::NONE;

    setFormats(nrChannels, engineFormat, internalTypeFormat);

    if (id != 0)
    {
        destroy();
    }

    create(TextureInfo{.width = static_cast<uint32_t>(width), .height = static_cast<uint32_t>(height), .depth = 1, .format = engineFormat});


	std::vector<uint8_t*> data(6);
    std::vector<std::thread> threads;

    for (uint32_t i = 0; i < paths.size(); i++)
    {
        threads.emplace_back([=, &data]() {
            int width;
            int height;
            int nrChannels;
        	
            auto tempData = stbi_load(paths[i].c_str(), &width, &height, &nrChannels, 0);

            if (tempData == nullptr)
            {
                throw std::runtime_error("Error: TextureCube::create(std::string path) - empty texture.");
            }

        	data[i] = tempData;
        });
    }

    for (uint32_t i = 0; i < paths.size(); i++)
    {
        threads[i].join();
    	
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, formatToGL(engineFormat), width, height, 0, internalTypeFormat, GL_UNSIGNED_BYTE, data[i]);
        stbi_image_free(data[i]);
    }
}

/**
 * @brief Binds the texture
 */
void dag::TextureCube::bind(uint32_t const& slot) const
{
    glBindTextureUnit(slot, id);
}

/**
 * @brief Binds the texture
 */
void dag::TextureCube::bind() const
{
    glBindTexture(GL_TEXTURE_CUBE_MAP, id);
}

void dag::TextureCube::unbind(uint32_t const& slot) const
{
    glBindTextureUnit(slot, 0);
}

void dag::TextureCube::unbind() const
{
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}
