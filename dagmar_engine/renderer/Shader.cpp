#include "dagmar/Shader.h"

void dag::SubShader::load(int32_t const& stages, std::string const& fullShaderSource)
{
    // E.g. the vertex shader can be id 0
    std::vector<std::string> processed;
    std::vector<int32_t> subShaderIDs;

    for (int32_t i = 0; i < ShaderData::totalStages; i++)
    {
        if(stages & (1 << i))
        {
			processed.emplace_back(process(1 << i, fullShaderSource));
			subShaderIDs.emplace_back(compileSubShader(ShaderData::stageToGL(static_cast<ShaderData::Stage>(1 << i)), processed.back().c_str()));
        }
    }

    if (id != 0)
    {
        destroy();
    }
	
    id = glCreateProgram();

    for (auto const& tsubShader : subShaderIDs)
    {
        glAttachShader(id, tsubShader);
    }

    glLinkProgram(id);

    checkShaderLinkError(id);

    // Delete the shaders now as the program is complete
    for (auto& subShaderID : subShaderIDs)
    {
        glDetachShader(id, subShaderID);
        glDeleteShader(subShaderID);
    }
}

void dag::SubShader::destroy() const
{
    glDeleteProgram(id);
}

bool dag::SubShader::hasInclude(int32_t const& includeID) const
{
    for (auto& ids : includesIDs)
    {
        if (includeID == ids)
        {
            return true;
        }
    }

    return false;
}

int32_t dag::SubShader::compileSubShader(int32_t const& type, const char* source)
{
    int32_t id = glCreateShader(type);

    glShaderSource(id, 1, &source, nullptr);
    glCompileShader(id);

    checkShaderCompileError(type, id);

    return id;
}

void dag::SubShader::checkShaderCompileError(int32_t const& type, int32_t const& subShaderID)
{
    int success;
    char infoLog[2048];

    glGetShaderiv(subShaderID, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(subShaderID, 2048, nullptr, infoLog);
        Log::error("SHADER::COMPILATION_FAILED: ", infoLog);
        throw dag::ShaderCompilationException(type);
    }
}

void dag::SubShader::checkShaderLinkError(int32_t const& shaderID)
{
    int success;
    char infoLog[2048];
    glGetProgramiv(shaderID, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(shaderID, 2048, nullptr, infoLog);
        // print linking errors if any
        Log::error("SHADER::PROGRAM::LINKING_FAILED: ", infoLog);
        throw dag::ShaderProgramLinkingException();
    }
}

std::string dag::SubShader::process(int32_t const& typeAsPow2, std::string const& source)
{
    std::string outSource = "#version 460\n";
    outSource += "#extension GL_ARB_separate_shader_objects : enable\n";
    outSource += "#extension GL_ARB_explicit_uniform_location : enable\n";
    outSource += "#define " + ShaderData::stagesNames[static_cast<int32_t>(log2f(typeAsPow2))] + " 1\n\n";
    outSource += source;

	
    std::regex const includeRegex(R"(#\s*include\s*([<"])([^>"]+)([>"]))");
    //std::regex const includeRegex(R"(#include "[0-9a-zA-Z\.\-/_]+")");
    auto match = std::smatch{};

	std::regex const innerRegex(R"(([<"])([^>"]+)([>"]))");


    while (std::regex_search(outSource, match, includeRegex))
    {
        auto const name = match.str();
        auto innerMatch = std::smatch{};
    	
    	std::regex_search(name, innerMatch, innerRegex);
    	
        auto const shaderIncludePath = innerMatch[2].str();
        auto const fullPath = dag::filesystem::getAssetsPath() / "shaders" / shaderIncludePath;

        auto includeCode = ShaderData::load(fullPath);

    	auto includeID = ShaderData::getIncludeID(fullPath);
    	
    	if (!hasInclude(includeID))
    	{
            includesIDs.emplace_back(includeID);
    	}

        // Please don't abuse of infinite inclusion
        outSource.replace(match.position(), name.length(), includeCode);
    }

    return outSource;
}

std::string dag::ShaderData::readShaderData(std::filesystem::path const& path)
{
    Log::debug("Attempting to create shader include from: " + path.string());
    std::string code;
    std::ifstream shaderFile;
    // ensure ifstream objects can throw exceptions:
    shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try
    {
        // open files
        shaderFile.open(path.string(),std::ifstream::in);
        Log::debug("Include shader file successfully opened.");
        std::stringstream shaderStream;

        // read file's buffer contents into streams
        shaderStream << shaderFile.rdbuf();

        // close file handlers
        shaderFile.close();

        // convert stream into string
        code = shaderStream.str();
    }
    catch (std::ifstream::failure& e)
    {
        Log::error("SHADER_DATA::FILE_NOT_SUCCESSFULLY_READ: ", e.what());
        throw std::runtime_error("error");
    }

    return code;
}

void dag::SubShader::bind() const
{
    glUseProgram(id);
}

void dag::SubShader::unbind() const
{
    glUseProgram(0);
}

std::string dag::ShaderData::load(std::filesystem::path const& includePath)
{
    int32_t id = getIncludeID(includePath);

    if (id == -1)
    {
        cachedPaths.emplace_back(includePath);
        cachedLastWriteTimes.emplace_back(last_write_time(includePath));
    }

    return readShaderData(includePath);
}

int32_t dag::ShaderData::getIncludeID(std::filesystem::path const& includePath)
{
    for (int32_t i = 0; i < cachedPaths.size(); i++)
    {
        if (cachedPaths[i] == includePath)
        {
            return i;
        }
    }

	return -1;
}

dag::UberShader::UberShader(std::filesystem::path const& shaderPath, int32_t const& stages)
{
    load(shaderPath, stages);
}

inline void dag::UberShader::load(std::filesystem::path const& shaderPath, int32_t const& stages)
{
    auto const code = ShaderData::readShaderData(shaderPath);
	
    this->path = shaderPath;
    this->stages = stages;
    this->lastWriteTime = std::filesystem::last_write_time(shaderPath);
    this->subShader.load(stages, code);
}

void dag::UberShader::destroy() const
{
    subShader.destroy();
}

bool dag::UberShader::hasInclude(int32_t const& id)
{
    return subShader.hasInclude(id);
}

auto dag::UberShader::bind() -> void
{
    subShader.bind();
}

void dag::UberShader::unbind() const
{
    subShader.unbind();
}

void dag::UberShader::reload()
{
    auto const code = ShaderData::readShaderData(path);
    subShader.load(stages, code);
}
