#pragma once

#include <filesystem>

namespace dag
{
    /**
     * @brief Shader module class
     */
    class ShaderModule
    {
      public:
        unsigned int id;
        int type;

      public:
        ShaderModule() = default;
        ShaderModule(std::filesystem::path const& shaderPath, int const& shaderType, std::vector<std::string> const& extraDefines = {});

        void destroy();
        void setCodeAndCompile(std::filesystem::path const& shaderPath, int const& shaderType, std::vector<std::string> const& extraDefines = {});

        bool operator==(ShaderModule const& other) const;
        bool operator!=(ShaderModule const& other) const;
    };
} // namespace dag
