#pragma once

#include <cstdint>

namespace dag
{
    /**
     * @brief Vertex Attribute
     * 
     * Responsible for holding data about a vertex attribute.
     * Can be asynchronously turned on during a vertex buffer creation.
     */
    class VertexAttribute
    {
      public:
        uint32_t index = 0;
        int32_t size = 0;
        uint32_t type = 0;
        bool normalized = false;
        int32_t stride = 0;
        void* offsetPtr = nullptr;

      public:
        // Constructor
        VertexAttribute(uint32_t const& index, int32_t const& size, uint32_t const& type, bool const& normalized, int32_t stride, void* offsetPtr);

        // Activates the vertex attributes
        void activate();
    };
} // namespace dag
