#pragma once
#include <GL/glew.h>
#include <array>
#include <filesystem>
#include <fstream>
#include <regex>

#include "dagmar/Exceptions.h"
#include "dagmar/FileSystem.h"
#include "dagmar/Log.h"

namespace dag
{
    /**
     * shader example
     *
     *
     * #ifdef VERTEX_SHADER
     *  layout(...) in vec4 pos;
     *  layout(...) out vec4 outPos;
     *  void main() { outPos = pos; }
     * #endif
     *
     * #ifdef FRAGMENT_SHADER
     *  layout(...) in vec4 pos;
     *  void main() { ... };
     * #endif
     *
     *  layout(std140, binding = 0) uniform scene_data
     *  {
     *      int light_count;
     *  };
     *
     *  layout(std140, binding = 2) uniform lights_data
     *  {
     *      Transform transforms[MAX_LIGHTS];
     *      LightProperties properties[MAX_LIGHTS];
     *  };
     *
     *  layout(std140, binding = 1) uniform entities_data
     *  {
     *      Transform transforms[MAX_ENTITIES];
     *      Material materials[MAX_ENTITIES];
     *  };
     *
     *  layout(std140, binding = 2) uniform per_entity_data
     *  {
     *      int entity_id;
     *  };
     *  
     *
     *  layout(std140, binding = x) uniform light_data
     *  {
     *      vec4 positions[MAX_LIGHTS];
     *      vec4 light_directions[MAX_LIGHTS];
     *      // EXTRA
     *  };
     *
     *  
     * 
     */

    /**
     * @brief General static data for the shaders 
     */
    class ShaderData
    {
      public:
        enum Stage : int32_t
        {
            ComputeShader = (1 << 0),
            VertexShader = (1 << 1),
            TessellationEvaluationShader = (1 << 2),
            TessellationControlShader = (1 << 3),
            GeometryShader = (1 << 4),
            FragmentShader = (1 << 5),
        };

    	inline static constexpr int32_t totalStages = 6;

        inline static std::array<std::string, totalStages> const stagesNames = {
            "COMPUTE_SHADER", "VERTEX_SHADER", "TESS_EVAL_SHADER", "TESS_CONTROL_SHADER", "GEOMETRY_SHADER", "FRAGMENT_SHADER"};

    	inline static std::vector<std::filesystem::path> cachedPaths;
        inline static std::vector<std::filesystem::file_time_type> cachedLastWriteTimes;


      public:
        static uint32_t stageToGL(Stage const& stage)
        {
            switch (stage)
            {
                case ComputeShader:
                    return GL_COMPUTE_SHADER;
                case VertexShader:
                    return GL_VERTEX_SHADER;
                case TessellationEvaluationShader:
                    return GL_TESS_EVALUATION_SHADER;
                case TessellationControlShader:
                    return GL_TESS_CONTROL_SHADER;
                case GeometryShader:
                    return GL_GEOMETRY_SHADER;
                case FragmentShader:
                    return GL_FRAGMENT_SHADER;
                default:
                    Log::error("stageToGL() -> Invalid stage");
                    throw std::runtime_error("Error: Invalid Stage.");
            }
        }

        static std::string readShaderData(std::filesystem::path const& path);

        static std::string load(std::filesystem::path const& includePath);

		static int32_t getIncludeID(std::filesystem::path const& includePath);
    };

    class SubShader
    {
      public:
        uint32_t id = 0;
        std::vector<int32_t> includesIDs;

      public:
        SubShader() = default;

        void bind() const;
        void unbind() const;
        void load(int32_t const& stages, std::string const& fullShaderSource);
        void destroy() const;

    	bool hasInclude(int32_t const& includeID) const;

      private:
        // Returns a handle to the compiled shader
        static int32_t compileSubShader(int32_t const& type, const char* source);

        static void checkShaderCompileError(int32_t const& type, int32_t const& subShaderID);
        static void checkShaderLinkError(int32_t const& shaderID);

        // Processes the original source given as input
        std::string process(int32_t const& type, std::string const& source);
    };

    /**
     * @brief Shader Class to replace the old shader module + shader program
     *
     * Mainly tries to create a simple variant of that shader
     */
    class UberShader
    {
      public:
        int32_t stages = 0;
        SubShader subShader;
    	
        std::filesystem::path path;
        std::filesystem::file_time_type lastWriteTime;

      public:
        UberShader(std::filesystem::path const& shaderPath, int32_t const& stages = ShaderData::Stage::VertexShader | ShaderData::Stage::FragmentShader);

        void load(std::filesystem::path const& shaderPath, int32_t const& stages);
        void destroy() const;

    	bool hasInclude(int32_t const& id);

    	void bind();
        void unbind() const;

    	void reload();
    };
} // namespace dag