#include <GL/glew.h>
#include <filesystem>
#include <fstream>
#include <limits>
#include <sstream>
#include <string>

#include "dagmar/Exceptions.h"
#include "dagmar/Log.h"
#include "dagmar/ShaderModule.h"

/**
 * @brief Shader module constructor
 */
dag::ShaderModule::ShaderModule(std::filesystem::path const& shaderPath, int const& shaderType, std::vector<std::string> const& extraDefines) :
    type(shaderType)
{
    setCodeAndCompile(shaderPath, shaderType, extraDefines);
}

/**
 * @brief Shader module destructor
 */
void dag::ShaderModule::destroy()
{
    glDeleteShader(id);
}

/**
 * @brief Compiles the shader module
 */
void dag::ShaderModule::setCodeAndCompile(std::filesystem::path const& shaderPath, int const& shaderType, std::vector<std::string> const& extraDefines)
{
    Log::debug("Attempting to create shader from: " + shaderPath.string());
    std::string code;
    std::ifstream shaderFile;
    // ensure ifstream objects can throw exceptions:
    shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try
    {
        // open files
        shaderFile.open(shaderPath);
        Log::debug("Vertex shader file successfully opened");
        std::stringstream shaderStream;

        // Eat first line in the glsl code
        std::string tstr;
        std::getline(shaderFile, tstr);

        shaderFile.seekg(tstr.length() + 1);
        // read file's buffer contents into streams
        shaderStream << shaderFile.rdbuf();

        // close file handlers
        shaderFile.close();

        // convert stream into string
        code = shaderStream.str();
    }
    catch (std::ifstream::failure& e)
    {
        Log::error("SHADER::FILE_NOT_SUCCESFULLY_READ");
        Log::error(e.what());
        throw std::runtime_error("error");    
    }

    std::string totalCode = "";

    for (auto extraLine : extraDefines)
    {
        totalCode += extraLine + "\n";
    }

    std::string firstLine = "#version 460\n";

    totalCode = firstLine + totalCode + code;

    const char* shader_code = totalCode.c_str();

    int success;
    char infoLog[512];

    id = glCreateShader(shaderType);

    glShaderSource(id, 1, &shader_code, nullptr);
    // glShaderSourceARB(id, 1, &shader_code, nullptr);
    glCompileShader(id);
    // print compile errors if any and throw
    glGetShaderiv(id, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(id, 512, NULL, infoLog);
        Log::error("SHADER::COMPILATION_FAILED:");
        Log::error(infoLog);
        throw dag::ShaderCompilationException(shaderType);
    }
}

bool dag::ShaderModule::operator==(dag::ShaderModule const& other) const
{
    if (id == other.id && type == other.type)
    {
        return true;
    }

    return false;
}

bool dag::ShaderModule::operator!=(dag::ShaderModule const& other) const
{
    if (*this == other)
    {
        return false;
    }

    return true;
}
