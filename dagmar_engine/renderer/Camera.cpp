#include "dagmar/Camera.h"

/**
 * @brief Default constructor, with y axis pointing upwards, 
 * looking towards the negative z-axis
 */
dag::Camera::Camera()
{
    this->position = glm::vec3(0.0f, 0.0f, 0.0f);
    this->upVector = glm::vec3(0.0f, 1.0f, 0.0f);
    this->forwardVector = glm::vec3(0.0f, 0.0f, -1.0f);
    this->rightVector = glm::vec3(1.0f, 0.0f, 0.0f);

    this->fov = glm::radians(45.0f);
    this->aspect = 1.0f;
    this->zNear = 0.01f;
    this->zFar = 100.0f;
}

/**
 * @brief Setter for camera position
 */
void dag::Camera::setPosition(glm::vec3 const& pos)
{
    this->position = pos;
}

/**
 * @brief Setter for camera up vector
 */
void dag::Camera::setUpVector(glm::vec3 const& up)
{
    this->upVector = glm::normalize(up);
}

/**
 * @brief Setter for camera forward vector
 */
void dag::Camera::setForwardVector(glm::vec3 const& forward)
{
    this->forwardVector = glm::normalize(forward);
}

/**
 * @brief Setter for camera right vector
 */
void dag::Camera::setRightVector(glm::vec3 const& right)
{
    this->rightVector = glm::normalize(right);
}

/**
 * @brief Setter for view parameters: position, up-vector and forwards
 */
void dag::Camera::setView(glm::vec3 const& pos, glm::vec3 const& up, glm::vec3 const& forward)
{
    this->position = pos;
    this->upVector = glm::normalize(up);
    this->forwardVector = glm::normalize(forward);
}

/**
 * @brief Setter for camera fov (in degrees)
 */
void dag::Camera::setFOV(float const& fovInAngles)
{
    this->fov = glm::radians(fovInAngles);
}

/**
 * @brief Setter for camera aspect
 */
void dag::Camera::setAspect(float const& aspect)
{
    this->aspect = aspect;
}

/**
 * @brief Setter for camera near plane
 */
void dag::Camera::setNear(float const& zNear)
{
    this->zNear = zNear;
}

/**
 * @brief Setter for camera far plane
 */
void dag::Camera::setFar(float const& zFar)
{
    this->zFar = zFar;
}

/**
 * @brief Setter for camera projection parameters: fov, aspect, near and far
 */
void dag::Camera::setProjection(float const& fovInAngles, float const& aspect, float const& zNear, float const& zFar)
{
    this->fov = glm::radians(fovInAngles);
    this->aspect = aspect;
    this->zNear = zNear;
    this->zFar = zFar;
}

/**
 * @brief Getter for camera view matrix
 */
glm::mat4 dag::Camera::getView() const
{
    return glm::lookAt(position, position + forwardVector, upVector);
}

/**
 * @brief Getter for camera projection matrix
 */
glm::mat4 dag::Camera::getProjection() const
{
    return glm::perspective(fov, aspect, zNear, zFar);
}

/**
 * @brief Getter for camera position
 */
glm::vec3 dag::Camera::getPosition() const
{
    return this->position;
}

/**
 * @brief Pitch the camera with given angle
 *  CW rotation assumed
 */
void dag::Camera::pitch(float const& angle)
{
    glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), glm::radians(-angle), rightVector);

    upVector = glm::vec3(rotation * glm::vec4(upVector, 0.0f));
    forwardVector = glm::vec3(rotation * glm::vec4(forwardVector, 0.0f));
}

/**
 * @brief Roll the camera with given angle
 *  CW rotation assumed
 */
void dag::Camera::roll(float const& angle)
{
    glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), glm::radians(angle), forwardVector);

    upVector = glm::vec3(rotation * glm::vec4(upVector, 0.0f));
    rightVector = glm::vec3(rotation * glm::vec4(rightVector, 0.0f));
}

/**
 * @brief Yaw the camera with given angle
 *  CW rotation assumed
 */
void dag::Camera::yaw(float const& angle)
{
    glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), glm::radians(angle), upVector);

    rightVector = glm::vec3(rotation * glm::vec4(rightVector, 0.0f));
    forwardVector = glm::vec3(rotation * glm::vec4(forwardVector, 0.0f));
}

/**
 * @brief Move the camera along the forward vector with given value
 */
void dag::Camera::moveForward(float const& value)
{
    position += forwardVector * value;
}

/**
 * @brief Move the camera along the up vector with given value
 */
void dag::Camera::moveUp(float const& value)
{
    position += upVector * value;
}

/**
 * @brief Move the camera along the right vector with given value
 */
void dag::Camera::moveRight(float const& value)
{
    position += rightVector * value;
}