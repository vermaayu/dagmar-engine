#include "dagmar/Scene.h"
#include "dagmar/FileSystem.h"
#include "dagmar/Log.h"
#include "dagmar/Mesh.h"
#include "dagmar/PlayerControllerSystem.h"
#include "dagmar/RenderingSystem.h"
#include "dagmar/ResourceManager.h"
#include "dagmar/VoxelChunk.h"
#include "dagmar/VoxelChunkManager.h"
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>
#include <string>

using json = nlohmann::json;

dag::Scene::Scene()
{
    currentEntity = -1;
    currentRenderableEntity = -1;
    currentSceneCamera = -1;

    defaultEntities = {
        ICON_FA_SQUARE "\tEmpty",
        ICON_FA_LIGHTBULB "\tLight",
        ICON_FA_VIDEO "\tCamera",
        ICON_FA_USER "\tPlayer",
        ICON_FA_CUBES "\tVoxel",
        ICON_FA_CUBE "\tCube",
        ICON_FA_GLOBE "\tSphere",
        ICON_FA_STAR "\tStarry",
        ICON_FA_SHAPES "\tTorus knot",
        ICON_FA_DATABASE "\tCylinder",
        ICON_FA_SHAPES "\tPlane",
        ICON_FA_SHAPES "\tTeapot",
        ICON_FA_SHAPES "\tTetrahedra",
        ICON_FA_SHAPES "\tTorus"};
    entitiesCounter.resize(defaultEntities.size(), 0);
}

/*
 * 
void dag::Scene::checkName(std::string& name)
{
    std::string tempName = name;
    int32_t number = 0;
    while (true)
    {
        int32_t prevNumber = number;

        for (auto const& entity : entities)
        {
            auto const& otherName = coordinator->getComponent<Name>(entity);

            if (otherName.name == tempName)
            {
                tempName = name + "_" + std::to_string(number);
                number++;
                break;
            }
        }

        if (prevNumber == number)
        {
            if (number != 0)
            {
                name = tempName + "_" + std::to_string(number);
            }
            return;
        }
    }
}
 */

/**
 * @brief Tries to find a unique name
 *
 * Currently a hack, so it's open to fail cases
 */
void dag::Scene::checkName(std::string& name)
{
    int number = -2;
    size_t tabIndex = name.find("\t");

    if (tabIndex != std::string::npos)
    {
        name = name.substr(tabIndex + 1);
    }

    for (auto const& entity : entities)
    {
        auto const& otherName = coordinator->getComponent<Name>(entity);

        if (otherName.name.find(name) != std::string::npos)
        {
            size_t index = otherName.name.find("_");

            if (index != std::string::npos)
            {
                try
                {
                    number = std::max(stoi(otherName.name.substr(index + 1)), number);
                }
                catch (std::exception& e)
                {
                    number = std::max(-1, number);
                }
            }
            else
            {
                number = std::max(-1, number);
            }
        }
    }
    if (number != -2)
    {
        if (name.back() == '_')
        {
            name = name + std::to_string(number + 1);
        }
        else
        {
            name = name + "_" + std::to_string(number + 1);
        }
    }
}

void dag::Scene::spawnParticles(glm::vec3 position, glm::vec3 const& color)
{
    for (int i = 0; i < 3; ++i)
    {
        auto camEnt = currentEntity;
        createEntity("Particle");
        auto& transform = coordinator->getComponent<Transform>(entities[currentEntity]);
        float displacementX = VoxelChunk::randomInRange(0.0f, 1.0f);
        float displacementY = VoxelChunk::randomInRange(0.0f, 1.0f);
        float displacementZ = VoxelChunk::randomInRange(0.0f, 1.0f);
        transform.position = position + glm::vec3(displacementX, displacementY, displacementZ);

        transform.scale = glm::vec3(0.1f);

        transform.updateModelMatrix();

        coordinator->addComponent(entities[currentEntity], resourceManager->meshRenderers[resourceManager->getMeshRenderer("Cube")]);

        float impulseX = VoxelChunk::randomInRange(-3.0f, 3.0f);
        float impulseZ = VoxelChunk::randomInRange(-3.0f, 3.0f);
        coordinator->addComponent(entities[currentEntity], RigidBody{.movable = true, .impulse = glm::vec3(impulseX, 8.5f, impulseZ), .mass = 1.0f});

        coordinator->addComponent(entities[currentEntity], MaterialComponent{.ambient = glm::vec4(color, 1.0f)});

        particles.emplace_back(Particle{.entity = entities[currentEntity], .lifespan = 40});
        currentEntity = camEnt;
    }
}

void dag::Scene::createDefaultEntity(std::string name)
{
    MeshRenderer defaultMesh;
    auto rs = coordinator->getSystem<RenderingSystem>();

    checkName(name);

    entities.push_back(coordinator->createEntity());
    auto& entity = entities[entities.size() - 1];

    coordinator->addComponent(entity, dag::Name{name});

    coordinator->addComponent(entity, dag::Transform{});
    coordinator->getComponent<dag::Transform>(entity).updateModelMatrix();

    dag::DescriptorSet descriptorSet;

    // IDs in the vector rather than their GL ID
    descriptorSet.shaderProgramID = static_cast<size_t>(RenderingSystem::Shader::ShadowmapLightingShader);
    coordinator->addComponent(entity, descriptorSet);

    currentEntity = entities.size() - 1;

    recomputeSceneGraph();
}

void dag::Scene::createEntity(std::string name)
{
    MeshRenderer defaultMesh;
    auto rs = coordinator->getSystem<RenderingSystem>();

    checkName(name);
    auto renderingSystem = coordinator->getSystem<RenderingSystem>();

    renderingSystem->dequeueLateOverlaysEntity();

    entities.push_back(coordinator->createEntity());
    auto& entity = entities[entities.size() - 1];

    coordinator->addComponent(entity, dag::Name{name});

    if (name.find("Light") != std::string::npos)
    {
        coordinator->addComponent(entity, dag::LightComponent{});
        coordinator->getSystem<LightSystem>()->updateLookupVectors();
    }
    else if (name.find("Camera") != std::string::npos)
    {
        coordinator->addComponent(entity, dag::CameraComponent{});
    }
    else if (name.find("Player") != std::string::npos)
    {
        coordinator->addComponent(entity, dag::PlayerController{});
        coordinator->addComponent(entity, dag::RigidBody{.movable = true, .mass = 1.0f});
        coordinator->addComponent(entity, dag::CollisionAABB{.width = 1.0f, .height = 1.0f, .depth = 1.0f, .center = glm::vec3(0.0f)});
        coordinator->getSystem<dag::PlayerControllerSystem>()->setPossessed(entity, true);
    }
    else if (name.find("Voxel") != std::string::npos)
    {
        coordinator->addComponent(entity, resourceManager->meshRenderers[resourceManager->getMeshRenderer("Voxel")]);
        coordinator->addComponent(entity, dag::MaterialComponent{.ambient = glm::vec4(0.5f), .specular = glm::vec4(0.0f)});
        coordinator->addComponent(entity, dag::VoxelComponent{});
        voxelManager->currVoxelMainEntity = entity;
        // coordinator->getComponent<VoxelComponent>(entity).focussedEntity = entity;
    }
    else if (name.find("Cube") != std::string::npos)
    {
        coordinator->addComponent(entity, resourceManager->meshRenderers[resourceManager->getMeshRenderer("Cube")]);
        coordinator->addComponent(entity, dag::MaterialComponent{});
    }
    else if (name.find("Sphere") != std::string::npos)
    {
        coordinator->addComponent(entity, resourceManager->meshRenderers[resourceManager->getMeshRenderer("Sphere")]);
        coordinator->addComponent(entity, dag::MaterialComponent{});
    }
    else if (name.find("Starry") != std::string::npos)
    {
        coordinator->addComponent(entity, resourceManager->meshRenderers[resourceManager->getMeshRenderer("Starry")]);
        coordinator->addComponent(entity, dag::MaterialComponent{});
    }
    else if (name.find("Torus knot") != std::string::npos)
    {
        coordinator->addComponent(entity, resourceManager->meshRenderers[resourceManager->getMeshRenderer("Torus Knot")]);
        coordinator->addComponent(entity, dag::MaterialComponent{});
    }
    else if (name.find("Cylinder") != std::string::npos)
    {
        coordinator->addComponent(entity, resourceManager->meshRenderers[resourceManager->getMeshRenderer("Cylinder")]);
        coordinator->addComponent(entity, dag::MaterialComponent{});
    }
    else if (name.find("Plane") != std::string::npos)
    {
        coordinator->addComponent(entity, resourceManager->meshRenderers[resourceManager->getMeshRenderer("Plane")]);
        coordinator->addComponent(entity, dag::MaterialComponent{});
    }
    else if (name.find("Teapot") != std::string::npos)
    {
        coordinator->addComponent(entity, resourceManager->meshRenderers[resourceManager->getMeshRenderer("Teapot")]);
        coordinator->addComponent(entity, dag::MaterialComponent{});
    }
    else if (name.find("Tetrahedra") != std::string::npos)
    {
        coordinator->addComponent(entity, resourceManager->meshRenderers[resourceManager->getMeshRenderer("Tetrahedra")]);
        coordinator->addComponent(entity, dag::MaterialComponent{});
    }
    else if (name.find("Torus") != std::string::npos)
    {
        coordinator->addComponent(entity, resourceManager->meshRenderers[resourceManager->getMeshRenderer("Torus")]);
        coordinator->addComponent(entity, dag::MaterialComponent{});
    }

    coordinator->addComponent(entity, dag::Transform{});
    coordinator->getComponent<dag::Transform>(entity).updateModelMatrix();

    dag::DescriptorSet descriptorSet;

    // IDs in the vector rather than their GL ID
    descriptorSet.shaderProgramID = static_cast<size_t>(RenderingSystem::Shader::ShadowmapLightingShader);
    coordinator->addComponent(entity, descriptorSet);

    currentEntity = entities.size() - 1;
    currentRenderableEntity = renderingSystem->getEntityOffset(entities[currentEntity]);

    renderingSystem->enqueueLateOverlaysEntity();
    renderingSystem->categorizeGeometry();

    recomputeSceneGraph();
}

void dag::Scene::destroyEntity(dag::Entity const& entity)
{
    auto renderingSystem = coordinator->getSystem<RenderingSystem>();
    renderingSystem->dequeueLateOverlaysEntity();

    coordinator->destroyEntity(entity);

    for (size_t i = 0; i < entities.size() - 1; ++i)
    {
        if (entity == entities[i])
        {
            for (size_t j = i; j < entities.size() - 1; ++j)
            {
                entities[j] = entities[j + 1];
            }
            break;
        }
    }
    entities.erase(entities.end() - 1);

    currentEntity = -1;
    currentRenderableEntity = -1;

    renderingSystem->categorizeGeometry();
    recomputeSceneGraph();
}

void dag::Scene::initialize()
{
    // TODO Remove
}

void dag::Scene::startup()
{
}

void dag::Scene::shutdown()
{
}

std::string dag::Scene::getName() const
{
    return "Scene";
}

void dag::Scene::recomputeSceneGraph()
{
    sceneGraph.resize(entities.size());
    int i = 0;
    for (auto const& entity : entities)
    {
        auto nameComponent = coordinator->getComponent<dag::Name>(entity);
        sceneGraph[i] = nameComponent.name;
        i++;
    }
}

void dag::Scene::serialize(std::filesystem::path const& output_path)
{
    std::filesystem::path output_path_non_c = output_path;

    json big_j;

    serialize(big_j);

    std::cout << "Writing json to: " << output_path_non_c.replace_extension(".json").generic_string() << std::endl;

    std::ofstream output_file_stream(output_path_non_c.replace_extension(".json").generic_string());

    output_file_stream << std::setw(4) << big_j.dump(4);

    output_file_stream << std::endl;

    output_file_stream.close();
}

void dag::Scene::serialize(json& big_j)
{
    json j;

    auto entityArray = json::array();

    for (dag::Entity const& e : entities)
    {
        j.clear();
        j["name"] = coordinator->getComponent<Name>(e).name;
        j["index"] = scene->getEntityIndex(e);

        if (coordinator->hasComponent<Transform>(e))
        {
            auto& transform = coordinator->getComponent<Transform>(e);
            j["transform"]["position"]["x"] = transform.position.x;
            j["transform"]["position"]["y"] = transform.position.y;
            j["transform"]["position"]["z"] = transform.position.z;
            j["transform"]["rotation"]["x"] = transform.rotation.x;
            j["transform"]["rotation"]["y"] = transform.rotation.y;
            j["transform"]["rotation"]["z"] = transform.rotation.z;
            j["transform"]["scale"]["x"] = transform.scale.x;
            j["transform"]["scale"]["y"] = transform.scale.y;
            j["transform"]["scale"]["z"] = transform.scale.z;
        }

        if (coordinator->hasComponent<MaterialComponent>(e))
        {
            auto& material = coordinator->getComponent<MaterialComponent>(e);
            j["material"]["ambient"]["r"] = material.ambient.r;
            j["material"]["ambient"]["g"] = material.ambient.g;
            j["material"]["ambient"]["b"] = material.ambient.b;
            j["material"]["diffuse"]["r"] = material.diffuse.r;
            j["material"]["diffuse"]["g"] = material.diffuse.g;
            j["material"]["diffuse"]["b"] = material.diffuse.b;
            j["material"]["specular"]["r"] = material.specular.r;
            j["material"]["specular"]["g"] = material.specular.g;
            j["material"]["specular"]["b"] = material.specular.b;
            j["material"]["emissive"]["r"] = material.emissive.r;
            j["material"]["emissive"]["g"] = material.emissive.g;
            j["material"]["emissive"]["b"] = material.emissive.b;
            j["material"]["specularexponent"] = material.specularExponent;
        }

        if (coordinator->hasComponent<RigidBody>(e))
        {
            auto const& rigidBody = coordinator->getComponent<RigidBody>(e);

            j["rigidbody"]["mass"] = rigidBody.mass;
            j["rigidbody"]["velocity"]["x"] = rigidBody.velocity.x;
            j["rigidbody"]["velocity"]["y"] = rigidBody.velocity.y;
            j["rigidbody"]["velocity"]["z"] = rigidBody.velocity.z;
            j["rigidbody"]["acceleration"]["x"] = rigidBody.acceleration.x;
            j["rigidbody"]["acceleration"]["y"] = rigidBody.acceleration.y;
            j["rigidbody"]["acceleration"]["z"] = rigidBody.acceleration.z;
            j["rigidbody"]["netForce"]["x"] = rigidBody.netForce.x;
            j["rigidbody"]["netForce"]["y"] = rigidBody.netForce.y;
            j["rigidbody"]["netForce"]["z"] = rigidBody.netForce.z;
            j["rigidbody"]["externalForce"]["x"] = rigidBody.externalForce.x;
            j["rigidbody"]["externalForce"]["y"] = rigidBody.externalForce.y;
            j["rigidbody"]["externalForce"]["z"] = rigidBody.externalForce.z;
            j["rigidbody"]["impulse"]["x"] = rigidBody.impulse.x;
            j["rigidbody"]["impulse"]["y"] = rigidBody.impulse.y;
            j["rigidbody"]["impulse"]["z"] = rigidBody.impulse.z;
            j["rigidbody"]["movable"] = rigidBody.movable;
            j["rigidbody"]["plasticcoefficient"] = rigidBody.plasticCoefficient;
        }

        if (coordinator->hasComponent<CollisionSphere>(e))
        {
            auto const& collisionSphere = coordinator->getComponent<CollisionSphere>(e);

            j["collisionsphere"]["radius"] = collisionSphere.radius;
            j["collisionsphere"]["center"]["x"] = collisionSphere.center.x;
            j["collisionsphere"]["center"]["y"] = collisionSphere.center.y;
            j["collisionsphere"]["center"]["z"] = collisionSphere.center.z;
        }

        if (coordinator->hasComponent<CollisionAABB>(e))
        {
            auto const& collisionAABB = coordinator->getComponent<CollisionAABB>(e);

            j["collisionaabb"]["width"] = collisionAABB.width;
            j["collisionaabb"]["height"] = collisionAABB.height;
            j["collisionaabb"]["depth"] = collisionAABB.depth;
            j["collisionaabb"]["center"]["x"] = collisionAABB.center.x;
            j["collisionaabb"]["center"]["y"] = collisionAABB.center.y;
            j["collisionaabb"]["center"]["z"] = collisionAABB.center.z;
        }

        if (coordinator->hasComponent<MeshRenderer>(e))
        {
            auto const& meshRenderer = coordinator->getComponent<MeshRenderer>(e);

            j["meshrenderer"]["name"] = meshRenderer.name;
            j["meshrenderer"]["vaoid"] = meshRenderer.vaoID;
            j["meshrenderer"]["hashIndex"] = meshRenderer.hashIndex;
            j["meshrenderer"]["textureHash"] = meshRenderer.textureHash;
        }

        if (coordinator->hasComponent<LightComponent>(e))
        {
            auto const& lightComponent = coordinator->getComponent<LightComponent>(e);

            j["lightcomponent"]["color"]["r"] = lightComponent.color.r;
            j["lightcomponent"]["color"]["g"] = lightComponent.color.g;
            j["lightcomponent"]["color"]["b"] = lightComponent.color.b;
            j["lightcomponent"]["type"] = static_cast<unsigned int>(lightComponent.type);
            j["lightcomponent"]["projectionType"] = static_cast<unsigned int>(lightComponent.projectionType);
        }

        if (coordinator->hasComponent<CameraComponent>(e))
        {
            auto const& cameraComponent = coordinator->getComponent<CameraComponent>(e);

            j["cameracomponent"]["isPossessed"] = cameraComponent.isPossessed;
            j["cameracomponent"]["isScreenDependent"] = cameraComponent.isScreenDependent;
            j["cameracomponent"]["fov"] = cameraComponent.fov;
            j["cameracomponent"]["aspect"] = cameraComponent.aspect;
            j["cameracomponent"]["zNear"] = cameraComponent.zNear;
            j["cameracomponent"]["zFar"] = cameraComponent.zFar;
            j["cameracomponent"]["cameraSpeed"] = cameraComponent.cameraSpeed;
        }

        if (coordinator->hasComponent<PlayerController>(e))
        {
            auto const& playerController = coordinator->getComponent<PlayerController>(e);

            j["playercontroller"]["isActive"] = playerController.isActive;
            j["playercontroller"]["height"] = playerController.height;
            j["playercontroller"]["walkSpeed"] = playerController.walkSpeed;
            j["playercontroller"]["runMultiplier"] = playerController.runMultiplier;
            j["playercontroller"]["velocity"]["x"] = playerController.velocity.x;
            j["playercontroller"]["velocity"]["y"] = playerController.velocity.y;
            j["playercontroller"]["velocity"]["z"] = playerController.velocity.z;
        }

        if (coordinator->hasComponent<VoxelComponent>(e))
        {
            auto const& voxelComponent = coordinator->getComponent<VoxelComponent>(e);

            std::cout << "FOCUSSED entity: " << voxelComponent.focussedEntity << std::endl;

            j["voxelComponent"]["startPosition"]["x"] = voxelComponent.startPosition.x;
            j["voxelComponent"]["startPosition"]["y"] = voxelComponent.startPosition.y;
            j["voxelComponent"]["startPosition"]["z"] = voxelComponent.startPosition.z;

            j["voxelComponent"]["maxSize"] = voxelComponent.maxSize;
            j["voxelComponent"]["loadSize"] = voxelComponent.loadSize;
            j["voxelComponent"]["chunkSize"] = voxelComponent.chunkSize;

            j["voxelComponent"]["playerPosition"]["x"] = voxelComponent.playerPosition.x;
            j["voxelComponent"]["playerPosition"]["y"] = voxelComponent.playerPosition.y;
            j["voxelComponent"]["playerPosition"]["z"] = voxelComponent.playerPosition.z;

            j["voxelComponent"]["scale2D"] = voxelComponent.scale2D;
            j["voxelComponent"]["scale3D"] = voxelComponent.scale3D;
            j["voxelComponent"]["iterations2D"] = voxelComponent.iterations2D;
            j["voxelComponent"]["iterations3D"] = voxelComponent.iterations3D;
            j["voxelComponent"]["persistence2D"] = voxelComponent.persistence2D;
            j["voxelComponent"]["persistence3D"] = voxelComponent.persistence3D;
            j["voxelComponent"]["hasWater"] = voxelComponent.hasWater;
            j["voxelComponent"]["waterHeight"] = voxelComponent.waterHeight;
            j["voxelComponent"]["hasTrees"] = voxelComponent.hasTrees;
            j["voxelComponent"]["treeProbability"] = voxelComponent.treeProbability;
            j["voxelComponent"]["minTreeHeight"] = voxelComponent.minTreeHeight;
            j["voxelComponent"]["maxTreeHeight"] = voxelComponent.maxTreeHeight;
            j["voxelComponent"]["minTreeWidth"] = voxelComponent.minTreeWidth;
            j["voxelComponent"]["maxTreeWidth"] = voxelComponent.maxTreeWidth;
            j["voxelComponent"]["hotReload"] = voxelComponent.hotReload;
            j["voxelComponent"]["terrainType"] = voxelComponent.terrainType;
            j["voxelComponent"]["terrainTypeName"] = voxelComponent.terrainTypeName;
            j["voxelComponent"]["focussedEntity"] = voxelComponent.focussedEntity;
            if (voxelComponent.focussedEntity != UINT32_MAX)
            {
                j["voxelComponent"]["focussedEntityName"] = coordinator->getComponent<Name>(voxelComponent.focussedEntity).name;
            }
            else
            {
                j["voxelComponent"]["focussedEntityName"] = "None";
            }
            j["voxelComponent"]["hasFlowers"] = voxelComponent.hasFlowers;
            j["voxelComponent"]["flowerProbability"] = voxelComponent.flowerProbability;
        }

        if (coordinator->hasComponent<Volume>(e))
        {
            auto const& volume = coordinator->getComponent<Volume>(e);

            j["volume"]["bloomData"]["threshold"] = volume.bloomData.threshold;
            j["volume"]["bloomData"]["offset"] = volume.bloomData.offset;
            j["volume"]["fogData"]["fogNear"] = volume.fogData.fogNear;
            j["volume"]["fogData"]["fogFar"] = volume.fogData.fogFar;
            j["volume"]["fogData"]["density"] = volume.fogData.density;
            j["volume"]["fogData"]["fogColor"]["r"] = volume.fogData.fogColor.r;
            j["volume"]["fogData"]["fogColor"]["g"] = volume.fogData.fogColor.g;
            j["volume"]["fogData"]["fogColor"]["b"] = volume.fogData.fogColor.b;
            j["volume"]["fogData"]["fogColor"]["a"] = volume.fogData.fogColor.a;
            j["volume"]["blurData"]["kernelSize"] = volume.blurData.kernelSize;
            j["volume"]["chromaticAberrationData"]["rOffset"] = volume.chromaticAberrationData.rOffset;
            j["volume"]["chromaticAberrationData"]["gOffset"] = volume.chromaticAberrationData.gOffset;
            j["volume"]["chromaticAberrationData"]["bOffset"] = volume.chromaticAberrationData.bOffset;
            j["volume"]["dilationData"]["kernelSize"] = volume.dilationData.kernelSize;
            j["volume"]["dilationData"]["separation"] = volume.dilationData.separation;
            j["volume"]["dilationData"]["minThreshold"] = volume.dilationData.minThreshold;
            j["volume"]["dilationData"]["maxThreshold"] = volume.dilationData.maxThreshold;
            j["volume"]["pixelizeData"]["pixelSize"] = volume.pixelizeData.pixelSize;
            j["volume"]["sharpenData"]["amount"] = volume.sharpenData.amount;
        }

        entityArray.push_back(j);
    }

    big_j["entities"] = entityArray;

    auto renderingSettings = json::array();
    {
        auto renderingSystem = coordinator->getSystem<RenderingSystem>();
        json settingsJSON;
        settingsJSON["mode"] = static_cast<size_t>(renderingSystem->currentRenderMode);
        renderingSettings.emplace_back(settingsJSON);
    }

    big_j["renderingSettings"] = renderingSettings;
}

void dag::Scene::deserialize(std::filesystem::path const& input_path)
{
    Log::warning("path deserializer");

    std::ifstream i(input_path);
    json j;
    i >> j;

    deserialize(j, input_path, true);
}

void dag::Scene::deserialize(json const& input_json, std::filesystem::path const& input_path, bool load_mesh)
{
    focussedEntity = "";
    for (auto entity : input_json["entities"])
    {
        dag::Entity curr_entity;
        if (load_mesh)
        {
            std::cout << "Loading: " << entity["name"] << std::endl;
            createDefaultEntity(entity["name"]);
            // scene->removeComponent<MeshRenderer>(entities.back());
            curr_entity = entities.back();
        }
        else
        {
            curr_entity = entities[entity["index"]];
        }
        // even easier with structured bindings (C++17)
        for (auto& [key, value] : entity.items())
        {
            std::cout << key << " : " << value << "\n";

            if (key == "collisionsphere")
            {
                if (coordinator->hasComponent<CollisionSphere>(curr_entity) && !load_mesh)
                {
                    dag::CollisionSphere& comp = coordinator->getComponent<CollisionSphere>(curr_entity);
                    comp.center = glm::vec3(value["center"]["x"], value["center"]["y"], value["center"]["z"]);
                    comp.radius = value["radius"];
                    continue;
                }
                coordinator->addComponent(curr_entity, dag::CollisionSphere{.radius = value["radius"], .center = glm::vec3(value["center"]["x"], value["center"]["y"], value["center"]["z"])});
            }
            else if (key == "collisionaabb")
            {
                if (coordinator->hasComponent<CollisionAABB>(curr_entity) && !load_mesh)
                {
                    dag::CollisionAABB& comp = coordinator->getComponent<CollisionAABB>(curr_entity);
                    comp.center = glm::vec3(value["center"]["x"], value["center"]["y"], value["center"]["z"]);
                    comp.depth = value["depth"];
                    comp.width = value["width"];
                    comp.height = value["height"];
                    continue;
                }
                coordinator->addComponent(curr_entity, dag::CollisionAABB{.width = value["width"], .height = value["height"], .depth = value["depth"], .center = glm::vec3(value["center"]["x"], value["center"]["y"], value["center"]["z"])});
            }
            else if (key == "rigidbody")
            {
                if (coordinator->hasComponent<RigidBody>(curr_entity) && !load_mesh)
                {
                    dag::RigidBody& comp = coordinator->getComponent<RigidBody>(curr_entity);
                    comp.movable = value["movable"];
                    comp.impulse = glm::vec3(value["impulse"]["x"],
                                             value["impulse"]["y"],
                                             value["impulse"]["z"]);
                    comp.plasticCoefficient = value["plasticcoefficient"];
                    comp.mass = value["mass"];
                    continue;
                }
                coordinator->addComponent(curr_entity,
                                          dag::RigidBody{.movable = value["movable"],
                                                         .impulse = glm::vec3(value["impulse"]["x"],
                                                                              value["impulse"]["y"],
                                                                              value["impulse"]["z"]),
                                                         .plasticCoefficient = value["plasticcoefficient"],
                                                         .mass = value["mass"]});
            }
            else if (key == "material")
            {
                if (coordinator->hasComponent<MaterialComponent>(curr_entity))
                {
                    dag::MaterialComponent& comp = coordinator->getComponent<MaterialComponent>(curr_entity);
                    comp.ambient = glm::vec4(value["ambient"]["r"], value["ambient"]["g"], value["ambient"]["b"], 1.0f);
                    comp.diffuse = glm::vec4(value["diffuse"]["r"], value["diffuse"]["g"], value["diffuse"]["b"], 1.0f);
                    comp.specular = glm::vec4(value["specular"]["r"], value["specular"]["g"], value["specular"]["b"], 1.0f);
                    comp.emissive = glm::vec4(value["emissive"]["r"], value["emissive"]["g"], value["emissive"]["b"], 1.0f);
                    //comp.color = glm::vec4(value["color"]["r"], value["color"]["g"], value["color"]["b"], 1.0f);
                    comp.specularExponent = value["specularexponent"];
                    continue;
                }
                coordinator->addComponent(curr_entity, dag::MaterialComponent{});
                dag::MaterialComponent& comp = coordinator->getComponent<MaterialComponent>(curr_entity);
                comp.ambient = glm::vec4(value["ambient"]["r"], value["ambient"]["g"], value["ambient"]["b"], 1.0f);
                comp.diffuse = glm::vec4(value["diffuse"]["r"], value["diffuse"]["g"], value["diffuse"]["b"], 1.0f);
                comp.specular = glm::vec4(value["specular"]["r"], value["specular"]["g"], value["specular"]["b"], 1.0f);
                comp.emissive = glm::vec4(value["emissive"]["r"], value["emissive"]["g"], value["emissive"]["b"], 1.0f);
                //comp.color = glm::vec4(value["color"]["r"], value["color"]["g"], value["color"]["b"], 1.0f);
                comp.specularExponent = value["specularexponent"];
            }
            else if (key == "transform")
            {
                if (coordinator->hasComponent<Transform>(curr_entity) && !load_mesh)
                {
                    dag::Transform& comp = coordinator->getComponent<Transform>(curr_entity);
                    comp.position = glm::vec3(value["position"]["x"], value["position"]["y"], value["position"]["z"]);
                    comp.rotation = glm::vec3(value["rotation"]["x"], value["rotation"]["y"], value["rotation"]["z"]);
                    comp.scale = glm::vec3(value["scale"]["x"], value["scale"]["y"], value["scale"]["z"]);
                    comp.updateModelMatrix();
                    continue;
                }
                dag::Transform& t = coordinator->getComponent<dag::Transform>(curr_entity);
                t.position = glm::vec3(value["position"]["x"], value["position"]["y"], value["position"]["z"]);
                t.rotation = glm::vec3(value["rotation"]["x"], value["rotation"]["y"], value["rotation"]["z"]);
                t.scale = glm::vec3(value["scale"]["x"], value["scale"]["y"], value["scale"]["z"]);
                t.updateModelMatrix();
            }
            else if (key == "meshrenderer")
            {
                if (load_mesh)
                {
                    auto rs = dag::coordinator->getSystem<dag::RenderingSystem>();
                    // if(coordinator->getComponent<Name>(curr_entity).name.find("Voxel") != std::string::npos){
                    //     coordinator->addComponent(curr_entity, resourceManager->meshRenderers[resourceManager->getMeshRenderer("Voxel")]);
                    //     continue;
                    // }
                    auto fileLocation = input_path.parent_path() / (std::to_string(value["hashIndex"].get<size_t>()) + ".dat");

                    std::cout << "|_ Loading: " << fileLocation.string() << std::endl;

                    if (!resourceManager->meshRenderers.contains(value["hashIndex"].get<size_t>()))
                    {
                        auto meshHash = dag::resourceManager->readBin(value["hashIndex"].get<size_t>(), fileLocation);

                        std::cout << "|_ New modelId: " << meshHash << std::endl;

                        for (auto& shape : dag::resourceManager->models[meshHash].shapes)
                        {
                            shape.material = dag::MaterialDataInfo();
                            rs->vaos.emplace_back(dag::resourceManager->generateVAO(shape, resourceManager->atts));
                            shape.VAOIndex = rs->vaos.size() - 1;
                            resourceManager->insertTexture(shape.material.textureDiffuseHash, (input_path.parent_path() / std::to_string(shape.material.textureDiffuseHash)).replace_extension(".dat"));
                        }

                        resourceManager->meshRenderers.insert({meshHash, dag::MeshRenderer{.name = value["name"], .vaoID = rs->vaos.size() - 1, .hashIndex = meshHash, .textureHash = value["textureHash"]}});
                    }

                    coordinator->addComponent<MeshRenderer>(curr_entity, resourceManager->meshRenderers[value["hashIndex"].get<size_t>()]);

                    // auto& mr = coordinator->getComponent<dag::MeshRenderer>(curr_entity);

                    // mr = rs->meshRenderers.back();
                    continue;
                }
            }
            else if (key == "lightcomponent")
            {
                if (coordinator->hasComponent<LightComponent>(curr_entity) && !load_mesh)
                {
                    dag::LightComponent& comp = coordinator->getComponent<LightComponent>(curr_entity);
                    comp.color = glm::vec3(value["color"]["r"], value["color"]["g"], value["color"]["b"]);
                    comp.type = static_cast<dag::LightComponent::Type>(value["type"]);
                    comp.projectionType = static_cast<dag::LightComponent::ProjectionType>(value["projectionType"]);
                    coordinator->getSystem<LightSystem>()->updateLookupVectors();
                    continue;
                }
                coordinator->addComponent(curr_entity,
                                          dag::LightComponent{.color = glm::vec3(value["color"]["r"], value["color"]["g"], value["color"]["b"]),
                                                              .type = static_cast<dag::LightComponent::Type>(value["type"]),
                                                              .projectionType = static_cast<dag::LightComponent::ProjectionType>(value["projectionType"])});

                auto const& lightSystem = coordinator->getSystem<LightSystem>();
                auto const& curr_pos = coordinator->getComponent<Transform>(curr_entity);
                lightSystem->descriptors.emplace_back(Descriptor(RenderingSystem::UniformFuncs::Uniform3fv, "lightPosition", curr_pos.position));
                coordinator->getSystem<LightSystem>()->updateLookupVectors();
            }
            else if (key == "cameracomponent")
            {
                if (coordinator->hasComponent<CameraComponent>(curr_entity) && !load_mesh)
                {
                    dag::CameraComponent& comp = coordinator->getComponent<CameraComponent>(curr_entity);
                    comp.isPossessed = value["isPossessed"];
                    comp.isScreenDependent = value["isScreenDependent"];
                    comp.fov = value["fov"];
                    comp.aspect = value["aspect"];
                    comp.zNear = value["zNear"];
                    comp.zFar = value["zFar"];
                    comp.cameraSpeed = value["cameraSpeed"];

                    if (static_cast<bool>(value["isPossessed"]))
                    {
                        coordinator->getSystem<CameraSystem>()->setPossessed(curr_entity, true);
                        std::cout << "isPossessed" << std::endl;
                    }

                    continue;
                }
                coordinator->addComponent(curr_entity,
                                          dag::CameraComponent{
                                              .isPossessed = value["isPossessed"],
                                              .isScreenDependent = value["isScreenDependent"],
                                              .fov = value["fov"],
                                              .aspect = value["aspect"],
                                              .zNear = value["zNear"],
                                              .zFar = value["zFar"],
                                              .cameraSpeed = value["cameraSpeed"]});

                if (static_cast<bool>(value["isPossessed"]))
                {
                    coordinator->getSystem<CameraSystem>()->setPossessed(curr_entity, true);
                    std::cout << "isPossessed" << std::endl;
                }
            }
            else if (key == "playercontroller")
            {
                if (coordinator->hasComponent<PlayerController>(curr_entity) && !load_mesh)
                {
                    dag::PlayerController& comp = coordinator->getComponent<PlayerController>(curr_entity);
                    comp.isActive = value["isActive"];
                    comp.height = value["height"];
                    comp.walkSpeed = value["walkSpeed"];
                    comp.runMultiplier = value["runMultiplier"];
                    comp.velocity = glm::vec3(value["velocity"]["x"], value["velocity"]["y"], value["velocity"]["z"]);
                    continue;
                }

                coordinator->addComponent(curr_entity,
                                          dag::PlayerController{
                                              .isActive = value["isActive"],
                                              .height = value["height"],
                                              .walkSpeed = value["walkSpeed"],
                                              .runMultiplier = value["runMultiplier"],
                                              .velocity = glm::vec3(value["velocity"]["x"], value["velocity"]["y"], value["velocity"]["z"])});

                if (static_cast<bool>(value["isActive"]))
                {
                    coordinator->getSystem<PlayerControllerSystem>()->setPossessed(curr_entity, true);
                }
            }
            else if (key == "voxelComponent")
            {

                auto renderingSystem = coordinator->getSystem<RenderingSystem>();
                if (coordinator->hasComponent<VoxelComponent>(curr_entity) && !load_mesh)
                {
                    dag::VoxelComponent& comp = coordinator->getComponent<VoxelComponent>(curr_entity);

                    voxelManager->startingPosition = comp.startPosition = glm::vec3(value["startPosition"]["x"], value["startPosition"]["y"], value["startPosition"]["z"]);
                    voxelManager->maxSize = comp.maxSize = value["maxSize"];
                    voxelManager->loadedSize = comp.loadSize = value["loadSize"];
                    voxelManager->chunkSize = comp.chunkSize = value["chunkSize"];

                    comp.playerPosition = glm::vec3(value["playerPosition"]["x"], value["playerPosition"]["y"], value["playerPosition"]["z"]);
                    voxelManager->scale2D = comp.scale2D = value["scale2D"];
                    voxelManager->scale3D = comp.scale3D = value["scale3D"];
                    voxelManager->iterations2D = comp.iterations2D = value["iterations2D"];
                    voxelManager->iterations3D = comp.iterations3D = value["iterations3D"];
                    voxelManager->persistence2D = comp.persistence2D = value["persistence2D"];
                    voxelManager->persistence3D = comp.persistence3D = value["persistence3D"];
                    voxelManager->hasWater = comp.hasWater = value["hasWater"];
                    voxelManager->waterHeight = comp.waterHeight = value["waterHeight"];
                    voxelManager->hasTrees = comp.hasTrees = value["hasTrees"];
                    voxelManager->treeProbability = comp.treeProbability = value["treeProbability"];
                    voxelManager->minTreeHeight = comp.minTreeHeight = value["minTreeHeight"];
                    voxelManager->maxTreeHeight = comp.maxTreeHeight = value["maxTreeHeight"];
                    voxelManager->minTreeWidth = comp.minTreeWidth = value["minTreeWidth"];
                    voxelManager->maxTreeWidth = comp.maxTreeWidth = value["maxTreeWidth"];
                    voxelManager->terrainType = comp.terrainType = value["terrainType"];
                    voxelManager->currVoxelEntity = comp.focussedEntity = value["focussedEntity"];
                    focussedEntity = value["focussedEntityName"];
                    voxelManager->hasFlowers = comp.hasFlowers = value["hasFlowers"];
                    voxelManager->flowerProbability = comp.flowerProbability = value["flowerProbability"];
                    voxelManager->currVoxelMainEntity = curr_entity;

                    if (voxelManager->currVoxelEntity == UINT32_MAX)
                    {
                        voxelManager->update(comp.playerPosition);
                        resourceManager->models[voxelManager->meshHash] = voxelManager->model;
                    }

                    continue;
                }

                coordinator->addComponent(curr_entity, dag::VoxelComponent{.startPosition = glm::vec3(value["startPosition"]["x"], value["startPosition"]["y"], value["startPosition"]["z"]), .maxSize = value["maxSize"], .loadSize = value["loadSize"], .chunkSize = value["chunkSize"], .playerPosition = glm::vec3(value["playerPosition"]["x"], value["playerPosition"]["y"], value["playerPosition"]["z"]), .scale2D = value["scale2D"], .scale3D = value["scale3D"], .iterations3D = value["iterations3D"], .iterations2D = value["iterations2D"], .persistence2D = value["persistence2D"], .persistence3D = value["persistence3D"], .hasWater = value["hasWater"], .waterHeight = value["waterHeight"], .hasTrees = value["hasTrees"], .treeProbability = value["treeProbability"], .minTreeHeight = value["minTreeHeight"], .maxTreeHeight = value["maxTreeHeight"], .minTreeWidth = value["minTreeWidth"], .maxTreeWidth = value["maxTreeWidth"], .hotReload = value["hotReload"], .terrainType = value["terrainType"], .terrainTypeName = value["terrainTypeName"], .focussedEntity = value["focussedEntity"], .hasFlowers = value["hasFlowers"], .flowerProbability = value["flowerProbability"]});
                auto& voxelComponent = coordinator->getComponent<VoxelComponent>(curr_entity);

                voxelManager->startingPosition = voxelComponent.startPosition;
                voxelManager->maxSize = voxelComponent.maxSize;
                voxelManager->loadedSize = voxelComponent.loadSize;
                voxelManager->chunkSize = voxelComponent.chunkSize;
                voxelManager->terrainType = voxelComponent.terrainType;
                voxelManager->scale2D = voxelComponent.scale2D;
                voxelManager->scale3D = voxelComponent.scale3D;
                voxelManager->iterations2D = voxelComponent.iterations2D;
                voxelManager->iterations3D = voxelComponent.iterations3D;
                voxelManager->persistence2D = voxelComponent.persistence2D;
                voxelManager->persistence3D = voxelComponent.persistence3D;
                voxelManager->hasWater = voxelComponent.hasWater;
                voxelManager->waterHeight = voxelComponent.waterHeight;
                voxelManager->hasTrees = voxelComponent.hasTrees;
                voxelManager->treeProbability = voxelComponent.treeProbability;
                voxelManager->minTreeHeight = voxelComponent.minTreeHeight;
                voxelManager->maxTreeHeight = voxelComponent.maxTreeHeight;
                voxelManager->minTreeWidth = voxelComponent.minTreeWidth;
                voxelManager->maxTreeWidth = voxelComponent.maxTreeWidth;
                voxelManager->currVoxelEntity = voxelComponent.focussedEntity;
                focussedEntity = value["focussedEntityName"];
                voxelManager->hasFlowers = voxelComponent.hasFlowers;
                voxelManager->flowerProbability = voxelComponent.flowerProbability;

                voxelManager->currVoxelMainEntity = curr_entity;

                //TODO think about how to get the focussed entity
                voxelManager->reset();

                if (voxelManager->currVoxelEntity == UINT32_MAX)
                {
                    voxelManager->update(voxelComponent.playerPosition);
                }

                resourceManager->models[voxelManager->meshHash] = voxelManager->model;
            }
            else if (key == "volume")
            {
                Volume volume;

                volume.bloomData.threshold = value["bloomData"]["threshold"];
                volume.bloomData.offset = value["bloomData"]["offset"];
                volume.fogData.fogNear = value["fogData"]["fogNear"];
                volume.fogData.fogFar = value["fogData"]["fogFar"];
                volume.fogData.density = value["fogData"]["density"];
                volume.fogData.fogColor.r = value["fogData"]["fogColor"]["r"];
                volume.fogData.fogColor.g = value["fogData"]["fogColor"]["g"];
                volume.fogData.fogColor.b = value["fogData"]["fogColor"]["b"];
                volume.fogData.fogColor.a = value["fogData"]["fogColor"]["a"];
                volume.blurData.kernelSize = value["blurData"]["kernelSize"];
                volume.chromaticAberrationData.rOffset = value["chromaticAberrationData"]["rOffset"];
                volume.chromaticAberrationData.gOffset = value["chromaticAberrationData"]["gOffset"];
                volume.chromaticAberrationData.bOffset = value["chromaticAberrationData"]["bOffset"];
                volume.dilationData.kernelSize = value["dilationData"]["kernelSize"];
                volume.dilationData.separation = value["dilationData"]["separation"];
                volume.dilationData.minThreshold = value["dilationData"]["minThreshold"];
                volume.dilationData.maxThreshold = value["dilationData"]["maxThreshold"];
                volume.pixelizeData.pixelSize = value["pixelizeData"]["pixelSize"];
                volume.sharpenData.amount = value["sharpenData"]["amount"];

                if (coordinator->hasComponent<Volume>(curr_entity) && !load_mesh)
                {
                    auto& volumeComponent = coordinator->getComponent<Volume>(curr_entity);
                    volumeComponent = volume;
                    continue;
                }

                coordinator->addComponent(curr_entity, volume);

                auto renderingSystem = coordinator->getSystem<RenderingSystem>();
                renderingSystem->updatePostProcessData(volume);
            }
        }
    }

    if (voxelManager->currVoxelEntity != UINT32_MAX && load_mesh)
    {
        std::cout << "Update by player " << focussedEntity << std::endl;

        for (size_t i = 0; i < entities.size(); ++i)
        {
            if (coordinator->getComponent<Name>(entities[i]).name == focussedEntity)
            {
                voxelManager->currVoxelEntity = entities[i];
                coordinator->getComponent<VoxelComponent>(voxelManager->currVoxelMainEntity).focussedEntity = entities[i];
                std::cout << entities[i] << std::endl;
                break;
            }
        }

        auto& transform = coordinator->getComponent<Transform>(voxelManager->currVoxelEntity);
        voxelManager->update(transform.position);
        resourceManager->models[voxelManager->meshHash] = voxelManager->model;
    }

    // Rendering serialization
    auto renderingSystem = coordinator->getSystem<RenderingSystem>();
    for (auto renderingSettings : input_json["renderingSettings"])
    {
        for (auto& [key, value] : renderingSettings.items())
        {
            if (key == "mode")
            {
                renderingSystem->currentRenderMode = static_cast<RenderingSystem::RenderMode>(value);
            }
        }
    }

    renderingSystem->setRenderingMode(renderingSystem->currentRenderMode, false, true);
    renderingSystem->dequeueLateOverlaysEntity();
    renderingSystem->enqueueLateOverlaysEntity();
    renderingSystem->categorizeGeometry();
}

int dag::Scene::getEntityIndex(Entity const& entity)
{
    for (size_t i = 0; i < entities.size(); i++)
    {
        if (entity == entities[i])
        {
            return i;
        }
    }

    return -1;
}
