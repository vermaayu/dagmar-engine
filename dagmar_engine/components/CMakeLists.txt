dagmar_add_library(components Dagmar::Components STATIC
    DODBlocks.cpp
)

target_link_libraries(components PUBLIC
	glm::glm
    Dagmar::Renderer
    Dagmar::Graphics    
    imguizmo::imguizmo
)

target_include_directories(components PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)
