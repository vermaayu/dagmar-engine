#pragma once

#define GLM_FORCE_RADIANS
#include <cstddef>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>
// #include <ImGuizmo.h>

#include <mutex>

#include "dagmar/Camera.h"
#include "dagmar/Descriptor.h"
#include "dagmar/Entity.h"

namespace dag
{
    struct Transform
    {
        Transform()
        {
            mmMutex.lock();
            vecMutex.lock();
            position = glm::vec3(0.0f);
            rotation = glm::vec3(0.0f);
            quatRotation = glm::quat(rotation);
            scale = glm::vec3(1.0f);
            modelMatrix = glm::mat4(1.0f);
            forwardVector = glm::vec3(0.0f, 0.0f, -1.0f);
            rightVector = glm::vec3(1.0f, 0.0f, 0.0f);
            mmMutex.unlock();
            vecMutex.unlock();
        }

        Transform& operator=(Transform const& otherT)
        {
            mmMutex.lock();
            vecMutex.lock();
            position = otherT.position;
            rotation = otherT.rotation;
            quatRotation = otherT.quatRotation;
            scale = otherT.scale;
            modelMatrix = otherT.modelMatrix;
            forwardVector = otherT.forwardVector;
            rightVector = otherT.rightVector;
            mmMutex.unlock();
            vecMutex.unlock();

            return *this;
        }

        Transform(Transform const& otherT)
        {
            mmMutex.lock();
            vecMutex.lock();
            position = otherT.position;
            rotation = otherT.rotation;
            quatRotation = otherT.quatRotation;
            scale = otherT.scale;
            modelMatrix = otherT.modelMatrix;
            forwardVector = otherT.forwardVector;
            rightVector = otherT.rightVector;
            mmMutex.unlock();
            vecMutex.unlock();
        }

      public:
        glm::vec3 position = glm::vec3(0.0f);
        glm::vec3 rotation = glm::vec3(0.0f);
        glm::quat quatRotation = glm::quat(rotation);
        glm::vec3 scale = glm::vec3(1.0f);
        glm::mat4 modelMatrix = glm::mat4(1.0f);
        std::mutex mmMutex;

        glm::vec3 forwardVector = glm::vec3(0.0f, 0.0f, -1.0f);
        glm::vec3 rightVector = glm::vec3(1.0f, 0.0f, 0.0f);
        std::mutex vecMutex;

      public:
        void updateModelMatrix()
        {
            {
                std::lock_guard mmLock(mmMutex);
                modelMatrix = glm::translate(glm::mat4(1.0f), position);
                modelMatrix = glm::rotate(modelMatrix, glm::radians(rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));
                modelMatrix = glm::rotate(modelMatrix, glm::radians(rotation.y), glm::vec3(0.0f, 1.0f, 0.0f));
                modelMatrix = glm::rotate(modelMatrix, glm::radians(rotation.z), glm::vec3(0.0f, 0.0f, 1.0f));
                modelMatrix = glm::scale(modelMatrix, scale);
            }

            {
                std::lock_guard vecLock(vecMutex);
                updateVectors();
            }
        }

        glm::mat4 getModelMatrix()
        {
            std::lock_guard mmLock(mmMutex);
            return modelMatrix;
        }

        glm::vec3 getForwardVector()
        {
            std::lock_guard vecLock(vecMutex);
            return forwardVector;
        }

        glm::vec3 getRightVector()
        {
            std::lock_guard vecLock(vecMutex);
            return rightVector;
        }

        void updateVectors()
        {
            auto inverted = glm::inverse(getModelMatrix());
            rightVector = normalize(glm::vec3(inverted[1]));
            forwardVector = normalize(glm::vec3(inverted[2]));
        }

        void setPosition(glm::vec3 const& pos)
        {
            position = pos;

            updateModelMatrix();
        }

        void setRotation(glm::vec3 const& rot)
        {
            rotation = rot;

            updateModelMatrix();
        }

        void setScale(glm::vec3 const& s)
        {
            scale = s;

            updateModelMatrix();
        }

        /**
         * @brief Move the camera along the forward vector with given value
         */
        void moveForward(float const& value, glm::vec3 const& forwardVector)
        {
            position += forwardVector * value;
        }

        /**
         * @brief Move the camera along the up vector with given value
         */
        void moveUp(float const& value, glm::vec3 const& upVector)
        {
            position += upVector * value;
        }

        /**
         * @brief Move the camera along the right vector with given value
         */
        void moveRight(float const& value, glm::vec3 const& rightVector)
        {
            position += rightVector * value;
        }
    };

    struct MeshRenderer
    {
        std::string name = "Empty";
        size_t vaoID = 0;
        size_t hashIndex;
        size_t textureHash = 0;
    };

    struct RigidBody
    {
        bool movable = false;
        glm::vec3 velocity;
        glm::vec3 acceleration;
        glm::vec3 netForce;
        glm::vec3 externalForce;
        glm::vec3 impulse;
        float plasticCoefficient;
        float mass;
    };

    // Only used in the Rendering System
    struct DescriptorSet
    {
        size_t shaderProgramID;
        std::vector<Descriptor> descriptors;
    };

    // centre and radius of bounding sphere
    struct CollisionSphere
    {
        float radius;
        glm::vec3 center;
    };

    struct CollisionAABB
    {
        float width;
        float height;
        float depth;
        glm::vec3 center;
    };

    struct Name
    {
        std::string name;
    };

    struct CameraComponent
    {
        bool isPossessed = false;
        bool isScreenDependent = false;
        bool thirdPerson = false;
        // in degrees, to be changed in radians in getters
        float fov = 45.0f;
        float aspect = 1.0f;
        float zNear = 0.01f;
        float zFar = 100.0f;
        float cameraSpeed = 1.0f;

        glm::vec3 offset = glm::vec3(0.0);

        /**
         * @brief Getter for camera view matrix
         */
        glm::mat4 getView(glm::vec3 const& position, glm::vec3 const& rotation)
        {
            // pitch
            glm::mat4 rotationMat = glm::rotate(glm::mat4(1.0f), glm::radians(rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));
            //yaw
            rotationMat = glm::rotate(rotationMat, glm::radians(rotation.y), glm::vec3(0.0f, 1.0f, 0.0f));
            // roll
            rotationMat = glm::rotate(rotationMat, glm::radians(rotation.z), glm::vec3(0.0f, 0.0f, 1.0f));

            glm::mat4 translationMat = glm::translate(glm::mat4(1.0f), glm::vec3(-position.x, -position.y, -position.z));

            glm::mat4 offsetMat = glm::translate(glm::mat4(1.0f), glm::vec3(offset.x, offset.y, offset.z));

            return rotationMat * translationMat * offsetMat;
        }

        /**
         * @brief Getter for camera projection matrix
         */
        glm::mat4 getProjection() const
        {
            return glm::perspective(glm::radians(fov), aspect, zNear, zFar);
        }
    };

    struct LightComponent
    {
        enum Type
        {
            Directional = 0,
            Point = 1,
            Spot = 2
        };

        enum ProjectionType
        {
            Orthographic = 0,
            Perspective = 1
        };

        glm::vec3 color = glm::vec3(0.5f);

        Type type;
        ProjectionType projectionType;

        glm::mat4 getProjection(float const& fovDeg, float const& aspect, float const& zNear, float const& zFar) const
        {
            return glm::perspective(glm::radians(fovDeg), aspect, zNear, zFar);
        }

        /**
         * @brief Gets a standard projection 1k x 1k
         */
        glm::mat4 getProjection() const
        {
            if (projectionType == ProjectionType::Perspective)
            {
                return glm::perspective(glm::radians(45.0f), 1.0f, 0.01f, 100.f);
            }
            else
            {
                return glm::ortho(-512.0f, 512.0f, -512.0f, 512.0f, 0.1f, 1024.0f);
            }
        }
    };

    // TODO combine somehow with the material class
    struct MaterialComponent
    {
        enum Queue : int
        {
	        Opaque = 0,
        	Transparent = 1
        };
    	
        glm::vec4 ambient = glm::vec4(0.1f, 0.1f, 0.1f, 1.0f);
        glm::vec4 diffuse = glm::vec4(1.0f);
        glm::vec4 specular = glm::vec4(0.3f, 0.3f, 0.3f, 1.0f);
        glm::vec4 emissive = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
        alignas(16) float specularExponent = 4.0f;


    	// TO Be set if there's a dissolve mask or transparent
    	// Queue queue = Opaque;
    };

    struct VoxelComponent
    {
        glm::vec3 startPosition = glm::vec3(0.0f, 0.0f, 0.0f);
        int maxSize = 10;
        int loadSize = 3;
        int chunkSize = 10;
        glm::vec3 playerPosition = glm::vec3(0.0f, 0.0, 0.0f);
        float scale2D = 0.005f;
        float scale3D = 0.013f;
        int iterations3D = 3;
        int iterations2D = 4;
        float persistence2D = 0.5f;
        float persistence3D = 0.2f;
        bool hasWater = true;
        int waterHeight = 30;
        bool hasTrees = true;
        float treeProbability = 1.08f;
        int minTreeHeight = 2;
        int maxTreeHeight = 4;
        int minTreeWidth = 1;
        int maxTreeWidth = 2;
        bool hotReload = false;
        int terrainType = 2; // 3d + 2d

        std::string terrainTypeName = "2D + 3D Noise";
        dag::Entity focussedEntity = UINT32_MAX;
        bool hasFlowers = true;
        float flowerProbability = 1.2f;
    };

    class ScriptComponent
    {
      public:
        std::string scriptName;
        bool scriptCreated = false;
        bool checkBox = false;

      public:
        virtual void startup(){};
        virtual void shutdown(){};

        virtual void save(){};
        virtual void reset(){};

        virtual void start(){};
        virtual void update(){};

        // PHYSICS STUFF TODO

        virtual void lateUpdate(){};

        // OnGUI STUFF TODO

        virtual void onPause(){};
    };

    struct PlayerController
    {
        bool isActive = true;
        bool fps_style = true;
        float height = 1.8f;
        float jumpSpeed = 5.0f;
        float walkSpeed = 1.0f;
        float maxWalkSpeed = 2.0f;
        float maxRunSpeed = 5.0f;
        float runMultiplier = 1.2f;
        glm::vec3 velocity = glm::vec3(0.0f);
    };

    struct Volume
    {
        enum Type
        {
            Global = 0,
            Local = 1
        };

        // Should also have a collider component if not global
        Type type = Type::Global;

        struct PostProcessBloomData
        {
            float threshold = 0.85f;
            float offset = 0.5f;
        };
    	
        struct PostProcessFogData
        {
            float fogNear = 0.01f;
            float fogFar = 5.0f;
            float density = 1.0f;
            glm::vec4 fogColor = glm::vec4(1.0f);
        };


        struct PostProcessBlurData
        {
            int kernelSize = 3;
        };

        struct PostProcessChromaticAberrationData
        {
            float rOffset = 0.009f;
            float gOffset = 0.006f;
            float bOffset = -0.006f;
        };

        struct PostProcessDilationData
        {
            int kernelSize = 3;
            float separation = 4;
            float minThreshold = 0.1f;
            float maxThreshold = 0.3f;
        };

        struct PostProcessPixelizeData
        {
            int pixelSize = 4;
        };

        struct PostProcessSharpenData
        {
            float amount = 0.4f;
        };

        PostProcessBloomData bloomData;
        PostProcessFogData fogData;
        PostProcessBlurData blurData;
        PostProcessChromaticAberrationData chromaticAberrationData;
        PostProcessDilationData dilationData;
        PostProcessPixelizeData pixelizeData;
        PostProcessSharpenData sharpenData;
    };
} // namespace dag
