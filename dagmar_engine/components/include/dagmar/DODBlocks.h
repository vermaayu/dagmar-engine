#pragma once
#include "Components.h"
#include <glm/glm.hpp>

namespace dag
{
    namespace EngineLimits
    {
        constexpr uint32_t MAX_CAMERAS = 8;
        constexpr uint32_t MAX_LIGHTS = 64;
        constexpr uint32_t MAX_RENDERABLE_ENTITIES = 512;

    	constexpr uint32_t MAX_DIR_LIGHTS = 3;
    	constexpr uint32_t MAX_POINT_LIGHTS = 8;
        constexpr uint32_t MAX_SPOT_LIGHTS = 32;

    	// can be 1024
    	constexpr uint32_t MAX_ENTITIES = 512;

    	// PER LIGHT  TODO REFACTOR
    	constexpr uint32_t DIR_LIGHT_SHADOW_MAPS = 1;
		constexpr uint32_t SPOT_LIGHT_SHADOW_MAPS = 1;
		constexpr uint32_t POINT_LIGHT_SHADOW_MAPS = 6;
    	
		constexpr uint32_t MAX_LIGHTS_WITH_ADDITIONAL_FACES = MAX_DIR_LIGHTS * DIR_LIGHT_SHADOW_MAPS + MAX_SPOT_LIGHTS * SPOT_LIGHT_SHADOW_MAPS + MAX_POINT_LIGHTS * POINT_LIGHT_SHADOW_MAPS;

        inline static size_t minimumUniformBufferOffsetAlignment = 1;

        static size_t calculateSize(size_t const& structSize)
        {
            return minimumUniformBufferOffsetAlignment * (structSize / minimumUniformBufferOffsetAlignment) + (structSize % minimumUniformBufferOffsetAlignment ? minimumUniformBufferOffsetAlignment : 0);
        }

    } // namespace EngineLimits

	namespace UniformBindings
    {
        inline static constexpr int32_t postProcessIndex = 7;
        inline static constexpr int32_t modelAmbientTextureIndex = 14;
        inline static constexpr int32_t modelDiffuseTextureIndex = 15;
        inline static constexpr int32_t modelSpecularTextureIndex = 16;
        inline static constexpr int32_t modelBumpTextureIndex = 17;
        inline static constexpr int32_t modelAlphaTextureIndex = 18;
        inline static constexpr int32_t directionalLightShadowMapsIndex = 20;
        inline static constexpr int32_t spotLightShadowMapsIndex = 21;
        inline static constexpr int32_t pointLightShadowMapsIndex = 22;
        inline static constexpr int32_t cubeMapIndex = 23;
    }


    struct SceneData
    {
        static constexpr uint32_t index = 0;

        int32_t selectedEntity = 0;
        int32_t currentLightCount = 0;
        int32_t selectedLight = 0;
        int32_t currentCamera = 0;
        int32_t selectedCamera = 0;
        float zNear = 0;
        float zFar = 0;

    	int32_t currentDirectionalLights = 0;
        int32_t currentSpotLights = 0;
        int32_t currentPointLights = 0;

    	int32_t padding_0;
        int32_t padding_1;

        static size_t size()
        {
            return EngineLimits::calculateSize(sizeof(SceneData));
        }
    };

    struct CameraData
    {
        static constexpr uint32_t index = 1;

        glm::vec4 positions[EngineLimits::MAX_CAMERAS];
        glm::mat4 projectionMatrices[EngineLimits::MAX_CAMERAS];
        glm::mat4 modelMatrices[EngineLimits::MAX_CAMERAS];
        glm::mat4 inverseViewProj[EngineLimits::MAX_CAMERAS];

        static size_t size()
        {
            return EngineLimits::calculateSize(sizeof(CameraData));
        }
    };

    struct LightData
    {
        static constexpr uint32_t index = 2;

        glm::vec4 positions[EngineLimits::MAX_LIGHTS_WITH_ADDITIONAL_FACES];
        glm::mat4 projectionMatrices[EngineLimits::MAX_LIGHTS_WITH_ADDITIONAL_FACES];
        glm::mat4 modelMatrices[EngineLimits::MAX_LIGHTS_WITH_ADDITIONAL_FACES];
        glm::mat4 inverseViewProj[EngineLimits::MAX_LIGHTS_WITH_ADDITIONAL_FACES];
        glm::vec4 colors[EngineLimits::MAX_LIGHTS_WITH_ADDITIONAL_FACES];

        static size_t size()
        {
            return EngineLimits::calculateSize(sizeof(LightData));
        }
    };

    struct MaterialData
    {
        static constexpr uint32_t index = 3;

        MaterialComponent materials[EngineLimits::MAX_RENDERABLE_ENTITIES];

        static size_t size()
        {
            return EngineLimits::calculateSize(sizeof(MaterialData));
        }
    };

	struct EntitiesData
    {
        static constexpr uint32_t index = 4;

        glm::mat4 modelMatrices[EngineLimits::MAX_ENTITIES];

        static size_t size()
        {
            return EngineLimits::calculateSize(sizeof(EntitiesData));
        }
    };

    struct DrawData
    {
        static constexpr uint32_t index = 5;

        int32_t entityID;
        int32_t entityMaterial;
        alignas(16) glm::vec4 encodedID;

        static size_t size()
        {
            return EngineLimits::calculateSize(sizeof(DrawData));
        }
    };

    struct LightDrawData
    {
        static constexpr uint32_t index = 6;
        int32_t lightEntityID;

        static size_t size()
        {
            return EngineLimits::calculateSize(sizeof(LightDrawData));
        }
    };

    struct PostProcessBloomSettings
    {
        static constexpr uint32_t index = UniformBindings::postProcessIndex;

        float threshold = 0.85f;
        float offset = 0.5f;

    	float padding_0 = 0;
        float padding_1 = 0;
    	
    	static size_t size()
        {
            return EngineLimits::calculateSize(sizeof(LightDrawData));
        }
    };

	struct PostProcessFog
    {
        static constexpr uint32_t index = UniformBindings::postProcessIndex;

        float fogNear = 0.01f;
        float fogFar = 5.0f;
        alignas(8) float density = 1.0f;
        glm::vec4 fogColor = glm::vec4(1.0f);

        static size_t size()
        {
            return EngineLimits::calculateSize(sizeof(PostProcessFog));
        }
    };

	struct PostProcessBlur
    {
        static constexpr uint32_t index = UniformBindings::postProcessIndex;

        alignas(16) int kernelSize = 3;

        static size_t size()
        {
            return EngineLimits::calculateSize(sizeof(PostProcessBlur));
        }
    };

	struct PostProcessChromaticAberration
    {
        static constexpr uint32_t index = UniformBindings::postProcessIndex;

        float rOffset = 0.009f;
        float gOffset = 0.006f;
        float bOffset = -0.006f;
        float padding_ = 0;
		
        static size_t size()
        {
            return EngineLimits::calculateSize(sizeof(PostProcessChromaticAberration));
        }
    };

	struct PostProcessDilation
    {
        static constexpr uint32_t index = UniformBindings::postProcessIndex;

		int kernelSize = 3;
        float separation = 4;
        float minThreshold = 0.1f;
        float maxThreshold = 0.3f;
        
        static size_t size()
        {
            return EngineLimits::calculateSize(sizeof(PostProcessDilation));
        }
    };

	struct PostProcessPixelize
    {
        static constexpr uint32_t index = UniformBindings::postProcessIndex;

        alignas(16) int pixelSize = 4;

        static size_t size()
        {
            return EngineLimits::calculateSize(sizeof(PostProcessPixelize));
        }
    };

	struct PostProcessSharpen
    {
        static constexpr uint32_t index = UniformBindings::postProcessIndex;

        alignas(16) float amount = 0.4f;

        static size_t size()
        {
            return EngineLimits::calculateSize(sizeof(PostProcessSharpen));
        }
    };
	
} // namespace dag