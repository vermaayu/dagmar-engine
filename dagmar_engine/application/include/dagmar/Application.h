#pragma once
#include "dagmar/LayerManager.h"
#include "dagmar/ModuleLifecycle.h"
#include "dagmar/WindowCloseEvent.h"
#include "dagmar/WindowResizeEvent.h"


namespace dag
{
    class Application
    {
      protected:
        bool running;
        bool minimized;
        float lastFrameTime;

        LayerManager layerManager;

      public:
        Application();
        virtual ~Application();

        void addLayer(std::shared_ptr<Layer>& layer);
        void removeLayer(std::shared_ptr<Layer>& layer);

        virtual void run();
        virtual void onEvent(Event& e);
        virtual bool onWindowClose(WindowCloseEvent& e);
        virtual bool onWindowResize(WindowResizeEvent& e);
    };
} // namespace dag