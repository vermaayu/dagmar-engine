dagmar_add_library(platform_detector Dagmar::PlatformDetector STATIC
    PlatformDetector.cpp
)

target_include_directories(platform_detector PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(platform_detector
    Dagmar::ModuleLifecycle
)
