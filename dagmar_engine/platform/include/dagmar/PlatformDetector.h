#pragma once
#include <string>
#include <memory>
#include "dagmar/Module.h"

namespace dag
{
    /**
     * @brief PlatformDetector class
     *
     * Singleton
     * Determines the platform and adjusts the settings for Windows and Linux
     *
     */
    class PlatformDetector : public dag::Module
    {
        public:
            /**
             * @brief Enum class for choosing platform
             */
            enum class Platform
            {
                Linux,
                Windows,
                None
            };

            // Reflection-like to get platform at runtime
            // without many ifdefs
            Platform platform = Platform::None;

        public:
            // Default constructor
            PlatformDetector() = default;

            std::string getName() const override;

            // Perform startup
            void startup() override;

            // Perform startup
            void shutdown() override;

        private:
            // Deleted copy constructor
            PlatformDetector(PlatformDetector const&) = delete;

            // Deleted assign operator
            PlatformDetector& operator=(const PlatformDetector&) = delete;

            // Function that detects the platform we're running on
            void detect();

            // Activates platform-specific features
            void activatePlatformFeatures();
    };

    inline std::shared_ptr<PlatformDetector> platformDetector(new PlatformDetector());
}
