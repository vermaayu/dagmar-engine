#include "dagmar/LightSystem.h"
#include "dagmar/Log.h"
#include "dagmar/RenderingSystem.h"

/**
 * @brief Default cosntructor to initialise the global variables & coefficients
  */
dag::LightSystem::LightSystem()
{
}

dag::LightSystem::~LightSystem()
{
}

/**
 * @brief Register the needed components and the system
 */

void dag::LightSystem::startup()
{
    coordinator->registerComponent<dag::LightComponent>();

    dag::Signature LightSystemSignature;
    LightSystemSignature.set(coordinator->getComponentType<dag::Transform>());
    LightSystemSignature.set(coordinator->getComponentType<dag::LightComponent>());

    coordinator->setSystemSignature<dag::LightSystem>(LightSystemSignature);
}

/**
 * @brief Gets the lights as descriptors
  */
std::vector<dag::Descriptor> const& dag::LightSystem::getLights()
{
    for (auto& entity : entities)
    {
        auto& transform = coordinator->getComponent<Transform>(entity);
        descriptors[0].data = transform.position;

        break;
    }

    return descriptors;
}

/**
 * @brief Kinda unsafe and should be used accordingly
 */
std::vector<dag::Descriptor> dag::LightSystem::getLightCameraDescriptors(dag::Entity const& entity)
{
    std::vector<dag::Descriptor> tempDescriptors;

    auto& transform = coordinator->getComponent<Transform>(entity);
    auto& comp = coordinator->getComponent<LightComponent>(entity);

    auto inverted = glm::inverse(transform.modelMatrix);
    auto fwd = normalize(glm::vec3(inverted[2]) * glm::vec3(1, 1, -1));

    tempDescriptors.push_back(Descriptor(12, "lightProjection", comp.getProjection()));
    tempDescriptors.push_back(Descriptor(12, "lightView", glm::lookAt(transform.position, transform.position + fwd, glm::vec3(0, 1, 0))));

    return tempDescriptors;
}

/**
 * @brief Updates the lookup vectors for the lights
 */
void dag::LightSystem::updateLookupVectors()
{
    directionalLightsIDs.clear();
    directionalLightsIDs.shrink_to_fit();
    directionalLightsEntities.clear();
    directionalLightsEntities.shrink_to_fit();

    spotLightsIDs.clear();
    spotLightsIDs.shrink_to_fit();
    spotLightsEntities.clear();
    spotLightsEntities.shrink_to_fit();

    pointLightsIDs.clear();
    pointLightsIDs.shrink_to_fit();
    pointLightsEntities.clear();
    pointLightsEntities.shrink_to_fit();

    size_t lightID = 0;

    for (auto const& entity : entities)
    {
        auto& component = coordinator->getComponent<LightComponent>(entity);

        if (component.type == LightComponent::Type::Directional)
        {
            directionalLightsEntities.emplace_back(entity);
            directionalLightsIDs.emplace_back(lightID);
        }
        else if (component.type == LightComponent::Type::Spot)
        {
            spotLightsEntities.emplace_back(entity);
            spotLightsIDs.emplace_back(lightID);
        }
        else if (component.type == LightComponent::Type::Point)
        {
            pointLightsEntities.emplace_back(entity);
            pointLightsIDs.emplace_back(lightID);
        }

        lightID++;
    }

	// Updates the rendering system textures for lights
    auto renderingSystem = coordinator->getSystem<RenderingSystem>();

    if (renderingSystem->directionalSMTextures.info.arrayLayers != directionalLightsIDs.size() * EngineLimits::DIR_LIGHT_SHADOW_MAPS)
    {
        renderingSystem->directionalSMTextures.resize(renderingSystem->directionalSMTextures.info.width, renderingSystem->directionalSMTextures.info.height, static_cast<uint32_t>(directionalLightsIDs.size() * EngineLimits::DIR_LIGHT_SHADOW_MAPS));
    }

    if (renderingSystem->spotSMTextures.info.arrayLayers != spotLightsIDs.size() * EngineLimits::SPOT_LIGHT_SHADOW_MAPS)
    {
        renderingSystem->spotSMTextures.resize(renderingSystem->spotSMTextures.info.width, renderingSystem->spotSMTextures.info.height, static_cast<uint32_t>(spotLightsIDs.size() * EngineLimits::SPOT_LIGHT_SHADOW_MAPS));
    }

    if (renderingSystem->pointSMTextures.info.arrayLayers != pointLightsIDs.size() * EngineLimits::POINT_LIGHT_SHADOW_MAPS)
    {
        renderingSystem->pointSMTextures.resize(renderingSystem->pointSMTextures.info.width, renderingSystem->pointSMTextures.info.height, static_cast<uint32_t>(pointLightsIDs.size() * EngineLimits::POINT_LIGHT_SHADOW_MAPS));
    }
}
