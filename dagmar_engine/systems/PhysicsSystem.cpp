#include "dagmar/PhysicsSystem.h"
#include "dagmar/Log.h"
#include "dagmar/PlayerControllerSystem.h"
#include "dagmar/ResourceManager.h"
#include "dagmar/Scene.h"
#include "dagmar/SystemsManager.h"
#include "dagmar/VoxelChunkManager.h"

#include <glm/gtx/string_cast.hpp>

#include <chrono>
#include <limits>
#include <thread>

/**
 * @brief Default cosntructor to initialise the global variables & coefficients
  */
dag::PhysicsSystem::PhysicsSystem()
{
    gravity = glm::vec3(0.0f, -9.8f, 0.0f);
    airResistance = 0.0f;
    epsilon = 0.0001f;
    dispersionCoefficient = 0.7f;
    //DIRTY HACK
    timeMultiplier = 7.0f;
}

dag::PhysicsSystem::~PhysicsSystem()
{
    std::unique_lock<std::shared_mutex> lock(this->physicsMutex);

    this->adjustingThreads = true;

    size_t count = 0;
    for (auto& pt : physicsThreads)
    {
        pt.join();
        std::cout << "Thread: " << count << " shut down" << std::endl;
        count++;
    }
}

void dag::PhysicsSystem::physicsThreadFunc(size_t threadNum)
{
    auto lastRun = std::chrono::high_resolution_clock::now();

    auto thread_num = threadNum;

    debugging_output_mutex.lock();
    std::cout << "thread: " << threadNum << " "
              << " creating" << std::endl;
    std::cout << "|_id: " << std::this_thread::get_id() << std::endl;
    auto start_iter = std::next(entities.begin(), thread_num * (entities.size() / this->numThreads));
    auto end_iter = std::next(entities.begin(), (thread_num + 1) * (entities.size() / this->numThreads));
    std::cout << "|_" << std::distance(entities.begin(), start_iter) << " -> " << std::distance(entities.begin(), end_iter) << std::endl;
    debugging_output_mutex.unlock();

    double timeSinceLastRun;
    while (!systemsManager->isShuttingDown() && !this->adjustingThreads)
    {
        if (gameState == GameState::Play)
        {

            std::shared_lock<std::shared_mutex> lock(this->physicsMutex);
            
            timeSinceLastRun = std::chrono::duration<double, std::ratio<1>>(std::chrono::high_resolution_clock::now() - lastRun).count();
            if (systemsManager->isShuttingDown())
            {
                break;
            }

            start_iter = std::next(entities.begin(), thread_num * (entities.size() / this->numThreads));
            end_iter = std::next(entities.begin(), (thread_num + 1) * (entities.size() / this->numThreads));
            if (thread_num == this->numThreads - 1)
            {
                end_iter = entities.end();
            }

            this->update((timeSinceLastRun / 10.0) * timeMultiplier, start_iter, end_iter);
            //sync
            checkForCollisions(start_iter, end_iter);
            //sync
        }

        lastRun = std::chrono::high_resolution_clock::now();
        // this should be sleep_until
        std::this_thread::sleep_until(lastRun + std::chrono::milliseconds(1000 / 120));
    }

    debugging_output_mutex.lock();
    std::cout << "thread: " << threadNum << " "
              << " exiting" << std::endl;
    std::cout << "|_id: " << std::this_thread::get_id() << std::endl;
    debugging_output_mutex.unlock();
}

void dag::PhysicsSystem::createThreads(size_t numberOfThreads)
{
    this->adjustingThreads = true;

    for (auto& pt : physicsThreads)
    {
        pt.join();
    }

    physicsThreads.clear();
    physicsThreads.shrink_to_fit();

    numThreads = numberOfThreads;

    this->adjustingThreads = false;

    for (size_t curr_thread = 0; curr_thread < numThreads; ++curr_thread)
    {
        physicsThreads.emplace_back(std::thread(&dag::PhysicsSystem::physicsThreadFunc, this, curr_thread));
    }
}

/**
 * @brief Register the needed components and the system
  */

void dag::PhysicsSystem::startup()
{
    coordinator->registerComponent<dag::RigidBody>();
    coordinator->registerComponent<dag::CollisionSphere>();
    coordinator->registerComponent<dag::CollisionAABB>();

    dag::Signature physicsSystemSignature;
    physicsSystemSignature.set(coordinator->getComponentType<dag::Transform>());
    physicsSystemSignature.set(coordinator->getComponentType<dag::RigidBody>());

    coordinator->setSystemSignature<dag::PhysicsSystem>(physicsSystemSignature);

    createThreads(4);
}

/**
 * @brief Sorts the vectors for the physics system relatively
 * so that there will be a match with the draw entity data
 */
void dag::PhysicsSystem::updateLookupVectors()
{
    /*
    debugPhysicsEntitiesIDs.clear();
    debugPhysicsEntitiesIDs.shrink_to_fit();

    auto physicsSystem = coordinator->getSystem<PhysicsSystem>();

    for (auto& pEntity : physicsSystem->entities)
    {
        int32_t count = 0;
        for (auto& rEntity : entities)
        {
            if (pEntity == rEntity)
            {
                debugPhysicsEntitiesIDs.emplace_back(count);
            }

            count++;
        }
    }
    */
}

bool dag::PhysicsSystem::haveCollidedSphereSphere(dag::Entity const& a, dag::Entity const& b)
{
    // get the relevant data for the entity
    auto& collisionSphere = coordinator->getComponent<dag::CollisionSphere>(a);
    auto& transform = coordinator->getComponent<dag::Transform>(a);
    float radius = collisionSphere.radius;
    glm::vec3 centerInWorld = transform.getModelMatrix() * glm::vec4(collisionSphere.center, 1.0f);

    auto& otherCollisionSphere = coordinator->getComponent<dag::CollisionSphere>(b);
    auto& otherTransform = coordinator->getComponent<dag::Transform>(b);
    glm::vec3 otherCenterInWorld = otherTransform.getModelMatrix() * glm::vec4(otherCollisionSphere.center, 1.0f);
    float otherRadius = otherCollisionSphere.radius;

    return glm::length(centerInWorld - otherCenterInWorld) < radius + otherRadius - epsilon;
}

void dag::PhysicsSystem::collisionResponseSphereSphere(dag::Entity const& a, dag::Entity const& b)
{
    // get the relevant data for the entity
    auto& collisionSphere = coordinator->getComponent<dag::CollisionSphere>(a);
    auto& transform = coordinator->getComponent<dag::Transform>(a);
    float radius = collisionSphere.radius * std::max(std::max(transform.scale.x, transform.scale.y), transform.scale.z);
    auto& rigidBody = coordinator->getComponent<dag::RigidBody>(a);
    glm::vec3 centerInWorld = transform.getModelMatrix() * glm::vec4(collisionSphere.center, 1.0f);

    auto& otherCollisionSphere = coordinator->getComponent<dag::CollisionSphere>(b);
    auto& otherTransform = coordinator->getComponent<dag::Transform>(b);
    glm::vec3 otherCenterInWorld = otherTransform.getModelMatrix() * glm::vec4(otherCollisionSphere.center, 1.0f);
    float otherRadius = otherCollisionSphere.radius * std::max(std::max(otherTransform.scale.x, otherTransform.scale.y), otherTransform.scale.z);

    auto& otherRigidBody = coordinator->getComponent<dag::RigidBody>(b);
    // compute the collision direction
    if (glm::length(centerInWorld - otherCenterInWorld) < 0.01)
    {
        return;
    }
    glm::vec3 toVector = glm::normalize(otherCenterInWorld - centerInWorld);
    // compute the midpoint between the objects
    glm::vec3 mid = (centerInWorld + otherCenterInWorld) * 0.5f;
    // update the positions as 'touching', no collision
    if (rigidBody.movable)
    {
        transform.setPosition(mid + (collisionSphere.radius + epsilon) * glm::normalize(centerInWorld - mid));
        rigidBody.externalForce = glm::reflect(rigidBody.externalForce, toVector) * rigidBody.plasticCoefficient;
        rigidBody.velocity = glm::reflect(rigidBody.velocity, toVector) * rigidBody.plasticCoefficient * dispersionCoefficient;
    }

    if (otherRigidBody.movable)
    {
        otherTransform.setPosition(mid + (otherCollisionSphere.radius + epsilon) * glm::normalize(otherCenterInWorld - mid));
        // reflect the forces that move towards the other object and multiply by the plastic coefficient
        otherRigidBody.externalForce = glm::reflect(otherRigidBody.externalForce, toVector) * otherRigidBody.plasticCoefficient;
        // update the velocity and disperse it if wanted
        otherRigidBody.velocity = glm::reflect(otherRigidBody.velocity, -toVector) * otherRigidBody.plasticCoefficient * dispersionCoefficient;
    }
}

bool dag::PhysicsSystem::haveCollidedAABBAABB(dag::Entity const& a, dag::Entity const& b)
{
    // get the relevant data for the entity
    auto& collisionAABB = coordinator->getComponent<dag::CollisionAABB>(a);
    auto& transform = coordinator->getComponent<dag::Transform>(a);
    glm::vec3 centerInWorld = transform.getModelMatrix() * glm::vec4(collisionAABB.center, 1.0f);
    float MinX = centerInWorld.x - (collisionAABB.width / 2.0f);
    float MinY = centerInWorld.y - (collisionAABB.height / 2.0f);
    float MinZ = centerInWorld.z - (collisionAABB.depth / 2.0f);
    float MaxX = centerInWorld.x + (collisionAABB.width / 2.0f);
    float MaxY = centerInWorld.y + (collisionAABB.height / 2.0f);
    float MaxZ = centerInWorld.z + (collisionAABB.depth / 2.0f);

    auto& otherCollisionAABB = coordinator->getComponent<dag::CollisionAABB>(b);
    auto& otherTransform = coordinator->getComponent<dag::Transform>(b);
    glm::vec3 otherCenterInWorld = otherTransform.getModelMatrix() * glm::vec4(otherCollisionAABB.center, 1.0f);
    float oMinX = otherCenterInWorld.x - (otherCollisionAABB.width / 2.0f);
    float oMinY = otherCenterInWorld.y - (otherCollisionAABB.height / 2.0f);
    float oMinZ = otherCenterInWorld.z - (otherCollisionAABB.depth / 2.0f);
    float oMaxX = otherCenterInWorld.x + (otherCollisionAABB.width / 2.0f);
    float oMaxY = otherCenterInWorld.y + (otherCollisionAABB.height / 2.0f);
    float oMaxZ = otherCenterInWorld.z + (otherCollisionAABB.depth / 2.0f);

    return (MinX <= oMaxX && MaxX >= oMinX) &&
           (MinY <= oMaxY && MaxY >= oMinY) &&
           (MinZ <= oMaxZ && MaxZ >= oMinZ);
}

void dag::PhysicsSystem::collisionResponseAABBAABB(dag::Entity const& a, dag::Entity const& b)
{
    auto& collisionAABB = coordinator->getComponent<dag::CollisionAABB>(a);
    auto& transform = coordinator->getComponent<dag::Transform>(a);
    glm::vec3 centerInWorld = transform.getModelMatrix() * glm::vec4(collisionAABB.center, 1.0f);
    float MinX = centerInWorld.x - (collisionAABB.width / 2.0f);
    float MinY = centerInWorld.y - (collisionAABB.height / 2.0f);
    float MinZ = centerInWorld.z - (collisionAABB.depth / 2.0f);
    float MaxX = centerInWorld.x + (collisionAABB.width / 2.0f);
    float MaxY = centerInWorld.y + (collisionAABB.height / 2.0f);
    float MaxZ = centerInWorld.z + (collisionAABB.depth / 2.0f);

    auto& otherCollisionAABB = coordinator->getComponent<dag::CollisionAABB>(b);
    auto& otherTransform = coordinator->getComponent<dag::Transform>(b);
    glm::vec3 otherCenterInWorld = otherTransform.getModelMatrix() * glm::vec4(otherCollisionAABB.center, 1.0f);
    float oMinX = otherCenterInWorld.x - (otherCollisionAABB.width / 2.0f);
    float oMinY = otherCenterInWorld.y - (otherCollisionAABB.height / 2.0f);
    float oMinZ = otherCenterInWorld.z - (otherCollisionAABB.depth / 2.0f);
    float oMaxX = otherCenterInWorld.x + (otherCollisionAABB.width / 2.0f);
    float oMaxY = otherCenterInWorld.y + (otherCollisionAABB.height / 2.0f);
    float oMaxZ = otherCenterInWorld.z + (otherCollisionAABB.depth / 2.0f);

    auto& rigidBody = coordinator->getComponent<dag::RigidBody>(a);
    auto& otherRigidBody = coordinator->getComponent<dag::RigidBody>(b);

    glm::vec3 unitCenterToCenter = glm::normalize((centerInWorld - otherCenterInWorld) / transform.scale);

    float movableConstant = 1.0f;

    if (rigidBody.movable && otherRigidBody.movable)
    {
        movableConstant = 0.5f;
    }

    if (rigidBody.movable)
    {
        auto degreesFromForwardY = glm::dot(glm::vec3(0.0, 1.0, 0.0), unitCenterToCenter);
        auto degreesFromBackwardY = glm::dot(glm::vec3(0.0, -1.0, 0.0), unitCenterToCenter);
        auto degreesFromForwardX = glm::dot(glm::vec3(1.0, 0.0, 0.0), unitCenterToCenter);
        auto degreesFromBackwardX = glm::dot(glm::vec3(-1.0, 0.0, 0.0), unitCenterToCenter);
        auto degreesFromForwardZ = glm::dot(glm::vec3(0.0, 0.0, 1.0), unitCenterToCenter);
        auto degreesFromBackwardZ = glm::dot(glm::vec3(0.0, 0.0, -1.0), unitCenterToCenter);

        if (degreesFromForwardX >= glm::pi<float>() / 4.0f)
        {
            transform.position.x += (oMaxX - MinX) * movableConstant;
            rigidBody.externalForce.x *= -1 * rigidBody.plasticCoefficient;
            rigidBody.velocity.x *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromBackwardX >= glm::pi<float>() / 4.0f)
        {
            transform.position.x -= (MaxX - oMinX) * movableConstant;
            rigidBody.externalForce.x *= -1 * rigidBody.plasticCoefficient;
            rigidBody.velocity.x *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromForwardY >= glm::pi<float>() / 4.0f)
        {
            transform.position.y += (oMaxY - MinY) * movableConstant;
            rigidBody.externalForce.y *= -1 * rigidBody.plasticCoefficient;
            rigidBody.velocity.y *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromBackwardY >= glm::pi<float>() / 4.0f)
        {
            transform.position.y -= (MaxY - oMinY) * movableConstant;
            rigidBody.externalForce.y *= -1 * rigidBody.plasticCoefficient;
            rigidBody.velocity.y *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromForwardZ >= glm::pi<float>() / 4.0f)
        {
            transform.position.z += (oMaxZ - MinZ) * movableConstant;
            rigidBody.externalForce.z *= -1 * rigidBody.plasticCoefficient;
            rigidBody.velocity.z *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromBackwardZ >= glm::pi<float>() / 4.0f)
        {
            transform.position.z -= (MaxZ - oMinZ) * movableConstant;
            rigidBody.externalForce.z *= -1 * rigidBody.plasticCoefficient;
            rigidBody.velocity.z *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
        }
        transform.updateModelMatrix();
    }

    if (otherRigidBody.movable)
    {
        auto degreesFromForwardY = glm::dot(glm::vec3(0.0, 1.0, 0.0), unitCenterToCenter);
        auto degreesFromBackwardY = glm::dot(glm::vec3(0.0, -1.0, 0.0), unitCenterToCenter);
        auto degreesFromForwardX = glm::dot(glm::vec3(1.0, 0.0, 0.0), unitCenterToCenter);
        auto degreesFromBackwardX = glm::dot(glm::vec3(-1.0, 0.0, 0.0), unitCenterToCenter);
        auto degreesFromForwardZ = glm::dot(glm::vec3(0.0, 0.0, 1.0), unitCenterToCenter);
        auto degreesFromBackwardZ = glm::dot(glm::vec3(0.0, 0.0, -1.0), unitCenterToCenter);

        if (degreesFromForwardX >= glm::pi<float>() / 4.0f)
        {
            otherTransform.position.x -= (oMaxX - MinX) * movableConstant;
            otherRigidBody.externalForce.x *= -1 * otherRigidBody.plasticCoefficient;
            otherRigidBody.velocity.x *= -1 * otherRigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromBackwardX >= glm::pi<float>() / 4.0f)
        {
            otherTransform.position.x += (MaxX - oMinX) * movableConstant;
            otherRigidBody.externalForce.x *= -1 * otherRigidBody.plasticCoefficient;
            otherRigidBody.velocity.x *= -1 * otherRigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromForwardY >= glm::pi<float>() / 4.0f)
        {
            otherTransform.position.y += (oMaxY - MinY) * movableConstant;
            otherRigidBody.externalForce.y *= -1 * otherRigidBody.plasticCoefficient;
            otherRigidBody.velocity.y *= -1 * otherRigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromBackwardY >= glm::pi<float>() / 4.0f)
        {
            otherTransform.position.y += (MaxY - oMinY) * movableConstant;
            otherRigidBody.externalForce.y *= -1 * otherRigidBody.plasticCoefficient;
            otherRigidBody.velocity.y *= -1 * otherRigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromForwardZ >= glm::pi<float>() / 4.0f)
        {
            otherTransform.position.z -= (oMaxZ - MinZ) * movableConstant;
            otherRigidBody.externalForce.z *= -1 * otherRigidBody.plasticCoefficient;
            otherRigidBody.velocity.z *= -1 * otherRigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromBackwardZ >= glm::pi<float>() / 4.0f)
        {
            otherTransform.position.z += (MaxZ - oMinZ) * movableConstant;
            otherRigidBody.externalForce.z *= -1 * otherRigidBody.plasticCoefficient;
            otherRigidBody.velocity.z *= -1 * otherRigidBody.plasticCoefficient * dispersionCoefficient;
        }
        otherTransform.updateModelMatrix();
    }
}

bool dag::PhysicsSystem::haveCollidedSphereAABB(dag::Entity const& a, dag::Entity const& b)
{
    // get the relevant data for the entity
    auto& collisionSphere = coordinator->getComponent<dag::CollisionSphere>(a);
    auto& transform = coordinator->getComponent<dag::Transform>(a);
    glm::vec3 centerInWorld = transform.getModelMatrix() * glm::vec4(collisionSphere.center, 1.0f);

    auto& otherCollisionAABB = coordinator->getComponent<dag::CollisionAABB>(b);
    auto& otherTransform = coordinator->getComponent<dag::Transform>(b);
    glm::vec3 otherCenterInWorld = otherTransform.getModelMatrix() * glm::vec4(otherCollisionAABB.center, 1.0f);
    float oMinX = otherCenterInWorld.x - (otherCollisionAABB.width / 2.0f);
    float oMinY = otherCenterInWorld.y - (otherCollisionAABB.height / 2.0f);
    float oMinZ = otherCenterInWorld.z - (otherCollisionAABB.depth / 2.0f);
    float oMaxX = otherCenterInWorld.x + (otherCollisionAABB.width / 2.0f);
    float oMaxY = otherCenterInWorld.y + (otherCollisionAABB.height / 2.0f);
    float oMaxZ = otherCenterInWorld.z + (otherCollisionAABB.depth / 2.0f);

    // calculate AABB info (center, half-extents)
    glm::vec3 aabb_half_extents(otherCollisionAABB.width / 2.0f, otherCollisionAABB.height / 2.0f, otherCollisionAABB.depth / 2.0f);

    // get difference vector between both centers
    glm::vec3 difference = centerInWorld - otherCenterInWorld;
    glm::vec3 clamped = glm::clamp(difference, -aabb_half_extents, aabb_half_extents);
    // add clamped value to AABB_center and we get the value of box closest to circle
    glm::vec3 closest = otherCenterInWorld + clamped;
    // retrieve vector between center circle and closest point AABB and check if length <= radius
    difference = closest - centerInWorld;

    return glm::length(difference) < collisionSphere.radius;
}

void dag::PhysicsSystem::collisionResponseSphereAABB(dag::Entity const& a, dag::Entity const& b)
{
    // get the relevant data for the entity
    auto& collisionSphere = coordinator->getComponent<dag::CollisionSphere>(a);
    auto& transform = coordinator->getComponent<dag::Transform>(a);
    glm::vec3 centerInWorld = transform.getModelMatrix() * glm::vec4(collisionSphere.center, 1.0f);

    auto& otherCollisionAABB = coordinator->getComponent<dag::CollisionAABB>(b);
    auto& otherTransform = coordinator->getComponent<dag::Transform>(b);
    glm::vec3 otherCenterInWorld = otherTransform.getModelMatrix() * glm::vec4(otherCollisionAABB.center, 1.0f);
    float oMinX = otherCenterInWorld.x - (otherCollisionAABB.width / 2.0f);
    float oMinY = otherCenterInWorld.y - (otherCollisionAABB.height / 2.0f);
    float oMinZ = otherCenterInWorld.z - (otherCollisionAABB.depth / 2.0f);
    float oMaxX = otherCenterInWorld.x + (otherCollisionAABB.width / 2.0f);
    float oMaxY = otherCenterInWorld.y + (otherCollisionAABB.height / 2.0f);
    float oMaxZ = otherCenterInWorld.z + (otherCollisionAABB.depth / 2.0f);

    auto& rigidBody = coordinator->getComponent<dag::RigidBody>(a);
    auto& otherRigidBody = coordinator->getComponent<dag::RigidBody>(b);

    glm::vec3 unitCenterToCenter = glm::normalize((centerInWorld - otherCenterInWorld) / otherTransform.scale);

    float movableConstant = 1.0f;

    if (rigidBody.movable && otherRigidBody.movable)
    {
        movableConstant = 0.5f;
    }

    if (rigidBody.movable)
    {
        auto degreesFromForwardY = glm::dot(glm::vec3(0.0, 1.0, 0.0), unitCenterToCenter);
        auto degreesFromBackwardY = glm::dot(glm::vec3(0.0, -1.0, 0.0), unitCenterToCenter);
        auto degreesFromForwardX = glm::dot(glm::vec3(1.0, 0.0, 0.0), unitCenterToCenter);
        auto degreesFromBackwardX = glm::dot(glm::vec3(-1.0, 0.0, 0.0), unitCenterToCenter);
        auto degreesFromForwardZ = glm::dot(glm::vec3(0.0, 0.0, 1.0), unitCenterToCenter);
        auto degreesFromBackwardZ = glm::dot(glm::vec3(0.0, 0.0, -1.0), unitCenterToCenter);

        if (degreesFromForwardX >= glm::pi<float>() / 4.0f)
        {
            transform.position.x = oMaxX + collisionSphere.radius * movableConstant;
            rigidBody.externalForce.x *= -1 * rigidBody.plasticCoefficient;
            rigidBody.velocity.x *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromBackwardX >= glm::pi<float>() / 4.0f)
        {
            transform.position.x = oMinX - collisionSphere.radius * movableConstant;
            rigidBody.externalForce.x *= -1 * rigidBody.plasticCoefficient;
            rigidBody.velocity.x *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromForwardY >= glm::pi<float>() / 4.0f)
        {
            transform.position.y = oMaxY + collisionSphere.radius * movableConstant;
            rigidBody.externalForce.y *= -1 * rigidBody.plasticCoefficient;
            rigidBody.velocity.y *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromBackwardY >= glm::pi<float>() / 4.0f)
        {
            transform.position.y = oMinY - collisionSphere.radius * movableConstant;
            rigidBody.externalForce.y *= -1 * rigidBody.plasticCoefficient;
            rigidBody.velocity.y *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromForwardZ >= glm::pi<float>() / 4.0f)
        {
            transform.position.z = oMaxZ + collisionSphere.radius * movableConstant;
            rigidBody.externalForce.z *= -1 * rigidBody.plasticCoefficient;
            rigidBody.velocity.z *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromBackwardZ >= glm::pi<float>() / 4.0f)
        {
            transform.position.z = oMinZ - collisionSphere.radius * movableConstant;
            rigidBody.externalForce.z *= -1 * rigidBody.plasticCoefficient;
            rigidBody.velocity.z *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
        }
        transform.updateModelMatrix();
    }

    if (otherRigidBody.movable)
    {
        auto degreesFromForwardY = glm::dot(glm::vec3(0.0, 1.0, 0.0), unitCenterToCenter);
        auto degreesFromBackwardY = glm::dot(glm::vec3(0.0, -1.0, 0.0), unitCenterToCenter);
        auto degreesFromForwardX = glm::dot(glm::vec3(1.0, 0.0, 0.0), unitCenterToCenter);
        auto degreesFromBackwardX = glm::dot(glm::vec3(-1.0, 0.0, 0.0), unitCenterToCenter);
        auto degreesFromForwardZ = glm::dot(glm::vec3(0.0, 0.0, 1.0), unitCenterToCenter);
        auto degreesFromBackwardZ = glm::dot(glm::vec3(0.0, 0.0, -1.0), unitCenterToCenter);

        if (degreesFromForwardX >= glm::pi<float>() / 4.0f)
        {
            otherTransform.position.x = (centerInWorld.x + collisionSphere.radius + (otherCollisionAABB.width / 2.0f)) * movableConstant;
            otherRigidBody.externalForce.x *= -1 * otherRigidBody.plasticCoefficient;
            otherRigidBody.velocity.x *= -1 * otherRigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromBackwardX >= glm::pi<float>() / 4.0f)
        {
            otherTransform.position.x = (centerInWorld.x - collisionSphere.radius - (otherCollisionAABB.width / 2.0f)) * movableConstant;
            otherRigidBody.externalForce.x *= -1 * otherRigidBody.plasticCoefficient;
            otherRigidBody.velocity.x *= -1 * otherRigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromForwardY >= glm::pi<float>() / 4.0f)
        {
            otherTransform.position.y = (centerInWorld.y + collisionSphere.radius + (otherCollisionAABB.height / 2.0f)) * movableConstant;
            otherRigidBody.externalForce.y *= -1 * otherRigidBody.plasticCoefficient;
            otherRigidBody.velocity.y *= -1 * otherRigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromBackwardY >= glm::pi<float>() / 4.0f)
        {
            otherTransform.position.y = (centerInWorld.y - collisionSphere.radius - (otherCollisionAABB.height / 2.0f)) * movableConstant;
            otherRigidBody.externalForce.y *= -1 * otherRigidBody.plasticCoefficient;
            otherRigidBody.velocity.y *= -1 * otherRigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromForwardZ >= glm::pi<float>() / 4.0f)
        {
            otherTransform.position.z = (centerInWorld.z + collisionSphere.radius + (otherCollisionAABB.depth / 2.0f)) * movableConstant;
            otherRigidBody.externalForce.z *= -1 * otherRigidBody.plasticCoefficient;
            otherRigidBody.velocity.z *= -1 * otherRigidBody.plasticCoefficient * dispersionCoefficient;
        }
        else if (degreesFromBackwardZ >= glm::pi<float>() / 4.0f)
        {
            otherTransform.position.z = (centerInWorld.z - collisionSphere.radius - (otherCollisionAABB.depth / 2.0f)) * movableConstant;
            otherRigidBody.externalForce.z *= -1 * otherRigidBody.plasticCoefficient;
            otherRigidBody.velocity.z *= -1 * otherRigidBody.plasticCoefficient * dispersionCoefficient;
        }
        otherTransform.updateModelMatrix();
    }
}

void dag::PhysicsSystem::haveCollided(dag::Entity const& a, dag::Entity const& b)
{
    auto& aTransform = coordinator->getComponent<Transform>(a);
    auto& bTransform = coordinator->getComponent<Transform>(b);

    if (coordinator->hasComponent<PlayerController>(a))
    {
        glm::vec3 diff_vec = glm::normalize(glm::vec3(bTransform.position - aTransform.position)) / bTransform.scale;
        if (dot(glm::vec3(0.0, -1.0, 0.0), diff_vec) >= 0)
        {
            coordinator->getSystem<PlayerControllerSystem>()->isInAir = false;
        }
    }
    else if (coordinator->hasComponent<PlayerController>(b))
    {
        glm::vec3 diff_vec = glm::normalize(glm::vec3(aTransform.position - bTransform.position)) / aTransform.scale;
        if (dot(glm::vec3(0.0, -1.0, 0.0), diff_vec) >= 0)
        {
            coordinator->getSystem<PlayerControllerSystem>()->isInAir = false;
        }
    }
}

glm::vec3 dag::PhysicsSystem::haveCollidedVoxelSphere(dag::Entity const& b)
{
    auto const& radius = coordinator->getComponent<CollisionSphere>(b).radius;
    auto const& center = coordinator->getComponent<CollisionSphere>(b).center;
    auto const& pos = coordinator->getComponent<Transform>(b).position;

    auto const globalPos = center + pos;

    /* glm::vec3 testVal = glm::vec3(0.0);
    for (int r = 0; r <= floor(radius * sqrt(0.5)); r++) {
        int d = int(floor(sqrt(radius * radius - r * r)));
        testVal = glm::vec3(center.x - d, center.y + r, center.z + r);
        if (voxelManager->getVoxel(testVal).isActive())
        {
            return testVal;
        }
        if (voxelManager->getVoxel(glm::vec3(center.x + d, center.y + r, center.z + r)).isActive())
        {
            glm::vec3(center.x + d, center.y + r, center.z + r)
        }
        // draw tile (center.x - d, center.y - r, center.z + r)
        // draw tile (center.x + d, center.y - r, center.z + r)
        // draw tile (center.x + r, center.y - d, center.z + r)
        // draw tile (center.x + r, center.y + d, center.z + r)
        // draw tile (center.x - r, center.y - d, center.z + r)
        // draw tile (center.x - r, center.y + d, center.z + r)
        // draw tile (center.x - d, center.y + r, center.z + d)
        // draw tile (center.x + d, center.y + r, center.z + d)
        // draw tile (center.x - d, center.y - r, center.z + d)
        // draw tile (center.x + d, center.y - r, center.z + d)
        // draw tile (center.x + r, center.y - d, center.z + d)
        // draw tile (center.x + r, center.y + d, center.z + d)
        // draw tile (center.x - r, center.y - d, center.z + d)
        // draw tile (center.x - r, center.y + d, center.z + d)
    }*/

    float closestVox = std::numeric_limits<float>::max();
    auto returnVal = glm::vec3(std::numeric_limits<float>::min());

    auto debug_dir = glm::vec3();

    for (int x = -1; x <= 1; ++x)
    {
        for (int y = -1; y <= 1; ++y)
        {
            for (int z = -1; z <= 1; ++z)
            {
                auto newPos = globalPos + glm::vec3(x, y, z);

                float dist = glm::distance(glm::vec3(globalPos), voxelManager->getVoxelPosition(newPos));

                if (voxelManager->getVoxel(newPos).isActive() && dist < 1.0f)
                {
                    if (dist < closestVox)
                    {
                        closestVox = glm::distance(glm::vec3(globalPos), voxelManager->getVoxelPosition(newPos));
                        debug_dir = glm::vec3(x, y, z);
                        returnVal = newPos;
                    }
                }
            }
        }
    }

    if (debug_dir.x != 0)
    {
        std::cout << "debug_dir: "
                  << "X" << std::endl;
    }
    else if (debug_dir.y != 0)
    {
        std::cout << "debug_dir: "
                  << "Y" << std::endl;
    }
    else if (debug_dir.z != 0)
    {
        std::cout << "debug_dir: "
                  << "Z" << std::endl;
    }

    return returnVal;
}

glm::vec3 dag::PhysicsSystem::haveCollidedVoxelAABB(dag::Entity const& b)
{
    auto& collisionAABB = coordinator->getComponent<CollisionAABB>(b);
    auto& rigidBody = coordinator->getComponent<RigidBody>(b);
    auto& transform = coordinator->getComponent<dag::Transform>(b);
    glm::vec3 centerInWorld = transform.getModelMatrix() * glm::vec4(collisionAABB.center, 1.0f);
    float minX = centerInWorld.x - (collisionAABB.width / 2.0f);
    float minY = centerInWorld.y - (collisionAABB.height / 2.0f);
    float minZ = centerInWorld.z - (collisionAABB.depth / 2.0f);
    float maxX = centerInWorld.x + (collisionAABB.width / 2.0f);
    float maxY = centerInWorld.y + (collisionAABB.height / 2.0f);
    float maxZ = centerInWorld.z + (collisionAABB.depth / 2.0f);

    for (float currX = minX; currX <= glm::ceil(maxX); currX += 1)
    {
        for (float currZ = minZ; currZ <= glm::ceil(maxZ); currZ += 1)
        {
            std::array<glm::vec3, 2> minMaxPos = {glm::vec3(currX, minY, currZ), glm::vec3(currX, maxY, currZ)};

            for (auto& currPos : minMaxPos)
            {
                auto& currVoxel = voxelManager->getVoxel(currPos);

                if (currVoxel.isActive())
                {
                    return currPos;
                }
            }
        }
    }

    for (float currY = minY; currY <= glm::ceil(maxY); currY += 1)
    {
        for (float currZ = minZ; currZ <= glm::ceil(maxZ); currZ += 1)
        {
            std::array<glm::vec3, 2> minMaxPos = {glm::vec3(minX, currY, currZ), glm::vec3(maxX, currY, currZ)};

            for (auto& currPos : minMaxPos)
            {
                auto& currVoxel = voxelManager->getVoxel(currPos);

                if (currVoxel.isActive())
                {
                    return currPos;
                }
            }
        }
    }

    for (float currY = minY; currY <= glm::ceil(maxY); currY += 1)
    {
        for (float currX = minX; currX <= glm::ceil(maxX); currX += 1)
        {
            std::array<glm::vec3, 2> minMaxPos = {glm::vec3(currX, currY, minZ), glm::vec3(currX, currY, maxZ)};

            for (auto& currPos : minMaxPos)
            {
                auto& currVoxel = voxelManager->getVoxel(currPos);

                if (currVoxel.isActive())
                {
                    return currPos;
                }
            }
        }
    }

    return glm::vec3(std::numeric_limits<float>::min());
}

void dag::PhysicsSystem::collisionResponseVoxelSphere(dag::Entity const& b, glm::vec3& currPos)
{
    auto& collisionSphere = coordinator->getComponent<CollisionSphere>(b);
    auto& rigidBody = coordinator->getComponent<RigidBody>(b);
    auto& transform = coordinator->getComponent<dag::Transform>(b);
    glm::vec3 centerInWorld = transform.getModelMatrix() * glm::vec4(collisionSphere.center, 1.0f);
    float minX = centerInWorld.x - collisionSphere.radius;
    float minY = centerInWorld.y - collisionSphere.radius;
    float minZ = centerInWorld.z - collisionSphere.radius;
    float maxX = centerInWorld.x + collisionSphere.radius;
    float maxY = centerInWorld.y + collisionSphere.radius;
    float maxZ = centerInWorld.z + collisionSphere.radius;

    auto currVoxelPos = voxelManager->getVoxelPosition(currPos);

    float oMinX = currVoxelPos.x - 0.5;
    float oMinY = currVoxelPos.y - 0.5;
    float oMinZ = currVoxelPos.z - 0.5;
    float oMaxX = currVoxelPos.x + 0.5;
    float oMaxY = currVoxelPos.y + 0.5;
    float oMaxZ = currVoxelPos.z + 0.5;

    glm::vec3 unitCenterToCenter = glm::normalize(currVoxelPos - centerInWorld);

    auto degreesFromForwardY = glm::dot(glm::vec3(0.0, 1.0, 0.0), unitCenterToCenter);
    auto degreesFromBackwardY = glm::dot(glm::vec3(0.0, -1.0, 0.0), unitCenterToCenter);
    auto degreesFromForwardX = glm::dot(glm::vec3(1.0, 0.0, 0.0), unitCenterToCenter);
    auto degreesFromBackwardX = glm::dot(glm::vec3(-1.0, 0.0, 0.0), unitCenterToCenter);
    auto degreesFromForwardZ = glm::dot(glm::vec3(0.0, 0.0, 1.0), unitCenterToCenter);
    auto degreesFromBackwardZ = glm::dot(glm::vec3(0.0, 0.0, -1.0), unitCenterToCenter);

    if (degreesFromForwardX >= 1.0f / glm::sqrt(3.0f))
    {
        std::cout << "|_degreesFromForwardX: " << degreesFromForwardX << std::endl;
        transform.position.x -= (maxX - oMinX);
        rigidBody.externalForce.x *= -1 * rigidBody.plasticCoefficient;
        rigidBody.velocity.x *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
    }
    else if (degreesFromBackwardX >= 1.0f / glm::sqrt(3.0f))
    {
        std::cout << "|degreesFromBackwardX: " << degreesFromBackwardX << std::endl;
        transform.position.x += (oMaxX - minX);
        rigidBody.externalForce.x *= -1 * rigidBody.plasticCoefficient;
        rigidBody.velocity.x *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
    }
    else if (degreesFromForwardY >= 1.0f / glm::sqrt(3.0f))
    {
        std::cout << "|degreesFromForwardY: " << degreesFromForwardY << std::endl;
        transform.position.y -= (maxY - oMinY);
        rigidBody.externalForce.y *= -1 * rigidBody.plasticCoefficient;
        rigidBody.velocity.y *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
    }
    else if (degreesFromBackwardY >= 1.0f / glm::sqrt(3.0f))
    {
        std::cout << "|degreesFromBackwardY: " << degreesFromBackwardY << std::endl;
        transform.position.y += (oMaxY - minY);
        rigidBody.externalForce.y *= -1 * rigidBody.plasticCoefficient;
        rigidBody.velocity.y *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
    }
    else if (degreesFromForwardZ >= 1.0f / glm::sqrt(3.0f))
    {
        std::cout << "|degreesFromForwardZ: " << degreesFromForwardZ << std::endl;
        transform.position.z -= (maxZ - oMinZ);
        rigidBody.externalForce.z *= -1 * rigidBody.plasticCoefficient;
        rigidBody.velocity.z *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
    }
    else if (degreesFromBackwardZ >= 1.0f / glm::sqrt(3.0f))
    {
        std::cout << "|degreesFromBackwardZ: " << degreesFromBackwardZ << std::endl;
        transform.position.z += (oMaxZ - minZ);
        rigidBody.externalForce.z *= -1 * rigidBody.plasticCoefficient;
        rigidBody.velocity.z *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
    }
    transform.updateModelMatrix();
}

void dag::PhysicsSystem::collisionResponseVoxelAABB(dag::Entity const& b, glm::vec3& currPos)
{
    auto& collisionAABB = coordinator->getComponent<CollisionAABB>(b);
    auto& rigidBody = coordinator->getComponent<RigidBody>(b);
    auto& transform = coordinator->getComponent<dag::Transform>(b);
    glm::vec3 centerInWorld = transform.getModelMatrix() * glm::vec4(collisionAABB.center, 1.0f);
    float minX = centerInWorld.x - (collisionAABB.width / 2.0f);
    float minY = centerInWorld.y - (collisionAABB.height / 2.0f);
    float minZ = centerInWorld.z - (collisionAABB.depth / 2.0f);
    float maxX = centerInWorld.x + (collisionAABB.width / 2.0f);
    float maxY = centerInWorld.y + (collisionAABB.height / 2.0f);
    float maxZ = centerInWorld.z + (collisionAABB.depth / 2.0f);

    auto currVoxelPos = voxelManager->getVoxelPosition(currPos);

    float oMinX = currVoxelPos.x - 0.5;
    float oMinY = currVoxelPos.y - 0.5;
    float oMinZ = currVoxelPos.z - 0.5;
    float oMaxX = currVoxelPos.x + 0.5;
    float oMaxY = currVoxelPos.y + 0.5;
    float oMaxZ = currVoxelPos.z + 0.5;

    glm::vec3 unitCenterToCenter = glm::normalize(currVoxelPos - centerInWorld);

    auto degreesFromForwardY = glm::dot(glm::vec3(0.0, 1.0, 0.0), unitCenterToCenter);
    auto degreesFromBackwardY = glm::dot(glm::vec3(0.0, -1.0, 0.0), unitCenterToCenter);
    auto degreesFromForwardX = glm::dot(glm::vec3(1.0, 0.0, 0.0), unitCenterToCenter);
    auto degreesFromBackwardX = glm::dot(glm::vec3(-1.0, 0.0, 0.0), unitCenterToCenter);
    auto degreesFromForwardZ = glm::dot(glm::vec3(0.0, 0.0, 1.0), unitCenterToCenter);
    auto degreesFromBackwardZ = glm::dot(glm::vec3(0.0, 0.0, -1.0), unitCenterToCenter);

    if (degreesFromForwardX >= glm::pi<float>() / 4.0f)
    {
        transform.position.x -= (maxX - oMinX);
        transform.position.y += 1;
        rigidBody.externalForce.x *= -1 * rigidBody.plasticCoefficient;
        rigidBody.velocity.x *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
    }
    else if (degreesFromBackwardX >= glm::pi<float>() / 4.0f)
    {
        transform.position.x += (oMaxX - minX);
        transform.position.y += 1;
        rigidBody.externalForce.x *= -1 * rigidBody.plasticCoefficient;
        rigidBody.velocity.x *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
    }
    else if (degreesFromForwardY >= glm::pi<float>() / 4.0f)
    {
        transform.position.y -= (maxY - oMinY);
        rigidBody.externalForce.y *= -1 * rigidBody.plasticCoefficient;
        rigidBody.velocity.y *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
    }
    else if (degreesFromBackwardY >= glm::pi<float>() / 4.0f)
    {
        transform.position.y += (oMaxY - minY);
        rigidBody.externalForce.y *= -1 * rigidBody.plasticCoefficient;
        rigidBody.velocity.y *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
    }
    else if (degreesFromForwardZ >= glm::pi<float>() / 4.0f)
    {
        transform.position.z -= (maxZ - oMinZ);
        transform.position.y += 1;
        rigidBody.externalForce.z *= -1 * rigidBody.plasticCoefficient;
        rigidBody.velocity.z *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
    }
    else if (degreesFromBackwardZ >= glm::pi<float>() / 4.0f)
    {
        transform.position.z += (oMaxZ - minZ);
        transform.position.y += 1;
        rigidBody.externalForce.z *= -1 * rigidBody.plasticCoefficient;
        rigidBody.velocity.z *= -1 * rigidBody.plasticCoefficient * dispersionCoefficient;
    }
    transform.updateModelMatrix();
}

/**
 * @brief Collision checking function for bounding spheres
  */
void dag::PhysicsSystem::checkForCollisions(std::set<dag::Entity>::iterator start_it, std::set<dag::Entity>::iterator end_it)
{
    // The voxel terrain should not be a rigid body
    if (voxelManager->currVoxelMainEntity != UINT32_MAX)
    {
        for (auto& it = start_it; it != end_it; ++it)
        {
            if (coordinator->hasComponent<RigidBody>(*it))
            {
                if (coordinator->getComponent<RigidBody>(*it).movable)
                {
                    if (coordinator->hasComponent<CollisionAABB>(*it))
                    {
                        auto collisionResult = haveCollidedVoxelAABB(*it);
                        if (collisionResult != glm::vec3(std::numeric_limits<float>::min()))
                        {
                            collisionResponseVoxelAABB(*it, collisionResult);

                            auto& eTrans = coordinator->getComponent<Transform>(*it);

                            glm::vec3 diff_vec = glm::normalize(glm::vec3(voxelManager->getVoxelPosition(collisionResult) - eTrans.position));
                            if (dot(glm::vec3(0.0, -1.0, 0.0), diff_vec) >= 0)
                            {
                                coordinator->getSystem<PlayerControllerSystem>()->isInAir = false;
                            }
                        }
                    }
                    else if (coordinator->hasComponent<CollisionSphere>(*it))
                    {
                        auto collisionResult = haveCollidedVoxelSphere(*it);
                        if (collisionResult != glm::vec3(std::numeric_limits<float>::min()))
                        {
                            collisionResponseVoxelSphere(*it, collisionResult);

                            auto& eTrans = coordinator->getComponent<Transform>(*it);
                        }
                    }
                }
            }
        }
    }

    // loop through all the unique pairs of entities
    for (auto& i = start_it; i != end_it; ++i)
    {
        if (coordinator->hasComponent<dag::CollisionSphere>(*i))
        {
            for (auto j = i; ++j != entities.end();)
            {
                if (coordinator->hasComponent<dag::CollisionSphere>(*j))
                {
                    // if the objects are closer than the sum of their radii, there is a collision
                    if (haveCollidedSphereSphere(*i, *j))
                    {
                        collisionResponseSphereSphere(*i, *j);
                        haveCollided(*i, *j);
                    }
                }
                else if (coordinator->hasComponent<dag::CollisionAABB>(*j))
                {
                    // if the objects are closer than the sum of their radii, there is a collision
                    if (haveCollidedSphereAABB(*i, *j))
                    {
                        collisionResponseSphereAABB(*i, *j);
                        haveCollided(*i, *j);
                    }
                }
            }
        }
        else if (coordinator->hasComponent<dag::CollisionAABB>(*i))
        {
            for (auto j = i; ++j != entities.end();)
            {
                if (coordinator->hasComponent<dag::CollisionSphere>(*j))
                {
                    // if the objects are closer than the sum of their radii, there is a collision
                    if (haveCollidedSphereAABB(*j, *i))
                    {
                        collisionResponseSphereAABB(*j, *i);
                        haveCollided(*i, *j);
                    }
                }
                else if (coordinator->hasComponent<dag::CollisionAABB>(*j))
                {
                    if (haveCollidedAABBAABB(*i, *j))
                    {
                        collisionResponseAABBAABB(*i, *j);
                        haveCollided(*i, *j);
                    }
                }
            }
        }
    }
}

/**
 * @brief Advance all the physics entities by timestep dt
  */
void dag::PhysicsSystem::update(float const& dt, std::set<dag::Entity>::iterator start_it, std::set<dag::Entity>::iterator end_it)
{
    for (auto& it = start_it; it != end_it; ++it)
    {
        // get relevant data about entity
        auto& transform = coordinator->getComponent<dag::Transform>(*it);

        auto& rigidBody = coordinator->getComponent<dag::RigidBody>(*it);

        // euler to move the system forward
        if (rigidBody.movable)
        {
            transform.setPosition(transform.position + rigidBody.velocity * dt);

            rigidBody.netForce = rigidBody.impulse + rigidBody.externalForce + airResistance * rigidBody.velocity + gravity;

            if (voxelManager->currVoxelMainEntity != UINT32_MAX)
            {
                if (voxelManager->getVoxel(coordinator->getComponent<Transform>(*it).position).blockType == dag::BlockTypes::Water)
                {
                    rigidBody.netForce += glm::vec3(0.0, 100.0, 0.0);
                    rigidBody.netForce *= 0.95;
                    rigidBody.velocity *= 0.95;
                }
            }

            rigidBody.acceleration = rigidBody.netForce * (1.0f / rigidBody.mass);

            rigidBody.velocity = rigidBody.velocity + rigidBody.acceleration * dt;
        }
    }
}