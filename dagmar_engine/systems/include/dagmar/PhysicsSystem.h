#pragma once

#include "dagmar/Components.h"
#include "dagmar/Coordinator.h"
#include "dagmar/Layer.h"
#include "dagmar/Voxel.h"
#include "dagmar/VoxelChunkManager.h"

#include <mutex>
#include <memory>
#include <thread>
#include <array>
#include <shared_mutex>

/**
 * @brief Physics system that deals with collision and applying forces
  */

namespace dag
{
    class PhysicsSystem : public System
    {
      public:
        std::shared_ptr<Coordinator> coordinator;
        glm::vec3 gravity;
        float airResistance;
        float epsilon;
        float dispersionCoefficient;
        float timeMultiplier;
        bool voxelCollision = true;

        std::shared_mutex physicsMutex;
        std::vector<std::thread> physicsThreads;
        std::size_t numThreads;
        std::atomic<bool> adjustingThreads = false;

        std::mutex debugging_output_mutex;

        // hack
        dag::GameState gameState = dag::GameState::Stop;

      public:
        PhysicsSystem();
        ~PhysicsSystem() override;

        void startup();

    	void updateLookupVectors();

        void createThreads(size_t numberOfThreads);
        void physicsThreadFunc(size_t threadNum);
        void update(float const& dt, std::set<dag::Entity>::iterator start_it, std::set<dag::Entity>::iterator end_it);
        void checkForCollisions(std::set<dag::Entity>::iterator start_it, std::set<dag::Entity>::iterator end_it);
        bool haveCollidedSphereSphere(dag::Entity const& a, dag::Entity const& b);
        void collisionResponseSphereSphere(dag::Entity const& a, dag::Entity const& b);
        bool haveCollidedAABBAABB(dag::Entity const& a, dag::Entity const& b);
        void collisionResponseAABBAABB(dag::Entity const& a, dag::Entity const& b);
        bool haveCollidedSphereAABB(dag::Entity const& a, dag::Entity const& b);
        void collisionResponseSphereAABB(dag::Entity const& a, dag::Entity const& b);
        void collisionResponseVoxelSphere(dag::Entity const& b, glm::vec3& currPos);
        void collisionResponseVoxelAABB(dag::Entity const& b, glm::vec3& currPos);
        glm::vec3 haveCollidedVoxelSphere(dag::Entity const& b);
        glm::vec3 haveCollidedVoxelAABB(dag::Entity const& b);
        void haveCollided(dag::Entity const& a, dag::Entity const& b);
    };
} // namespace dag
