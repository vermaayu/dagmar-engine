#pragma once
#include <memory>
#include <string>
#include <atomic>

#include "dagmar/Module.h"

namespace dag
{
    class SystemsManager : public dag::Module
    {
      private:
        std::atomic<bool> shuttingDown = false;

      public:
        // Should be implemented with contents that would
        // normally be in constructor
        void startup() override;

        // Should be implemented with contents that would
        // normally be in destructor
        void shutdown() override;

        // Implement a printable name for the manager in question
        std::string getName() const override;

        bool isShuttingDown();
    };

    inline std::shared_ptr<SystemsManager> systemsManager(new SystemsManager());
} // namespace dag
