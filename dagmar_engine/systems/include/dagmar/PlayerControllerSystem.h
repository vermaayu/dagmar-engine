#pragma once

#include "dagmar/Components.h"
#include "dagmar/Coordinator.h"

/**
 * @brief Player system
  */

namespace dag
{
    class PlayerControllerSystem : public System
    {
      public:
        std::shared_ptr<Coordinator> coordinator;
        Entity activePlayer;
        std::atomic<bool> isInAir = false;

      public:
        PlayerControllerSystem();
        ~PlayerControllerSystem() override;

        void startup();
        void setPossessed(Entity const& entity, bool active);
        
    };
} // namespace dag