#pragma once
#include "glm/glm.hpp"

#include "dagmar/Components.h"
#include "dagmar/Coordinator.h"

#include "dagmar/Log.h"

namespace dag
{
    /**
     * @brief Script System
     */
    class ScriptSystem : public System
    {
      public:
        
        std::shared_ptr<Coordinator> coordinator;

        // Reset is called when script is attached but the game is not on playmode/pauseMode
        bool onPlayMode = 0;
        // Update will call start if it's not been called already
        bool startFlag = 0;


      public:
        void startup();
        void shutdown();

        void save();
        void reset();
        void start();
        void update();
        void lateUpdate();
        void onPause();
    };
    
    //TODO: GameLayer
   /* class GameLayer : public Layer
    {
        onUpdate()
        {
            scriptSystem->update();
        }
    };*/

} // namespace dag