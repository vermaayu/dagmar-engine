#pragma once

#include "dagmar/Components.h"
#include "dagmar/Coordinator.h"

/**
 * @brief Camera system
  */

namespace dag
{
    class CameraSystem : public System
    {
      public:
        std::shared_ptr<Coordinator> coordinator;
        std::vector<Descriptor> descriptors;
        Entity editorCamera;
        Entity activeCamera;

      public:
        CameraSystem();
        ~CameraSystem() override;

        void startup();
        void setPossessed(Entity const& entity, bool active);

        void setEditorCamera(Entity const& entity);

        std::vector<Descriptor> const& getCurrentCamera();
    };
} // namespace dag