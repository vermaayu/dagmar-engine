#pragma once
#include "dagmar/Camera.h"
#include "dagmar/CameraSystem.h"
#include "dagmar/Components.h"
#include "dagmar/Coordinator.h"
#include "dagmar/Descriptor.h"
#include "dagmar/Framebuffer.h"
#include "dagmar/LightSystem.h"
#include "dagmar/ShaderProgram.h"
#include "dagmar/System.h"
#include "dagmar/UniformHandler.h"
#include "dagmar/VertexArrayObject.h"
#include <cstdint>
#include <functional>
#include <mutex>
#include <thread>
#include <vector>

#include "dagmar/Layer.h"
#include "dagmar/RenderPass.h"
#include "dagmar/Shader.h"
#include "dagmar/Texture2DArray.h"

namespace dag
{
    class RenderingSystem : public System
    {
      public:
        // Mainly used as a map
        enum UniformFuncs : size_t
        {
            Uniform1f = 0,
            Uniform1i = 1,
            Uniform1ui = 2,
            Uniform2fv = 3,
            Uniform3fv = 4,
            Uniform4fv = 5,
            Uniform2iv = 6,
            Uniform3iv = 7,
            Uniform4iv = 8,
            Uniform2uiv = 9,
            Uniform3uiv = 10,
            Uniform4uiv = 11,
            UniformMatrix4fv = 12
        };

      public:
        std::shared_ptr<Coordinator> coordinator;
        std::shared_ptr<CameraSystem> cameraSystem;
        std::shared_ptr<LightSystem> lightSystem;
        //std::shared_ptr<PhysicsSystem> physicsSystem;

        std::vector<std::pair<Entity, int32_t>> opaqueEntitiesToShapeID;
        std::vector<std::pair<Entity, int32_t>> transparentEntitiesToShapeID;

        MaterialComponent defaultMaterial;
        MeshRenderer defaultMesh;

        // Predefined order
        enum class VAONames : size_t
        {
            DefaultPlane = 0,
            DefaultLineBox,
            DefaultSkybox,
            DefaultSphereBounds
        };

        std::vector<VertexArrayObject<float, uint32_t>> vaos;
        std::vector<MeshRenderer> meshRenderers;

        enum class Shader : size_t
        {
            DefaultShader = 0,
            GridShader,
            CompositeShader,
            EncodingShader,
            OutlineShader,
            ShadowmapShader,
            ShadowmapLightingShader,
            LightFrustumShader,
            DeferredWriteShader,
            DeferredProcessShader,
            PostprocessBlurShader,
            PostprocessToneMappingShader,
            PostprocessChromaticAberrationShader,
            PostprocessFogShader,
            PostprocessSharpenShader,
            PostprocessPixelizeShader,
            PostprocessDilationShader,
            PostprocessDualFilterDownsampleShader,
            PostprocessDualFilterUpsampleShader,
            PostprocessBloomMaskShader,
            PostprocessBloomCombineShader,
            SkyboxShader,
            CameraFrustumShader,
            SphereBoundsShader,
            DebugForwardUnlitShader,
            DebugDeferredUnlitShader,
            DebugForwardNormalsShader,
            DebugDeferredNormalsShader,
            DebugForwardDepthShader,
            DebugDeferredDepthShader,
        };

        std::vector<UberShader> shaders;
        inline static uint32_t currentShaderBound;
        std::vector<std::function<void(Descriptor const&)>> shaderUpdateFuncs;

        enum class PostProcessFuncID : size_t
        {
            Fog = 0,
            Sharpen = 1,
            Blur = 2,
            ToneMapping = 3,
            ChromaticAberration = 4,
            Pixelize = 5,
            Dilation = 6,
            Bloom = 7
        };

        // Vector of indices of the post processing functions to execute
        std::vector<size_t> postProcessStack;
        std::vector<size_t> postProcessStackOrder = {
            static_cast<size_t>(PostProcessFuncID::Fog),
            static_cast<size_t>(PostProcessFuncID::Sharpen),
            static_cast<size_t>(PostProcessFuncID::Blur),
            static_cast<size_t>(PostProcessFuncID::ToneMapping),
            static_cast<size_t>(PostProcessFuncID::ChromaticAberration),
            static_cast<size_t>(PostProcessFuncID::Pixelize),
            static_cast<size_t>(PostProcessFuncID::Dilation),
            static_cast<size_t>(PostProcessFuncID::Bloom),

        };
        // Vector of functions to execute in post processing
        std::vector<std::function<void(RenderPass&, std::vector<Texture*> const&)>> postProcessFuncs;

        enum class OverlaysFuncID : size_t
        {
            Grid = 0,
            OutlineEntity,
            DirLightEntity,
            SpotLightEntity,
            PointLightEntity,
            CameraEntity,
            DebugColliders,
        	Skybox,
        	DeferredTransparentsRendering,
        	
        };

        std::vector<size_t> lateOverlaysStack;
        std::vector<size_t> lateOverlaysStackOrder = {
            static_cast<size_t>(OverlaysFuncID::Skybox),
            static_cast<size_t>(OverlaysFuncID::DeferredTransparentsRendering),
            static_cast<size_t>(OverlaysFuncID::Grid),
            static_cast<size_t>(OverlaysFuncID::OutlineEntity),
            static_cast<size_t>(OverlaysFuncID::DirLightEntity),
            static_cast<size_t>(OverlaysFuncID::SpotLightEntity),
            static_cast<size_t>(OverlaysFuncID::PointLightEntity),
            static_cast<size_t>(OverlaysFuncID::CameraEntity),
            static_cast<size_t>(OverlaysFuncID::DebugColliders),
        };
        std::vector<std::function<void()>> lateOverlayFuncs;

        // Framebuffers
        Framebuffer encoderFramebuffer;
        Framebuffer deferredFramebuffer;
        Framebuffer renderFramebuffer;
        Framebuffer lateForwardFramebuffer;
        Framebuffer compositeFramebuffer;
        Framebuffer cleanViewFramebuffer;
        Framebuffer backBufferFramebuffer;
        Framebuffer postProcessSwapFramebuffer;
        std::vector<Framebuffer> bloomScaledFramebuffers;

        Texture2D encoderTexture;
        Texture2D encoderDepthTexture;
        Texture2D compositeTexture;
        Texture2D lateFwdTexture;
        Texture2D lateFwdDepthTexture;
        Texture2D renderTexture;
        Texture2D renderDepthTexture;
        Texture2D cleanViewTexture;
        Texture2D cleanViewDepthTexture;
        Texture2D postProcessSwapTexture;
        Texture2D postProcessSwapDepthTexture;
        Texture2D deferredPositionsTexture;
        Texture2D deferredNormalsTexture;
        Texture2D deferredUVTexture;
        Texture2D deferredBaseColor;
        Texture2D deferredAmbient;
        Texture2D deferredDiffuse;
        Texture2D deferredSpecularWithExponentTexture;
        Texture2D deferredEmissive;
        Texture2D deferredDepth;

        int32_t bloomIterations = 4;
        std::vector<Texture2D> bloomTextures;

        Texture2DArray directionalSMTextures;
        Texture2DArray spotSMTextures;
        Texture2DArray pointSMTextures;
        Framebuffer directionalShadowMapFramebuffer;
        Framebuffer spotLightShadowMapFramebuffer;
        Framebuffer pointLightShadowMapFramebuffer;

        std::vector<std::function<void(RenderPass& renderPass)>> extRenderGraph;
        std::vector<RenderPass> renderPasses;

        UniformHandler uniformHandler;

        enum class UniformBufferNamesIDs : size_t
        {
            SceneData = 0,
            CameraData,
            LightData,
            RenderableEntitiesData,
            DrawData,
        };

        enum class RenderMode : size_t
        {
            ForwardRendering = 0,
            DeferredRendering
        };

        RenderMode currentRenderMode = RenderMode::ForwardRendering;

        enum class DebugRenderMode : size_t
        {
            None = 0,
            Wireframe,
            Unlit,
            Depth,
            Normals
        };

        DebugRenderMode currentDebugRenderMode = DebugRenderMode::None;

        // Framebuffer size related variables
        bool sizeDirty = false;
        uint32_t renderViewWidth = 0;
        uint32_t renderViewHeight = 0;

      private:
        std::mutex hotReloadShadersMutex;
        std::thread hotReloadShadersThread;
        std::shared_ptr<std::vector<size_t>> shadersToUpdate;

      public:
        RenderingSystem();
        ~RenderingSystem() override = default;

        void startup() override;
        void shutdown() override;
        void resizeViewFramebuffers();
        int32_t decodePixel(std::array<unsigned char, 4> const& pixelData);

        void createTextures();
        void setRenderingMode(RenderMode const& mode, bool const& composeInBackbuffer = false, bool const& isEditor = true);
        void setDebugMode(DebugRenderMode const& mode, bool const& composeInBackbuffer = false, bool const& isEditor = true);

        void categorizeGeometry();
        void sortTransparentGeometry();

        void renderSkybox();
        void renderGrid();
        void renderEntityShape(Entity const& entity, int32_t const& shapeID);
        void renderEntityShape(Entity const& entity, int32_t const& shapeID, Shader const& shader);
        void renderEntityShape(Entity const& entity, int32_t const& shapeID, Shader const& shader, bool const& textured);
    	
        void outlineEntity(Entity const& entityID);
        void renderEntity(Entity const& entityID);
        void renderEntity(Entity const& entityID, Shader const& shader);
        void renderEntity(Entity const& entityID, Shader const& shader, bool const& textured);
        void renderEntities();
        void renderTransparentEntities();
    	
        void renderOpaqueEntities();
        void renderOpaqueEntities(Shader const& shader);
        void renderOpaqueEntities(Shader const& shader, bool const& textured);
    	
        void renderEntities(Shader const& shader);
        void renderEntities(Shader const& shader, bool const& textured);
        void renderShaderFullscreen(Shader const& shaderID);
        void renderDebugPhysicsColliders();
        void update();
        void updateBuffers();

        void renderIDs();

        void setSizeDirty(uint32_t const& width, uint32_t const& height);
        void drawLightOutline(dag::Entity const& entityID);
        void drawCameraOutline(dag::Entity const& entityID);

        void updateShaders();
        void enqueuePostProcessEffect(PostProcessFuncID const& postProcessFuncID);
        void dequeuePostProcessEffect(PostProcessFuncID const& postProcessFuncID);

        void enqueueLateOverlays(OverlaysFuncID const& overlaysFuncID);
        void dequeueLateOverlays(OverlaysFuncID const& overlaysFuncID);

        void dequeueLateOverlaysEntity();
        void enqueueLateOverlaysEntity();

        void updatePostProcessData(Volume const& volume);

        int32_t getEntityOffset(Entity const& entity);

      private:
        // Renderpasses functions
        void execEncoderPass(RenderPass& renderPass);
        void execDirShadowAccumulationPass(RenderPass& renderPass);
        void execSpotShadowAccumulationPass(RenderPass& renderPass);
        void execPointShadowAccumulationPass(RenderPass& renderPass);
        void execForwardPass(RenderPass& renderPass);
        void execDeferredWritePass(RenderPass& renderPass);
        void execDeferredProcessPass(RenderPass& renderPass);
        void execPostProcessStack(RenderPass& renderPass);
        void execLateForwardPass(RenderPass& renderPass);
        void execCompositionPass(RenderPass& renderPass);

        void execLateSkyboxRender();
        void execLateTransparentsRender();
        void execLateOverlayGrid();
        void execLateOverlayDirLightOutline();
        void execLateOverlaySpotLightOutline();
        void execLateOverlayPointLightOutline();
        void execLateOverlayCameraOutline();
        void execLateOverlayEntityOutline();
        void execLateOverlayDebugPhysicsColliders();

        void execDebugRenderEntitiesWireframeForward(RenderPass& renderpass);
        void execDebugRenderEntitiesWireframeDeferredWrite(RenderPass& renderpass);
        void execDebugRenderEntitiesWireframeDeferredProcess(RenderPass& renderpass);
        void execDebugRenderEntitiesDepthForward(RenderPass& renderpass);
        void execDebugRenderEntitiesDepthDeferred(RenderPass& renderpass);
        void execDebugRenderEntitiesNormalsForward(RenderPass& renderpass);
        void execDebugRenderEntitiesNormalsDeferred(RenderPass& renderpass);
        void execDebugRenderEntitiesUnlitForward(RenderPass& renderpass);
        void execDebugRenderEntitiesUnlitDeferred(RenderPass& renderpass);

        void enqueueShaderReload(size_t const& shaderID);
        void setWireframe(bool const& value);

        // Postprocess
        void ppFog(RenderPass& renderpass, std::vector<Texture*> const& attachments);
        void ppSharpen(RenderPass& renderpass, std::vector<Texture*> const& attachments);
        void ppBlur(RenderPass& renderpass, std::vector<Texture*> const& attachments);
        void ppToneMapping(RenderPass& renderpass, std::vector<Texture*> const& attachments);
        void ppChromaticAberration(RenderPass& renderpass, std::vector<Texture*> const& attachments);
        void ppPixelize(RenderPass& renderpass, std::vector<Texture*> const& attachments);
        void ppDilation(RenderPass& renderpass, std::vector<Texture*> const& attachments);
        void ppBloom(RenderPass& renderpass, std::vector<Texture*> const& attachments);

      private:
        static void wrapUniform1f(Descriptor const& descriptor);
        static void wrapUniform1i(Descriptor const& descriptor);
        static void wrapUniform1ui(Descriptor const& descriptor);
        static void wrapUniform2fv(Descriptor const& descriptor);
        static void wrapUniform3fv(Descriptor const& descriptor);
        static void wrapUniform4fv(Descriptor const& descriptor);
        static void wrapUniform2iv(Descriptor const& descriptor);
        static void wrapUniform3iv(Descriptor const& descriptor);
        static void wrapUniform4iv(Descriptor const& descriptor);
        static void wrapUniform2uiv(Descriptor const& descriptor);
        static void wrapUniform3uiv(Descriptor const& descriptor);
        static void wrapUniform4uiv(Descriptor const& descriptor);
        static void wrapUniformMatrix4fv(Descriptor const& descriptor);
    };
} // namespace dag
