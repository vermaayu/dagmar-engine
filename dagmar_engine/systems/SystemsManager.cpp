#include "dagmar/SystemsManager.h"

#include "dagmar/Coordinator.h"
#include "dagmar/PhysicsSystem.h"
#include "dagmar/RenderingSystem.h"
#include "dagmar/ScriptSystem.h"
#include "dagmar/PlayerControllerSystem.h"
#include "dagmar/VoxelChunkManager.h"
#include "dagmar/Scene.h"

void dag::SystemsManager::startup()
{
    auto& ecsCoord = dag::coordinator;

    ecsCoord->registerComponent<MaterialComponent>();
    ecsCoord->registerComponent<VoxelComponent>();

    auto renderingSystem = ecsCoord->registerSystem<dag::RenderingSystem>();
    renderingSystem->coordinator = ecsCoord;
    renderingSystem->startup();

    auto physicsSystem = ecsCoord->registerSystem<dag::PhysicsSystem>();
    physicsSystem->coordinator = ecsCoord;
    physicsSystem->startup();

    auto cameraSystem = ecsCoord->registerSystem<dag::CameraSystem>();
    cameraSystem->coordinator = ecsCoord;
    cameraSystem->startup();

    auto lightSystem = ecsCoord->registerSystem<dag::LightSystem>();
    lightSystem->coordinator = ecsCoord;
    lightSystem->startup();

    auto scriptSystem = ecsCoord->registerSystem<dag::ScriptSystem>();
    scriptSystem->coordinator = ecsCoord;
    scriptSystem->startup();

    auto playerControllerSystem = ecsCoord->registerSystem<dag::PlayerControllerSystem>();
    playerControllerSystem->coordinator = ecsCoord;
    playerControllerSystem->startup();

    renderingSystem->cameraSystem = cameraSystem;
    renderingSystem->lightSystem = lightSystem;

    voxelManager->renderingSystem = renderingSystem;

    scene->coordinator = ecsCoord;
    ecsCoord->registerComponent<Name>();
}

void dag::SystemsManager::shutdown()
{
    shuttingDown = true;

    dag::coordinator->getSystem<LightSystem>()->shutdown();
    dag::coordinator->getSystem<CameraSystem>()->shutdown();
    dag::coordinator->getSystem<PhysicsSystem>()->shutdown();
    dag::coordinator->getSystem<RenderingSystem>()->shutdown();
}

std::string dag::SystemsManager::getName() const
{
    return "SystemsManager";
}


/**
 * @brief Returns atomically whether this will shut down
 */
bool dag::SystemsManager::isShuttingDown()
{
    return shuttingDown.load();
}
