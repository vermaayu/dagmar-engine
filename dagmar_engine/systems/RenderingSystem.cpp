#include "dagmar/RenderingSystem.h"

#include <mutex>

#include "dagmar/Components.h"
#include "dagmar/Descriptor.h"
#include "dagmar/KeyManager.h"
#include "dagmar/ResourceManager.h"

#include "dagmar/FileSystem.h"
#include "glm/gtc/type_ptr.hpp"

#include "dagmar/SystemsManager.h"

#include <algorithm>

#include "dagmar/DODBlocks.h"
#include "dagmar/PhysicsSystem.h"
#include "dagmar/RenderPass.h"
#include "dagmar/Scene.h"

#define BIND_PP_FN(fn) [this](RenderPass& r, std::vector<Texture*> const& v) { fn(r, v); }
#define BIND_FN(fn) [this]() { fn(); }
#define BIND_EXT_RENDER_GRAPH_FN(fn) [this](RenderPass& rp) { fn(rp); }

/**
 * @brief Default rendering system constructor
 */
dag::RenderingSystem::RenderingSystem()
{
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform1f);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform1i);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform1ui);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform2fv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform3fv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform4fv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform2iv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform3iv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform4iv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform2uiv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform3uiv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniform4uiv);
    shaderUpdateFuncs.emplace_back(RenderingSystem::wrapUniformMatrix4fv);

    // The post process stack order and funcs should match
    postProcessFuncs.emplace_back(BIND_PP_FN(ppFog));
    postProcessFuncs.emplace_back(BIND_PP_FN(ppSharpen));
    postProcessFuncs.emplace_back(BIND_PP_FN(ppBlur));
    postProcessFuncs.emplace_back(BIND_PP_FN(ppToneMapping));
    postProcessFuncs.emplace_back(BIND_PP_FN(ppChromaticAberration));
    postProcessFuncs.emplace_back(BIND_PP_FN(ppPixelize));
    postProcessFuncs.emplace_back(BIND_PP_FN(ppDilation));
    postProcessFuncs.emplace_back(BIND_PP_FN(ppBloom));

    lateOverlayFuncs.emplace_back(BIND_FN(execLateOverlayGrid));
    lateOverlayFuncs.emplace_back(BIND_FN(execLateOverlayEntityOutline));
    lateOverlayFuncs.emplace_back(BIND_FN(execLateOverlayDirLightOutline));
    lateOverlayFuncs.emplace_back(BIND_FN(execLateOverlaySpotLightOutline));
    lateOverlayFuncs.emplace_back(BIND_FN(execLateOverlayPointLightOutline));
    lateOverlayFuncs.emplace_back(BIND_FN(execLateOverlayCameraOutline));
    lateOverlayFuncs.emplace_back(BIND_FN(execLateOverlayDebugPhysicsColliders));
    lateOverlayFuncs.emplace_back(BIND_FN(execLateSkyboxRender));
    lateOverlayFuncs.emplace_back(BIND_FN(execLateTransparentsRender));
}

/**
 * @brief Startup func
 */
void dag::RenderingSystem::startup()
{
    /*
    glDebugMessageCallback([](GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* msg, const void* data) {
        Log::error(msg);
    }, nullptr);
     */

    int32_t minUBOAlignment;
    glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &minUBOAlignment);
    EngineLimits::minimumUniformBufferOffsetAlignment = minUBOAlignment;

    currentShaderBound = 0;
    shadersToUpdate = std::make_shared<std::vector<size_t>>();

    coordinator->registerComponent<dag::Transform>();
    coordinator->registerComponent<dag::DescriptorSet>();
    coordinator->registerComponent<dag::MeshRenderer>();
    coordinator->registerComponent<dag::Volume>();

    dag::Signature renderingSystemSignature;
    renderingSystemSignature.set(coordinator->getComponentType<dag::Transform>());
    renderingSystemSignature.set(coordinator->getComponentType<dag::DescriptorSet>());
    renderingSystemSignature.set(coordinator->getComponentType<dag::MeshRenderer>());

    coordinator->setSystemSignature<RenderingSystem>(renderingSystemSignature);

    sizeDirty = false;
    renderViewWidth = 1024;
    renderViewHeight = 768;

    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/default/default.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/grid/grid.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/screen_shader/screen_shader.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/encoder/encoder.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/outline/outline.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/shadowmap/shadowmap.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/shadowmap/shaded.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/light_frustum/light_frustum.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/deferred/deferred_write.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/deferred/deferred_process.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/postprocess/box_blur.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/postprocess/tone_map.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/postprocess/chromatic_aberration.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/postprocess/fog.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/postprocess/sharpen.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/postprocess/pixelize.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/postprocess/dilation.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/postprocess/dual_filter_blur_downsample.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/postprocess/dual_filter_blur_upsample.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/postprocess/bloom_mask.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/postprocess/bloom_combine.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/skybox/skybox.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/camera_frustum/camera_frustum.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/debug/bounds.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/debug/forward_unlit.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/debug/deferred_unlit.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/debug/forward_normals.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/debug/deferred_normals.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/debug/forward_depth.glsl"));
    shaders.emplace_back(UberShader(dag::filesystem::getAssetsPath() / "shaders/debug/deferred_depth.glsl"));

    // TODO Move this async
    hotReloadShadersThread = std::thread([&]() {
        while (!systemsManager->isShuttingDown())
        {
            // Iterate through all of the include paths
            for (size_t i = 0; i < ShaderData::cachedPaths.size(); i++)
            {
                auto newTime = std::filesystem::last_write_time(ShaderData::cachedPaths[i]);
                if (newTime != ShaderData::cachedLastWriteTimes[i])
                {
                    ShaderData::cachedLastWriteTimes[i] = newTime;

                    for (size_t j = 0; j < shaders.size(); j++)
                    {
                        if (shaders[j].hasInclude(i))
                        {
                            enqueueShaderReload(j);
                            Log::debug("Queued shader ", j);
                        }
                    }
                }
            }

            for (size_t i = 0; i < shaders.size(); i++)
            {
                auto newTime = std::filesystem::last_write_time(shaders[i].path);

                if (newTime != shaders[i].lastWriteTime)
                {
                    Log::debug("\nShader path", shaders[i].path);
                    Log::debug("Changed? time", newTime != shaders[i].lastWriteTime);
                    shaders[i].lastWriteTime = newTime;
                    enqueueShaderReload(i);
                    Log::debug("Queued shader ", i);
                }
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
    });

    { // Scene data buffer

        SceneData data{};
        Log::debug("Size of SCENE_DATA: ", SceneData::size());

        uniformHandler.ubSceneData = UniformBuffer<SceneData>({data});
        uniformHandler.ubSceneData.generate();
        uniformHandler.ubSceneData.bind();
        uniformHandler.ubSceneData.load(GL_DYNAMIC_DRAW);
        uniformHandler.ubSceneData.unbind();

        uniformHandler.ubSceneData.bindBufferBase(SceneData::index);
    }

    { // Camera data buffer

        CameraData data{};
        Log::debug("Size of CAMERA_DATA: ", CameraData::size());

        uniformHandler.ubCameraData = UniformBuffer<CameraData>({data});
        uniformHandler.ubCameraData.generate();
        uniformHandler.ubCameraData.bind();
        uniformHandler.ubCameraData.load(GL_DYNAMIC_DRAW);
        uniformHandler.ubCameraData.unbind();
        uniformHandler.ubCameraData.bindBufferBase(CameraData::index);
    }

    { // Light data buffer

        LightData data{};
        Log::debug("Size of LIGHT_DATA: ", LightData::size());

        uniformHandler.ubLightData = UniformBuffer<LightData>({data});
        uniformHandler.ubLightData.generate();
        uniformHandler.ubLightData.bind();
        uniformHandler.ubLightData.load(GL_DYNAMIC_DRAW);
        uniformHandler.ubLightData.unbind();

        uniformHandler.ubLightData.bindBufferBase(LightData::index);
    }

    { // Material data buffer

        MaterialData data{};
        Log::debug("Size of LIGHT_DATA: ", MaterialData::size());

        uniformHandler.ubMaterialData = UniformBuffer<MaterialData>({data});
        uniformHandler.ubMaterialData.generate();
        uniformHandler.ubMaterialData.bind();
        uniformHandler.ubMaterialData.load(GL_DYNAMIC_DRAW);
        uniformHandler.ubMaterialData.unbind();

        uniformHandler.ubMaterialData.bindBufferBase(MaterialData::index);
    }

    { // Entities transform buffer

        EntitiesData data{};
        Log::debug("Size of RE_DATA: ", EntitiesData::size());

        uniformHandler.ubEntitiesData = UniformBuffer<EntitiesData>({data});
        uniformHandler.ubEntitiesData.generate();
        uniformHandler.ubEntitiesData.bind();
        uniformHandler.ubEntitiesData.load(GL_DYNAMIC_DRAW);
        uniformHandler.ubEntitiesData.unbind();

        uniformHandler.ubEntitiesData.bindBufferBase(EntitiesData::index);
    }

    { // Draw data buffer
        uniformHandler.ubDrawData.data = new uint8_t[DrawData::size() * EngineLimits::MAX_ENTITIES];
        uniformHandler.ubDrawData.size = DrawData::size() * EngineLimits::MAX_ENTITIES;
        Log::debug("Size of Draw DATA: ", DrawData::size());

        for (int32_t i = 0; i < EngineLimits::MAX_ENTITIES; i++)
        {
            int r = (i & 0x000000FF) >> 0;
            int g = (i & 0x0000FF00) >> 8;
            int b = (i & 0x00FF0000) >> 16;

            auto id = glm::vec4(r / 255.0f, g / 255.0f, b / 255.0f, 1.0f);

            DrawData dd{.entityID = i, .entityMaterial = 0, .encodedID = glm::vec4(id.x, id.y, id.z, id.w)};

            std::memcpy(static_cast<uint8_t*>(uniformHandler.ubDrawData.data) + i * DrawData::size(), &dd, sizeof(DrawData));
        }

        uniformHandler.ubDrawData.generate();
        uniformHandler.ubDrawData.bind();
        uniformHandler.ubDrawData.load(GL_DYNAMIC_DRAW);
        uniformHandler.ubDrawData.unbind();
    }

    { // Light draw data buffer
        uniformHandler.ubLightDrawData.data = new uint8_t[LightDrawData::size() * EngineLimits::MAX_LIGHTS_WITH_ADDITIONAL_FACES];
        uniformHandler.ubLightDrawData.size = LightDrawData::size() * EngineLimits::MAX_LIGHTS_WITH_ADDITIONAL_FACES;

        for (int32_t i = 0; i < EngineLimits::MAX_LIGHTS_WITH_ADDITIONAL_FACES; i++)
        {
            LightDrawData dd{.lightEntityID = i};

            std::memcpy(static_cast<uint8_t*>(uniformHandler.ubLightDrawData.data) + i * LightDrawData::size(), &dd, sizeof(LightDrawData));
        }

        Log::debug("Size of DRAW_DATA: ", sizeof(LightDrawData));

        uniformHandler.ubLightDrawData.generate();
        uniformHandler.ubLightDrawData.bind();
        uniformHandler.ubLightDrawData.load(GL_DYNAMIC_DRAW);
        uniformHandler.ubLightDrawData.unbind();
    }

    // Post process data creation
    {

        PostProcessBloomSettings data{};
        Log::debug("Size of PP Bloom Data: ", PostProcessBloomSettings::size());

        uniformHandler.ubPPBloomData = UniformBuffer<PostProcessBloomSettings>({data});
        uniformHandler.ubPPBloomData.generate();
        uniformHandler.ubPPBloomData.bind();
        uniformHandler.ubPPBloomData.load(GL_DYNAMIC_DRAW);
        uniformHandler.ubPPBloomData.unbind();
    }

    { // Fog

        PostProcessFog data{};
        Log::debug("Size of PostProcessFog Data: ", PostProcessFog::size());

        uniformHandler.ubPPFogData = UniformBuffer<PostProcessFog>({data});
        uniformHandler.ubPPFogData.generate();
        uniformHandler.ubPPFogData.bind();
        uniformHandler.ubPPFogData.load(GL_DYNAMIC_DRAW);
        uniformHandler.ubPPFogData.unbind();
    }

    { // Blur

        PostProcessBlur data{};
        Log::debug("Size of PostProcessBlur Data: ", PostProcessBlur::size());

        uniformHandler.ubPPBlurData = UniformBuffer<PostProcessBlur>({data});
        uniformHandler.ubPPBlurData.generate();
        uniformHandler.ubPPBlurData.bind();
        uniformHandler.ubPPBlurData.load(GL_DYNAMIC_DRAW);
        uniformHandler.ubPPBlurData.unbind();
    }

    {

        PostProcessChromaticAberration data{};
        Log::debug("Size of PP PostProcessChromaticAberration Data: ", PostProcessChromaticAberration::size());

        uniformHandler.ubPPChromaticAberrationData = UniformBuffer<PostProcessChromaticAberration>({data});
        uniformHandler.ubPPChromaticAberrationData.generate();
        uniformHandler.ubPPChromaticAberrationData.bind();
        uniformHandler.ubPPChromaticAberrationData.load(GL_DYNAMIC_DRAW);
        uniformHandler.ubPPChromaticAberrationData.unbind();
    }

    {

        PostProcessDilation data{};
        Log::debug("Size of PP PostProcessDilation Data: ", PostProcessDilation::size());

        uniformHandler.ubPPDilationData = UniformBuffer<PostProcessDilation>({data});
        uniformHandler.ubPPDilationData.generate();
        uniformHandler.ubPPDilationData.bind();
        uniformHandler.ubPPDilationData.load(GL_DYNAMIC_DRAW);
        uniformHandler.ubPPDilationData.unbind();
    }

    {

        PostProcessPixelize data{};
        Log::debug("Size of PP PostProcessPixelize Data: ", PostProcessPixelize::size());

        uniformHandler.ubPPPixelizeData = UniformBuffer<PostProcessPixelize>({data});
        uniformHandler.ubPPPixelizeData.generate();
        uniformHandler.ubPPPixelizeData.bind();
        uniformHandler.ubPPPixelizeData.load(GL_DYNAMIC_DRAW);
        uniformHandler.ubPPPixelizeData.unbind();
    }

    {

        PostProcessSharpen data{};
        Log::debug("Size of PP PostProcessSharpen Data: ", PostProcessSharpen::size());

        uniformHandler.ubPPSharpenData = UniformBuffer<PostProcessSharpen>({data});
        uniformHandler.ubPPSharpenData.generate();
        uniformHandler.ubPPSharpenData.bind();
        uniformHandler.ubPPSharpenData.load(GL_DYNAMIC_DRAW);
        uniformHandler.ubPPSharpenData.unbind();
    }

    renderViewWidth = 1024;
    renderViewHeight = 768;

    createTextures();
}

void dag::RenderingSystem::shutdown()
{
    hotReloadShadersThread.join();

    encoderFramebuffer.destroy();

    deferredFramebuffer.destroy();

    renderFramebuffer.destroy();
    lateForwardFramebuffer.destroy();
    compositeFramebuffer.destroy();

    cleanViewFramebuffer.destroy();
    // no need to destroy the backbuffer
    postProcessSwapFramebuffer.destroy();

    for (auto& fb : bloomScaledFramebuffers)
    {
        fb.destroy();
    }

    // Destroys the used shader modules
    for (auto& shader : shaders)
    {
        shader.destroy();
    }
}

void dag::RenderingSystem::createTextures()
{
    TextureInfo const screenTextureInfo{.width = 1024, .height = 1024, .depth = 1, .samples = 1, .arrayLayers = 1, .format = TextureFormat::RGB8, .usage = TextureInfo::TypeBitsFlags::COLOR};
    TextureInfo const screenTextureInfoMS{.width = 1024, .height = 1024, .depth = 1, .samples = 1, .arrayLayers = 1, .format = TextureFormat::RGB8, .usage = TextureInfo::TypeBitsFlags::COLOR};
    TextureInfo const screenTextureDepthInfo{.width = 1024, .height = 1024, .depth = 1, .samples = 1, .arrayLayers = 1, .format = TextureFormat::DEPTH24STENCIL8, .usage = TextureInfo::TypeBitsFlags::DEPTH | TextureInfo::TypeBitsFlags::STENCIL};

    TextureInfo const deferred16Bit{.width = 1024, .height = 1024, .depth = 1, .samples = 1, .arrayLayers = 1, .format = TextureFormat::RGBA16F, .usage = TextureInfo::TypeBitsFlags::COLOR};
    TextureInfo const deferred8Bit{.width = 1024, .height = 1024, .depth = 1, .samples = 1, .arrayLayers = 1, .format = TextureFormat::RGBA8, .usage = TextureInfo::TypeBitsFlags::COLOR};

    // Initializing textures and framebuffers
    //
    encoderTexture.create(screenTextureInfo);
    encoderDepthTexture.create(screenTextureDepthInfo);
    encoderFramebuffer.create(encoderTexture.info.width, encoderTexture.info.height);
    encoderFramebuffer.attachTexture(&encoderTexture, TextureAttachment::COLOR_ATTACHMENT);
    encoderFramebuffer.attachTexture(&encoderDepthTexture, TextureAttachment::DEPTH_ATTACHMENT);

    compositeTexture.create(screenTextureInfo);
    compositeFramebuffer.create(compositeTexture.info.width, compositeTexture.info.height);
    compositeFramebuffer.attachTexture(&compositeTexture, TextureAttachment::COLOR_ATTACHMENT);

    lateFwdTexture.create(screenTextureInfo);
    lateFwdDepthTexture.create(screenTextureDepthInfo);
    lateForwardFramebuffer.create(lateFwdTexture.info.width, lateFwdTexture.info.height);
    lateForwardFramebuffer.attachTexture(&lateFwdTexture, TextureAttachment::COLOR_ATTACHMENT);
    lateForwardFramebuffer.attachTexture(&lateFwdDepthTexture, TextureAttachment::DEPTH_ATTACHMENT);

    renderTexture.create(screenTextureInfo);
    renderDepthTexture.create(screenTextureDepthInfo);
    renderFramebuffer.create(renderTexture.info.width, renderTexture.info.height);
    renderFramebuffer.attachTexture(&renderTexture, TextureAttachment::COLOR_ATTACHMENT);
    renderFramebuffer.attachTexture(&renderDepthTexture, TextureAttachment::DEPTH_ATTACHMENT);

    postProcessSwapTexture.create(screenTextureInfo);
    postProcessSwapDepthTexture.create(screenTextureDepthInfo);
    postProcessSwapFramebuffer.create(postProcessSwapTexture.info.width, postProcessSwapTexture.info.height);
    postProcessSwapFramebuffer.attachTexture(&postProcessSwapTexture, TextureAttachment::COLOR_ATTACHMENT);
    postProcessSwapFramebuffer.attachTexture(&postProcessSwapDepthTexture, TextureAttachment::DEPTH_ATTACHMENT);

    deferredPositionsTexture.create(deferred16Bit);
    deferredNormalsTexture.create(deferred16Bit);
    deferredUVTexture.create(deferred8Bit);
    deferredBaseColor.create(deferred8Bit);
    deferredAmbient.create(deferred8Bit);
    deferredDiffuse.create(deferred8Bit);
    deferredSpecularWithExponentTexture.create(deferred8Bit);
    deferredEmissive.create(deferred8Bit);
    deferredDepth.create(screenTextureDepthInfo);
    deferredFramebuffer.create(deferredPositionsTexture.info.width, deferredPositionsTexture.info.height);
    deferredFramebuffer.attachTexture(&deferredPositionsTexture, TextureAttachment::COLOR_ATTACHMENT, 0);
    deferredFramebuffer.attachTexture(&deferredNormalsTexture, TextureAttachment::COLOR_ATTACHMENT, 1);
    deferredFramebuffer.attachTexture(&deferredUVTexture, TextureAttachment::COLOR_ATTACHMENT, 2);
    deferredFramebuffer.attachTexture(&deferredBaseColor, TextureAttachment::COLOR_ATTACHMENT, 3);
    deferredFramebuffer.attachTexture(&deferredAmbient, TextureAttachment::COLOR_ATTACHMENT, 4);
    deferredFramebuffer.attachTexture(&deferredDiffuse, TextureAttachment::COLOR_ATTACHMENT, 5);
    deferredFramebuffer.attachTexture(&deferredSpecularWithExponentTexture, TextureAttachment::COLOR_ATTACHMENT, 6);
    deferredFramebuffer.attachTexture(&deferredEmissive, TextureAttachment::COLOR_ATTACHMENT, 7);
    deferredFramebuffer.attachTexture(&deferredDepth, TextureAttachment::DEPTH_ATTACHMENT);

    cleanViewTexture.create(screenTextureInfo);
    cleanViewDepthTexture.create(screenTextureDepthInfo);
    cleanViewFramebuffer.create(cleanViewTexture.info.width, cleanViewTexture.info.height);
    cleanViewFramebuffer.attachTexture(&cleanViewTexture, TextureAttachment::COLOR_ATTACHMENT, 0);
    cleanViewFramebuffer.attachTexture(&cleanViewDepthTexture, TextureAttachment::DEPTH_ATTACHMENT);

    if (bloomScaledFramebuffers.empty())
    {
        bloomScaledFramebuffers.resize(bloomIterations + 1);
        bloomTextures.resize(bloomIterations + 1);

        // 4 Iterations
        for (size_t i = 0; i < bloomIterations; i++)
        {
            TextureInfo screenTextureInfo{.width = renderViewWidth / static_cast<uint32_t>(std::pow(2, i)), .height = renderViewHeight / static_cast<uint32_t>(std::pow(2, i)), .depth = 1, .samples = 1, .arrayLayers = 1, .format = TextureFormat::RGB8, .usage = TextureInfo::TypeBitsFlags::COLOR};
            bloomTextures[i].create(screenTextureInfo);

            bloomScaledFramebuffers[i].create(bloomTextures[i].info.width, bloomTextures[i].info.height);
            bloomScaledFramebuffers[i].attachTexture(&bloomTextures[i], TextureAttachment::COLOR_ATTACHMENT);
        }

        TextureInfo screenTextureInfo{.width = renderViewWidth, .height = renderViewHeight, .depth = 1, .samples = 1, .arrayLayers = 1, .format = TextureFormat::RGB8, .usage = TextureInfo::TypeBitsFlags::COLOR};
        bloomTextures[bloomIterations].create(screenTextureInfo);
        bloomScaledFramebuffers[bloomIterations].create(bloomTextures[bloomIterations].info.width, bloomTextures[bloomIterations].info.height);
        bloomScaledFramebuffers[bloomIterations].attachTexture(&bloomTextures[bloomIterations], TextureAttachment::COLOR_ATTACHMENT);
    }

    backBufferFramebuffer.create(screenTextureInfo.width, screenTextureInfo.height, true);

    {
        TextureInfo directionalSMTextureInfo{.width = 8192, .height = 8192, .depth = 1, .samples = 1, .arrayLayers = 1, .format = TextureFormat::DEPTH32, .usage = TextureInfo::TypeBitsFlags::DEPTH};
        directionalSMTextures.create(directionalSMTextureInfo);

        directionalShadowMapFramebuffer.create(directionalSMTextureInfo.width, directionalSMTextureInfo.height);
        directionalShadowMapFramebuffer.attachTexture(&directionalSMTextures, TextureAttachment::DEPTH_ATTACHMENT, 0, 0);
    }

    {
        TextureInfo spotLightSMTextureInfo{.width = 1024, .height = 1024, .depth = 1, .samples = 1, .arrayLayers = 1, .format = TextureFormat::DEPTH32, .usage = TextureInfo::TypeBitsFlags::DEPTH};
        spotSMTextures.create(spotLightSMTextureInfo);

        spotLightShadowMapFramebuffer.create(spotLightSMTextureInfo.width, spotLightSMTextureInfo.height);
        spotLightShadowMapFramebuffer.attachTexture(&spotSMTextures, TextureAttachment::DEPTH_ATTACHMENT, 0, 0);
    }

    {
        TextureInfo pointLightSMTextureInfo{.width = 2048, .height = 2048, .depth = 1, .samples = 1, .arrayLayers = 1, .format = TextureFormat::DEPTH32, .usage = TextureInfo::TypeBitsFlags::DEPTH};
        pointSMTextures.create(pointLightSMTextureInfo);

        pointLightShadowMapFramebuffer.create(pointLightSMTextureInfo.width, pointLightSMTextureInfo.height);
        pointLightShadowMapFramebuffer.attachTexture(&pointSMTextures, TextureAttachment::DEPTH_ATTACHMENT, 0, 0);
    }
}

void dag::RenderingSystem::setRenderingMode(RenderMode const& mode, bool const& composeInBackbuffer, bool const& isEditor)
{
    currentRenderMode = mode;

    if (currentDebugRenderMode != DebugRenderMode::None)
    {
        return;
    }

    extRenderGraph.clear();
    extRenderGraph.shrink_to_fit();
    renderPasses.clear();
    renderPasses.shrink_to_fit();

    if (isEditor)
    {
        extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execEncoderPass));
        renderPasses.emplace_back(RenderPass{
            nullptr,
            nullptr,
            {},
            &encoderFramebuffer,
            {{1.0f, 1.0f, 1.0f, 1.0f},
             {1.0f, 1.0f}},
            "Encoder Pass"});
    }

    extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDirShadowAccumulationPass));
    renderPasses.emplace_back(RenderPass{
        nullptr,
        nullptr,
        {},
        &directionalShadowMapFramebuffer,
        {{1.0f, 1.0f, 1.0f, 1.0f},
         {1.0f, 1.0f}},
        "Directional Light Shadow Accumulation Pass"});

    extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execSpotShadowAccumulationPass));
    renderPasses.emplace_back(RenderPass{
        nullptr,
        nullptr,
        {},
        &spotLightShadowMapFramebuffer,
        {{1.0f, 1.0f, 1.0f, 1.0f},
         {1.0f, 1.0f}},
        "Spotlight Shadow Accumulation Pass"});

    extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execPointShadowAccumulationPass));
    renderPasses.emplace_back(RenderPass{
        nullptr,
        nullptr,
        {},
        &pointLightShadowMapFramebuffer,
        {{1.0f, 1.0f, 1.0f, 1.0f},
         {1.0f, 1.0f}},
        "Point Light Shadow Accumulation Pass"});

    switch (mode)
    {
        case RenderMode::ForwardRendering:
        {
            dequeueLateOverlays(OverlaysFuncID::Skybox);
            dequeueLateOverlays(OverlaysFuncID::DeferredTransparentsRendering);
        		
            extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execForwardPass));
            renderPasses.emplace_back(RenderPass{
                nullptr,
                nullptr,
                {},
                &renderFramebuffer,
                {{0.0f, 0.0f, 0.0f, 1.0f},
                 {1.0f, 1.0f}},
                "Forward Pass"});
            break;
        }
        case RenderMode::DeferredRendering:
        {
            enqueueLateOverlays(OverlaysFuncID::Skybox);
            enqueueLateOverlays(OverlaysFuncID::DeferredTransparentsRendering);

            extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDeferredWritePass));
            renderPasses.emplace_back(RenderPass{
                nullptr,
                nullptr,
                {},
                &deferredFramebuffer,
                {{0.0f, 0.0f, 0.0f, 0.0f},
                 {1.0f, 1.0f}},
                "Deferred Write Pass"});

            extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDeferredProcessPass));
            renderPasses.emplace_back(RenderPass{
                nullptr,
                &deferredFramebuffer,
                {},
                &renderFramebuffer,
                {{0.0f, 0.0f, 0.0f, 1.0f},
                 {1.0f, 1.0f}},
                "Deferred Process Pass"});

            break;
        }
    }

    extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execPostProcessStack));
    renderPasses.emplace_back(RenderPass{
        &renderFramebuffer,
        &renderFramebuffer,
        {},
        &cleanViewFramebuffer,
        {{0.0f, 0.0f, 0.0f, 1.0f},
         {1.0f, 1.0f}},
        "Post Process Pass"});

    extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execLateForwardPass));
    renderPasses.emplace_back(RenderPass{
        &cleanViewFramebuffer,
        &renderFramebuffer,
        {},
        &lateForwardFramebuffer,
        {{0.0f, 0.0f, 0.0f, 1.0f},
         {1.0f, 0.0f}},
        currentRenderMode == RenderMode::ForwardRendering ? "Late Forward Pass" : "Late Forward Pass with Transparent Rendering"});

    extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execCompositionPass));
    if (composeInBackbuffer)
    {
        renderPasses.emplace_back(RenderPass{
            &lateForwardFramebuffer,
            &lateForwardFramebuffer,
            {lateForwardFramebuffer.getColorAttachment(0)},
            &backBufferFramebuffer,
            {{0.0f, 0.0f, 0.0f, 1.0f},
             {1.0f, 1.0f}},
            "Composition Pass"});
    }
    else
    {
        renderPasses.emplace_back(RenderPass{
            &lateForwardFramebuffer,
            &lateForwardFramebuffer,
            {lateForwardFramebuffer.getColorAttachment(0)},
            &compositeFramebuffer,
            {{0.0f, 0.0f, 0.0f, 1.0f},
             {1.0f, 1.0f}},
            "Composition Pass"});
    }
}

void dag::RenderingSystem::setDebugMode(DebugRenderMode const& mode, bool const& composeInBackbuffer, bool const& isEditor)
{
    currentDebugRenderMode = mode;

    if (mode == DebugRenderMode::None)
    {
        setRenderingMode(currentRenderMode, composeInBackbuffer, isEditor);
        return;
    }

    extRenderGraph.clear();
    extRenderGraph.shrink_to_fit();
    renderPasses.clear();
    renderPasses.shrink_to_fit();

    if (isEditor)
    {
        extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execEncoderPass));
        renderPasses.emplace_back(RenderPass{
            nullptr,
            nullptr,
            {},
            &encoderFramebuffer,
            {{1.0f, 1.0f, 1.0f, 1.0f},
             {1.0f, 1.0f}}});
    }

    // Toggling stages here
    if (currentRenderMode == RenderMode::ForwardRendering)
    {
        switch (currentDebugRenderMode)
        {
            case DebugRenderMode::Wireframe:
            {
                extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDebugRenderEntitiesWireframeForward));
                break;
            }
            case DebugRenderMode::Unlit:
            {
                extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDebugRenderEntitiesUnlitForward));
                break;
            }
            case DebugRenderMode::Depth:
            {
                extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDebugRenderEntitiesDepthForward));
                break;
            }
            case DebugRenderMode::Normals:
            {
                extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDebugRenderEntitiesNormalsForward));
                break;
            }
            default:
                break;
        }

        renderPasses.emplace_back(RenderPass{
            nullptr,
            nullptr,
            {},
            &renderFramebuffer,
            {{0.0f, 0.0f, 0.0f, 1.0f},
             {1.0f, 1.0f}}});
    }
    else if (currentRenderMode == RenderMode::DeferredRendering)
    {
        switch (currentDebugRenderMode)
        {
            case DebugRenderMode::Wireframe:
            {
                extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDebugRenderEntitiesWireframeDeferredWrite));
                extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDebugRenderEntitiesWireframeDeferredProcess));
                break;
            }
            case DebugRenderMode::Unlit:
            {
                extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDeferredWritePass));
                extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDebugRenderEntitiesUnlitDeferred));
                break;
            }
            case DebugRenderMode::Depth:
            {
                extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDeferredWritePass));
                extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDebugRenderEntitiesDepthDeferred));
                break;
            }
            case DebugRenderMode::Normals:
            {
                extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDeferredWritePass));
                extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execDebugRenderEntitiesNormalsDeferred));
                break;
            }
            default:
                break;
        }

        renderPasses.emplace_back(RenderPass{
            nullptr,
            nullptr,
            {},
            &deferredFramebuffer,
            {{0.0f, 0.0f, 0.0f, 0.0f},
             {1.0f, 1.0f}}});
        renderPasses.emplace_back(RenderPass{
            nullptr,
            &deferredFramebuffer,
            {},
            &renderFramebuffer,
            {{0.0f, 0.0f, 0.0f, 1.0f},
             {1.0f, 1.0f}}});
    }

    // Leftovers
    extRenderGraph.emplace_back(BIND_EXT_RENDER_GRAPH_FN(execCompositionPass));
    if (composeInBackbuffer)
    {
        renderPasses.emplace_back(RenderPass{
            &renderFramebuffer,
            &renderFramebuffer,
            {renderFramebuffer.getColorAttachment(0)},
            &backBufferFramebuffer,
            {{0.0f, 0.0f, 0.0f, 1.0f},
             {1.0f, 1.0f}}});
    }
    else
    {
        renderPasses.emplace_back(RenderPass{
            &renderFramebuffer,
            &renderFramebuffer,
            {renderFramebuffer.getColorAttachment(0)},
            &compositeFramebuffer,
            {{0.0f, 0.0f, 0.0f, 1.0f},
             {1.0f, 1.0f}}});
    }
}

void dag::RenderingSystem::categorizeGeometry()
{
    // TODO Implement
    // Opaque and transparent
    opaqueEntitiesToShapeID.clear();
    opaqueEntitiesToShapeID.shrink_to_fit();
    opaqueEntitiesToShapeID = {};
    transparentEntitiesToShapeID.clear();
    transparentEntitiesToShapeID.shrink_to_fit();
    transparentEntitiesToShapeID = {};

    for (auto const& entity : entities)
    {
        auto meshRenderer = coordinator->getComponent<MeshRenderer>(entity);
        for (size_t shapeID = 0; shapeID < resourceManager->models[meshRenderer.hashIndex].shapes.size(); shapeID++)
        {
            auto materialInfo = resourceManager->models[meshRenderer.hashIndex].shapes[shapeID].material;
            if (materialInfo.dissolve < 1.0f || materialInfo.textureAlphaHash != 0)
            {
                transparentEntitiesToShapeID.emplace_back(std::make_pair(entity, shapeID));
            }
            else
            {
                opaqueEntitiesToShapeID.emplace_back(std::make_pair(entity, shapeID));
            }
        }
    }
}

void dag::RenderingSystem::sortTransparentGeometry()
{
    // Sorting the transparent objects back to front
    auto cameraEntity = scene->currentSceneCamera;

    if (cameraEntity == -1)
    {
        return;
    }

    auto cameraEntityTransform = coordinator->getComponent<Transform>(cameraEntity);
    std::sort(transparentEntitiesToShapeID.begin(), transparentEntitiesToShapeID.end(), [&](std::pair<Entity, int32_t>& left, std::pair<Entity, int32_t>& right) {
        auto entityLeftTransform = coordinator->getComponent<Transform>(left.first);
        auto entityRightTransform = coordinator->getComponent<Transform>(right.first);

        auto meshRendererLeft = coordinator->getComponent<MeshRenderer>(left.first);
        auto meshRendererRight = coordinator->getComponent<MeshRenderer>(right.first);

        auto shapeLeftPos = glm::vec3(entityLeftTransform.modelMatrix * glm::vec4(resourceManager->models[meshRendererLeft.hashIndex].shapes[left.second].center, 1.0f));
        auto shapeRightPos = glm::vec3(entityRightTransform.modelMatrix * glm::vec4(resourceManager->models[meshRendererRight.hashIndex].shapes[right.second].center, 1.0f));

        // BACK TO FRONT
        return glm::distance(cameraEntityTransform.position, shapeLeftPos) > glm::distance(cameraEntityTransform.position, shapeRightPos);
    });
}

void dag::RenderingSystem::resizeViewFramebuffers()
{
    if (sizeDirty)
    {
        auto& cameraComponent = coordinator->getComponent<CameraComponent>(coordinator->getSystem<dag::CameraSystem>()->activeCamera);

        if (cameraComponent.isScreenDependent)
        {
            cameraComponent.aspect = renderViewWidth / static_cast<float>(renderViewHeight);
        }

        encoderTexture.resize(renderViewWidth, renderViewHeight);
        encoderDepthTexture.resize(renderViewWidth, renderViewHeight);
        compositeTexture.resize(renderViewWidth, renderViewHeight);
        lateFwdTexture.resize(renderViewWidth, renderViewHeight);
        lateFwdDepthTexture.resize(renderViewWidth, renderViewHeight);
        renderTexture.resize(renderViewWidth, renderViewHeight);
        renderDepthTexture.resize(renderViewWidth, renderViewHeight);
        cleanViewTexture.resize(renderViewWidth, renderViewHeight);
        cleanViewDepthTexture.resize(renderViewWidth, renderViewHeight);

        postProcessSwapTexture.resize(renderViewWidth, renderViewHeight);
        postProcessSwapDepthTexture.resize(renderViewWidth, renderViewHeight);

        deferredPositionsTexture.resize(renderViewWidth, renderViewHeight);
        deferredNormalsTexture.resize(renderViewWidth, renderViewHeight);
        deferredUVTexture.resize(renderViewWidth, renderViewHeight);
        deferredBaseColor.resize(renderViewWidth, renderViewHeight);
        deferredAmbient.resize(renderViewWidth, renderViewHeight);
        deferredDiffuse.resize(renderViewWidth, renderViewHeight);
        deferredSpecularWithExponentTexture.resize(renderViewWidth, renderViewHeight);
        deferredEmissive.resize(renderViewWidth, renderViewHeight);
        deferredDepth.resize(renderViewWidth, renderViewHeight);

        for (size_t i = 0; i < bloomScaledFramebuffers.size() - 1; i++)
        {
            bloomTextures[i].resize(renderViewWidth / static_cast<uint32_t>(std::pow(2, i)), renderViewHeight / static_cast<uint32_t>(std::pow(2, i)));
        }
        bloomTextures[bloomIterations].resize(renderViewWidth, renderViewHeight);

        encoderFramebuffer.create(renderViewWidth, renderViewHeight);
        deferredFramebuffer.create(renderViewWidth, renderViewHeight);
        renderFramebuffer.create(renderViewWidth, renderViewHeight);
        lateForwardFramebuffer.create(renderViewWidth, renderViewHeight);
        compositeFramebuffer.create(renderViewWidth, renderViewHeight);
        cleanViewFramebuffer.create(renderViewWidth, renderViewHeight);
        backBufferFramebuffer.create(renderViewWidth, renderViewHeight, true);
        postProcessSwapFramebuffer.create(renderViewWidth, renderViewHeight);

        for (size_t i = 0; i < bloomScaledFramebuffers.size() - 1; i++)
        {
            bloomScaledFramebuffers[i].create(renderViewWidth / static_cast<uint32_t>(std::pow(2, i)), renderViewHeight / static_cast<uint32_t>(std::pow(2, i)));
        }
        bloomScaledFramebuffers[bloomIterations].create(renderViewWidth, renderViewHeight);

        sizeDirty = false;
    }
}

int32_t dag::RenderingSystem::decodePixel(std::array<unsigned char, 4> const& pixelData)
{
    return (pixelData[0] + (pixelData[1] << 8) + (pixelData[2] << 16));
}

/**
 * @brief Draws the grid
 */
void dag::RenderingSystem::renderGrid()
{
    shaders[static_cast<size_t>(Shader::GridShader)].bind();
    currentShaderBound = shaders[static_cast<size_t>(Shader::GridShader)].subShader.id;

    vaos[static_cast<size_t>(VAONames::DefaultPlane)].draw();

    shaders[static_cast<size_t>(Shader::GridShader)].unbind();
    currentShaderBound = 0;
}

void dag::RenderingSystem::renderEntityShape(Entity const& entity, int32_t const& shapeID)
{
    auto const descriptorSet = coordinator->getComponent<DescriptorSet>(entity);
    auto const meshRenderer = coordinator->getComponent<MeshRenderer>(entity);

    shaders[descriptorSet.shaderProgramID].bind();
    currentShaderBound = shaders[descriptorSet.shaderProgramID].subShader.id;

    auto& shape = resourceManager->models[meshRenderer.hashIndex].shapes[shapeID];

    resourceManager->textures[shape.material.textureAmbientHash].bind(UniformBindings::modelAmbientTextureIndex);
    resourceManager->textures[shape.material.textureDiffuseHash].bind(UniformBindings::modelDiffuseTextureIndex);
    resourceManager->textures[shape.material.textureSpecularHash].bind(UniformBindings::modelSpecularTextureIndex);
    resourceManager->textures[shape.material.textureBumpHash].bind(UniformBindings::modelBumpTextureIndex);
    resourceManager->textures[shape.material.textureAlphaHash].bind(UniformBindings::modelAlphaTextureIndex);

    // Draw the mesh
    vaos[shape.VAOIndex].draw();

    resourceManager->textures[shape.material.textureAmbientHash].unbind(UniformBindings::modelAmbientTextureIndex);
    resourceManager->textures[shape.material.textureDiffuseHash].unbind(UniformBindings::modelDiffuseTextureIndex);
    resourceManager->textures[shape.material.textureSpecularHash].unbind(UniformBindings::modelSpecularTextureIndex);
    resourceManager->textures[shape.material.textureBumpHash].unbind(UniformBindings::modelBumpTextureIndex);
    resourceManager->textures[shape.material.textureAlphaHash].unbind(UniformBindings::modelAlphaTextureIndex);

    shaders[descriptorSet.shaderProgramID].unbind();
    currentShaderBound = 0;
}

void dag::RenderingSystem::renderEntityShape(Entity const& entity, int32_t const& shapeID, Shader const& shader)
{
    auto const meshRenderer = coordinator->getComponent<MeshRenderer>(entity);

    shaders[static_cast<size_t>(shader)].bind();
    currentShaderBound = shaders[static_cast<size_t>(shader)].subShader.id;

    auto& shape = resourceManager->models[meshRenderer.hashIndex].shapes[shapeID];

    vaos[shape.VAOIndex].draw();

    shaders[static_cast<size_t>(shader)].unbind();
    currentShaderBound = 0;
}

void dag::RenderingSystem::renderEntityShape(Entity const& entityID, int32_t const& shapeID, Shader const& shader, bool const& textured)
{
    auto const meshRenderer = coordinator->getComponent<MeshRenderer>(entityID);

    shaders[static_cast<size_t>(shader)].bind();
    currentShaderBound = shaders[static_cast<size_t>(shader)].subShader.id;

    auto& shape = resourceManager->models[meshRenderer.hashIndex].shapes[shapeID];

    resourceManager->textures[shape.material.textureAmbientHash].bind(UniformBindings::modelAmbientTextureIndex);
    resourceManager->textures[shape.material.textureDiffuseHash].bind(UniformBindings::modelDiffuseTextureIndex);
    resourceManager->textures[shape.material.textureSpecularHash].bind(UniformBindings::modelSpecularTextureIndex);
    resourceManager->textures[shape.material.textureBumpHash].bind(UniformBindings::modelBumpTextureIndex);
    resourceManager->textures[shape.material.textureAlphaHash].bind(UniformBindings::modelAlphaTextureIndex);

    // Draw the mesh
    vaos[shape.VAOIndex].draw();

    resourceManager->textures[shape.material.textureAmbientHash].unbind(UniformBindings::modelAmbientTextureIndex);
    resourceManager->textures[shape.material.textureDiffuseHash].unbind(UniformBindings::modelDiffuseTextureIndex);
    resourceManager->textures[shape.material.textureSpecularHash].unbind(UniformBindings::modelSpecularTextureIndex);
    resourceManager->textures[shape.material.textureBumpHash].unbind(UniformBindings::modelBumpTextureIndex);
    resourceManager->textures[shape.material.textureAlphaHash].unbind(UniformBindings::modelAlphaTextureIndex);

    shaders[static_cast<size_t>(shader)].unbind();
    currentShaderBound = 0;
}

/**
 * @brief Renders the skybox
 */
void dag::RenderingSystem::renderSkybox()
{
    shaders[static_cast<size_t>(Shader::SkyboxShader)].bind();
    currentShaderBound = shaders[static_cast<size_t>(Shader::SkyboxShader)].subShader.id;

    resourceManager->textureCube[0].bind(UniformBindings::cubeMapIndex);
    vaos[static_cast<size_t>(VAONames::DefaultSkybox)].draw();
    resourceManager->textureCube[0].unbind(UniformBindings::cubeMapIndex);

    shaders[static_cast<size_t>(Shader::SkyboxShader)].unbind();
    currentShaderBound = 0;
}

/**
 * @brief Performs a draw with a texture
 */
void dag::RenderingSystem::renderShaderFullscreen(Shader const& shaderID)
{
    shaders[static_cast<size_t>(shaderID)].bind();
    currentShaderBound = shaders[static_cast<size_t>(shaderID)].subShader.id;
    vaos[static_cast<size_t>(VAONames::DefaultPlane)].draw();
    shaders[static_cast<size_t>(shaderID)].unbind();
    currentShaderBound = 0;
}

/**
 * @brief Renders an entity for the outlining part (stenciling)
 */
void dag::RenderingSystem::outlineEntity(dag::Entity const& entity)
{
    auto meshRenderer = coordinator->getComponent<MeshRenderer>(entity);

    shaders[static_cast<size_t>(Shader::OutlineShader)].bind();
    currentShaderBound = shaders[static_cast<size_t>(Shader::OutlineShader)].subShader.id;

    auto color = glm::vec4(1.0f, 0.65, 0.0f, 1.0f);
    Descriptor d(UniformFuncs::Uniform4fv, "outlineColor", color);
    shaderUpdateFuncs[d.fID](d);

    for (auto& s : resourceManager->models[meshRenderer.hashIndex].shapes)
    {
        // Draw the mesh
        vaos[s.VAOIndex].draw();
    }
    // Draw the mesh
    shaders[static_cast<size_t>(Shader::OutlineShader)].unbind();
    currentShaderBound = 0;
}

/**
 * @brief Renders an entity
 */
void dag::RenderingSystem::renderEntity(dag::Entity const& entity)
{
    auto descriptorSet = coordinator->getComponent<DescriptorSet>(entity);
    auto meshRenderer = coordinator->getComponent<MeshRenderer>(entity);

    shaders[descriptorSet.shaderProgramID].bind();
    currentShaderBound = shaders[descriptorSet.shaderProgramID].subShader.id;

    for (auto& shape : resourceManager->models[meshRenderer.hashIndex].shapes)
    {
        resourceManager->textures[shape.material.textureAmbientHash].bind(UniformBindings::modelAmbientTextureIndex);
        resourceManager->textures[shape.material.textureDiffuseHash].bind(UniformBindings::modelDiffuseTextureIndex);
        resourceManager->textures[shape.material.textureSpecularHash].bind(UniformBindings::modelSpecularTextureIndex);
        resourceManager->textures[shape.material.textureBumpHash].bind(UniformBindings::modelBumpTextureIndex);
        resourceManager->textures[shape.material.textureAlphaHash].bind(UniformBindings::modelAlphaTextureIndex);

        // Draw the mesh
        vaos[shape.VAOIndex].draw();

        resourceManager->textures[shape.material.textureAmbientHash].unbind(UniformBindings::modelAmbientTextureIndex);
        resourceManager->textures[shape.material.textureDiffuseHash].unbind(UniformBindings::modelDiffuseTextureIndex);
        resourceManager->textures[shape.material.textureSpecularHash].unbind(UniformBindings::modelSpecularTextureIndex);
        resourceManager->textures[shape.material.textureBumpHash].unbind(UniformBindings::modelBumpTextureIndex);
        resourceManager->textures[shape.material.textureAlphaHash].unbind(UniformBindings::modelAlphaTextureIndex);
    }

    shaders[descriptorSet.shaderProgramID].unbind();
    currentShaderBound = 0;
}

/**
 * @brief Renders an entity with a custom shader (without textures)
 */
void dag::RenderingSystem::renderEntity(Entity const& entityID, Shader const& shader)
{
    auto meshRenderer = coordinator->getComponent<MeshRenderer>(entityID);

    shaders[static_cast<size_t>(shader)].bind();
    currentShaderBound = shaders[static_cast<size_t>(shader)].subShader.id;

    for (auto& s : resourceManager->models[meshRenderer.hashIndex].shapes)
    {
        vaos[s.VAOIndex].draw();
    }

    shaders[static_cast<size_t>(shader)].unbind();
    currentShaderBound = 0;
}

void dag::RenderingSystem::renderEntity(Entity const& entityID, Shader const& shader, bool const& textured)
{
    auto meshRenderer = coordinator->getComponent<MeshRenderer>(entityID);

    shaders[static_cast<size_t>(shader)].bind();
    currentShaderBound = shaders[static_cast<size_t>(shader)].subShader.id;

    for (auto& shape : resourceManager->models[meshRenderer.hashIndex].shapes)
    {
        resourceManager->textures[shape.material.textureAmbientHash].bind(UniformBindings::modelAmbientTextureIndex);
        resourceManager->textures[shape.material.textureDiffuseHash].bind(UniformBindings::modelDiffuseTextureIndex);
        resourceManager->textures[shape.material.textureSpecularHash].bind(UniformBindings::modelSpecularTextureIndex);
        resourceManager->textures[shape.material.textureBumpHash].bind(UniformBindings::modelBumpTextureIndex);
        resourceManager->textures[shape.material.textureAlphaHash].bind(UniformBindings::modelAlphaTextureIndex);

        // Draw the mesh
        vaos[shape.VAOIndex].draw();

        resourceManager->textures[shape.material.textureAmbientHash].unbind(UniformBindings::modelAmbientTextureIndex);
        resourceManager->textures[shape.material.textureDiffuseHash].unbind(UniformBindings::modelDiffuseTextureIndex);
        resourceManager->textures[shape.material.textureSpecularHash].unbind(UniformBindings::modelSpecularTextureIndex);
        resourceManager->textures[shape.material.textureBumpHash].unbind(UniformBindings::modelBumpTextureIndex);
        resourceManager->textures[shape.material.textureAlphaHash].unbind(UniformBindings::modelAlphaTextureIndex);
    }

    shaders[static_cast<size_t>(shader)].unbind();
    currentShaderBound = 0;
}

void dag::RenderingSystem::renderEntities()
{
    uniformHandler.ubDrawData.bind();

    for (auto& opaqueEntityPair : opaqueEntitiesToShapeID)
    {
        uniformHandler.ubDrawData.bindBufferRange(DrawData::index, DrawData::size() * opaqueEntityPair.first, DrawData::size());
        renderEntityShape(opaqueEntityPair.first, opaqueEntityPair.second);
    }

    glDepthMask(GL_FALSE);
    for (auto& transparentEntityPair : transparentEntitiesToShapeID)
    {
        uniformHandler.ubDrawData.bindBufferRange(DrawData::index, DrawData::size() * transparentEntityPair.first, DrawData::size());
        renderEntityShape(transparentEntityPair.first, transparentEntityPair.second);
    }
    glDepthMask(GL_TRUE);

    uniformHandler.ubDrawData.unbind();
}

void dag::RenderingSystem::renderTransparentEntities()
{
    uniformHandler.ubDrawData.bind();

    glDepthMask(GL_FALSE);
    for (auto& transparentEntityPair : transparentEntitiesToShapeID)
    {
        uniformHandler.ubDrawData.bindBufferRange(DrawData::index, DrawData::size() * transparentEntityPair.first, DrawData::size());
        renderEntityShape(transparentEntityPair.first, transparentEntityPair.second);
    }
    glDepthMask(GL_TRUE);

    uniformHandler.ubDrawData.unbind();
}

void dag::RenderingSystem::renderOpaqueEntities()
{
    uniformHandler.ubDrawData.bind();

    for (auto& opaqueEntityPair : opaqueEntitiesToShapeID)
    {
        uniformHandler.ubDrawData.bindBufferRange(DrawData::index, DrawData::size() * opaqueEntityPair.first, DrawData::size());
        renderEntityShape(opaqueEntityPair.first, opaqueEntityPair.second);
    }

    uniformHandler.ubDrawData.unbind();
}

void dag::RenderingSystem::renderOpaqueEntities(Shader const& shader)
{
    uniformHandler.ubDrawData.bind();

    for (auto& opaqueEntityPair : opaqueEntitiesToShapeID)
    {
        uniformHandler.ubDrawData.bindBufferRange(DrawData::index, DrawData::size() * opaqueEntityPair.first, DrawData::size());
        renderEntityShape(opaqueEntityPair.first, opaqueEntityPair.second, shader);
    }

    uniformHandler.ubDrawData.unbind();
}


void dag::RenderingSystem::renderOpaqueEntities(Shader const& shader, bool const& textured)
{
    uniformHandler.ubDrawData.bind();

    for (auto& opaqueEntityPair : opaqueEntitiesToShapeID)
    {
        uniformHandler.ubDrawData.bindBufferRange(DrawData::index, DrawData::size() * opaqueEntityPair.first, DrawData::size());
        renderEntityShape(opaqueEntityPair.first, opaqueEntityPair.second, shader, textured);
    }

    uniformHandler.ubDrawData.unbind();
}


void dag::RenderingSystem::renderEntities(Shader const& shader)
{
    uniformHandler.ubDrawData.bind();

    for (auto& opaqueEntityPair : opaqueEntitiesToShapeID)
    {
        uniformHandler.ubDrawData.bindBufferRange(DrawData::index, DrawData::size() * opaqueEntityPair.first, DrawData::size());
        renderEntityShape(opaqueEntityPair.first, opaqueEntityPair.second, shader);
    }

    //glDepthMask(GL_FALSE);
    for (auto& transparentEntityPair : transparentEntitiesToShapeID)
    {
        uniformHandler.ubDrawData.bindBufferRange(DrawData::index, DrawData::size() * transparentEntityPair.first, DrawData::size());
        renderEntityShape(transparentEntityPair.first, transparentEntityPair.second, shader);
    }
    //glDepthMask(GL_TRUE);

    uniformHandler.ubDrawData.unbind();
}

void dag::RenderingSystem::renderEntities(Shader const& shader, bool const& textured)
{
    uniformHandler.ubDrawData.bind();

    for (auto& opaqueEntityPair : opaqueEntitiesToShapeID)
    {
        uniformHandler.ubDrawData.bindBufferRange(DrawData::index, DrawData::size() * opaqueEntityPair.first, DrawData::size());
        renderEntityShape(opaqueEntityPair.first, opaqueEntityPair.second, shader, true);
    }

    glDepthMask(GL_FALSE);

    for (auto& transparentEntityPair : transparentEntitiesToShapeID)
    {
        uniformHandler.ubDrawData.bindBufferRange(DrawData::index, DrawData::size() * transparentEntityPair.first, DrawData::size());
        renderEntityShape(transparentEntityPair.first, transparentEntityPair.second, shader, true);
    }
    glDepthMask(GL_TRUE);

    uniformHandler.ubDrawData.unbind();
}

/**
 * @brief RenderingSystem update (rendering main func)
 */
void dag::RenderingSystem::update()
{
    updateBuffers();
    sortTransparentGeometry();

    for (size_t i = 0; i < extRenderGraph.size(); i++)
    {
        extRenderGraph[i](renderPasses[i]);
    }
}

void dag::RenderingSystem::updateBuffers()
{
    updateShaders();
    resizeViewFramebuffers();

    int32_t currentLightCount = 0;
    int32_t currentCamera = 0;
    int32_t selectedLight = 0;
    int32_t selectedCamera = 0;
    int32_t currentDirectionalLights = 0;
    int32_t currentSpotLights = 0;
    int32_t currentPointLights = 0;
    float currentZNear = 0.0f;
    float currentZFar = 0.0f;

    // Camera Data update
    {
        auto& buf = uniformHandler.ubCameraData.elements[0];

        auto& cs = cameraSystem;
        size_t id = 0;

        for (auto& entity : cs->entities)
        {
            auto const& transform = coordinator->getComponent<Transform>(entity);
            auto& cameraComponent = coordinator->getComponent<CameraComponent>(entity);

            if (entity == cs->activeCamera)
            {
                currentCamera = id;
                currentZNear = cameraComponent.zNear;
                currentZFar = cameraComponent.zFar;
            }

            if (scene->currentEntity != -1)
            {
                if (entity == scene->entities[scene->currentEntity])
                {
                    selectedCamera = id;
                }
            }

            buf.positions[id] = glm::vec4(transform.position, 1.0f);
            buf.projectionMatrices[id] = cameraComponent.getProjection();
            buf.modelMatrices[id] = cameraComponent.getView(transform.position, transform.rotation);
            buf.inverseViewProj[id] = glm::inverse(buf.projectionMatrices[id] * buf.modelMatrices[id]);

            id++;
        }

        uniformHandler.ubCameraData.update();
    }

    // Light Data update
    { // Updating the light data buffer

        auto& buf = uniformHandler.ubLightData.elements[0];
        auto& ls = lightSystem;

        currentLightCount = ls->entities.size();

        currentDirectionalLights = ls->directionalLightsIDs.size();
        currentSpotLights = ls->spotLightsIDs.size();
        currentPointLights = ls->pointLightsIDs.size();

        for (size_t i = 0; i < currentDirectionalLights; i++)
        {
            size_t dodID = EngineLimits::DIR_LIGHT_SHADOW_MAPS * i;
            auto& entity = ls->directionalLightsEntities[i];
            auto const& transform = coordinator->getComponent<Transform>(entity);
            auto const& lightComponent = coordinator->getComponent<LightComponent>(entity);

            if (scene->currentEntity != -1)
            {
                if (entity == scene->entities[scene->currentEntity])
                {
                    selectedLight = dodID;
                }
            }

            for (size_t j = 0; j < EngineLimits::DIR_LIGHT_SHADOW_MAPS; j++)
            {
                buf.positions[dodID + j] = glm::vec4(transform.position, 1.0f);
                buf.projectionMatrices[dodID + j] = lightComponent.getProjection();
                buf.modelMatrices[dodID + j] = glm::inverse(transform.modelMatrix);
                buf.inverseViewProj[dodID + j] = glm::inverse(buf.projectionMatrices[dodID + j] * buf.modelMatrices[dodID + j]);
                buf.colors[dodID + j] = glm::vec4(lightComponent.color, 1.0f);
            }
        }

        for (size_t i = 0; i < currentSpotLights; i++)
        {
            size_t dodID = EngineLimits::DIR_LIGHT_SHADOW_MAPS * currentDirectionalLights + EngineLimits::SPOT_LIGHT_SHADOW_MAPS * i;

            auto& entity = ls->spotLightsEntities[i];
            auto const& transform = coordinator->getComponent<Transform>(entity);
            auto const& lightComponent = coordinator->getComponent<LightComponent>(entity);

            if (scene->currentEntity != -1)
            {
                if (entity == scene->entities[scene->currentEntity])
                {
                    selectedLight = dodID;
                }
            }

            for (size_t j = 0; j < EngineLimits::SPOT_LIGHT_SHADOW_MAPS; j++)
            {
                buf.positions[dodID + j] = glm::vec4(transform.position, 1.0f);
                buf.projectionMatrices[dodID + j] = lightComponent.getProjection(60.0f, 1.0f, 0.1f, 30.0f);
                buf.modelMatrices[dodID + j] = glm::inverse(transform.modelMatrix);
                buf.inverseViewProj[dodID + j] = glm::inverse(buf.projectionMatrices[dodID + j] * buf.modelMatrices[dodID + j]);
                buf.colors[dodID + j] = glm::vec4(lightComponent.color, 1.0f);
            }
        }

        for (size_t i = 0; i < currentPointLights; i++)
        {
            size_t dodID = EngineLimits::DIR_LIGHT_SHADOW_MAPS * currentDirectionalLights + EngineLimits::SPOT_LIGHT_SHADOW_MAPS * currentSpotLights + i * EngineLimits::POINT_LIGHT_SHADOW_MAPS;

            auto& entity = ls->pointLightsEntities[i];
            auto const& transform = coordinator->getComponent<Transform>(entity);
            auto const& lightComponent = coordinator->getComponent<LightComponent>(entity);

            if (scene->currentEntity != -1)
            {
                if (entity == scene->entities[scene->currentEntity])
                {
                    selectedLight = dodID;
                }
            }

            std::vector<glm::mat4> transforms =
                {
                    glm::lookAt(transform.position, transform.position + glm::vec3(1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)),
                    glm::lookAt(transform.position, transform.position + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)),
                    glm::lookAt(transform.position, transform.position + glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0)),
                    glm::lookAt(transform.position, transform.position + glm::vec3(0.0, -1.0, 0.0), glm::vec3(0.0, 0.0, -1.0)),
                    glm::lookAt(transform.position, transform.position + glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, -1.0, 0.0)),
                    glm::lookAt(transform.position, transform.position + glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, -1.0, 0.0))};

            for (size_t j = 0; j < EngineLimits::POINT_LIGHT_SHADOW_MAPS; j++)
            {
                buf.positions[dodID + j] = glm::vec4(transform.position, 1.0f);
                //buf.projectionMatrices[dodID + j] = lightComponent.getProjection();
                buf.projectionMatrices[dodID + j] = lightComponent.getProjection(90.0f, 1.0f, 0.1f, 30.0f);

                buf.modelMatrices[dodID + j] = transforms[j];
                buf.inverseViewProj[dodID + j] = glm::inverse(buf.projectionMatrices[dodID + j] * buf.modelMatrices[dodID + j]);

                buf.colors[dodID + j] = glm::vec4(lightComponent.color, 1.0f);
            }
        }

        uniformHandler.ubLightData.update();
    }

    // Material Data Update
    auto materialComponentArray = coordinator->getComponentArray<MaterialComponent>();
    {
        auto& buf = uniformHandler.ubMaterialData.elements[0];
        for (auto& entity : entities)
        {
            buf.materials[materialComponentArray->entityToIndex[entity]] = materialComponentArray->components[materialComponentArray->entityToIndex[entity]];
        }
        //std::memcpy(buf.materials, materialComponentArray->components.data(), EngineLimits::MAX_RENDERABLE_ENTITIES);
        uniformHandler.ubMaterialData.update();
    }

    // Entities Data Update
    auto transformComponentArray = coordinator->getComponentArray<Transform>();
    {
        auto& buf = uniformHandler.ubEntitiesData.elements[0];
        for (size_t i = 0; i < EngineLimits::MAX_ENTITIES; i++)
        {
            buf.modelMatrices[transformComponentArray->indexToEntity[i]] = transformComponentArray->components[i].getModelMatrix();
        }
        uniformHandler.ubEntitiesData.update();
    }

    {
        uniformHandler.ubSceneData.elements[0].selectedEntity = scene->currentEntity >= 0 ? scene->entities[scene->currentEntity] : 0;
        uniformHandler.ubSceneData.elements[0].currentLightCount = currentLightCount;
        uniformHandler.ubSceneData.elements[0].selectedLight = selectedLight;
        uniformHandler.ubSceneData.elements[0].currentCamera = currentCamera;
        uniformHandler.ubSceneData.elements[0].selectedCamera = selectedCamera;
        uniformHandler.ubSceneData.elements[0].currentDirectionalLights = currentDirectionalLights;
        uniformHandler.ubSceneData.elements[0].currentSpotLights = currentSpotLights;
        uniformHandler.ubSceneData.elements[0].currentPointLights = currentPointLights;
        uniformHandler.ubSceneData.elements[0].zNear = currentZNear;
        uniformHandler.ubSceneData.elements[0].zFar = currentZFar;
        uniformHandler.ubSceneData.update();
    }

    { // Draw data buffers
        for (auto const& entity : entities)
        {
            DrawData* dd = reinterpret_cast<DrawData*>(static_cast<uint8_t*>(uniformHandler.ubDrawData.data) + entity * DrawData::size());
            dd->entityMaterial = static_cast<int32_t>(materialComponentArray->entityToIndex[entity]);
        }
        uniformHandler.ubDrawData.update();
    }

    { // Update post process data
        uniformHandler.ubPPBloomData.update();
        uniformHandler.ubPPFogData.update();
        uniformHandler.ubPPBlurData.update();
        uniformHandler.ubPPChromaticAberrationData.update();
        uniformHandler.ubPPDilationData.update();
        uniformHandler.ubPPPixelizeData.update();
        uniformHandler.ubPPSharpenData.update();
    }

    lightSystem->updateLookupVectors();
}

/**
 * @brief Offscreen rendering for the entity id
 */
void dag::RenderingSystem::renderIDs()
{
    shaders[static_cast<size_t>(Shader::EncodingShader)].bind();
    currentShaderBound = shaders[static_cast<size_t>(Shader::EncodingShader)].subShader.id;

    uniformHandler.ubDrawData.bind();
    size_t id = 0;

    for (auto const& entity : entities)
    {
        uniformHandler.ubDrawData.bindBufferRange(DrawData::index, DrawData::size() * entity, sizeof(DrawData));
        auto meshRenderer = coordinator->getComponent<MeshRenderer>(entity);

        for (auto& s : resourceManager->models[meshRenderer.hashIndex].shapes)
        {
            // Draw the mesh
            vaos[s.VAOIndex].draw();
        }

        id++;
    }

    uniformHandler.ubDrawData.unbind();

    shaders[static_cast<size_t>(Shader::EncodingShader)].unbind();
    currentShaderBound = 0;
}

void dag::RenderingSystem::setSizeDirty(uint32_t const& width, uint32_t const& height)
{
    sizeDirty = true;
    renderViewWidth = width;
    renderViewHeight = height;
}

void dag::RenderingSystem::drawLightOutline(dag::Entity const& entityID)
{
    shaders[static_cast<size_t>(Shader::LightFrustumShader)].bind();
    currentShaderBound = shaders[static_cast<size_t>(Shader::LightFrustumShader)].subShader.id;
    // Draw the mesh
    vaos[static_cast<size_t>(VAONames::DefaultLineBox)].draw(GL_LINES);
    shaders[static_cast<size_t>(Shader::LightFrustumShader)].unbind();
    currentShaderBound = 0;
}

void dag::RenderingSystem::drawCameraOutline(dag::Entity const& entityID)
{
    auto descriptorSet = coordinator->getComponent<DescriptorSet>(entityID);
    shaders[static_cast<size_t>(Shader::CameraFrustumShader)].bind();
    currentShaderBound = shaders[static_cast<size_t>(Shader::CameraFrustumShader)].subShader.id;
    // Draw the mesh
    vaos[static_cast<size_t>(VAONames::DefaultLineBox)].draw(GL_LINES);
    shaders[static_cast<size_t>(Shader::CameraFrustumShader)].unbind();
    currentShaderBound = 0;
}

void dag::RenderingSystem::updateShaders()
{
    std::lock_guard<std::mutex> lock(this->hotReloadShadersMutex);

    for (auto& id : *shadersToUpdate)
    {
        try
        {
            shaders[id].reload();
        }
        catch (std::exception& e)
        {
            Log::error("Cannot compile shader: ", shaders[id].path.string(), " - Reverting to the old shader.");
        }
    }

    shadersToUpdate->clear();
}

void dag::RenderingSystem::enqueuePostProcessEffect(PostProcessFuncID const& postProcessFuncID)
{
    for (auto& id : postProcessStack)
    {
        if (id == static_cast<size_t>(postProcessFuncID))
        {
            return;
        }
    }

    postProcessStack.push_back(static_cast<size_t>(postProcessFuncID));

    std::sort(postProcessStack.begin(), postProcessStack.end(), [this](size_t& left, size_t& right) {
        size_t leftID = 0;
        size_t rightID = 0;

        for (size_t i = 0; i < postProcessStackOrder.size(); i++)
        {
            if (postProcessStackOrder[i] == left)
            {
                leftID = i;
            }

            if (postProcessStackOrder[i] == right)
            {
                rightID = i;
            }
        }

        return leftID < rightID;
    });
}

void dag::RenderingSystem::dequeuePostProcessEffect(PostProcessFuncID const& postProcessFuncID)
{
    int32_t id = -1;

    for (size_t i = 0; i < postProcessStack.size(); i++)
    {
        if (postProcessStack[i] == static_cast<size_t>(postProcessFuncID))
        {
            id = i;
        }
    }

    if (id >= 0)
    {
        postProcessStack.erase(postProcessStack.begin() + id, postProcessStack.begin() + id + 1);
    }
}

void dag::RenderingSystem::enqueueLateOverlays(OverlaysFuncID const& overlaysFuncID)
{
    for (auto& id : lateOverlaysStack)
    {
        if (id == static_cast<size_t>(overlaysFuncID))
        {
            return;
        }
    }

    lateOverlaysStack.push_back(static_cast<size_t>(overlaysFuncID));

    std::sort(lateOverlaysStack.begin(), lateOverlaysStack.end(), [this](size_t& left, size_t& right) {
        size_t leftID = 0;
        size_t rightID = 0;

        for (size_t i = 0; i < lateOverlaysStackOrder.size(); i++)
        {
            if (lateOverlaysStackOrder[i] == left)
            {
                leftID = i;
            }

            if (lateOverlaysStackOrder[i] == right)
            {
                rightID = i;
            }
        }

        return leftID < rightID;
    });
}

void dag::RenderingSystem::dequeueLateOverlays(OverlaysFuncID const& overlaysFuncID)
{
    int32_t id = -1;

    for (size_t i = 0; i < lateOverlaysStack.size(); i++)
    {
        if (lateOverlaysStack[i] == static_cast<size_t>(overlaysFuncID))
        {
            id = i;
        }
    }

    if (id >= 0)
    {
        lateOverlaysStack.erase(lateOverlaysStack.begin() + id, lateOverlaysStack.begin() + id + 1);
    }
}

void dag::RenderingSystem::dequeueLateOverlaysEntity()
{
    dequeueLateOverlays(OverlaysFuncID::OutlineEntity);
    dequeueLateOverlays(OverlaysFuncID::CameraEntity);
    dequeueLateOverlays(OverlaysFuncID::DirLightEntity);
    dequeueLateOverlays(OverlaysFuncID::SpotLightEntity);
    dequeueLateOverlays(OverlaysFuncID::PointLightEntity);
}

void dag::RenderingSystem::enqueueLateOverlaysEntity()
{
    if (scene->currentEntity != -1 && scene->currentEntity < scene->entities.size())
    {
        if (coordinator->hasComponent<LightComponent>(scene->entities[scene->currentEntity]))
        {
            auto& component = coordinator->getComponent<LightComponent>(scene->entities[scene->currentEntity]);

            if (component.type == LightComponent::Type::Directional)
            {
                enqueueLateOverlays(OverlaysFuncID::DirLightEntity);
            }
            if (component.type == LightComponent::Type::Spot)
            {
                enqueueLateOverlays(OverlaysFuncID::SpotLightEntity);
            }
            if (component.type == LightComponent::Type::Point)
            {
                enqueueLateOverlays(OverlaysFuncID::PointLightEntity);
            }
        }

        auto const cameraSystem = coordinator->getSystem<CameraSystem>();
        if (coordinator->hasComponent<CameraComponent>(scene->entities[scene->currentEntity]) && (cameraSystem->activeCamera != scene->entities[scene->currentEntity]))
        {
            enqueueLateOverlays(OverlaysFuncID::CameraEntity);
        }

        if (coordinator->hasComponent<MeshRenderer>(scene->entities[scene->currentEntity]))
        {
            enqueueLateOverlays(OverlaysFuncID::OutlineEntity);
        }
    }
}

void dag::RenderingSystem::updatePostProcessData(Volume const& volume)
{
    {
        auto& buf = uniformHandler.ubPPFogData.elements[0];
        buf.fogNear = volume.fogData.fogNear;
        buf.fogFar = volume.fogData.fogFar;
        buf.density = volume.fogData.density;
        buf.fogColor = volume.fogData.fogColor;
    }

    {
        auto& buf = uniformHandler.ubPPSharpenData.elements[0];
        buf.amount = volume.sharpenData.amount;
    }

    {
        auto& buf = uniformHandler.ubPPBlurData.elements[0];
        buf.kernelSize = volume.blurData.kernelSize;
    }

    {
        auto& buf = uniformHandler.ubPPChromaticAberrationData.elements[0];
        buf.rOffset = volume.chromaticAberrationData.rOffset;
        buf.gOffset = volume.chromaticAberrationData.gOffset;
        buf.bOffset = volume.chromaticAberrationData.bOffset;
    }

    {
        auto& buf = uniformHandler.ubPPPixelizeData.elements[0];
        buf.pixelSize = volume.pixelizeData.pixelSize;
    }

    {
        auto& buf = uniformHandler.ubPPDilationData.elements[0];
        buf.kernelSize = volume.dilationData.kernelSize;
        buf.separation = volume.dilationData.separation;
        buf.minThreshold = volume.dilationData.minThreshold;
        buf.maxThreshold = volume.dilationData.maxThreshold;
    }

    {
        auto& buf = uniformHandler.ubPPBloomData.elements[0];
        buf.threshold = volume.bloomData.threshold;
        buf.offset = volume.bloomData.offset;
    }
}

int32_t dag::RenderingSystem::getEntityOffset(Entity const& entity)
{
    size_t id = 0;

    for (auto& ent : entities)
    {
        if (entity == ent)
        {
            return id;
        }

        id++;
    }

    return -1;
}

void dag::RenderingSystem::execEncoderPass(RenderPass& renderPass)
{
    renderPass.begin();
    {
        glEnable(GL_DEPTH_TEST);
        renderIDs();
        glDisable(GL_DEPTH_TEST);
    }
    renderPass.end();
}

void dag::RenderingSystem::execDirShadowAccumulationPass(RenderPass& renderPass)
{
    auto const currentDirectionalLights = lightSystem->directionalLightsIDs.size();
    auto const currentSpotLights = lightSystem->spotLightsIDs.size();
    auto const currentPointLights = lightSystem->pointLightsIDs.size();

    glEnable(GL_DEPTH_TEST);

    uniformHandler.ubLightDrawData.bind();
    for (size_t i = 0; i < currentDirectionalLights; i++)
    {
        size_t offset = EngineLimits::DIR_LIGHT_SHADOW_MAPS * i;

        for (size_t j = 0; j < EngineLimits::DIR_LIGHT_SHADOW_MAPS; j++)
        {
            uniformHandler.ubLightDrawData.bindBufferRange(LightDrawData::index, LightDrawData::size() * (offset + j), LightDrawData::size());

            renderPass.writeFramebuffer->attachTexture(&directionalSMTextures, TextureAttachment::DEPTH_ATTACHMENT, 0, EngineLimits::DIR_LIGHT_SHADOW_MAPS * i + j);

            renderPass.begin();
            {
                renderEntities(Shader::ShadowmapShader);
            }
            renderPass.end();
        }
    }
    uniformHandler.ubLightDrawData.unbind();
    glDisable(GL_DEPTH_TEST);
}

void dag::RenderingSystem::execSpotShadowAccumulationPass(RenderPass& renderPass)
{
    auto const currentDirectionalLights = lightSystem->directionalLightsIDs.size();
    auto const currentSpotLights = lightSystem->spotLightsIDs.size();
    auto const currentPointLights = lightSystem->pointLightsIDs.size();

    glEnable(GL_DEPTH_TEST);

    uniformHandler.ubLightDrawData.bind();
    for (size_t i = 0; i < currentSpotLights; i++)
    {
        size_t offset = EngineLimits::DIR_LIGHT_SHADOW_MAPS * currentDirectionalLights + EngineLimits::SPOT_LIGHT_SHADOW_MAPS * i;

        for (size_t j = 0; j < EngineLimits::SPOT_LIGHT_SHADOW_MAPS; j++)
        {
            uniformHandler.ubLightDrawData.bindBufferRange(LightDrawData::index, LightDrawData::size() * (offset + j), LightDrawData::size());

            renderPass.writeFramebuffer->attachTexture(&spotSMTextures, TextureAttachment::DEPTH_ATTACHMENT, 0, EngineLimits::SPOT_LIGHT_SHADOW_MAPS * i + j);

            renderPass.begin();
            {
                renderEntities(RenderingSystem::Shader::ShadowmapShader);
            }
            renderPass.end();
        }
    }

    uniformHandler.ubLightDrawData.unbind();

    glDisable(GL_DEPTH_TEST);
}

void dag::RenderingSystem::execPointShadowAccumulationPass(RenderPass& renderPass)
{
    auto const currentDirectionalLights = lightSystem->directionalLightsIDs.size();
    auto const currentSpotLights = lightSystem->spotLightsIDs.size();
    auto const currentPointLights = lightSystem->pointLightsIDs.size();

    glEnable(GL_DEPTH_TEST);

    uniformHandler.ubLightDrawData.bind();
    for (size_t i = 0; i < currentPointLights; i++)
    {
        size_t offset = EngineLimits::DIR_LIGHT_SHADOW_MAPS * currentDirectionalLights + EngineLimits::SPOT_LIGHT_SHADOW_MAPS * currentSpotLights + EngineLimits::POINT_LIGHT_SHADOW_MAPS * i;

        for (size_t j = 0; j < EngineLimits::POINT_LIGHT_SHADOW_MAPS; j++)
        {
            uniformHandler.ubLightDrawData.bindBufferRange(LightDrawData::index, LightDrawData::size() * (offset + j), LightDrawData::size());

            renderPass.writeFramebuffer->attachTexture(&pointSMTextures, TextureAttachment::DEPTH_ATTACHMENT, 0, EngineLimits::POINT_LIGHT_SHADOW_MAPS * i + j);

            renderPass.begin();
            {
                renderEntities(Shader::ShadowmapShader);
            }
            renderPass.end();
        }
    }

    uniformHandler.ubLightDrawData.unbind();
    glDisable(GL_DEPTH_TEST);
}

void dag::RenderingSystem::execForwardPass(RenderPass& renderPass)
{
    renderPass.begin();
    {
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glDepthFunc(GL_LEQUAL);
        renderSkybox();
        glDepthFunc(GL_LESS);

        resourceManager->textureCube[0].bind(UniformBindings::cubeMapIndex);

        directionalSMTextures.bind(UniformBindings::directionalLightShadowMapsIndex);
        spotSMTextures.bind(UniformBindings::spotLightShadowMapsIndex);
        pointSMTextures.bind(UniformBindings::pointLightShadowMapsIndex);

        renderEntities();

        directionalSMTextures.unbind(UniformBindings::directionalLightShadowMapsIndex);
        spotSMTextures.unbind(UniformBindings::spotLightShadowMapsIndex);
        pointSMTextures.unbind(UniformBindings::pointLightShadowMapsIndex);

        resourceManager->textureCube[0].unbind(UniformBindings::cubeMapIndex);

        glDisable(GL_BLEND);
        glDisable(GL_DEPTH_TEST);
    }
    renderPass.end();
}

void dag::RenderingSystem::execDeferredWritePass(RenderPass& renderPass)
{
    renderPass.begin();
    {
        glEnable(GL_DEPTH_TEST);
        renderOpaqueEntities(Shader::DeferredWriteShader, true);
        glDisable(GL_DEPTH_TEST);
    }
    renderPass.end();
}

void dag::RenderingSystem::execDeferredProcessPass(RenderPass& renderPass)
{
    renderPass.begin();
    {
        for (size_t i = 0; i < 8; i++)
        {
            deferredFramebuffer.getColorAttachment(i)->bind(i);
        }

        resourceManager->textureCube[0].bind(UniformBindings::cubeMapIndex);

        directionalSMTextures.bind(UniformBindings::directionalLightShadowMapsIndex);
        spotSMTextures.bind(UniformBindings::spotLightShadowMapsIndex);
        pointSMTextures.bind(UniformBindings::pointLightShadowMapsIndex);

        renderShaderFullscreen(Shader::DeferredProcessShader);

        directionalSMTextures.unbind(UniformBindings::directionalLightShadowMapsIndex);
        spotSMTextures.unbind(UniformBindings::spotLightShadowMapsIndex);
        pointSMTextures.unbind(UniformBindings::pointLightShadowMapsIndex);
        resourceManager->textureCube[0].unbind(UniformBindings::cubeMapIndex);

        for (size_t i = 0; i < 8; i++)
        {
            deferredFramebuffer.getColorAttachment(i)->unbind(i);
        }    	
    }
    renderPass.end();

    Framebuffer::blit(*renderPass.readDepthFramebuffer, *renderPass.writeFramebuffer, GL_DEPTH_BUFFER_BIT);
}

void dag::RenderingSystem::execPostProcessStack(RenderPass& renderPass)
{
    Framebuffer::blit(*renderPass.readColorFramebuffer, postProcessSwapFramebuffer, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    for (size_t i = 0; i < postProcessStack.size(); i++)
    {
        postProcessFuncs[postProcessStack[i]](renderPass, {postProcessSwapFramebuffer.getColorAttachment(0), postProcessSwapFramebuffer.getDepthAttachment()});

        Framebuffer::blit(*renderPass.writeFramebuffer, postProcessSwapFramebuffer, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    Framebuffer::blit(postProcessSwapFramebuffer, *renderPass.writeFramebuffer, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void dag::RenderingSystem::execLateForwardPass(RenderPass& renderPass)
{
    renderPass.begin();
    {
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);

        glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        Framebuffer::blit(*renderPass.readColorFramebuffer, *renderPass.writeFramebuffer, GL_COLOR_BUFFER_BIT);
        Framebuffer::blit(*renderPass.readDepthFramebuffer, *renderPass.writeFramebuffer, GL_DEPTH_BUFFER_BIT);

        for (auto const& i : lateOverlaysStack)
        {
            lateOverlayFuncs[i]();
        }

        glDisable(GL_DEPTH_TEST);
        glDisable(GL_BLEND);
    }
    renderPass.end();
}

void dag::RenderingSystem::execCompositionPass(RenderPass& renderPass)
{
    Framebuffer::blit(*renderPass.readColorFramebuffer, *renderPass.writeFramebuffer, GL_COLOR_BUFFER_BIT);
}

void dag::RenderingSystem::execLateSkyboxRender()
{
    glDepthFunc(GL_LEQUAL);
    renderSkybox();
    glDepthFunc(GL_LESS);
}

void dag::RenderingSystem::execLateTransparentsRender()
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    resourceManager->textureCube[0].bind(UniformBindings::cubeMapIndex);

    directionalSMTextures.bind(UniformBindings::directionalLightShadowMapsIndex);
    spotSMTextures.bind(UniformBindings::spotLightShadowMapsIndex);
    pointSMTextures.bind(UniformBindings::pointLightShadowMapsIndex);

    renderTransparentEntities();

    directionalSMTextures.unbind(UniformBindings::directionalLightShadowMapsIndex);
    spotSMTextures.unbind(UniformBindings::spotLightShadowMapsIndex);
    pointSMTextures.unbind(UniformBindings::pointLightShadowMapsIndex);

    resourceManager->textureCube[0].unbind(UniformBindings::cubeMapIndex);

    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
}

void dag::RenderingSystem::execLateOverlayGrid()
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
    renderGrid();

	glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
}

void dag::RenderingSystem::execLateOverlayDirLightOutline()
{
    glDisable(GL_DEPTH_TEST);

    auto entity = uniformHandler.ubSceneData.elements[0].selectedLight;
    uniformHandler.ubLightDrawData.bind();

    for (size_t j = 0; j < EngineLimits::DIR_LIGHT_SHADOW_MAPS; j++)
    {
        uniformHandler.ubLightDrawData.bindBufferRange(LightDrawData::index, LightDrawData::size() * (entity + j), LightDrawData::size());
        drawLightOutline(entity + j);
    }

    uniformHandler.ubLightDrawData.unbind();
    glEnable(GL_DEPTH_TEST);
}

void dag::RenderingSystem::execLateOverlaySpotLightOutline()
{
    glDisable(GL_DEPTH_TEST);

    auto entity = uniformHandler.ubSceneData.elements[0].selectedLight;
    uniformHandler.ubLightDrawData.bind();

    for (size_t j = 0; j < EngineLimits::SPOT_LIGHT_SHADOW_MAPS; j++)
    {
        uniformHandler.ubLightDrawData.bindBufferRange(LightDrawData::index, LightDrawData::size() * (entity + j), LightDrawData::size());
        drawLightOutline(entity + j);
    }

    uniformHandler.ubLightDrawData.unbind();
    glEnable(GL_DEPTH_TEST);
}

void dag::RenderingSystem::execLateOverlayPointLightOutline()
{
    glDisable(GL_DEPTH_TEST);

    auto entity = uniformHandler.ubSceneData.elements[0].selectedLight;
    uniformHandler.ubLightDrawData.bind();

    for (size_t j = 0; j < EngineLimits::POINT_LIGHT_SHADOW_MAPS; j++)
    {
        uniformHandler.ubLightDrawData.bindBufferRange(LightDrawData::index, LightDrawData::size() * (entity + j), LightDrawData::size());
        drawLightOutline(entity + j);
    }

    uniformHandler.ubLightDrawData.unbind();
    glEnable(GL_DEPTH_TEST);
}

void dag::RenderingSystem::execLateOverlayCameraOutline()
{
    auto entity = scene->entities[scene->currentEntity];

    glDisable(GL_DEPTH_TEST);
    drawCameraOutline(entity);
    glEnable(GL_DEPTH_TEST);
}

void dag::RenderingSystem::execLateOverlayEntityOutline()
{
    auto entity = scene->entities[scene->currentEntity];

    uniformHandler.ubDrawData.bind();
    uniformHandler.ubDrawData.bindBufferRange(DrawData::index, DrawData::size() * entity, DrawData::size());

    glEnable(GL_STENCIL_TEST);

    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
    glStencilMask(0xFF);
    glStencilFunc(GL_ALWAYS, 1, 0xFF);
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    renderEntity(entity);
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

    glStencilMask(0xFF);
    glStencilFunc(GL_NOTEQUAL, 1, 0xFF);

    glDisable(GL_DEPTH_TEST);
    outlineEntity(entity);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_STENCIL_TEST);

    uniformHandler.ubDrawData.unbind();
}

void dag::RenderingSystem::execLateOverlayDebugPhysicsColliders()
{
    renderDebugPhysicsColliders();
}

void dag::RenderingSystem::execDebugRenderEntitiesWireframeForward(RenderPass& renderpass)
{
    renderpass.begin();
    {
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        setWireframe(true);
        renderEntities(Shader::DebugForwardUnlitShader, true);
        setWireframe(false);
        glDisable(GL_BLEND);
        glDisable(GL_DEPTH_TEST);
    }
    renderpass.end();
}

void dag::RenderingSystem::execDebugRenderEntitiesWireframeDeferredWrite(RenderPass& renderpass)
{
    renderpass.begin();
    {
        glEnable(GL_DEPTH_TEST);
        setWireframe(true);
        renderEntities(Shader::DeferredWriteShader, true);
        setWireframe(false);
        glDisable(GL_DEPTH_TEST);
    }
    renderpass.end();
}

void dag::RenderingSystem::execDebugRenderEntitiesWireframeDeferredProcess(RenderPass& renderpass)
{
    renderpass.begin();
    {
        for (size_t i = 0; i < renderpass.inputAttachments.size(); i++)
        {
            renderpass.inputAttachments[i]->bind(i);
        }

        renderShaderFullscreen(Shader::DebugDeferredUnlitShader);

        for (size_t i = 0; i < renderpass.inputAttachments.size(); i++)
        {
            renderpass.inputAttachments[i]->unbind(i);
        }
    }
    renderpass.end();

    Framebuffer::blit(*renderpass.readDepthFramebuffer, *renderpass.writeFramebuffer, GL_DEPTH_BUFFER_BIT);
}

void dag::RenderingSystem::execDebugRenderEntitiesDepthForward(RenderPass& renderpass)
{
    renderpass.begin();
    {
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        renderEntities(Shader::DebugForwardDepthShader, true);
        glDisable(GL_BLEND);
        glDisable(GL_DEPTH_TEST);
    }
    renderpass.end();
}

void dag::RenderingSystem::execDebugRenderEntitiesDepthDeferred(RenderPass& renderpass)
{
    renderpass.begin();
    {
        // TODO Replace with input attachments
        for (size_t i = 0; i < 8; i++)
        {
            deferredFramebuffer.getColorAttachment(i)->bind(i);
        }

        renderShaderFullscreen(Shader::DebugDeferredDepthShader);

        for (size_t i = 0; i < 8; i++)
        {
            deferredFramebuffer.getColorAttachment(i)->unbind(i);
        }
    }
    renderpass.end();

    Framebuffer::blit(*renderpass.readDepthFramebuffer, *renderpass.writeFramebuffer, GL_DEPTH_BUFFER_BIT);
}

void dag::RenderingSystem::execDebugRenderEntitiesNormalsForward(RenderPass& renderpass)
{
    renderpass.begin();
    {
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        renderEntities(Shader::DebugForwardNormalsShader, true);
        glDisable(GL_BLEND);
        glDisable(GL_DEPTH_TEST);
    }
    renderpass.end();
}

void dag::RenderingSystem::execDebugRenderEntitiesNormalsDeferred(RenderPass& renderpass)
{
    renderpass.begin();
    {
        // TODO Replace with input attachments
        for (size_t i = 0; i < 8; i++)
        {
            deferredFramebuffer.getColorAttachment(i)->bind(i);
        }

        renderShaderFullscreen(Shader::DebugDeferredNormalsShader);

        for (size_t i = 0; i < 8; i++)
        {
            deferredFramebuffer.getColorAttachment(i)->unbind(i);
        }
    }
    renderpass.end();

    Framebuffer::blit(*renderpass.readDepthFramebuffer, *renderpass.writeFramebuffer, GL_DEPTH_BUFFER_BIT);
}

void dag::RenderingSystem::execDebugRenderEntitiesUnlitForward(RenderPass& renderpass)
{
    renderpass.begin();
    {
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        renderEntities(Shader::DebugForwardUnlitShader, true);
        glDisable(GL_BLEND);
        glDisable(GL_DEPTH_TEST);
    }
    renderpass.end();
}

void dag::RenderingSystem::execDebugRenderEntitiesUnlitDeferred(RenderPass& renderpass)
{
    renderpass.begin();
    {
        // TODO Replace with input attachments
        for (size_t i = 0; i < 8; i++)
        {
            deferredFramebuffer.getColorAttachment(i)->bind(i);
        }

        renderShaderFullscreen(Shader::DebugDeferredUnlitShader);

        for (size_t i = 0; i < 8; i++)
        {
            deferredFramebuffer.getColorAttachment(i)->unbind(i);
        }
    }
    renderpass.end();

    Framebuffer::blit(*renderpass.readDepthFramebuffer, *renderpass.writeFramebuffer, GL_DEPTH_BUFFER_BIT);
}

void dag::RenderingSystem::enqueueShaderReload(size_t const& shaderID)
{
    std::lock_guard<std::mutex> lock(this->hotReloadShadersMutex);

    for (auto& id : *shadersToUpdate)
    {
        if (id == shaderID)
        {
            return;
        }
    }

    shadersToUpdate->push_back(shaderID);
}

void dag::RenderingSystem::setWireframe(bool const& value)
{
    if (value)
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    else
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
}

void dag::RenderingSystem::ppFog(RenderPass& renderpass, std::vector<Texture*> const& attachments)
{
    uniformHandler.ubPPFogData.bindBufferBase(PostProcessBloomSettings::index);

    for (size_t i = 0; i < attachments.size(); i++)
    {
        attachments[i]->bind(i);
    }

    renderpass.begin();
    {
        renderShaderFullscreen(Shader::PostprocessFogShader);
    }
    renderpass.end();

    for (size_t i = 0; i < attachments.size(); i++)
    {
        attachments[i]->unbind(i);
    }
}

void dag::RenderingSystem::ppSharpen(RenderPass& renderpass, std::vector<Texture*> const& attachments)
{
    uniformHandler.ubPPSharpenData.bindBufferBase(PostProcessSharpen::index);

    for (size_t i = 0; i < attachments.size(); i++)
    {
        attachments[i]->bind(i);
    }

    renderpass.begin();
    {
        renderShaderFullscreen(Shader::PostprocessSharpenShader);
    }
    renderpass.end();

    for (size_t i = 0; i < attachments.size(); i++)
    {
        attachments[i]->unbind(i);
    }
}

void dag::RenderingSystem::ppBlur(RenderPass& renderpass, std::vector<Texture*> const& attachments)
{
    uniformHandler.ubPPBlurData.bindBufferBase(PostProcessBlur::index);

    for (size_t i = 0; i < attachments.size(); i++)
    {
        attachments[i]->bind(i);
    }

    renderpass.begin();
    {
        renderShaderFullscreen(Shader::PostprocessBlurShader);
    }
    renderpass.end();

    for (size_t i = 0; i < attachments.size(); i++)
    {
        attachments[i]->unbind(i);
    }
}

void dag::RenderingSystem::ppToneMapping(RenderPass& renderpass, std::vector<Texture*> const& attachments)
{
    for (size_t i = 0; i < attachments.size(); i++)
    {
        attachments[i]->bind(i);
    }

    renderpass.begin();
    {
        renderShaderFullscreen(Shader::PostprocessToneMappingShader);
    }
    renderpass.end();

    for (size_t i = 0; i < attachments.size(); i++)
    {
        attachments[i]->unbind(i);
    }
}

void dag::RenderingSystem::ppChromaticAberration(RenderPass& renderpass, std::vector<Texture*> const& attachments)
{
    uniformHandler.ubPPChromaticAberrationData.bindBufferBase(PostProcessChromaticAberration::index);

    for (size_t i = 0; i < attachments.size(); i++)
    {
        attachments[i]->bind(i);
    }

    renderpass.begin();
    {
        renderShaderFullscreen(Shader::PostprocessChromaticAberrationShader);
    }
    renderpass.end();

    for (size_t i = 0; i < attachments.size(); i++)
    {
        attachments[i]->unbind(i);
    }
}

void dag::RenderingSystem::ppPixelize(RenderPass& renderpass, std::vector<Texture*> const& attachments)
{
    uniformHandler.ubPPPixelizeData.bindBufferBase(PostProcessPixelize::index);

    for (size_t i = 0; i < attachments.size(); i++)
    {
        attachments[i]->bind(i);
    }

    renderpass.begin();
    {
        renderShaderFullscreen(Shader::PostprocessPixelizeShader);
    }
    renderpass.end();

    for (size_t i = 0; i < attachments.size(); i++)
    {
        attachments[i]->unbind(i);
    }
}

void dag::RenderingSystem::ppDilation(RenderPass& renderpass, std::vector<Texture*> const& attachments)
{
    uniformHandler.ubPPDilationData.bindBufferBase(PostProcessDilation::index);

    for (size_t i = 0; i < attachments.size(); i++)
    {
        attachments[i]->bind(i);
    }

    renderpass.begin();
    {
        renderShaderFullscreen(Shader::PostprocessDilationShader);
    }

    for (size_t i = 0; i < attachments.size(); i++)
    {
        attachments[i]->unbind(i);
    }

    renderpass.end();
}

void dag::RenderingSystem::ppBloom(RenderPass& renderpass, std::vector<Texture*> const& attachments)
{
    uniformHandler.ubPPBloomData.bindBufferBase(PostProcessBloomSettings::index);

    // Mask
    {
        RenderPass maskPass{
            nullptr,
            nullptr,
            attachments,
            &bloomScaledFramebuffers[bloomIterations],
            renderpass.clearValues};

        maskPass.begin();
        {
            for (size_t i = 0; i < attachments.size(); i++)
            {
                attachments[i]->bind(i);
            }

            renderShaderFullscreen(Shader::PostprocessBloomMaskShader);

            for (size_t i = 0; i < attachments.size(); i++)
            {
                attachments[i]->unbind(i);
            }
        }
        maskPass.end();
    }

    // Downsample
    {
        for (size_t i = 0; i < bloomIterations; i++)
        {
            RenderPass downPass{
                nullptr,
                nullptr,
                attachments,
                &bloomScaledFramebuffers[i],
                renderpass.clearValues};

            downPass.begin();
            {
                if (i == 0)
                {
                    bloomScaledFramebuffers[bloomIterations].getColorAttachment(0)->bind(0);
                }
                else
                {
                    bloomScaledFramebuffers[i - 1].getColorAttachment(0)->bind(0);
                }

                renderShaderFullscreen(Shader::PostprocessDualFilterDownsampleShader);
                //renderShaderFullscreen(Shader::PostprocessBlurShader);

                if (i == 0)
                {
                    bloomScaledFramebuffers[bloomIterations].getColorAttachment(0)->unbind(0);
                }
                else
                {
                    bloomScaledFramebuffers[i - 1].getColorAttachment(0)->unbind(0);
                }
            }
            downPass.end();
        }
    }

    // Upsample
    {
        for (size_t i = bloomIterations - 1; i > 0; i--)
        {
            RenderPass upPass{
                nullptr,
                nullptr,
                attachments,
                &bloomScaledFramebuffers[i - 1],
                renderpass.clearValues};

            upPass.begin();
            {
                bloomScaledFramebuffers[i].getColorAttachment(0)->bind(0);

                renderShaderFullscreen(Shader::PostprocessDualFilterUpsampleShader);
                //renderShaderFullscreen(Shader::PostprocessBlurShader);

                bloomScaledFramebuffers[i].getColorAttachment(0)->unbind(0);
            }
            upPass.end();
        }
    }

    // Combine
    renderpass.begin();
    {
        for (size_t i = 0; i < attachments.size(); i++)
        {
            attachments[i]->bind(i);
        }

        bloomScaledFramebuffers[0].getColorAttachment(0)->bind(attachments.size());

        renderShaderFullscreen(Shader::PostprocessBloomCombineShader);

        bloomScaledFramebuffers[0].getColorAttachment(0)->unbind(attachments.size());

        for (size_t i = 0; i < attachments.size(); i++)
        {
            attachments[i]->unbind(i);
        }
    }
    renderpass.end();
}

/**
 * @brief Wrapper for updating a float uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform1f(Descriptor const& descriptor)
{
    glUniform1f(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), std::any_cast<float>(descriptor.data));
}

/**
 * @brief Wrapper for updating an integer uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform1i(Descriptor const& descriptor)
{
    glUniform1i(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), std::any_cast<int32_t>(descriptor.data));
}

/**
 * @brief Wrapper for updating an unsigned int uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform1ui(Descriptor const& descriptor)
{
    glUniform1ui(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), std::any_cast<uint32_t>(descriptor.data));
}

/**
 * @brief Wrapper for updating a float2 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform2fv(Descriptor const& descriptor)
{
    glUniform2fv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::vec2>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a float3 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform3fv(Descriptor const& descriptor)
{
    glUniform3fv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::vec3>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a float4 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform4fv(Descriptor const& descriptor)
{
    glUniform4fv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::vec4>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a int2 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform2iv(Descriptor const& descriptor)
{
    glUniform2iv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::ivec2>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a int3 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform3iv(Descriptor const& descriptor)
{
    glUniform3iv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::ivec3>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a int4 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform4iv(Descriptor const& descriptor)
{
    glUniform4iv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::ivec4>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a uint2 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform2uiv(Descriptor const& descriptor)
{
    glUniform2uiv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::uvec2>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a uint3 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform3uiv(Descriptor const& descriptor)
{
    glUniform3uiv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::uvec3>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a uint4 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniform4uiv(Descriptor const& descriptor)
{
    glUniform4uiv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, glm::value_ptr(std::any_cast<glm::uvec4>(descriptor.data)));
}

/**
 * @brief Wrapper for updating a matrix4x4 uniform
 * @param descriptor 
*/
void dag::RenderingSystem::wrapUniformMatrix4fv(Descriptor const& descriptor)
{
    glUniformMatrix4fv(glGetUniformLocation(currentShaderBound, descriptor.name.c_str()), 1, false, glm::value_ptr(std::any_cast<glm::mat4>(descriptor.data)));
}

void dag::RenderingSystem::renderDebugPhysicsColliders()
{
    auto physicsSystem = coordinator->getSystem<PhysicsSystem>();
    uniformHandler.ubDrawData.bind();
    for (auto& entity : physicsSystem->entities)
    {
        uniformHandler.ubDrawData.bindBufferRange(DrawData::index, DrawData::size() * entity, DrawData::size());

        shaders[static_cast<size_t>(Shader::SphereBoundsShader)].bind();
        currentShaderBound = shaders[static_cast<size_t>(Shader::SphereBoundsShader)].subShader.id;

        vaos[static_cast<size_t>(VAONames::DefaultSphereBounds)].draw(GL_LINES);

        shaders[static_cast<size_t>(Shader::SphereBoundsShader)].unbind();
        currentShaderBound = 0;
    }
    uniformHandler.ubDrawData.unbind();
}