#include "dagmar/ScriptSystem.h"


/**
 * @brief Startup function for the script system
 */
void dag::ScriptSystem::startup()
{
    Log::info("ScriptSystem: In startup()");

    coordinator->registerComponent<dag::ScriptComponent>();
    
    dag::Signature scriptSignature;
    scriptSignature.set(coordinator->getComponentType<dag::Transform>());
    scriptSignature.set(coordinator->getComponentType<dag::ScriptComponent>());

    coordinator->setSystemSignature<dag::ScriptSystem>(scriptSignature);

    Log::info("ScriptSystem: In startup()");
}

/*
 * @brief Shutdown function for the script system
 */
void dag::ScriptSystem::shutdown()
{
    startFlag = false;
    onPlayMode = false;
}

/*
 * @brief save: save state
 */
void dag::ScriptSystem::save()
{
    Log::info("ScriptSystem: In Save()");
    for (auto entity : entities)
    {
        coordinator->getComponent<ScriptComponent>(entity).save();
    }
}

/*
 * @brief reset: reset to start().
 */
void dag::ScriptSystem::reset()
{
    Log::info("ScriptSystem: In Reset()");
    for (auto entity : entities)
    {
        coordinator->getComponent<ScriptComponent>(entity).reset();
    }

    if (onPlayMode == 0)
    {

    }
}

/*
 * @brief start: called before update()
 */
void dag::ScriptSystem::start()
{
    Log::info("ScriptSystem: In Start()");
    startFlag = true;

    for (auto entity : entities)
    {
        coordinator->getComponent<ScriptComponent>(entity).start();
    }

    
}

/*
 * @brief update: Updates each frame
 */
void dag::ScriptSystem::update()
{
    Log::info("ScriptSystem: In Update()");

    if (!startFlag)
        dag::ScriptSystem::start();

    if (onPlayMode)
    {
        onPlayMode = 0;
        //Pause Routine
    }

    for (auto entity : entities)
    {
        coordinator->getComponent<ScriptComponent>(entity).update();
    }

}

/*
 * @brief lateUpdate: The last update befor exit
 */
void dag::ScriptSystem::lateUpdate()
{
    Log::info("ScriptSystem: In Start()");

    for (auto entity : entities)
    {
        coordinator->getComponent<ScriptComponent>(entity).lateUpdate();
    }
}

/*
 * @brief onPause: Pause Game State
 */
void dag::ScriptSystem::onPause()
{
    Log::info("ScriptSystem: In onPause()");

    for (auto entity : entities)
    {
        coordinator->getComponent<ScriptComponent>(entity).onPause();
    }
   
}

