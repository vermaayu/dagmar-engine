#include "dagmar/PlayerControllerSystem.h"
#include "dagmar/Log.h"

/**
 * @brief Default cosntructor to initialise the global variables & coefficients
  */
dag::PlayerControllerSystem::PlayerControllerSystem()
{
    activePlayer = -1;
}

dag::PlayerControllerSystem::~PlayerControllerSystem()
{
    
}

/**
 * @brief Register the needed components and the system
  */

void dag::PlayerControllerSystem::startup()
{
    coordinator->registerComponent<dag::PlayerController>();

    dag::Signature playerControllerSystemSignature;
    playerControllerSystemSignature.set(coordinator->getComponentType<dag::Transform>());
    playerControllerSystemSignature.set(coordinator->getComponentType<dag::PlayerController>());

    coordinator->setSystemSignature<dag::PlayerControllerSystem>(playerControllerSystemSignature);
}



/**
 * @brief deactivate all the other player controllers
  */
void dag::PlayerControllerSystem::setPossessed(dag::Entity const& activeEntity, bool active){
    // we possesed a camera
    if(active){
        activePlayer = activeEntity;
        for(auto& entity : entities){
            if(entity != activeEntity){
                auto& playerController = coordinator->getComponent<PlayerController>(entity);
                playerController.isActive = false;
            }
        }
    }
    else{
        // go back to default no player
        activePlayer = -1;
    }
}