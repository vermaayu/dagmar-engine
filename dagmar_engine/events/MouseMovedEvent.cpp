#include "dagmar/MouseMovedEvent.h"

/**
 * @brief Constructor for mouse moved event
 */
dag::MouseMovedEvent::MouseMovedEvent(float const& x, float const& y) :
    x(x), y(y)
{
}

/**
 * @brief Gets the x position of the mouse
 */
float dag::MouseMovedEvent::getX() const
{
    return x;
}

/**
 * @brief Gets the y position of the mouse
 */
float dag::MouseMovedEvent::getY() const
{
    return y;
}

/**
 * @brief Gets the static type
 */
dag::Event::Type dag::MouseMovedEvent::getStaticType()
{
    return Type::MouseMoved;
}

/**
 * @brief Gets the type of the object
 */
dag::Event::Type dag::MouseMovedEvent::getType() const
{
    return Type::MouseMoved;
}

/**
 * @brief Gets the name of the event
 */
std::string dag::MouseMovedEvent::getName() const
{
    return "MouseMovedEvent";
}

/**
 * @brief To string implementation
 */
std::string dag::MouseMovedEvent::toString()
{
    return getName() + ": x=" + std::to_string(x) + " y=" + std::to_string(y);
}

/**
 * @brief Gets the category bit flags
 */
int dag::MouseMovedEvent::getCategoryBitFlags() const
{
    return CategoryBitFlags::Mouse | CategoryBitFlags::Input;
}