#include "dagmar/WindowResizeEvent.h"

/**
 * @brief Window resize event constructor
 */
dag::WindowResizeEvent::WindowResizeEvent(uint32_t const& width, uint32_t const& height) :
    width(width), height(height)
{
}

/**
 * @brief Gets the width of the window
 */
uint32_t dag::WindowResizeEvent::getWidth() const
{
    return width;
}

/**
 * @brief Gets the height of the window
 */
uint32_t dag::WindowResizeEvent::getHeight() const
{
	return height;
}

/**
 * @brief Gets the static type
 */
dag::Event::Type dag::WindowResizeEvent::getStaticType()
{
    return Type::WindowResize;
}

/**
 * @brief Gets the type of the object
 */
dag::Event::Type dag::WindowResizeEvent::getType() const
{
    return getStaticType();
}

/**
 * @brief Gets the name of the object
 */
std::string dag::WindowResizeEvent::getName() const
{
    return "WindowResizeEvent";
}

/**
 * @brief To string implementation
 */
std::string dag::WindowResizeEvent::toString()
{
    return getName() + ": " + "width=" + std::to_string(width) + " height: " + std::to_string(height);
}