#include "dagmar/EventDispatcher.h"

/**
 * @brief Dispatcher constructor
 */
dag::EventDispatcher::EventDispatcher(Event& event) :
    event(event)
{
}