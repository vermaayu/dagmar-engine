#include "dagmar/WindowEvent.h"

/**
 * @brief Gets the category bit flags
 */
int dag::WindowEvent::getCategoryBitFlags() const
{
    return CategoryBitFlags::Application | CategoryBitFlags::Window;
}