#include "dagmar/WindowCloseEvent.h"

/**
 * @brief Gets the static type
 */
dag::Event::Type dag::WindowCloseEvent::getStaticType()
{
    return Type::WindowClose;
}

/**
 * @brief Gets the type of the object
 */
dag::Event::Type dag::WindowCloseEvent::getType() const
{
    return getStaticType();
}

/**
 * @brief Gets the name of the event
 */
std::string dag::WindowCloseEvent::getName() const
{
    return "WindowCloseEvent";
}

/**
 * @brief To string implementation
 */
std::string dag::WindowCloseEvent::toString()
{
    return getName() + ": closed";
}
