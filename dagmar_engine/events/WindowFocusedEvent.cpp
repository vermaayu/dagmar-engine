#include "dagmar/WindowFocusedEvent.h"

/**
 * @brief Gets the static type
 */
dag::Event::Type dag::WindowFocusedEvent::getStaticType()
{
    return Type::WindowFocus;
}

/**
 * @brief Gets the type of the object
 */
dag::Event::Type dag::WindowFocusedEvent::getType() const
{
    return getStaticType();
}

/**
 * @brief Gets the name of the event
 */
std::string dag::WindowFocusedEvent::getName() const
{
    return "WindowFocusedEvent";
}

/**
 * @brief To string implementation
 */
std::string dag::WindowFocusedEvent::toString()
{
    return getName() + ": window focused";
}