#include "dagmar/MouseButtonPressedEvent.h"

/**
 * @brief Constructor for mouse button pressed event
 */
dag::MouseButtonPressedEvent::MouseButtonPressedEvent(int const& button, int const& mods) :
    MouseButtonEvent(button, mods)
{
}

/**
 * @brief Gets the static type 
 */
dag::Event::Type dag::MouseButtonPressedEvent::getStaticType()
{
    return Type::MouseButtonPressed;
}

/**
 * @brief Gets the type of the instance
 */
dag::Event::Type dag::MouseButtonPressedEvent::getType() const
{
    return getStaticType();
}

/**
 * @brief Gets the name of the event
 */
std::string dag::MouseButtonPressedEvent::getName() const
{
    return "MouseButtonPressedEvent";
}

/**
 * @brief To string implementation
 */
std::string dag::MouseButtonPressedEvent::toString()
{
    return getName() + ": " + std::to_string(button);
}