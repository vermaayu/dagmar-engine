#pragma once
#include "Event.h"

namespace dag
{
    /**
     * @brief MouseScrolledEvent class
     *
     * Is an extension of the event class, used for scrolling
     */
    class MouseScrolledEvent : public Event
    {
      private:
        float xOffset;
        float yOffset;

      public:
        MouseScrolledEvent(float const& xOffset, float const& yOffset);
        float getXOffset() const;
        float getYOffset() const;

    	static Type getStaticType();
        Type getType() const override;
        std::string getName() const override;
        std::string toString() override;
        int getCategoryBitFlags() const override;
    };
} // namespace dag