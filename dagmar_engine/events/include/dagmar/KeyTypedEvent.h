#pragma once
#include "dagmar/KeyEvent.h"

namespace dag
{
    /**
     * @brief KeyTypedEvent class
     *
     * Is a KeyEvent extension, used for actual typing
     */
    class KeyTypedEvent : public KeyEvent
    {
      public:
        KeyTypedEvent(int const& keycode, int const& scanCode, int const& action, int const& mods);

        static Type getStaticType();
        Type getType() const override;
        std::string getName() const override;
        std::string toString() override;
    };
} // namespace dag