#pragma once

#include "dagmar/Event.h"

namespace dag
{
    /**
     * @brief KeyEvent class
     *
     * Mainly working with key events (keyboard)
     */
    class KeyEvent : public Event
    {
      protected:
        int keyCode;
        int scanCode;
        int action;
    	int mods;

      protected:
    	// Constructor
        KeyEvent(int const& keyCode, int const& scanCode, int const& action, int const& mods);

      public:
        int getKeyCode() const;
        int getScanCode() const;
        int getAction() const;
        int getMods() const;
        int getCategoryBitFlags() const override;
    };
} // namespace dag
