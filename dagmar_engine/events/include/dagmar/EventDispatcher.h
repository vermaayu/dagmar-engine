#pragma once
#include "Event.h"

namespace dag
{
/**
 * \brief Binding magic instead of std::bind(..., placeholder)
 * \param fn 
 */
#define BIND_EVENT_FN(fn) [this](auto&& arg) { return fn(std::forward<decltype(arg)>(arg)); }
    /**
     * @brief Event dispatcher class
     */
    class EventDispatcher
    {
      private:
        Event& event;

      public:
        // Constructor
        EventDispatcher(Event& event);

        /**
    	 * @brief Dispatches a function of an event
    	 */
        template <typename BaseEvent, typename F>
        bool dispatch(F const& fn)
        {
            //std::cout << (int)event.getType() << std::endl;
            if (event.getType() == BaseEvent::getStaticType())
            {
                event.consumed = fn(static_cast<BaseEvent&>(event));
                return true;
            }

            return false;
        }
    };
} // namespace dag