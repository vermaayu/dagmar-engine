#pragma once
#include "MouseButtonEvent.h"

namespace dag
{
    /**
     * @brief MouseButtonPressedEvent class
     *
     * Is an extension of MouseButtonEvent, used for pressed events
     */
    class MouseButtonPressedEvent : public MouseButtonEvent
    {
      public:
        MouseButtonPressedEvent(int const& button, int const& mods);

    	static Type getStaticType();
        Type getType() const override;
        std::string getName() const override;
        std::string toString() override;
    };
} // namespace dag