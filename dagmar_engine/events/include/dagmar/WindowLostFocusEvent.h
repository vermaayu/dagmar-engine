#pragma once
#include "dagmar/WindowEvent.h"

namespace dag
{
    /**
     * @brief WindowLostFocusEvent class
     *
     * Is an extension of WindowEvent, called when
     * the window loses focus
     */
    class WindowLostFocusEvent : public WindowEvent
    {
      public:
        WindowLostFocusEvent() = default;

        static Type getStaticType();
        Type getType() const override;
        std::string getName() const override;
        std::string toString() override;
    };
} // namespace dag
