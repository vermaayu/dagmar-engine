#pragma once
#include "Event.h"

namespace dag
{
    /**
     * @brief MouseButtonEvent class
     *
     * Is an event, which should be extended for mouse button events
     */
    class MouseButtonEvent : public Event
    {
      protected:
        int button;
        int mods;

      protected:
        MouseButtonEvent(int const& button, int const& mods);

      public:
        int getButton() const;
        int getMods() const;
        int getCategoryBitFlags() const override;
    };
} // namespace dag