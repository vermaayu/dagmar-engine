#pragma once
#include "dagmar/WindowEvent.h"

namespace dag
{
    /**
     * @brief WindowFocusedEvent class
     *
     * Is an extension of the WindowEvent class, called when the
     * window is focusesd
     */
    class WindowFocusedEvent : public WindowEvent
    {
      public:
        WindowFocusedEvent() = default;

        static Type getStaticType();
        Type getType() const override;
        std::string getName() const override;
        std::string toString() override;
    };
} // namespace dag
