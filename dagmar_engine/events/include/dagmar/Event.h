#pragma once
#include <string>

namespace dag
{
    /**
     * @brief Event class
     * Contains Type, CategoryBitFlags
     */
    class Event
    {
      public:
        enum class Type
        {
            None = 0,
            WindowFocus,
            WindowLostFocus,
            WindowResize,
            WindowMoved,
            WindowClose,
            KeyPressed,
            KeyReleased,
            KeyTyped,
            MouseButtonPressed,
            MouseButtonReleased,
            MouseMoved,
            MouseScrolled
        };

        enum CategoryBitFlags
        {
            None = 0,
            Application = 1,
            Input = 2,
            Keyboard = 4,
            Mouse = 8,
        	MouseButton = 16,
        	Window = 32
        };


        bool consumed = false;

      public:
    	virtual ~Event() = default;
        virtual Type getType() const = 0;
        virtual std::string getName() const = 0;
        virtual int getCategoryBitFlags() const = 0;

        virtual std::string toString();

        // Checks the category of the event
        bool isInCategory(CategoryBitFlags const& flags) const;
    };
} // namespace dag