#pragma once
#include "dagmar/Event.h"

namespace dag
{
    /**
     * @brief WindowEvent class
     *
     * Is an extension of the event class, used as the
     * base class for window events
     */
    class WindowEvent : public Event
    {
      protected:
        WindowEvent() = default;

      public:
        int getCategoryBitFlags() const override;
    };
} // namespace dag
