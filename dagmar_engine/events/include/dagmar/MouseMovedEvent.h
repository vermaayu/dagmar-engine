#pragma once
#include "Event.h"

namespace dag
{
    /**
     * @brief MouseMovedEvent class
     *
     * Is an extension of event, used when the mouse moves
     */
    class MouseMovedEvent : public Event
    {
      private:
        float x;
        float y;

      public:
        MouseMovedEvent(float const& x, float const& y);

        float getX() const;
        float getY() const;

        static Type getStaticType();
        Type getType() const override;
        std::string getName() const override;
        std::string toString() override;
        int getCategoryBitFlags() const override;
    };
} // namespace dag
