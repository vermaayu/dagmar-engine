#pragma once
#include "dagmar/WindowEvent.h"

namespace dag
{
    /**
     * @brief WindowCloseEvent class
     *
     * Is an extension of WindowEvent, called when the window closes
     */
    class WindowCloseEvent : public WindowEvent
    {
      public:
        WindowCloseEvent() = default;

        static Type getStaticType();
        Type getType() const override;
        std::string getName() const override;
        std::string toString() override;
    };
} // namespace dag