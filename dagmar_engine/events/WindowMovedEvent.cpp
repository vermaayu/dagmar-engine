#include "dagmar/WindowMovedEvent.h"

/**
 * @brief Constructor for window moved event
 */
dag::WindowMovedEvent::WindowMovedEvent(uint32_t const& x, uint32_t const& y) :
    x(x), y(y)
{
}

/**
 * @brief Gets the new x coordinate of the window position
 */
uint32_t dag::WindowMovedEvent::getX() const
{
    return x;
}

/**
 * @brief Gets the new y coordinate of the window position
 */
uint32_t dag::WindowMovedEvent::getY() const
{
    return y;
}

/**
 * @brief Gets the static type
 */
dag::Event::Type dag::WindowMovedEvent::getStaticType()
{
    return Type::WindowMoved;
}

/**
 * @brief Gets the type of the object
 */
dag::Event::Type dag::WindowMovedEvent::getType() const
{
    return getStaticType();
}

/**
 * @brief Gets the name of the event
 */
std::string dag::WindowMovedEvent::getName() const
{
    return "WindowMovedEvent";
}

/**
 * @brief To string implementation
 */
std::string dag::WindowMovedEvent::toString()
{
    return getName() + ": to X=" + std::to_string(x) + " Y=" + std::to_string(y);
}