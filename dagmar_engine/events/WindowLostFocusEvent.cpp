#include "dagmar/WindowLostFocusEvent.h"

/**
 * @brief Gets the static type
 */
dag::Event::Type dag::WindowLostFocusEvent::getStaticType()
{
    return Type::WindowLostFocus;
}

/**
 * @brief Gets the type of the object
 */
dag::Event::Type dag::WindowLostFocusEvent::getType() const
{
    return getStaticType();
}

/**
 * @brief Gets the name of the object
 */
std::string dag::WindowLostFocusEvent::getName() const
{
    return "WindowLostFocusEvent";
}

/**
 * @brief To string implementation
 */
std::string dag::WindowLostFocusEvent::toString()
{
    return getName() + ": lost focus on window";
}