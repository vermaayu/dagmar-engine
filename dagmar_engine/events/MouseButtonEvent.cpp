#include "dagmar/MouseButtonEvent.h"

/**
 * @brief Mouse button event constructor
 */
dag::MouseButtonEvent::MouseButtonEvent(int const& button, int const& mods) :
    button(button), mods(mods)
{
}

/**
 * @brief Gets the mouse button
 */
int dag::MouseButtonEvent::getButton() const
{
    return button;
}

int dag::MouseButtonEvent::getMods() const
{
    return mods;
}

/**
 * @brief Gets the category bit flags
 */
int dag::MouseButtonEvent::getCategoryBitFlags() const
{
    return CategoryBitFlags::Mouse | CategoryBitFlags::Input | CategoryBitFlags::MouseButton;
}