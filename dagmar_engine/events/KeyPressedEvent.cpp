#include "dagmar/KeyPressedEvent.h"

/**
 * @brief Key pressed event constructor
 */
dag::KeyPressedEvent::KeyPressedEvent(int const& keycode, int const& scanCode, int const& action, int const& mods,
	int const& repeatCount) : KeyEvent(keycode, scanCode, action, mods), repeatCount(repeatCount)
{
}

/**
 * @brief Gets the repeat count from a key pressed event
 *
 * The OS usually sends repeated keys while pressed
 */
int dag::KeyPressedEvent::getRepeatCount() const
{
    return repeatCount;
}

/**
 * @brief Gets the static type of the event
 */
dag::Event::Type dag::KeyPressedEvent::getStaticType()
{
    return Type::KeyPressed;
}

/**
 * @brief Gets the type of the event
 */
dag::Event::Type dag::KeyPressedEvent::getType() const
{
    return getStaticType();
}

/**
 * @brief Gets the name of the event
 */
std::string dag::KeyPressedEvent::getName() const
{
    return "KeyPressedEvent";
}

/**
 * @brief To string implementation
 */
std::string dag::KeyPressedEvent::toString()
{
    return getName() + ":" + std::to_string(keyCode) + "(" + std::to_string(repeatCount) + "repeats)";
}