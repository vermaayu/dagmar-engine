#include "dagmar/Allocator.h"

/**
 * @brief Constructor that allocates a predefined sized block
 * The size gets aligned to word size and the header size is
 * taken into acount for the final allocated size.
 * The user has access to padded(initialSize) memory.
 */
dag::Allocator::Allocator(word_t const& initialSize)
{
    word_t totalSize = align(initialSize) + getHeaderSize();

    memory = operator new(totalSize);
    word_t address = reinterpret_cast<word_t>(memory);

    start = static_cast<Block*>(memory);

    memory = reinterpret_cast<void*>(address + getHeaderSize());

    start->data = memory;
    start->header = (totalSize - getHeaderSize());
    start->next = nullptr;
    start->prev = nullptr;
    setUsed(start, false);
}

/**
 * @brief Destructor - frees allocated memory
 */
dag::Allocator::~Allocator()
{
    free(start);
}

/**
 * @brief Allocates a block from the already malloc'ed memory.
 * The function find the first block that would fit the required 
 * memory. If none is found, returns nullptr.
 */
void *dag::Allocator::allocate(word_t const& size)
{
    word_t alignedSize = align(size);

    Block* temp = start;

    while (temp != nullptr)
    {
        if (!isUsed(temp) && getSize(temp) >= alignedSize)
        {
            return breakBlock(temp, alignedSize)->data;
        }
        temp = temp->next;
    }

    return nullptr;
}

/**
 * @brief Frees a block. The block is set as free and if its neighbours 
 * are free as well, they get combined in a single, bigger block.
 */
void dag::Allocator::deallocate(void* block)
{
    if (block == nullptr)
    {
        return;
    }
    Block* freeBlock = reinterpret_cast<Block*>((reinterpret_cast<word_t>(block) - getHeaderSize()));

    setUsed(freeBlock, false);
    coalesce(freeBlock);
}

/**
 * @brief Combine a given block with previous and/or next neighbours
 * if they are free.
 */
void dag::Allocator::coalesce(Block* block)
{
    if (block->next && !isUsed(block->next))
    {
        block->header += getHeaderSize() + getSize(block->next);
        block->next = block->next->next;
    }
    if (block->prev && !isUsed(block->prev))
    {
        block->prev->next = block->next;
        block->prev->header += getHeaderSize() + getSize(block);
        block = block->prev;
    }
}

/**
 * @brief Computes the size in bytes of the header for a block.
 */
word_t dag::Allocator::getHeaderSize()
{
    return align(sizeof(Block::header) + sizeof(Block::next) + sizeof(Block::prev));
}

/**
 * @brief Breaks a given block into two smaller ones, if required.
 * If the remaining size is less than a header in size, the full
 * block is returned, no breaking happens.
 * Otherwise, the block gets broken into two blocks and its fields
 * are computed.
 */
dag::Allocator::Block* dag::Allocator::breakBlock(Block* block, word_t const& size)
{
    if (getSize(block) - size < getHeaderSize())
    {
        setUsed(block, true);
        return block;
    }

    word_t start = reinterpret_cast<word_t>(block->data);
    Block* newBlock = reinterpret_cast<Block*>(start + size);

    newBlock->next = block->next;
    block->next = newBlock;

    newBlock->prev = block;

    newBlock->header = getSize(block) - getHeaderSize() - size;
    block->header = size;

    word_t address = reinterpret_cast<word_t>(block->data);

    newBlock->data = reinterpret_cast<void*>(address + size + getHeaderSize());

    setUsed(block, true);
    setUsed(newBlock, false);

    return block;
}

/**
 * @brief Debug printing - outputs the list of blocks in the allocator.
 */
void dag::Allocator::debugPrint()
{
    Block* node = start;

    do
    {
        // TODO: add log once it can print multiple objects
        std::cout << "Node: " << node << "\nsize - " << getSize(node) << "\nused - " << isUsed(node) << "\ndata - " << node->data << "\nnext - " << node->next << "\nprev - " << node->prev << std::endl << std::endl;

        node = node->next;

    } while (node != nullptr);
}