#pragma once
#include <imgui.h>
#include <imgui_internal.h>

#include <array>
#include <ctime>
#include <string>
#include <vector>

enum class LogType
{
    Debug,
    Info,
    Warning,
    Error,
    Success
};

namespace dag
{
    class Console
    {
      public:
        static Console& get()
        {
            static Console console;
            return console;
        }

        struct LogEntry
        {
            std::string data;
            ImVec4 color;
            LogType type;
            std::time_t time;
            std::string timeString;
        };

        inline static std::vector<LogEntry> items;
        inline static bool autoScroll;
        inline static bool scrollToBottom;

        Console()
        {
            ClearLog();
            autoScroll = true;
            scrollToBottom = false;
        }
        ~Console()
        {
            ClearLog();
        }

        void ClearLog()
        {
            items.clear();
        }

        void AddLog(std::string const& log, LogType type)
        {
            auto currTime = std::time(nullptr);
            auto currLocalTime = std::localtime(&currTime);
            std::string timeString = "[" + std::to_string(currLocalTime->tm_hour) + ":" +
                                      std::to_string(currLocalTime->tm_min) + ":" +
                                      std::to_string(currLocalTime->tm_sec) + "]";

            if (items.size() < 300)
            {
                items.emplace_back();
            }
            else
            {
                std::rotate(items.begin(), items.begin() + 1, items.end());
            }
            items.back().data = log;
            items.back().type = type;
            items.back().time = currTime;
            items.back().timeString = timeString;

            switch (type)
            {
                case LogType::Debug:
                    items.back().color = ImVec4(1.0f, 0.0f, 1.0f, 1.0f);
                    break;
                case LogType::Error:
                    items.back().color = ImVec4(1.0f, 0.0f, 0.0f, 1.0f);
                    break;
                case LogType::Info:
                    items.back().color = ImVec4(1.0f, 1.0f, 0.0f, 1.0f);
                    break;
                case LogType::Success:
                    items.back().color = ImVec4(0.0f, 1.0f, 0.0f, 1.0f);
                    break;
                case LogType::Warning:
                    items.back().color = ImVec4(0.0f, 1.0f, 1.0f, 1.0f);
                    break;
            }
        }

        void draw(char const * title, bool* p_open)
        {
            ImGui::SetNextWindowSize(ImVec2(520, 600), ImGuiCond_FirstUseEver);
            if (!ImGui::Begin(title, p_open))
            {
                ImGui::End();
                return;
            }

            if (ImGui::BeginPopupContextItem())
            {
                if (ImGui::MenuItem("Close Console"))
                    *p_open = false;
                ImGui::EndPopup();
            }

            ImGui::BeginChild("ScrollingRegion", ImVec2(0, 0), false, ImGuiWindowFlags_HorizontalScrollbar);
            if (ImGui::BeginPopupContextWindow())
            {
                if (ImGui::Selectable("Clear"))
                    ClearLog();
                ImGui::EndPopup();
            }

            ImGuiListClipper clipper;
            clipper.Begin(items.size());
            while (clipper.Step())
            {
                for (int i = clipper.DisplayStart; i < clipper.DisplayEnd; i++)
                {
                    LogEntry const& curr_Entry = items[i];
                    ImGui::PushStyleColor(ImGuiCol_Text, curr_Entry.color);
                    ImGui::TextUnformatted((curr_Entry.timeString + curr_Entry.data).data());
                    ImGui::PopStyleColor();
                }
            }

            if (scrollToBottom || (autoScroll && ImGui::GetScrollY() >= ImGui::GetScrollMaxY()))
            {
                ImGui::SetScrollHereY(1.0f);
            }
            scrollToBottom = false;

            ImGui::EndChild();

            ImGui::End();
        }
    };
} // namespace dag