#pragma once
#include <string>
#include <iostream>
#include <sstream>
#include "dagmar/UI/Console.h"

#ifdef _WIN32
#include <windows.h>
#endif

#ifndef NDEBUG

/**
 * @brief Log class
 *
 * Collection of static functions to aid in printing text in the console.
 */
class Log
{
  public:
    inline static std::ostringstream oss;

  public:

    #ifdef _WIN32
        #define RED "\033[31m"
        #define GREEN "\033[32m"
        #define YELLOW "\033[33m"
        #define MAGENTA "\033[35m"
        #define CYAN "\033[36m"
        #define RESET "\033[0m"
    #else
        #define RED "\e[0;31m"
        #define GREEN "\e[0;32m"
        #define YELLOW "\e[0;33m"
        #define MAGENTA "\e[0;35m"
        #define CYAN "\e[0;36m"
        #define RESET "\033[0m"
    #endif

    /**
         * @brief Print log
         * @param text
         */
    template <typename... Targs>
    static void print(Targs const&... input, LogType const& logType)
    {
        switch (logType)
        {
            case LogType::Debug:
            {
                debug(input...);
                break;
            }
            case LogType::Info:
            {
                info(input...);
                break;
            }
            case LogType::Warning:
            {
                warning(input...);
                break;
            }
            case LogType::Error:
            {
                error(input...);
                break;
            }
            case LogType::Success:
            {
                success(input...);
                break;
            }
            default:
            {
                break;
            }
        };
    }

    static void output()
    {
        oss << "";
    }

    template <typename T, typename... Targs>
    static void output(T head, Targs... tail)
    {
        oss << head; 
        output(tail...);
    }

    /**
         * @brief Debug log
         * @param text
         */
    template <typename... Targs>
    static void debug(Targs const&... input)
    {
        oss.str(std::string());
        oss << "[DEBUG]: ";
        output(input...);
        std::cout << MAGENTA << oss.str() << RESET << std::endl;
        dag::Console::get().AddLog(oss.str(), LogType::Debug);
    }
    /**
         * @brief Info log
         * @param text
         */
    template <typename... Targs>
    static void info(Targs const&... input)
    {
        oss.str(std::string());
        oss << "[INFO]: ";
        output(input...);
        std::cout << YELLOW << oss.str() << RESET << std::endl;
        dag::Console::get().AddLog(oss.str(), LogType::Info);
    }

    /**
         * @brief Warning log
         * @param text
         */
    template <typename... Targs>
    static void warning(Targs const&... input)
    {
        oss.str(std::string());
        oss << "[WARNING]: ";
        output(input...);
        std::cout << CYAN << oss.str() << RESET << std::endl;
        dag::Console::get().AddLog(oss.str(), LogType::Warning);
    }

    /**
         * @brief Error log
         * @param text
         */
    template <typename... Targs>
    static void error(Targs const&... input)
    {
        oss.str(std::string());
        oss << "[ERROR]: ";
        output(input...);
        std::cout << RED << oss.str() << RESET << std::endl;
        dag::Console::get().AddLog(oss.str(), LogType::Error);
    }

    /**
         * @brief Success log
         * @param text
         */
    template <typename... Targs>
    static void success(Targs const&... input)
    {
        oss.str(std::string());
        oss << "[SUCCESS]: ";
        output(input...);
        std::cout << GREEN << oss.str() << RESET << std::endl;
        dag::Console::get().AddLog(oss.str(), LogType::Success);
    }
};

#else

class Log
{
public:
    inline static std::ostringstream oss;

public:

#ifdef _WIN32
#define RED "\033[31m"
#define GREEN "\033[32m"
#define YELLOW "\033[33m"
#define MAGENTA "\033[35m"
#define CYAN "\033[36m"
#define RESET "\033[0m"
#else
#define RED "\e[0;31m"
#define GREEN "\e[0;32m"
#define YELLOW "\e[0;33m"
#define MAGENTA "\e[0;35m"
#define CYAN "\e[0;36m"
#define RESET "\033[0m"
#endif

      /**
           * @brief Print log
           * @param text
           */
      template <typename... Targs>
      static void print(Targs const&... input, LogType const& logType)
      {
          switch (logType)
          {
          case LogType::Debug:
          {
              debug(input...);
              break;
          }
          case LogType::Info:
          {
              info(input...);
              break;
          }
          case LogType::Warning:
          {
              warning(input...);
              break;
          }
          case LogType::Error:
          {
              error(input...);
              break;
          }
          case LogType::Success:
          {
              success(input...);
              break;
            }
          default:
          {
              break;
          }
        };
    }

    static void output()
    {
        oss << "";
    }

    template <typename T, typename... Targs>
    static void output(T head, Targs... tail)
    {
        oss << head;
        output(tail...);
    }
    template <typename... Targs>
    static void debug(Targs const&... input)
    {
    }
    /**
         * @brief Info log
         * @param text
         */
    template <typename... Targs>
    static void info(Targs const&... input)
    {
        oss.str(std::string());
        oss << "[INFO]: ";
        output(input...);
        std::cout << YELLOW << oss.str() << RESET << std::endl;
        dag::Console::get().AddLog(oss.str(), LogType::Info);
    }
    template <typename... Targs>
    static void warning(Targs const&... input)
    {
    }
    /**
         * @brief Error log
         * @param text
         */
    template <typename... Targs>
    static void error(Targs const&... input)
    {
        oss.str(std::string());
        oss << "[ERROR]: ";
        output(input...);
        std::cout << RED << oss.str() << RESET << std::endl;
        dag::Console::get().AddLog(oss.str(), LogType::Error);
    }

    /**
         * @brief Success log
         * @param text
         */
    template <typename... Targs>
    static void success(Targs const&... input)
    {
        oss.str(std::string());
        oss << "[SUCCESS]: ";
        output(input...);
        std::cout << GREEN << oss.str() << RESET << std::endl;
        dag::Console::get().AddLog(oss.str(), LogType::Success);
    }
};

#endif
