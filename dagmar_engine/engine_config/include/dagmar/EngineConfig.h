#pragma once
#include "glm/glm.hpp"
#include "glm/vec2.hpp"
#include "glm/vec3.hpp"
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <stdlib.h>
#include <vector>

#include "dagmar/Log.h"
#include "dagmar/Module.h"

namespace dag
{
    /**
     * @brief Sections class has section name and it's properties as key-value pair (map).
     */
    class Sections
    {
      public:
        std::string sectionName;
        std::map<std::string, std::string> properties;
    };

    /**
     * @brief Engine Configuration
     */
    class EngineConfig : public dag::Module
    {
      public:
        std::vector<Sections> arrSections;

    public:
        EngineConfig() = default;

        // Override startup for the module
        void startup() override;

        // Override shutdown for the module
        void shutdown() override;

        // Override name for the module
        std::string getName() const override;

        // Parse the EngineConfig.ini file
        void parse(std::filesystem::path const& path);

        // Access a value according to the key and the scetion it belongs to
        std::string getValue(const std::string key, const std::string sectionName);
    };
    inline std::shared_ptr<EngineConfig> engineConfig(new EngineConfig());
} //namespace dag