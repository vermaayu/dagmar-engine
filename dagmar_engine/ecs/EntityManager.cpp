#include "dagmar/EntityManager.h"
#include "dagmar/Exceptions.h"
#include <cassert>

/**
 * @brief Default constructor
 */
dag::EntityManager::EntityManager()
{
    availableEntities = {};
    signatures = {};
    aliveEntities = 0;

    // Initialize the queue
    for (Entity entity = 0; entity < MAX_ENTITIES; entity++)
    {
        availableEntities.push(entity);
    }
}

/**
 * @brief Creates an entity if possible
 * @return The id of the created entity
 */
dag::Entity dag::EntityManager::createEntity()
{
    if (aliveEntities >= MAX_ENTITIES)
    {
        throw dag::ECSException(dag::ECSException::ErrorType::CreateEntityTooManyEntities);
    }

    assert(aliveEntities < MAX_ENTITIES && "Too many entities alive.");

    dag::Entity id = availableEntities.front();
    availableEntities.pop();
    aliveEntities++;

    return id;
}

/**
 * @brief Tries to destroy an entity
 */
void dag::EntityManager::destroyEntity(dag::Entity const& entity)
{
    if (entity >= MAX_ENTITIES)
    {
        throw dag::ECSException(dag::ECSException::ErrorType::DestroyEntityOutOfRange);
    }

    // Invalidate the entity's signature (components)
    signatures[entity].reset();

    // Making the ID available again
    availableEntities.push(entity);
    aliveEntities--;
}

/**
 * @brief Sets the signature of an entity
 */
void dag::EntityManager::setSignature(dag::Entity const& entity, dag::Signature const& signature)
{
    if (entity >= MAX_ENTITIES)
    {
        throw dag::ECSException(dag::ECSException::ErrorType::SetEntitySignatureOutOfRange);
    }

    // Sets the signature of an entity
    signatures[entity] = signature;
}

/**
 * @brief Gets the signature of an entity
 */
dag::Signature const dag::EntityManager::getSignature(dag::Entity const& entity) const
{
    if (entity >= MAX_ENTITIES)
    {
        throw dag::ECSException(dag::ECSException::ErrorType::GetEntitySignatureOutOfRange);
    }

    return signatures[entity];
}
