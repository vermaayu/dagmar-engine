#pragma once

#include "Entity.h"
#include <set>

namespace dag
{
    /**
     * @brief Simple system class
     */
    class System
    {
      public:
        virtual ~System() = default;

        virtual void startup(){};
        virtual void shutdown(){};

        std::set<Entity> entities;
    };
} // namespace dag
