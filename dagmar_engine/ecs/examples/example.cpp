#include "include/dagmar/Coordinator.h"
#include "include/dagmar/Entity.h"
#include <chrono>
#include <iostream>
#include <random>
#include <vector>

dag::Coordinator coord;

struct Transform
{
    float x = 0,y = 0,z = 0;
};

struct Gravity
{
    float dx = 0, dy = 9.8, dz = 0;
};

class PhysicsSystem : public dag::System
{
    public:
        void update(float const& dt)
        {
            int count = 0;
            for (auto const& entity : entities)
            {
                auto& transform = coord.getComponent<Transform>(entity);
                auto& gravity = coord.getComponent<Gravity>(entity);

                transform.y -= dt * gravity.dy;
                count++;
            }
        }
};

int main()
{
    coord.startup();

    coord.registerComponent<Transform>();
    coord.registerComponent<Gravity>();
    
    auto ps = coord.registerSystem<PhysicsSystem>();

    dag::Signature psSig;
    psSig.set(coord.getComponentType<Transform>());
    psSig.set(coord.getComponentType<Gravity>());

    coord.setSystemSignature<PhysicsSystem>(psSig);

    std::vector<dag::Entity> entities(1);

    std::default_random_engine generator;
    std::uniform_real_distribution<float> randPosition(0, 100.f);

    for(auto& entity : entities)
    {
        entity = coord.createEntity();

        coord.addComponent(entity, Transform{0.0f, randPosition(generator), 0.0f});
        coord.addComponent(entity, Gravity{});
    }

    float dt = 0.0f;

    while(true)
    {
        auto startTime = std::chrono::high_resolution_clock::now();

        ps->update(dt);

        auto stopTime = std::chrono::high_resolution_clock::now();

        dt = std::chrono::duration<float, std::chrono::seconds::period>(stopTime - startTime).count();
    }

    coord.shutdown();
    return 0;
}

