#include "dagmar/ResourceManager.h"
#include "dagmar/FileSystem.h"
#include "dagmar/Module.h"
#include "dagmar/Texture2D.h"
#include <cstring>
#include <fstream>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <nlohmann/json.hpp>
#include <tiny_obj_loader.h>

using json = nlohmann::json;

/**
 * @brief Startup function for the resource manager
 */
void dag::ResourceManager::startup()
{
    atts = {
        dag::VertexAttribute(0, 3, GL_FLOAT, GL_FALSE, 12 * sizeof(float), (void*)0),
        dag::VertexAttribute(1, 3, GL_FLOAT, GL_FALSE, 12 * sizeof(float), (void*)(3 * sizeof(float))),
        dag::VertexAttribute(2, 2, GL_FLOAT, GL_FALSE, 12 * sizeof(float), (void*)(6 * sizeof(float))),
        dag::VertexAttribute(3, 4, GL_FLOAT, GL_FALSE, 12 * sizeof(float), (void*)(8 * sizeof(float)))};
}

/*
 * @brief Shutdown function for the resource manager
 */
void dag::ResourceManager::shutdown()
{
}

size_t dag::ResourceManager::getMeshRenderer(std::string name)
{
    for (auto it = meshRenderers.begin(); it != meshRenderers.end(); ++it)
    {
        if (it->second.name == name)
        {
            return it->first;
        }
    }
}

/*
 * @brief Get name of the module
 */
std::string dag::ResourceManager::getName() const
{
    return "ResourceManager";
}

/**
 * @brief Parse object file to main memory
 */
size_t dag::ResourceManager::parseOBJ(std::filesystem::path const& inputfile)
{
    Log::warning("parsing: ", inputfile);

    size_t inputHash = std::hash<std::string>{}(std::filesystem::relative(inputfile, dag::filesystem::getAssetsPath()).string());

    if (models.contains(inputHash))
    {
        Log::debug("Data already loaded.");
        Log::debug("|_ ", models[inputHash].numVertices, " vertices");
        return inputHash;
    }

    Model tempModel{};

    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string warn, err;

    if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, inputfile.generic_string().c_str(), inputfile.parent_path().generic_string().c_str()))
    {
        throw std::runtime_error(warn + err);
    }

    if (!warn.empty())
    {
        Log::debug("TinyObj Warning: ", warn);
    }
    if (!err.empty())
    {
        Log::debug("TinyObj Warning: ", err);
    }

    // loop through shapes (i.e. face)
    for (const auto& shape : shapes)
    {
        // create a map for the vertices to keep them uniquely stored
        std::unordered_map<int, std::unordered_map<Vertex, uint32_t>> uniqueVertices;
        // midpoint to centre the object

        std::unordered_map<int, Shape> tempShapes;

        for (int matIndex = 0; matIndex < shape.mesh.material_ids.size(); ++matIndex)
        {
            if (!tempShapes.contains(shape.mesh.material_ids[matIndex]))
            {
                tempShapes.insert({shape.mesh.material_ids[matIndex], Shape{}});
                uniqueVertices.insert({shape.mesh.material_ids[matIndex], std::unordered_map<Vertex, uint32_t>{}});

                if (shape.mesh.material_ids[matIndex] != -1)
                {
                    MaterialDataInfo tempMaterial;

                    auto& currMaterial = materials[shape.mesh.material_ids[matIndex]];
                    tempMaterial.name = currMaterial.name;

                    tempMaterial.ambience.x = currMaterial.ambient[0];
                    tempMaterial.ambience.y = currMaterial.ambient[1];
                    tempMaterial.ambience.z = currMaterial.ambient[2];

                    tempMaterial.diffuse.x = currMaterial.diffuse[0];
                    tempMaterial.diffuse.y = currMaterial.diffuse[1];
                    tempMaterial.diffuse.z = currMaterial.diffuse[2];

                    tempMaterial.specular.x = currMaterial.specular[0];
                    tempMaterial.specular.y = currMaterial.specular[1];
                    tempMaterial.specular.z = currMaterial.specular[2];

                    tempMaterial.emission.x = currMaterial.emission[0];
                    tempMaterial.emission.y = currMaterial.emission[1];
                    tempMaterial.emission.z = currMaterial.emission[2];

                    tempMaterial.dissolve = currMaterial.dissolve;

                    //Respective textures
                    if (!currMaterial.diffuse_texname.empty())
                    {
#ifndef _WIN32
                        std::replace(currMaterial.diffuse_texname.begin(), currMaterial.diffuse_texname.end(), '\\', '/');
#endif

                        tempMaterial.textureDiffuseHash = getFileHash((inputfile.parent_path() / currMaterial.diffuse_texname).generic_string());
                        if (!textures.contains(tempMaterial.textureDiffuseHash))
                        {
                            textures.insert({tempMaterial.textureDiffuseHash, dag::Texture2D{}});
                            texture_names.insert({tempMaterial.textureDiffuseHash, (inputfile.parent_path() / currMaterial.diffuse_texname).generic_string()});
                            textures[tempMaterial.textureDiffuseHash].create((inputfile.parent_path() / currMaterial.diffuse_texname).generic_string());
                        }
                    }
                    else
                    {
                        tempMaterial.textureDiffuseHash = 0;
                    }

                    if (!currMaterial.ambient_texname.empty())
                    {
#ifndef _WIN32
                        std::replace(currMaterial.ambient_texname.begin(), currMaterial.ambient_texname.end(), '\\', '/');
#endif

                        tempMaterial.textureAmbientHash = getFileHash((inputfile.parent_path() / currMaterial.ambient_texname).generic_string());
                        if (!textures.contains(tempMaterial.textureDiffuseHash))
                        {
                            textures.insert({tempMaterial.textureAmbientHash, dag::Texture2D{}});
                            texture_names.insert({tempMaterial.textureAmbientHash, (inputfile.parent_path() / currMaterial.ambient_texname).generic_string()});
                            textures[tempMaterial.textureAmbientHash].create((inputfile.parent_path() / currMaterial.ambient_texname).generic_string());
                        }
                    }
                    else
                    {
                        tempMaterial.textureAmbientHash = 0;
                    }

                    if (!currMaterial.specular_texname.empty())
                    {
#ifndef _WIN32
                        std::replace(currMaterial.specular_texname.begin(), currMaterial.specular_texname.end(), '\\', '/');
#endif

                        tempMaterial.textureSpecularHash = getFileHash((inputfile.parent_path() / currMaterial.specular_texname).generic_string());
                        if (!textures.contains(tempMaterial.textureSpecularHash))
                        {
                            textures.insert({tempMaterial.textureSpecularHash, dag::Texture2D{}});
                            texture_names.insert({tempMaterial.textureSpecularHash, (inputfile.parent_path() / currMaterial.specular_texname).generic_string()});
                            textures[tempMaterial.textureSpecularHash].create((inputfile.parent_path() / currMaterial.specular_texname).generic_string());
                        }
                    }
                    else
                    {
                        tempMaterial.textureSpecularHash = 0;
                    }

                    if (!currMaterial.bump_texname.empty())
                    {
#ifndef _WIN32
                        std::replace(currMaterial.bump_texname.begin(), currMaterial.bump_texname.end(), '\\', '/');
#endif

                        tempMaterial.textureBumpHash = getFileHash((inputfile.parent_path() / currMaterial.bump_texname).generic_string());
                        if (!textures.contains(tempMaterial.textureBumpHash))
                        {
                            textures.insert({tempMaterial.textureBumpHash, dag::Texture2D{}});
                            texture_names.insert({tempMaterial.textureBumpHash, (inputfile.parent_path() / currMaterial.bump_texname).generic_string()});
                            textures[tempMaterial.textureBumpHash].create((inputfile.parent_path() / currMaterial.bump_texname).generic_string());
                        }
                    }
                    else
                    {
                        tempMaterial.textureBumpHash = 0;
                    }

                    if (!currMaterial.alpha_texname.empty())
                    {
#ifndef _WIN32
                        std::replace(currMaterial.alpha_texname.begin(), currMaterial.alpha_texname.end(), '\\', '/');
#endif

                        tempMaterial.textureAlphaHash = getFileHash((inputfile.parent_path() / currMaterial.alpha_texname).generic_string());
                        if (!textures.contains(tempMaterial.textureAlphaHash))
                        {
                            textures.insert({tempMaterial.textureAlphaHash, dag::Texture2D{}});
                            texture_names.insert({tempMaterial.textureAlphaHash, (inputfile.parent_path() / currMaterial.alpha_texname).generic_string()});
                            textures[tempMaterial.textureAlphaHash].create((inputfile.parent_path() / currMaterial.alpha_texname).generic_string());
                        }
                    }
                    else
                    {
                        tempMaterial.textureAlphaHash = 0;
                    }

                    tempShapes[shape.mesh.material_ids[matIndex]].material = tempMaterial;
                }
            }
        }

        size_t counter = 0;
        for (const auto& index : shape.mesh.indices)
        {
            Vertex vertex{};
            // get the vertex position
            vertex.pos = {
                attrib.vertices[3 * index.vertex_index + 0],
                attrib.vertices[3 * index.vertex_index + 1],
                attrib.vertices[3 * index.vertex_index + 2]};

            if (attrib.texcoords.size() > 2 * index.texcoord_index)
            {
                // get the vertex texture coordinate
                vertex.texCoord = {
                    attrib.texcoords[2 * index.texcoord_index + 0],
                    1.0 - attrib.texcoords[2 * index.texcoord_index + 1]};
            }
            else
            {
                vertex.texCoord = {0.0, 0.0};
            }

            // set the default color
            vertex.color = {1.0f, 1.0f, 1.0f, 1.0f};

            if (attrib.normals.size() > 3 * index.normal_index)
            {
                // get the vertex normal
                vertex.normal = {
                    attrib.normals[3 * index.normal_index + 0],
                    attrib.normals[3 * index.normal_index + 1],
                    attrib.normals[3 * index.normal_index + 2]};
            }
            else
            {
                vertex.normal = {0.0, 1.0, 0.0};
            }

            // if the vertex has not been encountered before
            if (uniqueVertices[shape.mesh.material_ids[counter / 3]].count(vertex) == 0)
            {
                // add it to the map
                uniqueVertices[shape.mesh.material_ids[counter / 3]][vertex] = static_cast<uint32_t>(tempShapes[shape.mesh.material_ids[counter / 3]].vertices.size());
                // add it to the vertices
                tempShapes[shape.mesh.material_ids[counter / 3]].vertices.push_back(vertex);
            }
            // add the indices
            tempShapes[shape.mesh.material_ids[counter / 3]].indices.push_back(uniqueVertices[shape.mesh.material_ids[counter / 3]][vertex]);
            counter++;
        }

        for (auto& subShape : tempShapes)
        {
            tempModel.shapes.push_back(subShape.second);
            tempModel.numVertices += tempModel.shapes.back().vertices.size();
        }
    }

    glm::vec3 center = glm::vec3(0.0f);

    for (int s = 0; s < tempModel.shapes.size(); ++s)
    {
        glm::vec3 localCenter = glm::vec3(0.0f);
        for (int v = 0; v < tempModel.shapes[s].vertices.size(); ++v)
        {
            localCenter += tempModel.shapes[s].vertices[v].pos;
            center += tempModel.shapes[s].vertices[v].pos;
        }
        tempModel.shapes[s].center = localCenter;
    }

    tempModel.center = center / static_cast<float>(tempModel.numVertices);

    float maxRadius = 0.0f;
    for (int s = 0; s < tempModel.shapes.size(); ++s)
    {
        for (int v = 0; v < tempModel.shapes[s].vertices.size(); ++v)
        {
            float length = glm::length(tempModel.shapes[s].vertices[v].pos - tempModel.center);
            if (length > maxRadius)
            {
                maxRadius = length;
            }
        }
    }

    tempModel.radius = maxRadius;

    // for (auto& shape : models[inputHash].shapes)
    // {
    //     std::cout << "shape: " << std::endl;
    //     std::cout << shape.indices.size() << std::endl;
    //     std::cout << shape.vertices.size() << std::endl;
    //     std::cout << shape.material.name << std::endl;
    //     ->vaos.emplace_back(dag::resourceManager->generateVAO(shape, resourceManager->atts));
    //     shape.VAOIndex = rs->vaos.size() - 1;
    // }

    Log::debug("Finished parsing. inputHash: ", inputHash, " tempModel: ", tempModel.numVertices);

    if (inputfile.string().find("flower") != std::string::npos)
    {
        std::cout << "Shape no: " << tempModel.shapes.size() << std::endl;
        std::cout << "Vertices: " << tempModel.shapes[0].vertices.size() << std::endl;
        std::cout << "Indices: " << tempModel.shapes[0].indices.size() << std::endl;
        for (auto vert : tempModel.shapes[0].vertices)
        {
            std::cout << "{.pos = {" << vert.pos.x * 5.0f << ',' << vert.pos.y * 5.0f << ',' << vert.pos.z * 5.0f << "},";
            std::cout << ".normal = {" << vert.normal.x << ',' << vert.normal.y << ',' << vert.normal.z << "}},";
        }
        std::cout << std::endl;
        for (auto index : tempModel.shapes[0].indices)
        {
            std::cout << index << ", ";
        }
        std::cout << std::endl;
    }

    models.insert({inputHash, tempModel});
    return inputHash;
}

/*size_t dag::ResourceManager::createModel(std::vector<Vertex> const& vertices, std::vector<uint32_t> const& indices, std::string const& name)
{
    size_t inputHash = std::hash<std::string>{}(name);

    if (models.contains(inputHash))
    {
        Log::debug("Data already loaded. Mesh has:");
        Log::debug("|_ ", models[inputHash].numVertices, " vertices");
        return inputHash;
    }

    Model tempModel;

    tinyobj::attrib_t attrib;

    tempModel.vertices = vertices;
    tempModel.indices = indices;

    glm::vec3 center = glm::vec3(0);

    for (int v = 0; v < tempModel.vertices.size(); ++v)
    {
        center += tempModel.vertices[v].pos;
    }

    tempModel.center = center / static_cast<float>(tempModel.vertices.size());

    float maxRadius = 0.0f;
    for (int v = 0; v < tempModel.vertices.size(); ++v)
    {
        float length = glm::length(tempModel.vertices[v].pos - tempModel.center);
        if (length > maxRadius)
        {
            maxRadius = length;
        }
    }

    tempModel.radius = maxRadius;

    Log::debug("Finished parsing. inputHash: ", inputHash, " tempModel: ", tempModel.vertices.size(), " ", tempModel.indices.size());

    models.insert({inputHash, tempModel});
    return inputHash;
}*/

/**
 * @brief Formats the file for obj naming
 */
std::string dag::ResourceManager::getObjName(std::string const& filename)
{
    return filename.substr(0, filename.find(".obj"));
}

void dag::ResourceManager::insertTexture(size_t inputHash, std::filesystem::path texture)
{
    if (!textures.contains(inputHash))
    {
        textures.insert({inputHash, dag::Texture2D()});
        textures[inputHash].create(texture.generic_string());
        Log::debug("Texture: ", texture.generic_string(), " created");
        Log::debug("Texture: ", inputHash, " created");
    }
    else
    {
        Log::debug("Texture: ", texture.generic_string(), " already created");
    }
}

/**
 * @brief Writes the resource to binary (.dat)
 */
void dag::ResourceManager::writeBin(size_t inputHash, std::filesystem::path outPath)
{

    if (outPath.filename().empty())
    {
        //Call getFilename / setFilename first.
        Log::error("Error: filename not set.");
        exit(EXIT_FAILURE);
    }

    std::cout << "Opening binary file: " << outPath << std::endl;

    std::ofstream outBinFile;
    outBinFile.open(outPath.replace_extension(".dat"), std::ios::out | std::ios::binary);

    std::cout << "Opened binary file: " << outPath << std::endl;

    //For keeping track of the number of entities while reading
    size_t tempSize = 0;

    //Number of Models
    tempSize = models.size();
    outBinFile.write((char*)&tempSize, sizeof(size_t));

    auto& currModel = models[inputHash];

    size_t numShapes = currModel.shapes.size();

    outBinFile.write((char*)&numShapes, sizeof(size_t));

    std::cout << "Num Shapes: " << tempSize << std::endl;

    if (tempSize != 0)
    {
        for (int s = 0; s < numShapes; ++s)
        {
            //Vertices
            //Position
            tempSize = currModel.shapes[s].vertices.size();
            outBinFile.write((char*)&tempSize, sizeof(size_t));

            std::cout << "Num vertices: " << tempSize << std::endl;

            if (tempSize != 0)
            {
                for (int i = 0; i < tempSize; ++i)
                {
                    outBinFile.write((char*)&currModel.shapes[s].vertices[i].pos, sizeof(currModel.shapes[s].vertices[i].pos));

                    outBinFile.write((char*)&currModel.shapes[s].vertices[i].normal, sizeof(currModel.shapes[s].vertices[i].normal));

                    outBinFile.write((char*)&currModel.shapes[s].vertices[i].texCoord, sizeof(currModel.shapes[s].vertices[i].texCoord));

                    outBinFile.write((char*)&currModel.shapes[s].vertices[i].color, sizeof(currModel.shapes[s].vertices[i].color));
                }
            }

            std::cout << "Verticies written" << std::endl;

            //faces
            tempSize = currModel.shapes[s].indices.size();
            outBinFile.write((char*)&tempSize, sizeof(size_t));

            std::cout << "Num indices: " << tempSize << std::endl;

            for (size_t i = 0; i < currModel.shapes[s].indices.size(); i++)
            {
                outBinFile.write((char*)&currModel.shapes[s].indices[i], sizeof(uint32_t));
            }

            std::cout << "Indices written" << std::endl;

            //Material
            tempSize = strlen(currModel.shapes[s].material.name.c_str());
            outBinFile.write((char*)&tempSize, sizeof(size_t));
            outBinFile.write(currModel.shapes[s].material.name.c_str(), tempSize);
            outBinFile.write((char*)&currModel.shapes[s].material.ambience, sizeof(currModel.shapes[s].material.ambience));
            outBinFile.write((char*)&currModel.shapes[s].material.diffuse, sizeof(currModel.shapes[s].material.diffuse));
            outBinFile.write((char*)&currModel.shapes[s].material.specular, sizeof(currModel.shapes[s].material.specular));
            outBinFile.write((char*)&currModel.shapes[s].material.emission, sizeof(currModel.shapes[s].material.emission));
            outBinFile.write((char*)&currModel.shapes[s].material.dissolve, sizeof(currModel.shapes[s].material.dissolve));

            outBinFile.write((char*)&currModel.shapes[s].material.textureAmbientHash, sizeof(currModel.shapes[s].material.textureAmbientHash));

            outBinFile.write((char*)&currModel.shapes[s].material.textureDiffuseHash, sizeof(currModel.shapes[s].material.textureDiffuseHash));

            outBinFile.write((char*)&currModel.shapes[s].material.textureSpecularHash, sizeof(currModel.shapes[s].material.textureSpecularHash));
        	
            outBinFile.write((char*)&currModel.shapes[s].material.textureBumpHash, sizeof(currModel.shapes[s].material.textureBumpHash));

            outBinFile.write((char*)&currModel.shapes[s].material.textureAlphaHash, sizeof(currModel.shapes[s].material.textureAlphaHash));

            std::cout << "Material written" << std::endl;
        }
    }

    outBinFile.close();
}

/**
 * @brief Read Binary File to Main Memory
 */
size_t dag::ResourceManager::readBin(std::filesystem::path const& datPath)
{
    size_t inputHash = std::hash<std::string>{}(std::filesystem::relative(datPath, dag::filesystem::getAssetsPath()).string());
    return readBin(inputHash, datPath);
}

size_t dag::ResourceManager::readBin(size_t inputHash, std::filesystem::path const& datPath)
{
    Log::warning("readBin: ", datPath);
    Log::debug("path: ", datPath);
    Log::debug("inputHash: ", inputHash);

    if (models.contains(inputHash))
    {
        Log::debug("Data already loaded. Mesh has:");
        return inputHash;
    }

    std::string datFileDirec = datPath.string();

    if (std::string(datFileDirec).empty())
    {
        //Call getFilename / setFilename first.
        Log::error("Error: filename not set.");
        exit(EXIT_FAILURE);
    }

    std::ifstream inBinFile(datFileDirec, std::ios::in | std::ios::binary);

    size_t nModels;
    size_t nShapes;
    size_t nIndices;
    size_t nMaterials;

    size_t tempSize = 0;

    inBinFile.read((char*)&nModels, sizeof(size_t));

    models[inputHash] = Model{};

    inBinFile.read((char*)&nShapes, sizeof(size_t));
    models[inputHash].shapes.resize(nShapes);

    for (int s = 0; s < nShapes; ++s)
    {

        //Vertices
        //Position
        inBinFile.read((char*)&tempSize, sizeof(size_t));
        models[inputHash].shapes[s].vertices.resize(tempSize);

        for (int i = 0; i < tempSize; ++i)
        {
            inBinFile.read((char*)glm::value_ptr(models[inputHash].shapes[s].vertices[i].pos), sizeof(models[inputHash].shapes[s].vertices[i].pos));

            //Normals
            inBinFile.read((char*)glm::value_ptr(models[inputHash].shapes[s].vertices[i].normal), sizeof(models[inputHash].shapes[s].vertices[i].normal));

            //Texture Coordinates
            inBinFile.read((char*)glm::value_ptr(models[inputHash].shapes[s].vertices[i].texCoord), sizeof(models[inputHash].shapes[s].vertices[i].texCoord));

            //Colours
            inBinFile.read((char*)glm::value_ptr(models[inputHash].shapes[s].vertices[i].color), sizeof(models[inputHash].shapes[s].vertices[i].color));
        }

        //Indices
        inBinFile.read((char*)&nIndices, sizeof(size_t));

        models[inputHash].shapes[s].indices.resize(nIndices);
        for (size_t j = 0; j < nIndices; j++)
        {
            inBinFile.read((char*)&models[inputHash].shapes[s].indices[j], sizeof(uint32_t));
        }

        inBinFile.read((char*)&tempSize, sizeof(size_t));
        models[inputHash].shapes[s].material.name.resize(tempSize);
        inBinFile.read(&models[inputHash].shapes[s].material.name[0], models[inputHash].shapes[s].material.name.size());
        inBinFile.read((char*)&models[inputHash].shapes[s].material.ambience, sizeof(models[inputHash].shapes[s].material.ambience));
        inBinFile.read((char*)&models[inputHash].shapes[s].material.diffuse, sizeof(models[inputHash].shapes[s].material.diffuse));
        inBinFile.read((char*)&models[inputHash].shapes[s].material.specular, sizeof(models[inputHash].shapes[s].material.specular));
        inBinFile.read((char*)&models[inputHash].shapes[s].material.emission, sizeof(models[inputHash].shapes[s].material.emission));
        inBinFile.read((char*)&models[inputHash].shapes[s].material.dissolve, sizeof(models[inputHash].shapes[s].material.dissolve));

        inBinFile.read((char*)&models[inputHash].shapes[s].material.textureAmbientHash, sizeof(models[inputHash].shapes[s].material.textureAmbientHash));
        inBinFile.read((char*)&models[inputHash].shapes[s].material.textureDiffuseHash, sizeof(models[inputHash].shapes[s].material.textureDiffuseHash));
        inBinFile.read((char*)&models[inputHash].shapes[s].material.textureSpecularHash, sizeof(models[inputHash].shapes[s].material.textureSpecularHash));
        inBinFile.read((char*)&models[inputHash].shapes[s].material.textureBumpHash, sizeof(models[inputHash].shapes[s].material.textureBumpHash));
        inBinFile.read((char*)&models[inputHash].shapes[s].material.textureAlphaHash, sizeof(models[inputHash].shapes[s].material.textureAlphaHash));
    }

    inBinFile.close();

    Log::debug("Finished parsing.");

    return inputHash;
    return 0;
}

/**
 * @brief Copy material information to a MaterialData class object.
 */
void dag::ResourceManager::copyMaterial(std::vector<dag::MaterialDataInfo> mat, size_t const& modelIndex)
{
}

void dag::ResourceManager::getMesh(std::filesystem::path const& datPath, dag::Mesh& outputMesh)
{
    /*std::size_t inputHash = std::hash<std::string>{}(datPath.string());

    getMesh(inputHash, outputMesh);*/
}

void dag::ResourceManager::getMesh(dag::Shape& shape, dag::Mesh& outputMesh)
{
    std::vector<float> vertices(shape.vertices.size() * 12);

    for (int v = 0; v < shape.vertices.size(); ++v)
    {
        vertices[12 * v] = shape.vertices[v].pos.x;
        vertices[12 * v + 1] = shape.vertices[v].pos.y;
        vertices[12 * v + 2] = shape.vertices[v].pos.z;

        vertices[12 * v + 3] = shape.vertices[v].normal.x;
        vertices[12 * v + 4] = shape.vertices[v].normal.y;
        vertices[12 * v + 5] = shape.vertices[v].normal.z;

        vertices[12 * v + 6] = shape.vertices[v].texCoord.x;
        vertices[12 * v + 7] = shape.vertices[v].texCoord.y;

        vertices[12 * v + 8] = shape.vertices[v].color.x;
        vertices[12 * v + 9] = shape.vertices[v].color.y;
        vertices[12 * v + 10] = shape.vertices[v].color.z;
        vertices[12 * v + 11] = shape.vertices[v].color.w;
    }

    outputMesh.vertices = vertices;
    outputMesh.indices = shape.indices;
}

void dag::ResourceManager::serialize(std::filesystem::path outputDir)
{
    auto genAssetsPath = dag::filesystem::getExePath().parent_path() / "../game_output/assets";

    if (!std::filesystem::exists(outputDir))
    {
        std::filesystem::create_directories(outputDir);
    }

    json big_j;

    json j;

    auto modelArray = json::array();

    for (auto const& [key, val] : models)
    {
        writeBin(key, outputDir / (std::to_string(key) + std::string(".dat")));
        j.clear();
        j["key"] = key;
        j["path"] = std::to_string(key) + std::string(".dat");

        modelArray.push_back(j);
    }
    for (auto const& [key, val] : texture_names)
    {
        std::cout << "Serializing texture: " << val << " to " << (std::to_string(key) + std::string(".dat")) << std::endl;
        {
            std::ofstream(outputDir / (std::to_string(key) + std::string(".dat")));
        }
        std::filesystem::copy(val, outputDir / (std::to_string(key) + std::string(".dat")), std::filesystem::copy_options::overwrite_existing);
    }

    big_j["models"] = modelArray;

    std::ofstream output_file_stream(outputDir / "resource_manager_test.json");

    output_file_stream << std::setw(4) << big_j.dump(4);

    output_file_stream.close();
}

//get hash of file
size_t dag::ResourceManager::getFileHash(std::filesystem::path const& filepath)
{
    return std::hash<std::string>{}(std::filesystem::relative(filepath, dag::filesystem::getAssetsPath()).string());
}

//checks if mesh loaded
bool dag::ResourceManager::containsMesh(std::filesystem::path const& filepath)
{
    size_t inputHash = std::hash<std::string>{}(std::filesystem::relative(filepath, dag::filesystem::getAssetsPath()).string());
    return containsMesh(inputHash);
}

//checks if mesh loaded
bool dag::ResourceManager::containsMesh(size_t const& index)
{
    return models.contains(index);
}

dag::VertexArrayObject<float, uint32_t> dag::ResourceManager::generateVAO(dag::Shape& shape, std::vector<dag::VertexAttribute> const& attributes)
{
    dag::Mesh shapeMesh;
    getMesh(shape, shapeMesh);

    auto vb = dag::VertexBuffer<float>(shapeMesh.vertices);
    vb.generate();

    for (auto const& att : attributes)
    {
        vb.addAttribute(att);
    }

    auto ib = dag::IndexBuffer<uint32_t>(shapeMesh.indices);
    ib.generate();

    auto vao = dag::VertexArrayObject<float, uint32_t>(vb, ib);
    vao.generate();
    vao.aggregate(GL_STATIC_DRAW);

    return vao;
}