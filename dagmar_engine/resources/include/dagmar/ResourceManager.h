#pragma once
#include "dagmar/ResourceData.h"
#include "dagmar/Components.h"
// #include "dagmar/RenderingSystem.h"
#include "dagmar/Texture2D.h"
#include "glm/glm.hpp"

#include <filesystem>
#include <string>
#include <unordered_map>
#include <vector>

#include "dagmar/Log.h"
#include "dagmar/Mesh.h"
#include "dagmar/Module.h"
#include "dagmar/TextureCube.h"

namespace dag
{
    /**
     * @brief Resource Manager
     */
    class ResourceManager : public dag::Module
    {
      public:
        //models -> parseOBJ(), writeBin()
        std::unordered_map<size_t, Model> models;
        std::unordered_map<size_t, dag::Texture2D> textures;
        std::unordered_map<size_t, dag::TextureCube> textureCube;
        std::unordered_map<size_t, std::filesystem::path> texture_names;
        std::vector<dag::VertexAttribute> atts;
        std::unordered_map<size_t, MeshRenderer> meshRenderers;

      
        size_t readBin(size_t inputHash, std::filesystem::path const& datPath);

      public:
        ResourceManager() = default;

        // Override startup for the module
        void startup() override;

        // Override shutdown for the module
        void shutdown() override;

        // Override name for the module
        std::string getName() const override;

        //Obj file to main memory
        size_t parseOBJ(std::filesystem::path const& inputfile);

        size_t createModel(std::vector<Vertex> const& vertices, std::vector<uint32_t> const& indices, std::string const& name);

        //Main memory to Binary file(.dat)
        void writeBin(size_t inputHash, std::filesystem::path outPath);

        //Binary File to Main Memory
        size_t readBin(std::filesystem::path const& datPath);

        //Copy material information to a MaterialDataInfo class object
        void copyMaterial(std::vector<MaterialDataInfo> mat, size_t const& modelIndex);

        //File formatting
        std::string getObjName(std::string const& filename);

        //gets loaded mesh from a path
        void getMesh(std::filesystem::path const& datPath, dag::Mesh& outputMesh);

        //get hash of file
        size_t getFileHash(std::filesystem::path const& filepath);

        //checks if mesh loaded
        bool containsMesh(std::filesystem::path const& filepath);

        //checks if mesh loaded
        bool containsMesh(size_t const& index);

        //gets loaded mesh from a hash
        void getMesh(dag::Shape& shape, dag::Mesh& outputMesh);

        //serializes all resources
        void serialize(std::filesystem::path outputDir);

        void insertTexture(size_t inputHash, std::filesystem::path texture);

        size_t getMeshRenderer(std::string name);

        VertexArrayObject<float, uint32_t> generateVAO(dag::Shape& shape, std::vector<dag::VertexAttribute> const& attributes);
    };

    inline std::shared_ptr<ResourceManager> resourceManager(new ResourceManager());
} // namespace dag