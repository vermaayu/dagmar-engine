#pragma once
#include <cstdlib>
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include <glm/gtx/hash.hpp>

#include "dagmar/Texture2D.h"

namespace dag
{

    // vertex struct to keep a 3d vertex and its attributes
    struct Vertex
    {
        glm::vec3 pos;
        glm::vec4 color;
        glm::vec3 normal;
        glm::vec2 texCoord;

        // overloaded operator to check if two vertices are the same
        bool operator==(const Vertex& other) const
        {
            return pos == other.pos && color == other.color && texCoord == other.texCoord && normal == other.normal;
        }
    };

    /**
     * @brief Material info, class for re usability
     */
    class MaterialDataInfo
    {
      public:
        std::string name;
        glm::vec3 ambience = glm::vec3(0.0f);
        glm::vec3 diffuse = glm::vec3(0.0f);
        glm::vec3 specular = glm::vec3(0.0f);
        glm::vec3 emission = glm::vec3(0.0f);
        float dissolve = 1.0f;

        size_t textureAmbientHash;
        size_t textureDiffuseHash;
        size_t textureSpecularHash;
        size_t textureBumpHash;
        size_t textureAlphaHash;
    };

    /**
     * @brief Model class
     *
     * Contains information about the vertex data, shapes and materials
     * Model -> Shapes -> Faces -> VertexIndices
     */
    class Shape
    {
    public:
        std::vector<Vertex> vertices;
        std::vector<uint32_t> indices;
        MaterialDataInfo material;
        size_t VAOIndex; //into renderingSystem->vaos
        glm::vec3 center = glm::vec3(0.0f); 	
    };

    class Model
    {
    public:
        std::vector<Shape> shapes;
        size_t numVertices;
        glm::vec3 center;
        float radius;
    };
} // namespace dag


// custom hashing for the vertices
namespace std
{
    template <>
    struct hash<dag::Vertex>
    {
        size_t operator()(dag::Vertex const& vertex) const
        {
            return ((hash<glm::vec3>()(vertex.pos) ^
                     (hash<glm::vec3>()(vertex.color) << 1)) >>
                    1) ^
                   (hash<glm::vec2>()(vertex.texCoord) << 1);
        }
    };
} // namespace std
