#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include "dagmar/Camera.h"

TEST_CASE("Testing Camera: view parameters", "[view_test]")
{
    dag::Camera camera = dag::Camera();
    glm::vec3 position = glm::vec3(1, 10, 5);
    glm::vec3 up = glm::vec3(0, -1, 0);
    glm::vec3 forward = glm::vec3(0, 0, 1);
    camera.setPosition(position);
    camera.setUpVector(up);
    camera.setForwardVector(forward);

    REQUIRE(camera.getView() == glm::lookAt(position, position + forward, up));

    camera.setView(position, up, forward);

    REQUIRE(camera.getView() == glm::lookAt(position, position + forward, up));
}

TEST_CASE("Testing Camera: projection parameters", "[projection_test]")
{
    dag::Camera camera = dag::Camera();
    float fov = 45.0f;
    float aspect = 1.0f;
    float zNear = 0.1f;
    float zFar = 10.0f;

    camera.setFOV(fov);
    camera.setAspect(aspect);
    camera.setNear(zNear);
    camera.setFar(zFar);

    REQUIRE(camera.getProjection() == glm::perspective(glm::radians(fov), aspect, zNear, zFar));

    camera.setProjection(fov, aspect, zNear, zFar);

    REQUIRE(camera.getProjection() == glm::perspective(glm::radians(fov), aspect, zNear, zFar));
}


TEST_CASE("Testing Camera: movement", "[movement_test]")
{
    dag::Camera camera = dag::Camera();
    camera.moveForward(1.0f);
    
    REQUIRE(camera.getPosition() == glm::vec3(0, 0, -1));

    camera.moveRight(1.0f);
    
    REQUIRE(camera.getPosition() == glm::vec3(1, 0, -1));

    camera.moveUp(1.0f);
    
    REQUIRE(camera.getPosition() == glm::vec3(1, 1, -1));

    camera.roll(90.0f);
    camera.moveUp(1.0f);

    REQUIRE(glm::length(camera.getPosition() - glm::vec3(2, 1, -1)) < 0.001f);

    camera.pitch(90.0f);
    camera.moveForward(1.0f);

    REQUIRE(glm::length(camera.getPosition() - glm::vec3(1, 1, -1)) < 0.001f);

    camera.yaw(90.0f);
    camera.moveRight(1.0f);

    REQUIRE(glm::length(camera.getPosition() - glm::vec3(0, 1, -1)) < 0.001f);
}