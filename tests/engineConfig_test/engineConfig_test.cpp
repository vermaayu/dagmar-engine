#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include "dagmar/EngineConfig.h"
#include "dagmar/FileSystem.h"
#include "dagmar/ModuleLifecycle.h"
#include <catch2/catch.hpp>
#include <iostream>
#include <string>

TEST_CASE("EngineConfig Tests")
{
    SECTION("TEST 1")
    {
        dag::EngineConfig test1;
        const std::filesystem::path path = "../test_assets/EngineConfig.ini";
        test1.parse(path);

        REQUIRE(test1.arrSections[0].sectionName == "Macro");
        REQUIRE(test1.getValue("Light", "Macro") == "1");
        REQUIRE(test1.getValue("Light", "Macro") == ("1"));
        REQUIRE(test1.getValue("Triangulate", "Macro") == ("0"));
        REQUIRE(test1.arrSections[1].sectionName == "Physics");
        REQUIRE(test1.getValue("Gravity", "Physics") == ("9.8"));
        REQUIRE(test1.getValue("Epsilon", "Physics") == ("0.5"));
        REQUIRE(test1.arrSections[2].sectionName == "MoonPhysics");
        REQUIRE(test1.getValue("Gravity", "MoonPhysics") == ("4.6"));
    }
}
