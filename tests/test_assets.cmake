set(TEST_ASSETS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/test_assets)
set(TEST_ASSETS_BUILD_DIR ${PROJECT_BINARY_DIR}/tests/test_assets)

# get all files in asset folder
file(GLOB_RECURSE TEST_ASSET_FILES
    ${TEST_ASSETS_DIR}/*
)

# this will be used to ensure unique naming
set(TEST_ASSET_COUNT 0)

# for each file we found
foreach(TEST_ASSET_FILE IN LISTS TEST_ASSET_FILES)
    # get the path relative to the asset folder
    file(RELATIVE_PATH RELATIVE_TO_TEST_ASSET_FOLDER ${TEST_ASSETS_DIR} ${TEST_ASSET_FILE})

    # the command to actually copy out of date assets
    add_custom_command(OUTPUT ${TEST_ASSETS_INSTALL_DIR}/${RELATIVE_TO_TEST_ASSET_FOLDER}
        COMMENT "Copying asset: ${RELATIVE_TO_TEST_ASSET_FOLDER}"
        COMMAND ${CMAKE_COMMAND} -E copy ${TEST_ASSET_FILE} ${TEST_ASSETS_BUILD_DIR}/${RELATIVE_TO_TEST_ASSET_FOLDER}
        DEPENDS ${TEST_ASSET_FILE}
    )

    # this tracks whether or not something is out of date
    add_custom_target(test_asset_${TEST_ASSET_COUNT} ALL
       DEPENDS ${TEST_ASSETS_INSTALL_DIR}/${RELATIVE_TO_TEST_ASSET_FOLDER}
    )

    MATH(EXPR TEST_ASSET_COUNT "${TEST_ASSET_COUNT}+1")
endforeach()