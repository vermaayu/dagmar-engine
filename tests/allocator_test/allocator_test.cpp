#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <string>
#include <iostream>
#include "dagmar/Allocator.h"
dag::Allocator allocator = dag::Allocator(1024);

TEST_CASE("Testing Allocator::aligning less", "[align_test]") {
    REQUIRE(allocator.align(10) == 16);
}

TEST_CASE("Testing Allocator::aligning same", "[align_test]") {
    REQUIRE(allocator.align(16) == 16);
}

TEST_CASE("Testing Allocator::allocate too much", "[allocate_toomuch_test]") {
    void* allocateMoreThanAvail = allocator.allocate(1025);
    
    REQUIRE(allocateMoreThanAvail == nullptr);
}

TEST_CASE("Testing Allocator::allocate", "[allocate_test]") {
    // allocate the memory
    int* allocated = static_cast<int*>(allocator.allocate(10 * sizeof(int)));
    // assign values to the memory and check
    for(size_t i = 0; i < 10; ++i){
        allocated[i] = 10;
        REQUIRE(allocated[i] == 10);
    }
}

TEST_CASE("Testing Allocator::free", "[free_test]") {
    // allocate some memory
    void* allocated = allocator.allocate(10);
    // free the memory
    allocator.deallocate(allocated);
    // try to allocate the full memory to check coalescence and
    // freeing are both correctly done
    allocated = allocator.allocate(1024);

    REQUIRE(allocated != nullptr);
}
