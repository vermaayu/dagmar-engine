add_executable(ecs_test
    ecs_test.cpp
)

target_link_libraries(ecs_test PRIVATE 
	Catch2::Catch2
    Dagmar::ModuleLifecycle
    Dagmar::ECS
    Dagmar::Exceptions
)

catch_discover_tests(ecs_test)

add_custom_command(TARGET ecs_test POST_BUILD
    COMMAND ctest -C $<CONFIGURATION> --output-on-failure
)
