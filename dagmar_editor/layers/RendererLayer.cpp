#include "dagmar/RendererLayer.h"

#include "dagmar/EditorUILayer.h"
#include "dagmar/EventDispatcher.h"
#include "dagmar/Scene.h"
#include "dagmar/WindowManager.h"

#define BIND_RENDER_GRAPH_FN(fn) [this]() { fn(); }

dag::RendererLayer::RendererLayer() :
    Layer("RendererLayer")
{
}

void dag::RendererLayer::onAttach()
{
    renderingSystem = coordinator->getSystem<RenderingSystem>();
    renderingSystem->setRenderingMode(renderingSystem->currentRenderMode, false);
}

void dag::RendererLayer::onDetach()
{
}

void dag::RendererLayer::onUpdate(float const& dt)
{
    renderingSystem->update();
}

void dag::RendererLayer::onEvent(Event& event)
{
    EventDispatcher dispatcher(event);

    dispatcher.dispatch<WindowResizeEvent>(BIND_EVENT_FN(RendererLayer::onWindowResize));
}

bool dag::RendererLayer::onWindowResize(WindowResizeEvent& event)
{
    return false;
}