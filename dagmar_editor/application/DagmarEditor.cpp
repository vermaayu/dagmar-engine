#include "dagmar/DagmarEditor.h"

#include "dagmar/EventDispatcher.h"
#include "dagmar/PhysicsSystem.h"
#include "dagmar/ResourceLoader.h"
#include "dagmar/Scene.h"
#include "dagmar/GameLayer.h"

dag::DagmarEditor::DagmarEditor() :
    Application()
{
    // Load the other VAOs
    resourceLoader->loadScreenQuad();
    resourceLoader->loadInternalAssets();

    scene->initialize();

    windowManager->swapContext();
	
    editorLayer = std::make_shared<EditorUILayer>();
	layerManager.addLayer(editorLayer);
    gameLayer = std::make_shared<GameLayer>();
    layerManager.addLayer(gameLayer); 
    rendererLayer = std::make_shared<RendererLayer>();
    layerManager.addLayer(rendererLayer);

	// Set the communication here
    editorLayer->rendererLayer = rendererLayer;
    rendererLayer->editorUILayer = editorLayer;
	
    windowManager->window.maximize();
}

void dag::DagmarEditor::run()
{
    while (running)
    {
        timeManager->updateFrameStart();    	
        layerManager.updateLayers();
    	windowManager->window.onUpdate();
        rendererLayer->gameState = editorLayer->gameState;
        gameLayer->gameState = editorLayer->gameState;
    }
}

void dag::DagmarEditor::onEvent(dag::Event& e)
{
    Application::onEvent(e);
}

bool dag::DagmarEditor::onWindowClose(dag::WindowCloseEvent& e)
{
    return Application::onWindowClose(e);
}